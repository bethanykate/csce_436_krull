--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	01/24/22
-- File:	counter.vhd
-- HW:		homework 4
-- Crs:		CSCE 436
--
-- Purp:	Count 0-4 according to the logic in the assignment - output
--          a signal when counter rolls over from 4->0
--
-- Documentation:	I worked on my own.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;

entity counter is
    port(   clk, reset  : in std_logic;
                ctrl        : in std_logic;
                Q           : out unsigned(2 downto 0);
                rollOUT     : out std_logic);
end counter;

architecture Behavioral of counter is
signal tempQ : unsigned(2 downto 0) := "000";
begin
    process(clk)
    begin
        if (rising_edge(clk)) then
            if (reset = '0') then
                tempQ <= "000";
                rollOUT <= '0';
            elsif ((ctrl = '1') and (tempQ < 4)) then
                tempQ <= tempQ + 1;
                if (tempQ < 3) then
                    rollOUT <= '0';
                else rollOUT <= '1';
                end if;
            elsif ((ctrl = '1') and (tempQ = 4)) then
                tempQ <= "000";
                rollOUT <= '0';
            end if;
        end if;
    end process;
    Q <= tempQ;
end Behavioral;
