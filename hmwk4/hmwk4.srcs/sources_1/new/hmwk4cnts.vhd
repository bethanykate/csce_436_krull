--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	01/24/22
-- File:	hmwk4cnts.vhd
-- HW:		homework 4
-- Crs:		CSCE 436
--
-- Purp:	Two cascaded counters count up according to input with a base
--          of 5 (00,01,02,03,04,10,11,...). Output to 2 outputs 
--          representing the least and most significant bit.
--
-- Documentation:	I worked on my own.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;

entity hmwk4cnts is
    port(   clk, reset  : in std_logic;
            ctrl        : in std_logic;
            Q1, Q0      : out unsigned(2 downto 0));
end hmwk4cnts;

architecture Structure of hmwk4cnts is
signal rollWire : std_logic;
signal glueWire : std_logic;
component counter 
    port(   clk, reset  : in std_logic;
            ctrl        : in std_logic;
            Q           : out unsigned(2 downto 0);
            rollOUT     : out std_logic);
end component;
begin
    leastSigCnt: counter
        port map(   clk => clk,
                    reset => reset,
                    ctrl => ctrl,
                    Q => Q0,
                    rollOUT => rollWire);
    mostSigCnt: counter
        port map(   clk => clk,
                    reset => reset,
                    ctrl => glueWire,
                    Q => Q1);
    glueWire <= ctrl and rollWire;
end Structure;

