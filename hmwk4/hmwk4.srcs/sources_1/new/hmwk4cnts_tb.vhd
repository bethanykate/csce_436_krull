--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	01/25
-- File:	hmwk4cnts_tb.vhd
-- HW:		homework 4
-- Crs:		CSCE 436
--
-- Purp:	provide simulation/testing for cascaded counter system
--
-- Documentation:	I worked on my own.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;

entity hmwk4cnts_tb is
end hmwk4cnts_tb;

architecture Behavioral of hmwk4cnts_tb is
component hmwk4cnts
   port(   clk, reset  : in std_logic;
           ctrl        : in std_logic;
           Q1, Q0      : out unsigned(2 downto 0));    
end component;
signal clkSIM, resetSIM, ctrlSIM : std_logic;
signal Q1SIM, Q0SIM : unsigned(2 downto 0);
constant clk_period : time := 500ns;

begin
    uut: hmwk4cnts port map(
        clk => clkSIM,
        reset => resetSIM,
        ctrl => ctrlSIM,
        Q1 => Q1SIM,
        Q0 => Q0SIM);

    clk_process : process
        begin
        clkSIM <= '0';
        wait for clk_period/2;
        clkSIM <= '1';
        wait for clk_period/2;
    end process;
    
    resetSIM <= '0', '1' after 4us;
    ctrlSIM <= '1',  '0' after 8.5us, '1' after 9us;
end Behavioral;
