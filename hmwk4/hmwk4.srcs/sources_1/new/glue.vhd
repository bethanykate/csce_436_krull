--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	01/25/22
-- File:	glue.vhd
-- HW:		homework 4
-- Crs:		CSCE 436
--
-- Purp:	provide rollover logic between the least significant 
--          counter and the most significant that still obeys the 
--          logic of the system's ctrl input
--
-- Documentation:	I talked briefly with Tate Anderson
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;

entity glue is
    port(   rollIN, ctrl: in std_logic;
            glueOUT     : out std_logic);
end glue;

-------------------------------------
--rollIN    |   ctrl    |   glueOUT
-------------------------------------
--0         |   0       |   0
--0         |   1       |   0
--1         |   0       |   0
--1         |   1       |   1
-------------------------------------

architecture Behavioral of glue is
signal tempOUT : std_logic;
begin
    process(clk)
    begin
        if((rollIN = '1') and (ctrl = '1')) then
            tempOUT <= '1';
        else
            tempOUT <= '0';
        end if;
    end process;
    glueOUT <= tempOUT;      
end Behavioral;
