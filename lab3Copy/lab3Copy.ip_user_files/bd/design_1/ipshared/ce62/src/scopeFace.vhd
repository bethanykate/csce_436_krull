--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	02/02/22
-- File:	scopeFace.vhd
-- HW:		Lab 1
-- Crs:		CSCE 436
--
-- Purp:	Output the correct color for each pixel according to the 
--          counters' coordinates
--
-- Documentation:	I worked on my own - used the give entity scopeFace 
--                  code
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;

entity scopeFace is
    Port (  row : in  unsigned(9 downto 0);
            column : in  unsigned(9 downto 0);
			letter1 : in std_logic;
			letter2 : in std_logic;
			letter3 : in std_logic;
			letter4 : in std_logic;
			letter5 : in std_logic;
            r : out  std_logic_vector(7 downto 0);
            g : out  std_logic_vector(7 downto 0);
            b : out  std_logic_vector(7 downto 0));
end scopeFace;

architecture Behavioral of scopeFace is
signal midLine : std_logic;
signal L1 : std_logic;
signal L2 : std_logic;
signal L3 : std_logic;
signal L4 : std_logic;
signal L5 : std_logic;
begin

    -- the RGB value is set according to which screen item is true at a given coordinate
    -- priority goes from top -> down

    R <=    x"FF" when (L1 = '1') else
            x"FF" when (L2 = '1') else
            x"FF" when (L3 = '1') else
            x"FF" when (L4 = '1') else
            x"FF" when (L5 = '1') else
            x"FF" when (midLine = '1') else x"00";
    G <=    x"FF" when (L1 = '1') else
            x"FF" when (L2 = '1') else
            x"FF" when (L3 = '1') else
            x"FF" when (L4 = '1') else
            x"FF" when (L5 = '1') else
            x"FF" when (midLine = '1') else x"00";  
    B <=    x"FF" when (L1 = '1') else
            x"FF" when (L2 = '1') else
            x"FF" when (L3 = '1') else
            x"FF" when (L4 = '1') else
            x"FF" when (L5 = '1') else
            x"FF" when (midLine = '1') else x"00";       

    -- the plain grid lines
    midLine <= '1' when (row >= 20 and row <= 420 and column>= 20 and column <= 620) and
                         row = 300 else '0';
                        
    L1 <= '1' when (row >= 20 and row <= 420 and column>= 20 and column <= 620) and
                    letter1 = '1' and
                    column = 50 else '0';
                    
    L2 <= '1' when (row >= 20 and row <= 420 and column>= 20 and column <= 620) and
                    letter2 = '1' and
                    column = 100 else '0';

    L3 <= '1' when (row >= 20 and row <= 420 and column>= 20 and column <= 620) and
                    letter3 = '1' and
                    column = 150 else '0';
                    
    L4 <= '1' when (row >= 20 and row <= 420 and column>= 20 and column <= 620) and
                    letter4 = '1' and
                    column = 200 else '0';                    
                    
    L5 <= '1' when (row >= 20 and row <= 420 and column>= 20 and column <= 620) and
                    letter5 = '1' and
                    column = 250 else '0';                    
                            
end Behavioral;






