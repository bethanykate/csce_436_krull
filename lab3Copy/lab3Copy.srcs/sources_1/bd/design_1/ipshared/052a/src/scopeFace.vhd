--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	02/02/22
-- File:	scopeFace.vhd
-- HW:		Lab 1
-- Crs:		CSCE 436
--
-- Purp:	Output the correct color for each pixel according to the 
--          counters' coordinates
--
-- Documentation:	I worked on my own - used the give entity scopeFace 
--                  code
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;

entity scopeFace is
    Port ( row : in  unsigned(9 downto 0);
           column : in  unsigned(9 downto 0);
			  trigger_volt: in unsigned (9 downto 0);
			  trigger_time: in unsigned (9 downto 0);
           r : out  std_logic_vector(7 downto 0);
           g : out  std_logic_vector(7 downto 0);
           b : out  std_logic_vector(7 downto 0);
			  ch1: in std_logic;
			  ch1_enb: in std_logic;
			  ch2: in std_logic;
			  ch2_enb: in std_logic);
end scopeFace;

architecture Behavioral of scopeFace is
signal grid : std_logic;
signal ch1Trace : std_logic;
signal ch2Trace : std_logic;
signal voltArrow : std_logic;
signal timeArrow : std_logic;
signal voltWire : unsigned(9 downto 0);
begin

    -- the RGB value is set according to which screen item is true at a given coordinate
    -- priority goes from top -> down

    R <=    x"FF" when (voltArrow = '1') else
            x"FF" when (timeArrow = '1') else
            x"FF" when (ch1Trace = '1') else
            x"50" when (ch2Trace = '1') else
            x"FF" when (grid = '1') else x"00";
    G <=    x"FF" when (voltArrow = '1') else
            x"FF" when (timeArrow = '1') else
            x"EA" when (ch1Trace = '1') else
            x"C8" when (ch2Trace = '1') else 
            x"FF" when (grid = '1') else x"00";  
    B <=    x"FF" when (voltArrow = '1') else
            x"FF" when (timeArrow = '1') else
            x"00" when (ch1Trace = '1') else
            x"78" when (ch2Trace = '1') else
            x"FF" when (grid = '1') else x"00";       

    -- the plain grid lines
    grid <= '1' when    (row >= 20 and row <= 420 and column>= 20 and column <= 620) and
                        -- COLUMN
                        ((column mod 60 = 20) or
                        -- ROW
                        (row mod 50 = 20) or
                        -- VERTICAL CENTER TICKS
                        (column = 319 and row mod 10 = 0) or
                        (column = 321 and row mod 10 = 0) or
                        -- HORIZONTAL CENTER TICKS
                        (row = 221 and column mod 15 = 5) or 
                        (row = 219 and column mod 15 = 5)) else '0';
      
    -- voltage trigger
    voltArrow <= '1' when (column = 21 and row = trigger_volt) or
                            (column = 21 and row = trigger_volt + 1) or
                            (column = 21 and row = trigger_volt + 2) or
                            (column = 21 and row = trigger_volt - 1) or
                            (column = 21 and row = trigger_volt - 2) or
                            (column = 22 and row = trigger_volt) or
                            (column = 22 and row = trigger_volt + 1) or
                            (column = 22 and row = trigger_volt -1) or
                            (column = 23 and row = trigger_volt) else '0';
       
    -- time trigger
    timeArrow <= '1' when (column = trigger_time and row = 21) or
                            (column = trigger_time - 1 and row = 21) or
                            (column = trigger_time  - 2 and row = 21) or
                            (column = trigger_time + 1 and row = 21) or
                            (column = trigger_time + 2 and row = 21) or
                            (column = trigger_time and row = 22) or
                            (column = trigger_time - 1 and row = 22) or
                            (column = trigger_time + 1 and row = 22) or
                            (column = trigger_time and row = 23) else '0';                            

 
    ch1Trace <= '1' when ((row >= 20 and row <= 420 and column>= 20 and column <= 620 and ch1 = '1') and ch1_enb = '1') else '0';
    ch2Trace <= '1' when ((row >= 20 and row <= 420 and column>= 20 and column <= 620 and ch2 = '1') and ch2_enb = '1') else '0';
    --ch1Trace <= '1' when ch1 = '1' else '0';
    --ch2Trace <= '1' when ch2 = '1' else '0';
    
end Behavioral;