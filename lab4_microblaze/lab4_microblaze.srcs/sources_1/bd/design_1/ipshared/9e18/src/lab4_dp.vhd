--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	04/04/22
-- File:	lab4_dp.vhd
-- HW:		Lab 4
-- Crs:		CSCE 436
--
-- Purp:	datapath for Lab 4
--
-- Documentation:	I used code from class
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;
library UNIMACRO;
use UNIMACRO.vcomponents.all;
use ieee.std_logic_unsigned.all;

entity lab4_dp is
    port(   clk : in  STD_LOGIC;
            reset_n : in  STD_LOGIC;
		    ac_mclk : out STD_LOGIC;
		    ac_adc_sdata : in STD_LOGIC;
		    ac_dac_sdata : out STD_LOGIC;
		    ac_bclk : out STD_LOGIC;
		    ac_lrclk : out STD_LOGIC;
            scl : inout STD_LOGIC;
            sda : inout STD_LOGIC;
		    --tmds : out  STD_LOGIC_VECTOR (3 downto 0);
            --tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
		    btn : in STD_LOGIC_VECTOR(4 downto 0);
		    switch : in std_logic_vector(7 downto 0);
		    --ready : in std_logic;
		    exSel : in std_logic;
		    ex_phase_inc : in std_logic_vector(15 downto 0);
		    sw : out std_logic_vector(0 downto 0);
		    cw : in std_logic_vector(0 downto 0));
end lab4_dp;

architecture Behavioral of lab4_dp is

    component Audio_Codec_Wrapper
        port(   clk : in STD_LOGIC;
                reset_n : in STD_LOGIC;
                ac_mclk : out STD_LOGIC;
                ac_adc_sdata : in STD_LOGIC;
                ac_dac_sdata : out STD_LOGIC;
                ac_bclk : out STD_LOGIC;
                ac_lrclk : out STD_LOGIC;
                ready : out STD_LOGIC;
                L_bus_in : in std_logic_vector(17 downto 0); -- left channel input to DAC
                R_bus_in : in std_logic_vector(17 downto 0); -- right channel input to DAC
                L_bus_out : out  std_logic_vector(17 downto 0); -- left channel output from ADC
                R_bus_out : out  std_logic_vector(17 downto 0); -- right channel output from ADC
                scl : inout STD_LOGIC;
                sda : inout STD_LOGIC);
    end component;
    
    signal old_button, button_activity : std_logic_vector(4 downto 0) := "00000";
    signal L_bus_inWire, R_bus_inWire, L_bus_outWire, R_bus_outWire : std_logic_vector(17 downto 0) := (others => '0');
    signal swWire : std_logic_vector(2 downto 0) := (others => '0');
  
    signal sinWire, sinCosWire : std_logic_vector(17 downto 0);
    signal sinCosWireTemp, sinWireTemp : unsigned(31 downto 0);
    signal amp : unsigned(15 downto 0):= x"3FFF";
    signal phase_inc : unsigned(15 downto 0);
    signal index_cntr : std_logic_vector(15 downto 0) := (others => '0');
    signal counterOut : unsigned(9 downto 0);
    signal baseIndex : std_logic_vector(9 downto 0);
    signal baseIndexInc : std_logic_vector(9 downto 0);
    signal deltaIndex : std_logic_vector(9 downto 0);
    signal offset : std_logic_vector(5 downto 0);
    signal address : std_logic_vector(9 downto 0);  
    signal sinBRAM, sinCosBRAM : std_logic_vector(15 downto 0);
    signal BRAMrst : std_logic;
    signal addressTemp : std_logic_vector(15 downto 0);
    signal L_bus_inWireTemp : unsigned(31 downto 0);
    signal switchWaves : std_logic := '0';
    signal ready : std_logic;
    signal cwWire : std_logic_vector(0 downto 0);

begin
    cwWire <= cw;

    lab2_Audio_Wrapper: Audio_Codec_Wrapper
    port map(   clk => clk,
                reset_n => reset_n,
                ac_mclk => ac_mclk,
                ac_adc_sdata => ac_adc_sdata,
                ac_dac_sdata => ac_dac_sdata,
                ac_bclk => ac_bclk,
                ac_lrclk => ac_lrclk,
                ready => sw(0),
                L_bus_in => L_bus_inWire,
                R_bus_in => R_bus_inWire,
                L_bus_out => L_bus_outWire,
                R_bus_out => R_bus_outWire,
                scl => scl,
                sda => sda);

    sinWave : BRAM_SDP_MACRO
        generic map (
			BRAM_SIZE => "18Kb", 					-- Target BRAM, "18Kb" or "36Kb"
			DEVICE => "7SERIES", 					-- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6, 7SERIES"
			DO_REG => 0, 							-- Optional output register disabled
			--INIT => X"000000000000000000",			-- Initial values on output port
			INIT_FILE => "NONE",					-- Not sure how to initialize the RAM from a file
			WRITE_WIDTH => 16, 						-- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
			READ_WIDTH => 16, 						-- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
			SIM_COLLISION_CHECK => "NONE", 			-- Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
			SRVAL => X"0000000000000000",			-- Set/Reset value for port output
            
            INIT_00 => x"8BC38AFB8A32896A88A187D987108647857E84B683ED8324825B819280C98000",
            INIT_01 => x"9833976D96A795E1951B9455938E92C79200913990728FAB8EE38E1B8D538C8B",
            INIT_02 => x"A467A3A6A2E4A223A161A09F9FDC9F199E569D939CCF9C0B9B469A8299BD98F8",
            INIT_03 => x"B041AF86AECCAE10AD54AC98ABDBAB1EAA61A9A3A8E5A826A767A6A7A5E7A527",
            INIT_04 => x"BBA4BAF2BA3FB98CB8D8B824B76FB6B9B603B54DB496B3DEB326B26DB1B4B0FB",
            INIT_05 => x"C674C5CCC523C47AC3D0C325C279C1CDC120C073BFC5BF16BE67BDB7BD07BC56",
            INIT_06 => x"D097CFFACF5DCEBFCE20CD80CCE0CC3FCB9DCAFACA57C9B3C90EC869C7C3C71C",
            INIT_07 => x"D9F3D963D8D3D842D7B0D71DD689D5F4D55FD4C9D432D39AD301D268D1CED133",
            INIT_08 => x"E271E1F0E16EE0EBE067DFE2DF5DDED6DE4FDDC6DD3DDCB3DC28DB9CDB0FDA81",
            INIT_09 => x"E9FCE98BE919E8A5E831E7BCE745E6CEE656E5DDE562E4E7E46BE3EEE370E2F1",
            INIT_0A => x"F082F022EFC0EF5EEEFAEE95EE30EDC9ED61ECF8EC8EEC23EBB7EB4AEADBEA6C",
            INIT_0B => x"F5F3F5A4F554F503F4B1F45EF40AF3B5F35EF306F2AEF254F1F9F19DF140F0E1",
            INIT_0C => x"FA41FA04F9C7F989F949F908F8C6F883F83FF7F9F7B3F76BF722F6D8F68DF640",
            INIT_0D => x"FD61FD38FD0EFCE2FCB6FC88FC59FC29FBF7FBC4FB91FB5CFB25FAEEFAB5FA7C",
            INIT_0E => x"FF4CFF37FF20FF08FEEFFED4FEB9FE9CFE7EFE5EFE3EFE1CFDF9FDD5FDB0FD89",
            INIT_0F => x"FFFEFFFCFFF9FFF5FFEFFFE8FFE0FFD7FFCDFFC1FFB4FFA6FF96FF86FF74FF61",
            INIT_10 => x"FF74FF86FF96FFA6FFB4FFC1FFCDFFD7FFE0FFE8FFEFFFF5FFF9FFFCFFFEFFFF",
            INIT_11 => x"FDB0FDD5FDF9FE1CFE3EFE5EFE7EFE9CFEB9FED4FEEFFF08FF20FF37FF4CFF61",
            INIT_12 => x"FAB5FAEEFB25FB5CFB91FBC4FBF7FC29FC59FC88FCB6FCE2FD0EFD38FD61FD89",
            INIT_13 => x"F68DF6D8F722F76BF7B3F7F9F83FF883F8C6F908F949F989F9C7FA04FA41FA7C",
            INIT_14 => x"F140F19DF1F9F254F2AEF306F35EF3B5F40AF45EF4B1F503F554F5A4F5F3F640",
            INIT_15 => x"EADBEB4AEBB7EC23EC8EECF8ED61EDC9EE30EE95EEFAEF5EEFC0F022F082F0E1",
            INIT_16 => x"E370E3EEE46BE4E7E562E5DDE656E6CEE745E7BCE831E8A5E919E98BE9FCEA6C",
            INIT_17 => x"DB0FDB9CDC28DCB3DD3DDDC6DE4FDED6DF5DDFE2E067E0EBE16EE1F0E271E2F1",
            INIT_18 => x"D1CED268D301D39AD432D4C9D55FD5F4D689D71DD7B0D842D8D3D963D9F3DA81",
            INIT_19 => x"C7C3C869C90EC9B3CA57CAFACB9DCC3FCCE0CD80CE20CEBFCF5DCFFAD097D133",
            INIT_1A => x"BD07BDB7BE67BF16BFC5C073C120C1CDC279C325C3D0C47AC523C5CCC674C71C",
            INIT_1B => x"B1B4B26DB326B3DEB496B54DB603B6B9B76FB824B8D8B98CBA3FBAF2BBA4BC56",
            INIT_1C => x"A5E7A6A7A767A826A8E5A9A3AA61AB1EABDBAC98AD54AE10AECCAF86B041B0FB",
            INIT_1D => x"99BD9A829B469C0B9CCF9D939E569F199FDCA09FA161A223A2E4A3A6A467A527",
            INIT_1E => x"8D538E1B8EE38FAB90729139920092C7938E9455951B95E196A7976D983398F8",
            INIT_1F => x"80C98192825B832483ED84B6857E8647871087D988A1896A8A328AFB8BC38C8B",
            INIT_20 => x"743C750475CD7695775E782678EF79B87A817B497C127CDB7DA47E6D7F367FFF",
            INIT_21 => x"67CC689269586A1E6AE46BAA6C716D386DFF6EC66F8D7054711C71E472AC7374",
            INIT_22 => x"5B985C595D1B5DDC5E9E5F60602360E661A9626C633063F464B9657D66426707",
            INIT_23 => x"4FBE5079513351EF52AB5367542454E1559E565C571A57D9589859585A185AD8",
            INIT_24 => x"445B450D45C04673472747DB4890494649FC4AB24B694C214CD94D924E4B4F04",
            INIT_25 => x"398B3A333ADC3B853C2F3CDA3D863E323EDF3F8C403A40E94198424842F843A9",
            INIT_26 => x"2F68300530A2314031DF327F331F33C03462350535A8364C36F13796383C38E3",
            INIT_27 => x"260C269C272C27BD284F28E229762A0B2AA02B362BCD2C652CFE2D972E312ECC",
            INIT_28 => x"1D8E1E0F1E911F141F98201D20A2212921B0223922C2234C23D7246324F0257E",
            INIT_29 => x"1603167416E6175A17CE184318BA193119A91A221A9D1B181B941C111C8F1D0E",
            INIT_2A => x"0F7D0FDD103F10A11105116A11CF1236129E1307137113DC144814B515241593",
            INIT_2B => x"0A0C0A5B0AAB0AFC0B4E0BA10BF50C4A0CA10CF90D510DAB0E060E620EBF0F1E",
            INIT_2C => x"05BE05FB0638067606B606F70739077C07C00806084C089408DD0927097209BF",
            INIT_2D => x"029E02C702F1031D0349037703A603D60408043B046E04A304DA0511054A0583",
            INIT_2E => x"00B300C800DF00F70110012B01460163018101A101C101E30206022A024F0276",
            INIT_2F => x"000100030006000A00100017001F00280032003E004B005900690079008B009E",
            INIT_30 => x"008B007900690059004B003E00320028001F00170010000A0006000300010001",
            INIT_31 => x"024F022A020601E301C101A1018101630146012B011000F700DF00C800B3009E",
            INIT_32 => x"054A051104DA04A3046E043B040803D603A603770349031D02F102C7029E0276",
            INIT_33 => x"0972092708DD0894084C080607C0077C073906F706B60676063805FB05BE0583",
            INIT_34 => x"0EBF0E620E060DAB0D510CF90CA10C4A0BF50BA10B4E0AFC0AAB0A5B0A0C09BF",
            INIT_35 => x"152414B5144813DC13711307129E123611CF116A110510A1103F0FDD0F7D0F1E",
            INIT_36 => x"1C8F1C111B941B181A9D1A2219A9193118BA184317CE175A16E6167416031593",
            INIT_37 => x"24F0246323D7234C22C2223921B0212920A2201D1F981F141E911E0F1D8E1D0E",
            INIT_38 => x"2E312D972CFE2C652BCD2B362AA02A0B297628E2284F27BD272C269C260C257E",
            INIT_39 => x"383C379636F1364C35A83505346233C0331F327F31DF314030A230052F682ECC",
            INIT_3A => x"42F84248419840E9403A3F8C3EDF3E323D863CDA3C2F3B853ADC3A33398B38E3",
            INIT_3B => x"4E4B4D924CD94C214B694AB249FC4946489047DB4727467345C0450D445B43A9",
            INIT_3C => x"5A185958589857D9571A565C559E54E15424536752AB51EF513350794FBE4F04",
            INIT_3D => x"6642657D64B963F46330626C61A960E660235F605E9E5DDC5D1B5C595B985AD8",
            INIT_3E => x"72AC71E4711C70546F8D6EC66DFF6D386C716BAA6AE46A1E6958689267CC6707",
            INIT_3F => x"7F367E6D7DA47CDB7C127B497A8179B878EF7826775E769575CD7504743C7374")

		 port map (
			DO => sinBRAM,						-- Output read data port, width defined by READ_WIDTH parameter
			RDADDR => address,					-- Input address, width defined by port depth
			RDCLK => clk,	 						-- 1-bit input clock
			RST => BRAMrst,							-- active high reset
			RDEN => '1',							-- read enable 
			REGCE => '1',							-- 1-bit input read output register enable - ignored
			DI => "0000000000000000",						-- Input data port, width defined by WRITE_WIDTH parameter	
			WE => "00",					-- since RAM is byte read, this determines high or low byte
			WRADDR => "0000000000",					-- Input write address, width defined by write port depth
			WRCLK => clk,							-- 1-bit input write clock
			WREN => '0');	-- set to 1 for now    	-- 1-bit input write port enable
       
    sinCosWave : BRAM_SDP_MACRO
        generic map (
			BRAM_SIZE => "18Kb", 					-- Target BRAM, "18Kb" or "36Kb"
			DEVICE => "7SERIES", 					-- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6, 7SERIES"
			DO_REG => 0, 							-- Optional output register disabled
			--INIT => X"000000000000000000",			-- Initial values on output port
			INIT_FILE => "NONE",					-- Not sure how to initialize the RAM from a file
			WRITE_WIDTH => 16, 						-- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
			READ_WIDTH => 16, 						-- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
			SIM_COLLISION_CHECK => "NONE", 			-- Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
			SRVAL => X"0000000000000000",			-- Set/Reset value for port output
            
            INIT_00 => x"8BAA8AE68A22895D889787D1870B8643857C84B483EC8323825A819280C98000",
            INIT_01 => x"975596A495F2953E948893D19319926091A590E9902C8F6E8EAF8DEF8D2E8C6C",
            INIT_02 => x"A175A0E2A04D9FB59F1C9E809DE39D439CA29BFE9B599AB29A09995F98B39805",
            INIT_03 => x"A965A8F9A88AA819A7A5A72EA6B5A63AA5BBA53BA4B8A433A3ABA321A294A206",
            INIT_04 => x"AEB1AE71AE2FADEAADA2AD57AD09ACB8AC65AC0EABB5AB59AAFBAA99AA35A9CF",
            INIT_05 => x"B11BB10AB0F7B0E0B0C7B0AAB08BB069B044B01BAFF0AFC2AF92AF5EAF27AEED",
            INIT_06 => x"B0A4B0C0B0D9B0F0B104B116B124B130B139B13FB143B143B141B13CB134B129",
            INIT_07 => x"AD86ADCAAE0CAE4BAE88AEC2AEFBAF30AF64AF95AFC3AFEFB018B03FB063B085",
            INIT_08 => x"A836A898A8FAA959A9B7AA13AA6DAAC5AB1BAB70ABC2AC13AC61ACAEACF8AD40",
            INIT_09 => x"A150A1C7A23DA2B2A326A399A40AA47BA4EAA559A5C5A631A69BA704A76CA7D1",
            INIT_0A => x"99939A129A909B0E9B8C9C099C879D049D809DFC9E789EF39F6E9FE7A060A0D9",
            INIT_0B => x"91C9924292BC933793B2942E94AA952695A39621969E971C979A981898979915",
            INIT_0C => x"8AB88B218B8A8BF58C618CCF8D3D8DAD8E1E8E908F038F778FEC906290D99151",
            INIT_0D => x"8512855F85AD85FE865186A686FC875487AE880A886788C68927898989EC8A52",
            INIT_0E => x"8161818B81B881E78218824C828182B982F48330836E83AF83F28437847E84C7",
            INIT_0F => x"80018004800B8013801E802C803C804E8063807A809480B080CF80F081138139",
            INIT_10 => x"811380F080CF80B08094807A8063804E803C802C801E8013800B800480018000",
            INIT_11 => x"847E843783F283AF836E833082F482B98281824C821881E781B8818B81618139",
            INIT_12 => x"89EC8989892788C68867880A87AE875486FC86A6865185FE85AD855F851284C7",
            INIT_13 => x"90D990628FEC8F778F038E908E1E8DAD8D3D8CCF8C618BF58B8A8B218AB88A52",
            INIT_14 => x"98979818979A971C969E962195A3952694AA942E93B2933792BC924291C99151",
            INIT_15 => x"A0609FE79F6E9EF39E789DFC9D809D049C879C099B8C9B0E9A909A1299939915",
            INIT_16 => x"A76CA704A69BA631A5C5A559A4EAA47BA40AA399A326A2B2A23DA1C7A150A0D9",
            INIT_17 => x"ACF8ACAEAC61AC13ABC2AB70AB1BAAC5AA6DAA13A9B7A959A8FAA898A836A7D1",
            INIT_18 => x"B063B03FB018AFEFAFC3AF95AF64AF30AEFBAEC2AE88AE4BAE0CADCAAD86AD40",
            INIT_19 => x"B134B13CB141B143B143B13FB139B130B124B116B104B0F0B0D9B0C0B0A4B085",
            INIT_1A => x"AF27AF5EAF92AFC2AFF0B01BB044B069B08BB0AAB0C7B0E0B0F7B10AB11BB129",
            INIT_1B => x"AA35AA99AAFBAB59ABB5AC0EAC65ACB8AD09AD57ADA2ADEAAE2FAE71AEB1AEED",
            INIT_1C => x"A294A321A3ABA433A4B8A53BA5BBA63AA6B5A72EA7A5A819A88AA8F9A965A9CF",
            INIT_1D => x"98B3995F9A099AB29B599BFE9CA29D439DE39E809F1C9FB5A04DA0E2A175A206",
            INIT_1E => x"8D2E8DEF8EAF8F6E902C90E991A59260931993D19488953E95F296A497559805",
            INIT_1F => x"80C98192825A832383EC84B4857C8643870B87D18897895D8A228AE68BAA8C6C",
            INIT_20 => x"7455751975DD76A27768782E78F479BC7A837B4B7C137CDC7DA57E6D7F367FFF",
            INIT_21 => x"68AA695B6A0D6AC16B776C2E6CE66D9F6E5A6F166FD370917150721072D17393",
            INIT_22 => x"5E8A5F1D5FB2604A60E3617F621C62BC635D640164A6654D65F666A0674C67FA",
            INIT_23 => x"569A5706577557E6585A58D1594A59C55A445AC45B475BCC5C545CDE5D6B5DF9",
            INIT_24 => x"514E518E51D05215525D52A852F65347539A53F1544A54A65504556655CA5630",
            INIT_25 => x"4EE44EF54F084F1F4F384F554F744F964FBB4FE4500F503D506D50A150D85112",
            INIT_26 => x"4F5B4F3F4F264F0F4EFB4EE94EDB4ECF4EC64EC04EBC4EBC4EBE4EC34ECB4ED6",
            INIT_27 => x"5279523551F351B45177513D510450CF509B506A503C50104FE74FC04F9C4F7A",
            INIT_28 => x"57C95767570556A6564855EC5592553A54E4548F543D53EC539E5351530752BF",
            INIT_29 => x"5EAF5E385DC25D4D5CD95C665BF55B845B155AA65A3A59CE596458FB5893582E",
            INIT_2A => x"666C65ED656F64F1647363F6637862FB627F62036187610C609160185F9F5F26",
            INIT_2B => x"6E366DBD6D436CC86C4D6BD16B556AD96A5C69DE696168E3686567E7676866EA",
            INIT_2C => x"754774DE7475740A739E733072C2725271E1716F70FC708870136F9D6F266EAE",
            INIT_2D => x"7AED7AA07A527A0179AE7959790378AB785177F57798773976D87676761375AD",
            INIT_2E => x"7E9E7E747E477E187DE77DB37D7E7D467D0B7CCF7C917C507C0D7BC87B817B38",
            INIT_2F => x"7FFE7FFB7FF47FEC7FE17FD37FC37FB17F9C7F857F6B7F4F7F307F0F7EEC7EC6",
            INIT_30 => x"7EEC7F0F7F307F4F7F6B7F857F9C7FB17FC37FD37FE17FEC7FF47FFB7FFE8000",
            INIT_31 => x"7B817BC87C0D7C507C917CCF7D0B7D467D7E7DB37DE77E187E477E747E9E7EC6",
            INIT_32 => x"7613767676D87739779877F5785178AB7903795979AE7A017A527AA07AED7B38",
            INIT_33 => x"6F266F9D7013708870FC716F71E1725272C27330739E740A747574DE754775AD",
            INIT_34 => x"676867E7686568E3696169DE6A5C6AD96B556BD16C4D6CC86D436DBD6E366EAE",
            INIT_35 => x"5F9F60186091610C61876203627F62FB637863F6647364F1656F65ED666C66EA",
            INIT_36 => x"589358FB596459CE5A3A5AA65B155B845BF55C665CD95D4D5DC25E385EAF5F26",
            INIT_37 => x"53075351539E53EC543D548F54E4553A559255EC564856A65705576757C9582E",
            INIT_38 => x"4F9C4FC04FE75010503C506A509B50CF5104513D517751B451F35235527952BF",
            INIT_39 => x"4ECB4EC34EBE4EBC4EBC4EC04EC64ECF4EDB4EE94EFB4F0F4F264F3F4F5B4F7A",
            INIT_3A => x"50D850A1506D503D500F4FE44FBB4F964F744F554F384F1F4F084EF54EE44ED6",
            INIT_3B => x"55CA5566550454A6544A53F1539A534752F652A8525D521551D0518E514E5112",
            INIT_3C => x"5D6B5CDE5C545BCC5B475AC45A4459C5594A58D1585A57E657755706569A5630",
            INIT_3D => x"674C66A065F6654D64A66401635D62BC621C617F60E3604A5FB25F1D5E8A5DF9",
            INIT_3E => x"72D17210715070916FD36F166E5A6D9F6CE66C2E6B776AC16A0D695B68AA67FA",
            INIT_3F => x"7F367E6D7DA57CDC7C137B4B7A8379BC78F4782E776876A275DD751974557393")

		 port map (
			DO => sinCosBRAM,						-- Output read data port, width defined by READ_WIDTH parameter
			RDADDR => address,					-- Input address, width defined by port depth
			RDCLK => clk,	 						-- 1-bit input clock
			RST => BRAMrst,							-- active high reset
			RDEN => '1',							-- read enable 
			REGCE => '1',							-- 1-bit input read output register enable - ignored
			DI => "0000000000000000",						-- Input data port, width defined by WRITE_WIDTH parameter	
			WE => "00",					-- since RAM is byte read, this determines high or low byte
			WRADDR => "0000000000",					-- Input write address, width defined by write port depth
			WRCLK => clk,							-- 1-bit input write clock
			WREN => '0');	-- set to 1 for now    	-- 1-bit input write port enable           
             
    BRAMrst <= not reset_n;             
    
    address <= index_cntr(15 downto 6); 
           
    --baseIndex <= index_cntr(15 downto 6);
    --baseIndexInc <= baseIndex + 1;
    --deltaIndex <= baseIndexInc - baseIndex;
    --offset <= index_cntr(5 downto 0);
    --addressTemp <= (offset * deltaIndex) + baseIndex;
    --address <= addressTemp(15 downto 6);
    
    --L_bus_inWire <= std_logic_vector((unsigned(BRAMout) - x"8000")) & "00";
    sinWireTemp <= amp * unsigned(sinBRAM);
    sinWire <= std_logic_vector(sinWireTemp(31 downto 16) - x"8000") & "00";
    
    sinCosWireTemp <= amp * unsigned(sinCosBRAM);
    sinCosWire <= std_logic_vector(sinCosWireTemp(31 downto 16) - x"8000") & "00";
            
    L_bus_inWire <= sinWire when switchWaves = '0' else sinCosWire;
            
    -- BUTTON LOGIC               
    process(clk)
    begin
        -- if the button press is new button_activity is updated else it's set to 0
        -- old_button is assigned the current btn value so btn can take in the newest button state
        if(rising_edge(clk)) then
            if(reset_n = '0') then
                old_button <= "00000";
                button_activity <= "00000";
            elsif(((old_button xor btn) and btn) /= "00000") then
                old_button <= btn;
                button_activity <= btn;
            else 
                old_button <= btn;
                button_activity <= "00000";
            end if;
        end if;
    end process;
   
    
    -- AMP / PHASE LOGIC / SWITCH Waveform control
    process(clk)
    begin
        if(rising_edge(clk)) then
            if(reset_n = '0') then
                phase_inc <= x"0259";
                amp <= x"3FFF";
            -- LEFT btn increases phase
            elsif(button_activity = "00010") then
                --phase_inc <= phase_inc + unsigned(x"00" & switch);
                phase_inc <= phase_inc + unsigned(x"00" & switch);
            -- RIGHT btn decreases phase
            elsif(button_activity = "01000") then
                phase_inc <= phase_inc - unsigned(x"00" & switch);
            elsif(button_activity = "00001") then
                amp <= amp + unsigned(switch & x"00");
            elsif(button_activity = "00100") then
                amp <= amp - unsigned(switch & x"00");
            elsif(button_activity = "10000") then
                if(switchWaves = '1') then
                    switchWaves <= '0';             -- sinWave
                else
                    switchWaves <= '1';             -- sinCosWave
                end if;
            else
                phase_inc <= phase_inc;
                amp <= amp;
            end if;
        end if;
    end process;
   
    
    --counter
    process(clk)
    begin
        if(rising_edge(clk)) then
            if(reset_n = '0') then
                index_cntr <= (others => '0');
            elsif(exSel = '1') then
                index_cntr <= std_logic_vector(unsigned(index_cntr) + unsigned(ex_phase_inc));
            elsif(cw(0) = '1') then
                index_cntr <= std_logic_vector(unsigned(index_cntr) + phase_inc);
            end if;
        end if;
    end process;
    
end Behavioral;
