/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
/*--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	04/08/22
-- File:	lab4.c
-- Event:	lab 4
-- Crs:		CSCE 436
--
-- Purp:	change function generator phase with keyboard
--
-- Documentation:	MicroBlaze Tutorial - modified from lec20.c given in class
--
-- Academic Integrity Statement: I certify that, while others may have
-- assisted me in brain storming, debugging and validating this program,
-- the program itself is my own work. I understand that submitting code
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to
-- another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
/***************************** Include Files ********************************/

#include "xparameters.h"
#include "stdio.h"
#include "xstatus.h"

#include "platform.h"
#include "xil_printf.h"						// Contains xil_printf
#include <xuartlite_l.h>					// Contains XUartLite_RecvByte
#include <xil_io.h>							// Contains Xil_Out8 and its variations
#include <xil_exception.h>


/************************** Constant Definitions ****************************/

/*
 * The following constants define the slave registers used for our Counter PCORE
 */
#define funGenBase			0x44a00000
#define funGenExSel			funGenBase			//slv 0
#define	funGenPhaseInc		funGenBase+4		//slv 1
//#define	oscopeExSel			oscopeBase+8		//slv 2
//#define	oscopeLBusOut		oscopeBase+0xC		//slv 3
//#define oscopeRBusOut		oscopeBase+0x10		//slv 4
//#define oscopeExLBus		oscopeBase+0x14		//slv 5
//#define oscopeExRBus		oscopeBase+0x18		//slv 6
//#define oscopeFlag			oscopeBase+0x1C		//slv 7
//							//+0x20				//slv 8
//#define oscopeTrigTimeIn	oscopeBase+0x24		//slv 9
//#define oscopeTrigVoltIn	oscopeBase+0x28		//slv 10
//#define oscopeCh1Enable		oscopeBase+0x2C		//slv 11
//#define oscopeCh2Enable		oscopeBase+0x30		//slv 12
//#define oscopeExWrAddr2		oscopeBase+0x34		//slv 13

#define printf xil_printf			/* A smaller footprint printf */

#define	uartRegAddr			0x40600000		// read <= RX, write => TX


/************************** Function Prototypes ****************************/

/************************** Variable Definitions **************************/


int main(void) {

	unsigned char c;
	int phaseInc;

	init_platform();

	print("Welcome to Lab 4 - Bethany Krull\n\r");

	//microblaze_register_handler((XInterruptHandler) myISR, (void *) 0);
	//microblaze_enable_interrupts();

	//Xil_Out8(oscopeFlag, 0x01);					// Clear the flag and then you MUST
	//Xil_Out8(oscopeFlag, 0x00);					// allow the flag to be reset later

	//set initials
	Xil_Out8(funGenExSel,1);
	Xil_Out16(funGenPhaseInc,0);

    while(1) {
    	if(!XUartLite_IsReceiveEmpty(uartRegAddr)){
    		c=XUartLite_RecvByte(uartRegAddr);
    	} else {
    		c = 0;
    	}

		switch(c) {

    		// display MENU
    		case '?':
    			printf("--------------------------\r\n");
    			printf("	LAB 4 MENU\r\n");
    			printf("--------------------------\r\n");
    			printf("?: help menu\r\n");
    			printf("o: k\r\n");
    			printf("a: set amp\r\n");
    			printf("p: set phase\r\n");
    			printf("i: interrupts OFF\r\n");
    			printf("I: interrupts ON\r\n");
    			printf("c: clear terminal\r\n");
    			break;

    		case 'o':
    			printf("k \r\n");
    			break;

//			// trigger time - left
//			case 'a':
//				if(time-10 > 20){
//					time = time-10;
//					Xil_Out16(oscopeTrigTimeIn,time);
//					BRAM();
//					printf("< \r\n");
//				}
//				break;

			// trigger time - right
			case 'p':
				phaseInc=XUartLite_RecvByte(uartRegAddr);
				Xil_Out16(funGenPhaseInc,phaseInc);
				break;

			// disable interrupt mode
			case 'i':
				Xil_Out8(funGenExSel,0);
				printf("interrupts OFF \r\n");
				break;

		    // enable interrupt mode
			case 'I':
				Xil_Out8(funGenExSel,1);
				printf("interrupts ON \r\n");
				break;

			// clear terminal
            case 'c':
            	for (c=0; c<40; c++) printf("\r\n");
               	break;

			//unknown character input
    		default:
    			printf("unrecognized character: %c\r\n",c);
    			break;
    	} // end case


    } // end while 1

    cleanup_platform();

    return 0;
} // end main


