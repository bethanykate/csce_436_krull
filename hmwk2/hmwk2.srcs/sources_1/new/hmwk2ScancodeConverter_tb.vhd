--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	01/23/22
-- File:	hmwk2ScancodeConverter_tb.vhd
-- HW:		Homework 2
-- Crs:		CSCE 436
--
-- Purp:	provide testing for the circuit designed in 
--          hmwk2ScancodeConverter.vhd verify that all I/Os work as
--          expected
--
-- Documentation:	I worked on my own.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;

entity hmwk2ScancodeConverter_tb is
end hmwk2ScancodeConverter_tb;

architecture Behavioral of hmwk2ScancodeConverter_tb is
component hmwk2ScancodeConverter
    port(D : in STD_LOGIC_VECTOR (7 downto 0);
         H : out STD_LOGIC_VECTOR (3 downto 0));
end component;
signal dataIN : STD_LOGIC_VECTOR (7 downto 0);
signal dataOUT : STD_LOGIC_VECTOR (3 downto 0);
begin
    uut: hmwk2ScancodeConverter port map(
        D => dataIN,
        H => dataOUT);
process 
begin    
    dataIN <= "01000101";
    wait for 20us;
    dataIN <= "00010110";
    wait for 20us;
    dataIN <= "00011110";
    wait for 20us;
    dataIN <= "00100110";
    wait for 20us;
    dataIN <= "00100101";
    wait for 20us;
    dataIN <= "00101110";
    wait for 20us;
    dataIN <= "00110110";
    wait for 20us;
    dataIN <= "00111101";
    wait for 20us;
    dataIN <= "00111110";
    wait for 20us;
    dataIN <= "01000110";
    wait for 20us;
end process;
end Behavioral;
