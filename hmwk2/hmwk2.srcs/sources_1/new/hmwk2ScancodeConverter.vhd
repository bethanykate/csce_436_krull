--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	01/23/22
-- File:	hmwk2ScancodeConverter.vhd
-- HW:		Homework 2
-- Crs:		CSCE 436
--
-- Purp:	Convert inputed scancode values (8-bit) to a decimal
--          number between 0-9 (4-bit)
--
-- Documentation:	I worked alone
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;

entity hmwk2ScancodeConverter is
    Port (D : in STD_LOGIC_VECTOR (7 downto 0);
          H : out STD_LOGIC_VECTOR (3 downto 0));
end hmwk2ScancodeConverter;

architecture Behavioral of hmwk2ScancodeConverter is
begin
    H <=    "0000" when D = "01000101" else
            "0001" when D = "00010110" else
            "0010" when D = "00011110" else
            "0011" when D = "00100110" else
            "0100" when D = "00100101" else
            "0101" when D = "00101110" else
            "0110" when D = "00110110" else
            "0111" when D = "00111101" else
            "1000" when D = "00111110" else
            "1001";
end Behavioral;
