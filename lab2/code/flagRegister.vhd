--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	02/24/22
-- File:	flagRegister.vhd
-- HW:		lab 2
-- Crs:		CSCE 436
--
-- Purp:	2-line handshake for lab 3 microblaze processor
--
-- Documentation:	I worked on my own
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;

entity flagRegister is
    Generic (N: integer := 8);
	Port(	clk: in  STD_LOGIC;
			reset : in  STD_LOGIC;
			set, clear: in std_logic_vector(N-1 downto 0);
			Q: out std_logic_vector(N-1 downto 0) := (others => '1'));
end flagRegister;

architecture Behavioral of flagRegister is
signal procQ : std_logic_vector(N-1 downto 0);


--set <= "00000" & ready & v_synch & maxCnt
-- v_synch from the vga component add it to the port map as an output to the top level

begin
    process(clk)
    begin
        if(rising_edge(clk)) then
            if(reset = '0') then
                procQ <= (others => '0');
            elsif((set /= "00000000") and (clear = "00000000")) then
                procQ <= "11111111";
            elsif((set = "00000000") and (clear /= "00000000")) then
                procQ <= "00000000";
            end if;
        end if;
    Q <= procQ;
    end process;
end Behavioral;
