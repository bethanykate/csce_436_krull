--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	
-- File:	
-- HW:		
-- Crs:		CSCE 436
--
-- Purp:	
--
-- Documentation:	
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;
library UNIMACRO;
use UNIMACRO.vcomponents.all;
use work.lab2Parts.all;	

entity lab2_flagReg_tb is
end lab2_flagReg_tb;

architecture Behavioral of lab2_flagReg_tb is

signal clk : std_logic;
signal reset : std_logic;
signal set : std_logic_vector(7 downto 0);
signal clear : std_logic_vector(7 downto 0);
signal Q : std_logic_vector(7 downto 0) := (others => '0');
constant clk_period : time := 10 ns;

begin
    uut : flagRegister
        generic map(8)
        port map (  clk => clk,
			        reset => reset,
			        set => set,
			        clear => clear,
			        Q => Q);
    
    clk_process : process
    begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
    end process;
    
    reset <= '0', '1' after 5us;
    set <= "00000000", "00000001" after 20us;
    clear <= "00000000", "00000001" after 15us, "00000000" after 25us;

end Behavioral;
