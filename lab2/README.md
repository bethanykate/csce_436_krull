# LAB 2 Report - CSCE439 Advanced Embedded Systems

## By Bethany Krull

## Table of Contents
1. Objectives or Purpose
2. Preliminary Design
3. Software flow chart or algorithms
4. Hardware schematic
5. Debugging
6. Testing methodology or results
7. Answers to Lab Questions
8. Observations and Conclusions
9. Documentation
 
### Objectives or Purpose 
This lab was the continuation off of the work done in lab 1. In the first lab the grid was printed on the screen. Voltage and time trigger markers, and 
fake channel lines were applied to the screen as well. Lab 2 saw the application of real signal values to the oscilliscope channel waves via the left and right
sides of an audio jack. For full functionality, the buttons implemented in the first lab were to shift the left channel wave (channel 1) horizontally along the
wave to match the voltage of the trigger marker. This lab also utilized a given architecture diagram and code that had aspects that lead into lab 3 where we 
implement a MicroBlaze processor.

<br>
### Preliminary design
The preliminary design was given to use as part of the series of labs - lab 1's design was a jumping off point for lab 2, along with the given documentation
and system designs. This block diagram was a roadmap during the design and coding process of lab 2.
<br>
![Memory Dump](extra/blockDiagPrelim.png)
*Fig1 - given block diagram*
<br>

Prelab:  <br>
There was no prelab for lab 2.
<br>

#### Code:
Code is given in bit bucket repository.
<br>

### Software flow chart or algorithms
<br>
![Memory Dump](extra/stateDiag.jpg)
*Fig2 - state diagram*
<br>

Definition of sw and cw: <br>
sw:<br>
sw(2) - Audio Codec ready signal -> 1 when ready else 0<br>
sw(1) - BRAMM 3FF compare -> 1 when BRAMCnt == 0x3FF else 0<br>
sw(0) - trigger comparison -> 1 when trigger is between past/current volt else 0<br>
<br>

cw:<br>
cw(2) - BRAM wrENB mux -> 0 selects lab 2 audio signal / 1 is for lab 3<br>
cw(1->0) - BRAMCounter control -> (00=hold, 01=inc, 11=set to 20)<br>
<br>

The state diagram above describes the behavior of the whole system and the logic of the
FSM component. The system starts in the reset state (where the BRAM write enable is off 
and BRAM counter is reset to 20) it then always moves to the waitTrigger 
state. It sits in the waitTrigger state until trigger comparisonis true then it 
moves to waitAudio. waitAudio either goes back to reset when the BRAM counter reaches its
max and there is no audio ready signl or it moves to the storeBRAM state when there is
an audio ready signal. 

Up until this point the cw for both waitTrigger and waitAudio had
set the system to not write and hold the BRAMCount value. But once the system enters
the storeBRAM state the cw = 100 which allows writting to the BRAM for one cycle. Then
the system moves on to the inc_count state where the BRAM write enable is once again 
turned off and the BRAM can increment for 1 cycle. 

From the inc_count state the system will go back to the waitAudio state and repeat the
loop until the BRAM count is at its max of 0x3FF, then the system starts all over again 
at the reset state.

#### Pseudocode:
<br>
Ther is no pseudocode.

### Hardware schematic
<br>
The diagram below is a block diagram that reflects the system of comoponents
and wires for my system at the end of the lab. 
<br>
![Memory Dump](extra/blockDiagFinal.png)
*Fig3 - final block diagram*
<br>

The lab 2 components encapsulated the 
work done in lab 1 in a heirarchy of nested components. Lab 1 consists of the Video 
component and the 2 processes next to it - Button Logic and Trigger Logic. The Video
component has many components defined inside it that handle drawing the screen according
to what values are fed into ch1 and ch2. All of the arcitecture beyond the Video 
component has to do with determining what value ch1 and ch2 should have at any given time.
<br>

The Audio_Codec_Wrapper is not a comoponent that I created - it takes in audio signals 
and outputs the audio signal from the left and right sides to the L_bus_outWire and 
R_bus_outWire. From here, several steps are taken to get the L_bus values into the right
form to feed into the BRAM component. The L_bus wires are fed through the Loopback Reg
which firstly loops the audio signals back to the Audio_Codec_Wrapper so the signal can
be heard from the board; this is useful for debugging purposes to see if the system is 
working. The register feeds right into the sign2Unsign step which is a concurrent 
assignment that mainly helps trigger comparison calculations (discussed later). 
<br>

The unsigned audio signals are then type changed into std_logic_vectors and fed into the
DI (data input) port of the BRAM. Technically, there is a mux component that will 
eventually help the MicroBlaze processer in lab 3, but for now exSel is set to 
'0' to plug the unsignedLBus wire to the BRAM.
<br>

The write address (WRADDR) of BRAM is determined with a counter component (BRAMCounter) 
that counts from 20 -> 3FF (resetting to 20 at every rollover). A comparison is made with
the BRAMCounter output (BRAMCnt) to determine if the whole screen had been run through. 
The outcome of that comparison is assigned to sw(1) which is used by the FSM component to
determine what state the machine should be in. There is another mux between BRAMCnt and 
the BRAM component that is not being used currently (will be used in lab 3), so 
essentially it feeds straight to the write address of the BRAM. The read address of the 
BRAM is much simpiler as it is just the columnWire value determined by the counters 
inside the Video component.
<br>

WREN (write enable) of the BRAM will utilize a mux in the future, but for now it is 
turned on and off by the outcome of trigger comparisons. These comparisons are done with
a past and current version of the unsignedLBus. The past version is compared against the
trigger_voltWire (where the triangle trigger mark is on the screen) - equals '1' if 
it's greater than the trigger. The current LBus is also compared with the trigger to see
if it is less than it. If both of these scenarios is true the sw(0) bit is '1'. Through 
the logic in the FSM component this determines if cwWire(2) is '1' - if so then the BRAM 
is set to write.
<br>

The final step in setting the value of the ch1 is to compare the output of the BRAM 
(readL) with the current row (via rowWire). If these values match (adjusting for the 292 offset) then ch1 is set to 1 
and the line will be drawn accordingly. There is also a flag register that is purely for
future implementation.
<br>

### Debugging
I had gotten all of the A-Level Functionality working including the step by step 
scrolling through the ch1 wave via the board buttons, but I couldn't get my trigger
volt curser to line up with the actual waveform along the left side of the screen. It 
still worked properly, but it seemed offset. At first I though it was a timing issue so 
I switched the order of my store to BRAM state and my BRAM counter increment state to see
if that would help, but it didn't. Next I tried to flip the greater-than/less-than logic
of the trigger compare calculations. I had thought that maybe it was backwards and 
causing this offset. When I did that all that happened was that my wave was drawn 
"flipped" but there was still an offset between the curser and the beginning of the wave.
I was looking at the screen and I know that the distance between horizontal lines in the
grid is around 50 and visually I could tell that the curser might have been maybe 20 
pixels off. I then remembered that we were supposed to restart the BRAM counter at 20 
after maxing out at 0x3FF. It was a little detail but I had forgotten that requriement 
and never picked up on it when looking at my simulation files earlier on.

### Testing methodology or results
Gatecheck 1:<br>
The first gate check was to loop the output of the Audio_Codec_Wrapper back to itself
and hear whatever signal is fed into the board being played from the output jack. This
gatecheck also required that all of the previous grid drawing functionality gained during
lab 1 was still working.

LINK TO GATECHECK 1 VIDEO:<br>
https://photos.app.goo.gl/d4BXmHbpPydXCvyRA

Gatecheck 2:<br>
Gatecheck 2 required the implementation of all the components given in the preliminary
block diagram minus the trigger comparison calculations and the FSM. The result of this
gatecheck meant that a signal would show up on the oscilliscope grid as a collection of 
different lines, not yet forming a wave.

![Memory Dump](extra/gate2Far.png)
*Fig4 - Gate 2 simulation overview*

![Memory Dump](extra/gate2UpClose.png)
*Fig5 - Gate 2 simulation detailed*

LINK TO GATECHECK 2 VIDEO:<br>
https://photos.app.goo.gl/Vzg4pX1PrmmTaG3u5

Required, B, and A-Level Functionality:<br>
This step required the implementation of the FSM component (described in the software 
flowchart section) as well as the trigger compare calculations and a flag register. This 
allowed the system to draw audio signals as a waveform on the grid as well as "scroll through" the ch1 wave
with the tigger voltage button.
<br>

![Memory Dump](extra/flagRegSim.png)
*Fig6 - flag register simulation*
<br>

![Memory Dump](extra/fsmSimFar.png)
*Fig7 - FSM simulation overview*
<br>

![Memory Dump](extra/fsmSimClose.png)
*Fig8 - FSM simulation detailed*
<br>

![Memory Dump](extra/BRAMCalcSimFar.png)
*Fig9 - Datapath simulation overview*
<br>

![Memory Dump](extra/BRAMCalcSimClose.png)
*Fig10 - Datapath simulation detailed*
<br>

### Answers to Lab Questions - Capability
<br>
1. 1/600 sec - The frame rate is 60Hz so a single frame takes 1/60sec to draw so by dividing that by 10 I get 1/600sec.<br>
2. 1/2400 sec - The section time divided by 4 -> 1/600 * 1/4<br>
3. I can't compute this because I don't have 2 audio jack to audio jack wires - I would need one to input and output the audio signal and the kit only has 1. I used a 272Hz signal with an amp of 100 from the Husker Scope app. This drew a wave that took 8 ticks. I wouldv'e plugged that same frequency/amp into the board and taken another wire and fed the signal into the oscilliscope function of the app and done the math with that voltage value.<br>
4. 0.0213 sec - While there are other delays and steps in the system besides the loop between the waitAudio/storeBRAM/inc_count the system clock is 100MHz so it's fast enough that I'm only calculating based off the Audio signal frequency. 1023 entries have to be made to the BRAM to fill it up so: 1/48000 * 1023 = 0.0213<br>
5. 1/60 sec - using the frame rate (60Hz)<br>
6. 60us - measured off of a simulation picture from lab 1 - v_synch is low for 2 rows of the screen.<br>
### Observations and Conclusions
This lab focused on utilizing the BRAM component on the board as well as implementing a state driven system. Using the BRAM in conjunction with other fairly complicated components (Audio_Codec_Wrapper, Video) was challenging because there were so many moving parts that were all talking to each other to make the oscilliscope. It got pretty complicated when I first started hooking everything up and adding in the logic given to us in the block diagram. 
<br>

At first, I struggled with the flow of the architecture. I tried to make different components with seperate vhd files for each box I saw on the diagram - meaning I made a comparison, mux, and register component. I realized pretty quickly that this was very messy with all the sepereate wires connecting everything together, and since all the logic was hiden in all the component files it was pretty hard to debug. I got rid of all of that and was able to do most of the logic in the block diagram via concurrent statements. This lab did a good job of forcing me to pick an effective design strategy that would be appropriate for the project. In some scenarios having all the logic tucked away is advantageous, for example all of the code relating to building and drawing on the grid is kept in the Video component and doesn't clutter up the datapath component. And techinically there's no reason why havning a million connected components wouldn't work, but for this lab it just would have made the design process more difficult. 
<br>

Additionally, I learned how to use a finite state machine to build waves as opposed to just scattered lines. Using the FSM allowed for a much more complex system that is cleanly managed with seperate datapath and control components. Connecting all the cw and sw bits correctly forced me to be careful and double check myself because poor wiring was the cause of some of my errors.

In the future I would change the given block diagram a bit. The loopback and trigger compare registers have the ready signal being arrowed in right by the clk triangle and that seemed intentional to me when I was using it as a guide. So, I spent some time trying to figure out if the ready signal was what drove the process or if it was the system's clk.

<br>

### Documentation
I talked with Bearden, Anderson, Brandon, and Falkinburg at various points throughout lab 2. Specifics are documented on those files.
<br>
