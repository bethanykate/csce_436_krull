--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	02/08/22
-- File:	lab1.vhdl
-- HW:		lab 1
-- Crs:		CSCE 436
--
-- Purp:	this file is the top-level entity and only intakes buttons 
--          and outputs signals to display via HDMI - contains 
--          processes to read in button presses and increment the 
--          trigger count accordingly to send to the VGA component
--
-- Documentation:	I used the given code and edited as needed.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity lab1 is
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           btn: in	STD_LOGIC_VECTOR(4 downto 0);
           tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0));
end lab1;

architecture structure of lab1 is

	signal trigger_time, trigger_volt : unsigned(9 downto 0) := "0001100100"; 
	signal row, column: unsigned(9 downto 0);
	signal old_button, button_activity: std_logic_vector(4 downto 0) := "10101";
	signal ch1_wave, ch2_wave: std_logic;
	
	component video is
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
			  trigger_time: in unsigned(9 downto 0);
			  trigger_volt: in unsigned (9 downto 0);
			  row: out unsigned(9 downto 0);
			  column: out unsigned(9 downto 0);
			  ch1: in std_logic;
			  ch1_enb: in std_logic;
			  ch2: in std_logic;
			  ch2_enb: in std_logic);
	end component;

begin


	------------------------------------------------------------------------------
	-- the variable button_activity will contain a '1' in any position which 
	-- has been pressed or released.  The buttons are all nominally 0
	-- and equal to 1 when pressed.
	------------------------------------------------------------------------------
    process(clk)
    begin
        -- if the button press is new button_activity is updated else it's set to 0
        -- old_button is assigned the current btn value so btn can take in the newest button state
        if(rising_edge(clk)) then
            if(reset_n = '0') then
                old_button <= "00000";
                button_activity <= "00000";
            elsif(((old_button xor btn) and btn) /= "00000") then
                old_button <= btn;
                button_activity <= btn;
            else 
                old_button <= btn;
                button_activity <= "00000";
            end if;
        end if;
    end process;

	------------------------------------------------------------------------------
	-- If a button has been pressed then increment of decrement the trigger vars
	------------------------------------------------------------------------------
    process(clk)
	begin
	   if(rising_edge(clk)) then
	        -- according to which button is pressed a trigger is incremented
	        -- or decremented - the triggers are signals fed to VGA to help the scopeFace
	        if((button_activity and "01000") = "01000") then
                -- trigger time up
                trigger_time <= trigger_time + 10; 
            elsif((button_activity and "00100") = "00100") then
                -- trigger volt down
                trigger_volt <= trigger_volt + 10;
            elsif((button_activity and "00010") = "00010") then
                -- trigger time left
                trigger_time <= trigger_time - 10;
            elsif((button_activity and "00001") = "00001") then
                -- trigger volt up
                trigger_volt <= trigger_volt - 10;
           end if;
       end if;
   end process;
               
                
	------------------------------------------------------------------------------
	------------------------------------------------------------------------------
	video_inst: video port map( 
		clk => clk,
		reset_n => reset_n,
		tmds => tmds,
		tmdsb => tmdsb,
		trigger_time => trigger_time,
		trigger_volt => trigger_volt,
		row => row, 
		column => column,
		ch1 => ch1_wave,
		ch1_enb => ch1_wave,
		ch2 => ch2_wave,
		ch2_enb => ch2_wave); 

	
end structure;