--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	02/21/22
-- File:	comp.vhd
-- HW:		lab 2
-- Crs:		CSCE 436
--
-- Purp:	generic comparator
--
-- Documentation:	I worked on my own.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;

entity comp is
    generic(N : integer);
    port(   A, B    : in  std_logic_vector(N-1 downto 0);
            L,G,E   : out std_logic); 
end comp;

architecture Behavioral of comp is
begin
    L <= '1' when A < B else '0';
    G <= '1' when A > B else '0';
    E <= '1' when A = B else '0';
end Behavioral;