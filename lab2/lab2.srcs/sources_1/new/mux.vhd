--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	02/22/22
-- File:	mux.vhd
-- HW:		lab 2
-- Crs:		CSCE 436
--
-- Purp:	generic 2 option mux
--
-- Documentation:	I worked on my own.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;

entity mux is
    generic(N : integer);
    port(   A, B    : in unsigned(N-1 downto 0);
            S       : in std_logic;
            Y       : out unsigned(N-1 downto 0));
end mux;

architecture Behavioral of mux is
begin
    Y <= A when S = '1' else B;
end Behavioral;
