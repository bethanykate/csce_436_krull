--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	02/22/22
-- File:	sign2Unsign.vhd
-- HW:		lab 2
-- Crs:		CSCE 436
--
-- Purp:	convert from signed from Audio Codec Wrapper to 
--          unsigned for BRAM
--
-- Documentation:	I worked on my own.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;

entity sign2Unsign is
    port(   signedIn    : in std_logic_vector(17 downto 0);
            unsignedOut : out unsigned(17 downto 0)); 
end sign2Unsign;

architecture Behavioral of sign2Unsign is
begin
    unsignedOut <= unsigned(signedIn) + 131072;
end Behavioral;
