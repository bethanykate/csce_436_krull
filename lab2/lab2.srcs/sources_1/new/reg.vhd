--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	02/22/22
-- File:	reg.vhd
-- HW:		lab 2
-- Crs:		CSCE 436
--
-- Purp:	generic reigster component
--
-- Documentation:	I worked on my own.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;

entity reg is
    generic(N : integer);
    port(   clk     : in std_logic;
            reset   : in std_logic;
            D       : in std_logic_vector(N-1 downto 0);
            Q       : out std_logic_vector(N-1 downto 0));
end reg;

architecture Behavioral of reg is
signal procQ : std_logic_vector(N-1 downto 0);

begin
    process(clk)
    begin
        if(rising_edge(clk)) then
            if(reset = '0') then
                procQ <= (others => '0');
            else
                procQ <= D;
            end if;
        end if;
        Q <= procQ;
    end process;
end Behavioral;
