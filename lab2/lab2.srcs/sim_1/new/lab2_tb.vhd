LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.VComponents.all;
use work.lab2Parts.all;		

entity lab2_tb is
end lab2_tb;

architecture Behavioral of lab2_tb is
signal clk : STD_LOGIC;
signal reset_n : STD_LOGIC;
signal ac_mclk : STD_LOGIC;
signal ac_adc_sdata : STD_LOGIC;
signal ac_dac_sdata : STD_LOGIC;
signal ac_bclk : STD_LOGIC;
signal ac_lrclk : STD_LOGIC;
signal scl : STD_LOGIC;
signal sda : STD_LOGIC;
signal tmds : STD_LOGIC_VECTOR (3 downto 0);
signal tmdsb : STD_LOGIC_VECTOR (3 downto 0);
signal btn: STD_LOGIC_VECTOR(4 downto 0);

constant clk_period : time := 10 ns;  -- Sets clock to ~ 100MHz
--   constant BIT_CLK_period : time := 80 ns;  -- Sets Bit Clock for AC'97 to the necessary 12.288 MHz
   constant BIT_CLK_period : time := 40 ns;  -- Sets Bit Clock for Audio Codec to the necessary 25 MHz


begin

    uut: lab2
        port map(  clk => clk,
                   reset_n => reset_n,
                   ac_mclk => ac_mclk,
                   ac_adc_sdata => ac_adc_sdata,
                   ac_dac_sdata => ac_dac_sdata,
                   ac_bclk => ac_bclk,
                   ac_lrclk => ac_lrclk,
                   scl => scl,
                   sda => sda,
                   tmds => tmds,
                   tmdsb => tmdsb,
                   btn => btn);

            SDATA_process :process  -- Inputs alternating 1's and 0's on each Bit Clock
            begin
                 ac_adc_sdata <= '0';
                 wait for BIT_CLK_period;
                 ac_adc_sdata <= '1';
                 wait for BIT_CLK_period*2;
            end process;
        
           clk_process :process
           begin
                clk <= '0';
                wait for clk_period/2;
                clk <= '1';
                wait for clk_period/2;
           end process;

end Behavioral;
