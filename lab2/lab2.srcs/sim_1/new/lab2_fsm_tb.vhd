--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	02/24/22
-- File:	lab2_fsm_tb.vhd
-- HW:		lab 2
-- Crs:		CSCE 436
--
-- Purp:	test bench for control unit (lab 2)
--
-- Documentation:	I worked on my own.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;
use UNISIM.VComponents.all;
use work.lab2Parts.all;	

entity lab2_fsm_tb is
end lab2_fsm_tb;

architecture Behavioral of lab2_fsm_tb is

signal clk : STD_LOGIC;
signal reset_n : std_logic;
signal sw: std_logic_vector(2 downto 0);
signal cw: std_logic_vector (2 downto 0);
constant clk_period : time := 10 ns;

begin

    uut : lab2_fsm
        port map(   clk => clk,
                    reset_n => reset_n,
                    sw => sw,
                    cw => cw);
                    
    clk_process : process
    begin
        clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
    end process;
                   
    reset_n <= '0', '1' after 5us;               
    sw <= "000", "001" after 10us, "100" after 15us, "010" after 25us,"001" after 30us, "100" after 35us, "010" after 40us;

end Behavioral;
