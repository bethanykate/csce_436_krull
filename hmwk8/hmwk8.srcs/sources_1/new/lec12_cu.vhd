--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	02/16/22
-- File:	lec12_cu.vhd
-- HW:		homework 8
-- Crs:		CSCE 436
--
-- Purp:	the state logic for lec12 system
--
-- Documentation:	I worked on my own - used given code.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;

entity lec12_cu is
	Port(	clk: in  STD_LOGIC;
			reset : in  STD_LOGIC;
			kbClk: in std_logic;
			cw: out STD_LOGIC_VECTOR(2 downto 0);
			sw: in STD_LOGIC;
			busy: out std_logic);
end lec12_cu;

architecture Behavioral of lec12_cu is
type STATE_TYPE is (restart, waitStart, init, comp, while1, shift, while0, inc);
signal curr_st : STATE_TYPE;
signal nx_st : STATE_TYPE;
begin
    process(clk)
        begin
            if(rising_edge(clk)) then
                -- CW and BUSY LOGIC
                -- cw   2 1 0   shift-reg       counter
                ---------------------------------------
                --      0 0 0   hold            hold
                --      0 0 1   hold            count up
                --      0 1 0   hold            unused
                --      0 1 1   hold            synch reset
                --      1 0 0   shift-right     hold
                --      1 0 1   shift-right     count up
                --      1 1 0   shift-right     unused
                --      1 1 1   shift-right     synch reset          
                
                case nx_st is
                    when restart => 
                        busy <= '0';
                        cw <= "011";
                    when waitStart => 
                        busy <= '0';
                        cw <= "000";
                    when init => 
                        busy <= '1';
                        cw <= "000";
                    when comp => 
                        busy <= '1';
                        cw <= "000";
                    when while1 => 
                        busy <= '1';
                        cw <= "000";
                    when shift => 
                        busy <= '1';
                        cw <= "100";
                        
                    when while0 => 
                        busy <= '1';
                        cw <= "000"; 
                    when inc => 
                        busy <= '1';
                        cw <= "001";
                    when others => 
                        busy <= '0';
                        cw <= "000";
                end case; 
                
                if(reset='0') then
                    curr_st <= restart;
                else
                    curr_st <= nx_st;
                end if;
                
            end if;
    end process;

    -- NEXT STATE LOGIC
    nx_st <=    waitStart when (curr_st = restart) else
                init when ((curr_st = waitStart) AND (kbClk = '0')) else
                comp when ((curr_st = init) OR (curr_st = inc)) else
                while1 when ((curr_st = comp) AND (sw = '0')) else
                shift when ((curr_st = while1) AND (kbClk = '0')) else
                while0 when (curr_st = shift) else
                inc when ((curr_st = while0) AND (kbClk = '1')) else
                restart when ((curr_st = comp) AND (sw = '1'));
            
end Behavioral;