--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	04/21/22
-- File:	finalPrj_dp.vhd
-- HW:		final project
-- Crs:		CSCE 436
--
-- Purp:	provide the datapath for the final project system
--
-- Documentation:	modeled off of class code
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;
library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity finalPrj_dp is
    port(  clk : in  STD_LOGIC;
	       reset_n : in  STD_LOGIC;
	       tmds : out  STD_LOGIC_VECTOR (3 downto 0);
	       tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
	       sw: out std_logic_vector(2 downto 0);
	       cw: in std_logic_vector (2 downto 0);
	       letter1 : in std_logic;
	       letter2 : in std_logic;
	       letter3 : in std_logic;
	       letter4 : in std_logic;
	       letter5 : in std_logic;
	       blueToothIn : in std_logic;
	       blueToothOut : out std_logic);
end finalPrj_dp;

architecture Behavioral of finalPrj_dp is

    component video is
        port(  clk : in  STD_LOGIC;
               reset_n : in  STD_LOGIC;
               tmds : out  STD_LOGIC_VECTOR (3 downto 0);
               tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
               letter1 : in std_logic;
               letter2 : in std_logic;
               letter3 : in std_logic;
               letter4 : in std_logic;
               letter5 : in std_logic;
               row: out unsigned(9 downto 0);
               column: out unsigned(9 downto 0));
   end component;
    
    -- STATE
    signal swWire : std_logic_vector(2 downto 0) := (others => '0');
    signal cwWire : std_logic_vector(2 downto 0) := (others => '0');
    signal swFlag : std_logic_vector(7 downto 0) := (others => '0');
    
    signal rowWire, columnWire : unsigned(9 downto 0);
    signal v_synch : std_logic;
     
   
    
begin
    cwWire <= cw;
    swFlag <= "00000" & swWire(2) & v_synch & swWire(1);
    blueToothOut <= blueToothIn;
    sw <= swWire;

    lab2_video: video
        port map(   clk => clk,
                    reset_n => reset_n,
                    tmds => tmds,
                    tmdsb => tmdsb,
                    row => rowWire,
                    column => columnWire,
                    letter1 => letter1,
                    letter2 => letter2,
                    letter3 => letter3,
                    letter4 => letter4, 
                    letter5 => letter5);
    
end Behavioral;








