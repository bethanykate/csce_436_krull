--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	02/18/22
-- File:	lab2_datapath.vhd
-- HW:		lab 2
-- Crs:		CSCE 436
--
-- Purp:	provide the datapath for the lab 2 system (audio in/out)
--          and hdmi (out)
--
-- Documentation:	I talked with a TA (Anderson) - used given code.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;
library UNIMACRO;
use UNIMACRO.vcomponents.all;
use work.lab2Parts.all;	

entity lab2_datapath is
    port(  clk : in  STD_LOGIC;
	       reset_n : in  STD_LOGIC;
	       exCh1 : in std_logic;
	       exCh2 : in std_logic;
           ac_mclk : out STD_LOGIC;
	       ac_adc_sdata : in STD_LOGIC;
	       ac_dac_sdata : out STD_LOGIC;
	       ac_bclk : out STD_LOGIC;
	       ac_lrclk : out STD_LOGIC;
	       scl : inout STD_LOGIC;
	       sda : inout STD_LOGIC;	
	       tmds : out  STD_LOGIC_VECTOR (3 downto 0);
	       tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
	       sw: out std_logic_vector(2 downto 0);
	       cw: in std_logic_vector (2 downto 0);
	       btn: in	STD_LOGIC_VECTOR(4 downto 0);
	       exWrAddr: in std_logic_vector(9 downto 0);
	       exWrAddr2: in std_logic_vector(9 downto 0);
	       exWen, exSel: in std_logic;
	       Lbus_out, Rbus_out: out std_logic_vector(15 downto 0);
	       exLbus, exRbus: in std_logic_vector(15 downto 0);
	       flagQ: out std_logic_vector(7 downto 0);
	       flagClear: in std_logic_vector(7 downto 0);
	       trigger_time_in : in std_logic_vector(9 downto 0);
	       trigger_volt_in: in std_logic_vector(9 downto 0)); 
end lab2_datapath;

architecture Behavioral of lab2_datapath is
    -- VIDEO
    signal trigger_timeWire : unsigned(9 downto 0) := "0101000000";
    signal trigger_voltWire : unsigned(9 downto 0) := "0011011100";
    signal ch1Wire, ch1_enbWire, ch2Wire, ch2_enbWire : std_logic;
    signal rowWire, columnWire : unsigned(9 downto 0);
    signal old_button, button_activity : std_logic_vector(4 downto 0) := "00000";
    signal ch1Temp, ch2Temp : std_logic;
    -- AUDIO CODEC
    signal L_bus_inWire, R_bus_inWire, L_bus_outWire, R_bus_outWire : std_logic_vector(17 downto 0) := (others => '0');
    -- BRAM
    signal BRAMCnt : unsigned(9 downto 0):= (others => '0');
    signal vecAddrRead, vecAddrWriteL, vecAddrWriteR : std_logic_vector(9 downto 0);
    signal writeInputLeft, writeInputRight : std_logic_vector(17 downto 0);
    signal readL, readR : std_logic_vector(17 downto 0);
    signal unsignedLBus, unsignedRBus : unsigned(17 downto 0);
    signal regRBus, regLBus : std_logic_vector(17 downto 0);
    signal writeEnable: std_logic;
    signal BRAMrst : std_logic; 
    -- STATE
    signal swWire : std_logic_vector(2 downto 0) := (others => '0');
    signal cwWire : std_logic_vector(2 downto 0) := (others => '0');
    signal swFlag : std_logic_vector(7 downto 0) := (others => '0');
    -- TRIGGER COMPARE
    signal currUnsignedLBus : unsigned(17 downto 0);
    signal v_synch : std_logic;
     
   -- SW  
   ------------------------------  
   -- 2: Audio Codec ready signal -> 1: Audio Codec is ready else 0
   -- 1: BRAM 3FF compare output -> 1: BRAMCntr == 0x3FF else 0
   -- 0: trigger
   
   -- CW
   ------------------------------
   -- 2 BRAM wrENB mux
   -- 1-0: BRAM address counter (00=hold, 01=inc, 11="0")
    
begin
    cwWire <= cw;
    swFlag <= "00000" & swWire(2) & v_synch & swWire(1);
--    trigger_timeWire <= unsigned(trigger_time_in);
--    trigger_voltWire <= unsigned(trigger_volt_in);
    
    lab2_flagReg : flagRegister
        generic map(8)
        port map(	clk => clk,
                    reset => reset_n,
                    set => swFlag,
                    clear => flagClear,
                    Q => flagQ);
                                      
             
    lab2_BRAMCntr : BRAMCounter
        port map(   clk => clk,
                    reset => reset_n,
                    ctrl => cw(1 downto 0),
                    count => BRAMCnt);

    lab2_video: video
        port map(   clk => clk,
                    reset_n => reset_n,
                    tmds => tmds,
                    tmdsb => tmdsb,
                    trigger_time => trigger_timeWire,
                    trigger_volt => trigger_voltWire,
                    row => rowWire,
                    column => columnWire,
                    v_synchOut => v_synch,
                    ch1 => ch1Wire,
                    ch1_enb => ch1_enbWire,
                    ch2 => ch2Wire,
                    ch2_enb => ch2_enbWire);
                    
    lab2_Audio_Wrapper: Audio_Codec_Wrapper
        port map(   clk => clk,
                    reset_n => reset_n,
                    ac_mclk => ac_mclk,
                    ac_adc_sdata => ac_adc_sdata,
                    ac_dac_sdata => ac_dac_sdata,
                    ac_bclk => ac_bclk,
                    ac_lrclk => ac_lrclk,
                    ready => swWire(2),
                    L_bus_in => L_bus_inWire,
                    R_bus_in => R_bus_inWire,
                    L_bus_out => L_bus_outWire,
                    R_bus_out => R_bus_outWire,
                    scl => scl,
                    sda => sda);

    BRAMrst <= not reset_n;         -- BRAM is active high reset - flipping system reset to match

	leftBRAM: BRAM_SDP_MACRO
		generic map (
			BRAM_SIZE => "18Kb", 					-- Target BRAM, "18Kb" or "36Kb"
			DEVICE => "7SERIES", 					-- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6, 7SERIES"
			DO_REG => 0, 							-- Optional output register disabled
			INIT => X"000000000000000000",			-- Initial values on output port
			INIT_FILE => "NONE",					-- Not sure how to initialize the RAM from a file
			WRITE_WIDTH => 18, 						-- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
			READ_WIDTH => 18, 						-- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
			SIM_COLLISION_CHECK => "NONE", 			-- Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
			SRVAL => X"000000000000000000")			-- Set/Reset value for port output
		port map (
			DO => readL,						-- Output read data port, width defined by READ_WIDTH parameter
			RDADDR => vecAddrRead,					-- Input address, width defined by port depth
			RDCLK => clk,	 						-- 1-bit input clock
			RST => BRAMrst,							-- active high reset
			RDEN => '1',							-- read enable 
			REGCE => '1',							-- 1-bit input read output register enable - ignored
			DI => writeInputLeft,						-- Input data port, width defined by WRITE_WIDTH parameter	
			WE => "11",					-- since RAM is byte read, this determines high or low byte
			WRADDR => vecAddrWriteL,					-- Input write address, width defined by write port depth
			WRCLK => clk,							-- 1-bit input write clock
			WREN => writeEnable);	-- set to 1 for now    	-- 1-bit input write port enable
            
      rightBRAM: BRAM_SDP_MACRO
		generic map (
			BRAM_SIZE => "18Kb", 					-- Target BRAM, "18Kb" or "36Kb"
			DEVICE => "7SERIES", 					-- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6, 7SERIES"
			DO_REG => 0, 							-- Optional output register disabled
			INIT => X"000000000000000000",			-- Initial values on output port
			INIT_FILE => "NONE",					-- Not sure how to initialize the RAM from a file
			WRITE_WIDTH => 18, 						-- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
			READ_WIDTH => 18, 						-- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
			SIM_COLLISION_CHECK => "NONE", 			-- Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
			SRVAL => X"000000000000000000")			-- Set/Reset value for port output
		port map (
			DO => readR,						-- Output read data port, width defined by READ_WIDTH parameter
			RDADDR => vecAddrRead,					-- Input address, width defined by port depth
			RDCLK => clk,	 						-- 1-bit input clock
			RST => BRAMrst,							-- active high reset
			RDEN => '1',							-- read enable 
			REGCE => '1',							-- 1-bit input read output register enable - ignored
			DI => writeInputRight,						-- Input data port, width defined by WRITE_WIDTH parameter	
			WE => "11",					-- since RAM is byte read, this determines high or low byte
			WRADDR => vecAddrWriteR,					-- Input write address, width defined by write port depth
			WRCLK => clk,							-- 1-bit input write clock
			WREN => '1');	-- set to 1 for now    	-- 1-bit input write port enable
			
    -- BUS UNSIGNING
    unsignedLBus <= (unsigned(regLBus) + 131072);
    unsignedRBus <= (unsigned(regRBus) + 131072);
    
    -- TRIGGER VOLT COMPARE
    swWire(0) <= '1' when ((unsignedLBus(17 downto 8) - 292 > trigger_voltWire) and (currUnsignedLBus(17 downto 8) - 292 < trigger_voltWire)) else '0';
     
    -- BRAM 
    writeEnable <= cw(2) when exSel = '0' else exWen;
    
    ch1_enbWire <= '1' when exSel = '0' else exCh1;
    ch2_enbWire <= '1' when exSel = '0' else exCh2;
    
    -- when BRAM counter rolls over
    swWire(1) <= '1' when BRAMCnt = x"3FF" else '0';
    
    -- prepping BRAM inputs (std_logic_vectors)
    vecAddrRead <= (std_logic_vector(columnWire));
    vecAddrWriteL <= (std_logic_vector(BRAMCnt)) when exSel = '0' else (exWrAddr); 
    vecAddrWriteR <= (std_logic_vector(BRAMCnt)) when exSel = '0' else (exWrAddr2); 
    writeInputLeft <= (std_logic_vector(unsignedLBus)) when exSel = '0' else (exLBus & "00");
    writeInputRight <= (std_logic_vector(unsignedRBus)) when exSel = '0' else (exRBus & "00");
    
    -- comparing BRAM out to current row - adjust by 292 to fix offset
    ch1Wire <= '1' when ((unsigned(readL(17 downto 8)) - 292) = rowWire) else '0';
    ch2Wire <= '1' when ((unsigned(readR(17 downto 8)) - 292) = rowWire) else '0';
     
    -- BUTTON LOGIC               
    process(clk)
    begin
        -- if the button press is new button_activity is updated else it's set to 0
        -- old_button is assigned the current btn value so btn can take in the newest button state
        if(rising_edge(clk)) then
            if(reset_n = '0') then
                old_button <= "00000";
                button_activity <= "00000";
            elsif(((old_button xor btn) and btn) /= "00000") then
                old_button <= btn;
                button_activity <= btn;
            else 
                old_button <= btn;
                button_activity <= "00000";
            end if;
        end if;
    end process;
    
    -- TRIGGER UP/DOWN
    process(clk)
    begin
	   if(rising_edge(clk)) then
	        -- according to which button is pressed a trigger is incremented
	        -- or decremented - the triggers are signals fed to VGA to help the scopeFace
	       if(exSel = '1') then
	           trigger_voltWire <= unsigned(trigger_volt_in);
               trigger_timeWire <= unsigned(trigger_time_in);
	       elsif((button_activity and "01000") = "01000") then
                -- trigger time up
               trigger_timeWire <= trigger_timeWire + 10; 
           elsif((button_activity and "00100") = "00100") then
                -- trigger volt down
               trigger_voltWire <= trigger_voltWire + 10;
            elsif((button_activity and "00010") = "00010") then
                -- trigger time left
               trigger_timeWire <= trigger_timeWire - 10;
            elsif((button_activity and "00001") = "00001") then
                -- trigger volt up
               trigger_voltWire <= trigger_voltWire - 10;
            end if;
       end if;
    end process;
    
    -- LOOPBACK REGISTER
    process(clk)                        
    begin
        if(rising_edge(clk)) then
            if(reset_n = '0') then
                L_bus_inWire <= (others => '0');
                R_bus_inWire <= (others => '0');
            elsif(swWire(2) = '1') then
                L_bus_inWire <= L_bus_outWire;
                R_bus_inWire <= R_bus_outWire;
                regLBus <= L_bus_outWire;
                regRBus <= R_bus_outWire;
                currUnsignedLBus <= unsignedLBus;
            end if;
        end if;
    end process;
    
    sw <= swWire;
    
    Lbus_out <= std_logic_vector(unsignedLBus(17 downto 2));
    Rbus_out <= std_logic_vector(unsignedRBus(17 downto 2));  
end Behavioral;








