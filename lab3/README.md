# LAB 3 Report - CSCE439 Advanced Embedded Systems

## By Bethany Krull

## Table of Contents
1. Objectives or Purpose
2. Preliminary Design
3. Software flow chart or algorithms
4. Hardware schematic
5. Debugging
6. Testing methodology or results
7. Answers to Lab Questions
8. Observations and Conclusions
9. Documentation
 
### Objectives or Purpose 
The goal of his lab was to first, implement the existing lab 2 functionality with the MicroBlaze processor. This required me to make all of lab 2 into a custom ip element that communicates smoothly with the processor. Beyond establishing MicroBlaze control over lab 2 functionality the main focus of lab 3 was to introduce interrupts to the process. The ready signal sent from the Audio Codec Wrapper inside the datapath file is ported out to the processor as an interrupt that makes the c code operation more streamoined and efficient. Allow with interrupts lab 3 required a new triggering setup that aligned the waveform with the intersection of the volt and time triggers. This required a data buffer that could be sorted through to allow the triggers to shift through the waveform with ease. This level of complexity justifies setting up the ready signal interrupt because the system was always filling and updating a buffer that could be pulled from at any time. The final product at the end of lab 3 is a fully functioning oscilloscope that allows the user to use the keyboard to manipulate the waveforms on screen.  

<br>
### Preliminary design
Lab 3 is the 3rd installment of this oscilloscope project, so a majority of the preliminary design for lab 3 is already established by previous labs. The previous lectures, 18 through 20, covered the creation of a custom ip element and how to connect it to the MicroBlaze. So, much of the planning for execution of lab 3 was based closely on the work I had done previously in those lectures. I even took the c code from 19 and modified it to work with my lab 2 ip element. The following hardware schematic was crucial to my design and it guided me as I made connection choices during the lab. Additionally, I referenced the hardware schematic from lab 2 often because it helped me see where the signals I was manipulating in the processor came from/went to in the lab 2 logic.

The first step is to create an ip element from lab 2 and connect it to the MicroBlaze. Once that is connected with the addition of the ready interrupt signal (according to the given schematic). I skip to B-Level Functionality to implement interrupt control to handle the stuff in the Required Functionality. For me using interrupts right away makes more sense. Then once the interrupt is working and the waveform moves according to the movements of the trigger volt and time markers, the rest is straightforward.

Adding in rising vs falling-edge control requires adding a line of logic with the order of the comparisons flipped. Getting the system to trigger off of either channel is similarly easy because it requires just adding the option of doing all the left channel functionality using the right array buffer. Going in and adding channel specific enabling wasn't difficult, but it did require me to go back to the ip element and add signals to write enables to from the c code in the MicroBlaze. 

<br>
![Memory Dump](doc/schematic.png)
*Fig1 - given block design*
<br>

<br>
![Memory Dump](doc/blockDiagFinal.png)
*Fig2 - hardware schematic from Lab 2*
<br>

Prelab:  <br>
There was no prelab for lab 3.
<br>

#### Code:
<br>
Code is available in bitbucket repository.

### Software flow chart or algorithms
<br>
![Memory Dump](doc/lab3Software.png)
*Fig3 - software flowchart for lab 3 c code*
<br>

The main function handles all of the user I/O and calls the BRAM() function that updates the waveform as needed. The user had a variety of keypress options they could use to control the system as outlined in the user menu that could be printed by typing the '?' key. Similary, with the introduction of A-Level Funcitonality I found it difficult to remember the current state of the system and created a status printout that informed the user which channels were enabled, which channel was being triggered off of, and if that trigger was supposed to be for a rising or falling edge. 

Once the BRAM function was called it would wait until it got a signal from the ready signal ISR that the buffer arrays was done being updated. Once the buffers are full comparisons start. Depending on the setting the user has set different trigger comparison logic occurs to find the target in the buffer array that will be centered between the volt and time triggers. With this target position a for loop is called to write the new values to the external left and right buses to the lab 2 ip element. The data sent out is shifted to set the target position where it should go.

In the background the ISR is running to keep the buffer arrays utilized in the BRAM function up to date nearly constantly. This is done with a light and lean ISR function that is triggered by the ready signal sent by the Audio Codec Wrapper. This function also sends a signal to the BRAM to say when each buffer is full. 

#### Pseudocode:
<br>
There is no pseudocode.

### Hardware schematic
<br>
![Memory Dump](doc/schematicUpdated.png)
*Fig4 - updated hardware schematic*
<br>

I very lightly edited the given schematic to include my BRAM() function in the MicroBlaze c code and to add that I use an ip called my_oscope2. This lab is not very hardware intensive so not much was done to the hardware except to get some signals to the surface of the ip element to use in the processor.

### Debugging
At first in the BRAM() logic I had the 20->620 for loop to store the correctly shifted buffer values into the BRAM to be drawn on the screen was inside of the 0->1023 for loop I used to sort through the buffer array and find a position that satisfies the trigger comparisons. I had it set up to run the storing loop upon finding the target position, but if the sorting loop found another target postion within the 0->1023 array the storing/drawing loop would be called again for the same waveform. This resulted in the wave scattering at the end of the screen (about the right 25% - I wish I had a picture to show).

To fix this logical bug I moved the storing loop out of the sorting loop and only look to find 1 target postion per sorting loop. This resulted in the wave staying as a wave across the whole screen and not scattering at any point.

### Testing methodology or results
Gatecheck 1:<br>
I connected the lap 2 ip element to the MicroBlaze and implemented full lab 2 functionality.

LINK TO GATECHECK 1 VIDEO:<br>
https://photos.app.goo.gl/LSUHz7TfCuFkPp7R9

Gatecheck 2:<br>
I was able to increment and decrement both the trigger volt and trigger time arrows on the screen.

LINK TO GATECHECK 2 VIDEO:<br>
https://photos.app.goo.gl/htxfL9dnxZcW7fBh8

A-Level Functionality:<br>
LINK TO A LEVEL FUNC VIDEO:<br>
https://photos.app.goo.gl/aRmnt9XyLUnpX5j77

### Answers to Lab Questions 
There are no questions for this lab.

### Observations and Conclusions
Lab 3 added a layer of complexity to the existing lab 2. The oscilloscope is much more functional now and geared towards accomidating for user needs - you can switch channels and flip the rising and falling edge. Adding the use of the MicroBlaze was very challenging at first. In gatechecks 1 and 2 I was very nervous to add components to the slave register/etc because I was not confident in how they all connected. However, by the A-Level Functionality section I felt very comfortable going into the datapath level inside the ip element and pulling the channel enable signals up to the processor.

### Documentation
I worked with the instructor and many of the TAs. I also utilized given code and diagrams from lecture.