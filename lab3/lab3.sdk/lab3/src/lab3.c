/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
/*--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	03/09/22
-- File:	lab3.c
-- Event:	lab 3
-- Crs:		CSCE 436
--
-- Purp:	operate o-scope from MicroBlaze - use keyboard direction
--
-- Documentation:	MicroBlaze Tutorial - modified from lec20.c given in class
--					help from instructor and TAs
--
-- Academic Integrity Statement: I certify that, while others may have
-- assisted me in brain storming, debugging and validating this program,
-- the program itself is my own work. I understand that submitting code
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to
-- another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
/***************************** Include Files ********************************/

#include "xparameters.h"
#include "stdio.h"
#include "xstatus.h"

#include "platform.h"
#include "xil_printf.h"						// Contains xil_printf
#include <xuartlite_l.h>					// Contains XUartLite_RecvByte
#include <xil_io.h>							// Contains Xil_Out8 and its variations
#include <xil_exception.h>


/************************** Constant Definitions ****************************/

/*
 * The following constants define the slave registers used for our Counter PCORE
 */
#define oscopeBase			0x44a00000
#define oscopeExWrAddr		oscopeBase			//slv 0
#define	oscopeExWen			oscopeBase+4		//slv 1
#define	oscopeExSel			oscopeBase+8		//slv 2
#define	oscopeLBusOut		oscopeBase+0xC		//slv 3
#define oscopeRBusOut		oscopeBase+0x10		//slv 4
#define oscopeExLBus		oscopeBase+0x14		//slv 5
#define oscopeExRBus		oscopeBase+0x18		//slv 6
#define oscopeFlag			oscopeBase+0x1C		//slv 7
							//+0x20				//slv 8
#define oscopeTrigTimeIn	oscopeBase+0x24		//slv 9
#define oscopeTrigVoltIn	oscopeBase+0x28		//slv 10
#define oscopeCh1Enable		oscopeBase+0x2C		//slv 11
#define oscopeCh2Enable		oscopeBase+0x30		//slv 12
#define oscopeExWrAddr2		oscopeBase+0x34		//slv 13

#define printf xil_printf			/* A smaller footprint printf */

#define	uartRegAddr			0x40600000		// read <= RX, write => TX


/************************** Function Prototypes ****************************/

void myISR(void);
void BRAM();

/************************** Variable Definitions **************************/

int incomingLBus[1024];
int incomingRBus[1024];
u16 volt;
u16 time;
int trigEvent = 0;
int arrayIndex = 20;

//status variables
int edge = 0;			//0 = rising-edge	|	1 = falling-edge
int triggerCh = 1;		//1 = ch 1			|	2 = ch 2
int right = 1;			//1 = enabled		|   0 = disabled
int left = 1;			//1 = enabled		|   0 = disabled



int main(void) {

	unsigned char c;
	volt = 220;
	time = 320;
	int line = 0;
	int data = 0;

	init_platform();

	print("Welcome to Lab 3 - Bethany Krull\n\r");

	microblaze_register_handler((XInterruptHandler) myISR, (void *) 0);
	microblaze_enable_interrupts();

	Xil_Out8(oscopeFlag, 0x01);					// Clear the flag and then you MUST
	Xil_Out8(oscopeFlag, 0x00);					// allow the flag to be reset later

	//set initials
	Xil_Out8(oscopeExSel,1);
	Xil_Out16(oscopeTrigVoltIn,volt);
	Xil_Out16(oscopeTrigTimeIn,time);
	Xil_Out8(oscopeExWen,0);
	Xil_Out8(oscopeCh1Enable,1);
	Xil_Out8(oscopeCh2Enable,1);

    while(1) {
    	if(!XUartLite_IsReceiveEmpty(uartRegAddr)){
    		c=XUartLite_RecvByte(uartRegAddr);
    	} else {
    		c = 0;
    	}

		switch(c) {

    		// display MENU
    		case '?':
    			printf("--------------------------\r\n");
    			printf("	LAB 3 MENU\r\n");
    			printf("--------------------------\r\n");
    			printf("?: help menu\r\n");
    			printf("o: k\r\n");
    			printf("w: UP\r\n");
    			printf("a: LEFT\r\n");
    			printf("s: DOWN\r\n");
    			printf("d: RIGHT\r\n");
    			printf("i: interrupts OFF\r\n");
    			printf("I: interrupts ON\r\n");
    			printf("e: rising edge\r\n");
    			printf("E: falling-edge\r\n");
    			printf("l: disable left channel / channel 1\r\n");
    			printf("L: enable left channel / channel 1\r\n");
    			printf("r: disable right channel / channel 2\r\n");
    			printf("R: enable right channel / channel 2\r\n");
    			printf("t: trigger off of left channel / channel 1\r\n");
    			printf("T: trigger off of right channel / channel 2\r\n");
    			printf("!: STATUS\r\n");
    			printf("c: clear terminal\r\n");
    			break;

    		case 'o':
    			printf("k \r\n");
    			break;

			// trigger volt - up
        	case 'w':
        		if(volt-10 > 20){
        			volt = volt-10;
					Xil_Out16(oscopeTrigVoltIn,volt);
					BRAM();
					printf("^ \r\n");
        		}
        		break;

			// trigger time - left
			case 'a':
				if(time-10 > 20){
					time = time-10;
					Xil_Out16(oscopeTrigTimeIn,time);
					BRAM();
					printf("< \r\n");
				}
				break;

			// trigger volt - down
			case 's':
				if(volt+10 < 420){
					volt = volt+10;
					Xil_Out16(oscopeTrigVoltIn,volt);
					BRAM();
					printf("v \r\n");
				}
				break;

			// trigger time - right
			case 'd':
				if(time + 10 < 620){
					time = time+10;
					Xil_Out16(oscopeTrigTimeIn,time);
					BRAM();
					printf("> \r\n");
				}
				break;

			// disable interrupt mode
			case 'i':
				Xil_Out8(oscopeExSel,0);
				printf("interrupts OFF \r\n");
				break;

		    // enable interrupt mode
			case 'I':
				Xil_Out8(oscopeExSel,1);
				printf("interrupts ON \r\n");
				break;

			// set rising-edge mode
			case 'e':
				edge = 0;
				printf("rising-edge mode \r\n");
				break;

			// set falling-edge mode
			case 'E':
				edge = 1;
				printf("falling-edge mode \r\n");
				break;

			// disable left channel / channel 1
			case 'l':
				Xil_Out8(oscopeCh1Enable,0);
				left = 0;
				printf("disable left channel / channel 1\r\n");
				break;

			// enable left channel / channel 1
			case 'L':
				Xil_Out8(oscopeCh1Enable,1);
				left = 1;
				printf("enable left channel / channel 1\r\n");
				break;

			// disable right channel / channel 2
			case 'r':
				Xil_Out8(oscopeCh2Enable,0);
				right = 0;
				printf("enable right channel / channel 2\r\n");
				break;

			// enable right channel / channel 2
			case 'R':
				Xil_Out8(oscopeCh2Enable,1);
				right = 1;
				printf("enable right channel / channel 2\r\n");
				break;

			// trigger off of ch 1
			case 't':
				triggerCh = 1;
				printf("trigger off of ch 1\r\n");
				break;

			// trigger off of ch 2
			case 'T':
				triggerCh = 2;
				printf("trigger off of ch 2\r\n");
				break;

			// print status of all edge/enable/triggers
			case '!':
				printf("STATUS\r\n");
				if(left == 1){
					printf("Channel 1: ON\r\n");
				}else{
					printf("Channel 1: OFF\r\n");
				}
				if(right == 1){
					printf("Channel 2: ON\r\n");
				}else{
					printf("Channel 2: OFF\r\n");
				}
				printf("Triggers off channel %d\r\n", triggerCh);
				if(edge == 0){
					printf("Triggers on rising-edge\r\n");
				}else{
					printf("Triggers on falling-edge\r\n");
				}
				break;


			// clear terminal
            case 'c':
            	for (c=0; c<40; c++) printf("\r\n");
               	break;

			//unknown character input
    		default:
    			printf("unrecognized character: %c\r\n",c);
    			break;
    	} // end case


    } // end while 1

    cleanup_platform();

    return 0;
} // end main




void myISR(void) {
	//indexes and fills left/right buffer arrays
	if(trigEvent == 0){
		if(arrayIndex == 1024){
			trigEvent = 1;
			arrayIndex = 0;
		} else {
			incomingLBus[arrayIndex] = Xil_In16(oscopeLBusOut);
			incomingRBus[arrayIndex] = Xil_In16(oscopeRBusOut);
			arrayIndex = arrayIndex + 1;
		}
	}

	Xil_Out8(oscopeFlag, 0x01);					// Clear the flag and then you MUST
	Xil_Out8(oscopeFlag, 0x00);					// allow the flag to be reset later
}

void BRAM(){
	microblaze_enable_interrupts();
	trigEvent = 0;
	int target = 0;
	//adjust volt to be comparable with buffer values
	u16 adjTrigVolt = (volt + 292) << 6;
	//waits for ISR to finish filling an array
	while(trigEvent == 0){}
		microblaze_disable_interrupts();
		//sort through buffer to find target position
		for(int i = time; i < 1023; i++){
			//channel 1
			if(triggerCh == 1){
				//rising edge
				if(edge == 0){
					if(incomingLBus[i] > adjTrigVolt && incomingLBus[i+1] < adjTrigVolt){
						target = i - time;
						break;
					}
				}
				//falling edge
				else if(edge == 1){
					if(incomingLBus[i] < adjTrigVolt && incomingLBus[i+1] > adjTrigVolt){
						target = i - time;
						break;
					}
				}
			}
			//channel 2
			else if(triggerCh == 2){
				//rising edge
				if(edge == 0){
					if(incomingRBus[i] > adjTrigVolt && incomingRBus[i+1] < adjTrigVolt){
						target = i - time;
						break;
					}
				}
				//falling edge
				else if(edge == 1){
					if(incomingRBus[i] < adjTrigVolt && incomingRBus[i+1] > adjTrigVolt){
						target = i - time;
						break;
					}
				}
			}
		}
		//stores correctly shifted range of values into BRAM for display
		if(triggerCh == 1){
			printf("%d",triggerCh);
			printf("%d",edge);
			for(int j = 20; j < 620; j++){
				Xil_Out16(oscopeExWrAddr,j);
				Xil_Out16(oscopeExWrAddr2,j);
				Xil_Out16(oscopeExLBus,incomingLBus[target+j]);
				Xil_Out16(oscopeExRBus,incomingRBus[j]);
				Xil_Out8(oscopeExWen,1);
				Xil_Out8(oscopeExWen,0);
			}
		}else if(triggerCh == 2){
			printf("%d",triggerCh);
			printf("%d",edge);
			for(int j = 20; j < 620; j++){
				Xil_Out16(oscopeExWrAddr,j);
				Xil_Out16(oscopeExWrAddr2,j);
				Xil_Out16(oscopeExLBus,incomingLBus[j]);
				Xil_Out16(oscopeExRBus,incomingRBus[target+j]);
				Xil_Out8(oscopeExWen,1);
				Xil_Out8(oscopeExWen,0);
			}
		}
	microblaze_enable_interrupts();
}
