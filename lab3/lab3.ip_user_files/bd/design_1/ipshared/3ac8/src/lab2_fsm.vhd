--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	02/24/22
-- File:	lab2_fsm.vhd
-- HW:		lab 2
-- Crs:		CSCE 436
--
-- Purp:	control unit for lab 2
--
-- Documentation:	Talked with Brandon and Falkinburg - used given 
--                  code.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;
use UNISIM.VComponents.all;
use work.lab2Parts.all;	

entity lab2_fsm is
    port(   clk : in  STD_LOGIC;
            reset_n : in  STD_LOGIC;
            sw: in std_logic_vector(2 downto 0) := (others => '0');
            cw: out std_logic_vector (2 downto 0) := (others => '0'));
end lab2_fsm;

-- SW  
   ------------------------------  
   -- 2: Audio Codec ready signal -> 1: Audio Codec is ready else 0
   -- 1: BRAM 3FF compare output -> 1: BRAMCntr == 0x3FF else 0
   -- 0: trigger
   
   -- CW
   ------------------------------
   -- 2 BRAM wrENB mux
   -- 1-0: BRAM address counter (00=hold, 01=inc, 11="0")

architecture Behavioral of lab2_fsm is
type state_type is (reset, waitTrigger, inc_count, storeBuffer, waitAudio);
signal state : state_type;
begin
    process(clk)
    begin
        if(rising_edge(clk))then
            if(reset_n = '0')then
                state <= reset;
            else
                case state is
                    when reset =>                               -- reset -> waitTrigger
                        state <= waitTrigger;
                    when waitTrigger =>
                        if(sw(0) = '1') then                    -- waitTrigger -> waitAudio when there is a trigger
                            state <= waitAudio;
                        end if;
                    when waitAudio =>
                        if(sw(2) = '0' and sw(1) = '1') then    -- waitAudio -> reset if the BRAM count maxes out before there's an Audio ready signal
                            state <= reset;
                        elsif(sw(2) = '1') then
                            state <= storeBuffer;               -- waitAudio -> storeBuffer if there is an Audio ready signal
                        end if;
                    when storeBuffer =>                         -- storeBuffer -> inc_count
                        state <= inc_count;
                    when inc_count =>
                        if(sw(1) = '1') then                    -- inc_count -> reset if BRAM count maxes out
                            state <= reset;
                        else
                            state <= waitAudio;                 -- inc_count -> waitAudio
                        end if;                    
                end case;
            end if;
        end if;
    end process;
    
    cw <=   "011" when state = reset else               -- no BRAM write, reset BRAM count to 20
            "000" when state = waitTrigger else         -- no BRAM write or count
            "001" when state = inc_count else           -- not BRAM write, increment BRAM counter
            "100" when state = storeBuffer else         -- write to BRAM, no increment
            "000" when state = waitAudio else           -- no BRAM write or count
            "000";

end Behavioral;
