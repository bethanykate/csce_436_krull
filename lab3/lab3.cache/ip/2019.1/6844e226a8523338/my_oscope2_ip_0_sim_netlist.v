// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Mar  8 23:29:26 2022
// Host        : DESKTOP-JGL14SE running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ my_oscope2_ip_0_sim_netlist.v
// Design      : my_oscope2_ip_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a200tsbg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Audio_Codec_Wrapper
   (s00_axi_aresetn_0,
    ready_sig_reg_0,
    s00_axi_aresetn_1,
    D,
    s00_axi_aclk,
    s00_axi_aresetn,
    S,
    \axi_rdata_reg[0] ,
    Q,
    \axi_rdata_reg[0]_0 ,
    \axi_rdata_reg[0]_1 );
  output s00_axi_aresetn_0;
  output ready_sig_reg_0;
  output s00_axi_aresetn_1;
  output [0:0]D;
  input s00_axi_aclk;
  input s00_axi_aresetn;
  input [0:0]S;
  input \axi_rdata_reg[0] ;
  input [4:0]Q;
  input \axi_rdata_reg[0]_0 ;
  input \axi_rdata_reg[0]_1 ;

  wire [0:0]D;
  wire [4:0]Q;
  wire [0:0]S;
  wire [2:0]ac_lrclk_count;
  wire \ac_lrclk_count[0]_i_1_n_0 ;
  wire \ac_lrclk_count[1]_i_1_n_0 ;
  wire \ac_lrclk_count[2]_i_1_n_0 ;
  wire ac_lrclk_sig_prev_reg_n_0;
  wire audio_inout_n_1;
  wire audio_inout_n_2;
  wire audio_inout_n_3;
  wire \axi_rdata[0]_i_4_n_0 ;
  wire \axi_rdata_reg[0] ;
  wire \axi_rdata_reg[0]_0 ;
  wire \axi_rdata_reg[0]_1 ;
  wire ready_sig_reg_0;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire s00_axi_aresetn_0;
  wire s00_axi_aresetn_1;

  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h8A20)) 
    \ac_lrclk_count[0]_i_1 
       (.I0(s00_axi_aresetn),
        .I1(ac_lrclk_sig_prev_reg_n_0),
        .I2(audio_inout_n_1),
        .I3(ac_lrclk_count[0]),
        .O(\ac_lrclk_count[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h8A20AA00)) 
    \ac_lrclk_count[1]_i_1 
       (.I0(s00_axi_aresetn),
        .I1(ac_lrclk_sig_prev_reg_n_0),
        .I2(audio_inout_n_1),
        .I3(ac_lrclk_count[1]),
        .I4(ac_lrclk_count[0]),
        .O(\ac_lrclk_count[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8AAAAAAA20000000)) 
    \ac_lrclk_count[2]_i_1 
       (.I0(s00_axi_aresetn),
        .I1(ac_lrclk_sig_prev_reg_n_0),
        .I2(audio_inout_n_1),
        .I3(ac_lrclk_count[1]),
        .I4(ac_lrclk_count[0]),
        .I5(ac_lrclk_count[2]),
        .O(\ac_lrclk_count[2]_i_1_n_0 ));
  FDRE \ac_lrclk_count_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ac_lrclk_count[0]_i_1_n_0 ),
        .Q(ac_lrclk_count[0]),
        .R(1'b0));
  FDRE \ac_lrclk_count_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ac_lrclk_count[1]_i_1_n_0 ),
        .Q(ac_lrclk_count[1]),
        .R(1'b0));
  FDRE \ac_lrclk_count_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ac_lrclk_count[2]_i_1_n_0 ),
        .Q(ac_lrclk_count[2]),
        .R(1'b0));
  FDRE ac_lrclk_sig_prev_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(audio_inout_n_2),
        .Q(ac_lrclk_sig_prev_reg_n_0),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_i2s_ctl audio_inout
       (.LRCLK_reg_0(audio_inout_n_1),
        .SR(s00_axi_aresetn_0),
        .ac_lrclk_count(ac_lrclk_count),
        .ac_lrclk_sig_prev_reg(ac_lrclk_sig_prev_reg_n_0),
        .ready_sig_reg(audio_inout_n_3),
        .ready_sig_reg_0(ready_sig_reg_0),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_aresetn_0(audio_inout_n_2));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_rdata[0]_i_1 
       (.I0(\axi_rdata_reg[0] ),
        .I1(Q[3]),
        .I2(\axi_rdata_reg[0]_0 ),
        .I3(Q[4]),
        .I4(\axi_rdata[0]_i_4_n_0 ),
        .O(D));
  LUT6 #(
    .INIT(64'hAAAA800000008000)) 
    \axi_rdata[0]_i_4 
       (.I0(Q[3]),
        .I1(Q[0]),
        .I2(ready_sig_reg_0),
        .I3(Q[1]),
        .I4(Q[2]),
        .I5(\axi_rdata_reg[0]_1 ),
        .O(\axi_rdata[0]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'hF8)) 
    \currUnsignedLBus[17]_i_1 
       (.I0(s00_axi_aresetn),
        .I1(ready_sig_reg_0),
        .I2(S),
        .O(s00_axi_aresetn_1));
  FDRE ready_sig_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(audio_inout_n_3),
        .Q(ready_sig_reg_0),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_BRAMCounter
   (\tempCnt_reg[9]_0 ,
    Q,
    \tempCnt_reg[8]_0 ,
    \tempCnt_reg[9]_1 ,
    sw,
    \tempCnt_reg[4]_0 ,
    E,
    s00_axi_aclk);
  output \tempCnt_reg[9]_0 ;
  output [9:0]Q;
  output \tempCnt_reg[8]_0 ;
  output [0:0]\tempCnt_reg[9]_1 ;
  input [0:0]sw;
  input [0:0]\tempCnt_reg[4]_0 ;
  input [0:0]E;
  input s00_axi_aclk;

  wire [0:0]E;
  wire [9:0]Q;
  wire [9:0]plusOp__1;
  wire s00_axi_aclk;
  wire [0:0]sw;
  wire \tempCnt[9]_i_5_n_0 ;
  wire [0:0]\tempCnt_reg[4]_0 ;
  wire \tempCnt_reg[8]_0 ;
  wire \tempCnt_reg[9]_0 ;
  wire [0:0]\tempCnt_reg[9]_1 ;

  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \FSM_sequential_state[0]_i_2 
       (.I0(Q[8]),
        .I1(Q[6]),
        .I2(\tempCnt[9]_i_5_n_0 ),
        .I3(Q[7]),
        .I4(Q[9]),
        .I5(sw),
        .O(\tempCnt_reg[8]_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF80000000)) 
    \FSM_sequential_state[2]_i_5 
       (.I0(Q[9]),
        .I1(Q[7]),
        .I2(\tempCnt[9]_i_5_n_0 ),
        .I3(Q[6]),
        .I4(Q[8]),
        .I5(sw),
        .O(\tempCnt_reg[9]_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \tempCnt[0]_i_1__1 
       (.I0(Q[0]),
        .O(plusOp__1[0]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \tempCnt[1]_i_1__1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .O(plusOp__1[1]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \tempCnt[2]_i_1 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(Q[1]),
        .O(plusOp__1[2]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \tempCnt[3]_i_1 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[2]),
        .O(plusOp__1[3]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \tempCnt[4]_i_1 
       (.I0(Q[4]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[3]),
        .O(plusOp__1[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \tempCnt[5]_i_1 
       (.I0(Q[5]),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(Q[2]),
        .I5(Q[4]),
        .O(plusOp__1[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \tempCnt[6]_i_1 
       (.I0(Q[6]),
        .I1(\tempCnt[9]_i_5_n_0 ),
        .O(plusOp__1[6]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \tempCnt[7]_i_1 
       (.I0(Q[7]),
        .I1(\tempCnt[9]_i_5_n_0 ),
        .I2(Q[6]),
        .O(plusOp__1[7]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \tempCnt[8]_i_1 
       (.I0(Q[8]),
        .I1(Q[6]),
        .I2(\tempCnt[9]_i_5_n_0 ),
        .I3(Q[7]),
        .O(plusOp__1[8]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \tempCnt[9]_i_3 
       (.I0(Q[9]),
        .I1(Q[7]),
        .I2(\tempCnt[9]_i_5_n_0 ),
        .I3(Q[6]),
        .I4(Q[8]),
        .O(plusOp__1[9]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \tempCnt[9]_i_4 
       (.I0(Q[9]),
        .I1(Q[7]),
        .I2(\tempCnt[9]_i_5_n_0 ),
        .I3(Q[6]),
        .I4(Q[8]),
        .O(\tempCnt_reg[9]_1 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \tempCnt[9]_i_5 
       (.I0(Q[5]),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(Q[2]),
        .I5(Q[4]),
        .O(\tempCnt[9]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[0] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(plusOp__1[0]),
        .Q(Q[0]),
        .R(\tempCnt_reg[4]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[1] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(plusOp__1[1]),
        .Q(Q[1]),
        .R(\tempCnt_reg[4]_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \tempCnt_reg[2] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(plusOp__1[2]),
        .Q(Q[2]),
        .S(\tempCnt_reg[4]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[3] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(plusOp__1[3]),
        .Q(Q[3]),
        .R(\tempCnt_reg[4]_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \tempCnt_reg[4] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(plusOp__1[4]),
        .Q(Q[4]),
        .S(\tempCnt_reg[4]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[5] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(plusOp__1[5]),
        .Q(Q[5]),
        .R(\tempCnt_reg[4]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[6] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(plusOp__1[6]),
        .Q(Q[6]),
        .R(\tempCnt_reg[4]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[7] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(plusOp__1[7]),
        .Q(Q[7]),
        .R(\tempCnt_reg[4]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[8] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(plusOp__1[8]),
        .Q(Q[8]),
        .R(\tempCnt_reg[4]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[9] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(plusOp__1[9]),
        .Q(Q[9]),
        .R(\tempCnt_reg[4]_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder
   (D,
    Q,
    \dc_bias_reg[3]_0 ,
    \dc_bias_reg[1]_0 ,
    \dc_bias_reg[1]_1 ,
    \dc_bias_reg[1]_2 ,
    \encoded_reg[7]_0 ,
    clk_out1,
    \encoded_reg[5]_0 ,
    \encoded_reg[3]_0 ,
    \encoded_reg[1]_0 ,
    \encoded_reg[0]_0 ,
    \encoded_reg[9]_0 ,
    \encoded_reg[8]_0 ,
    \dc_bias_reg[0]_0 ,
    \dc_bias_reg[0]_1 ,
    \dc_bias_reg[3]_1 ,
    \dc_bias_reg[2]_0 ,
    \dc_bias_reg[3]_2 ,
    \dc_bias_reg[1]_3 ,
    \encoded_reg[6]_0 ,
    \encoded_reg[6]_1 ,
    \encoded_reg[6]_2 ,
    \dc_bias_reg[3]_3 );
  output [9:0]D;
  output [3:0]Q;
  output \dc_bias_reg[3]_0 ;
  output \dc_bias_reg[1]_0 ;
  output \dc_bias_reg[1]_1 ;
  output \dc_bias_reg[1]_2 ;
  input \encoded_reg[7]_0 ;
  input clk_out1;
  input \encoded_reg[5]_0 ;
  input \encoded_reg[3]_0 ;
  input \encoded_reg[1]_0 ;
  input \encoded_reg[0]_0 ;
  input \encoded_reg[9]_0 ;
  input \encoded_reg[8]_0 ;
  input \dc_bias_reg[0]_0 ;
  input \dc_bias_reg[0]_1 ;
  input \dc_bias_reg[3]_1 ;
  input \dc_bias_reg[2]_0 ;
  input \dc_bias_reg[3]_2 ;
  input \dc_bias_reg[1]_3 ;
  input \encoded_reg[6]_0 ;
  input \encoded_reg[6]_1 ;
  input \encoded_reg[6]_2 ;
  input [0:0]\dc_bias_reg[3]_3 ;

  wire [9:0]D;
  wire [3:0]Q;
  wire clk_out1;
  wire \dc_bias[0]_i_1_n_0 ;
  wire \dc_bias[1]_i_1_n_0 ;
  wire \dc_bias[1]_i_2_n_0 ;
  wire \dc_bias[1]_i_4_n_0 ;
  wire \dc_bias[2]_i_1_n_0 ;
  wire \dc_bias[2]_i_3_n_0 ;
  wire \dc_bias[2]_i_4_n_0 ;
  wire \dc_bias[2]_i_5_n_0 ;
  wire \dc_bias[3]_i_1_n_0 ;
  wire \dc_bias[3]_i_2_n_0 ;
  wire \dc_bias[3]_i_3__0_n_0 ;
  wire \dc_bias_reg[0]_0 ;
  wire \dc_bias_reg[0]_1 ;
  wire \dc_bias_reg[1]_0 ;
  wire \dc_bias_reg[1]_1 ;
  wire \dc_bias_reg[1]_2 ;
  wire \dc_bias_reg[1]_3 ;
  wire \dc_bias_reg[2]_0 ;
  wire \dc_bias_reg[3]_0 ;
  wire \dc_bias_reg[3]_1 ;
  wire \dc_bias_reg[3]_2 ;
  wire [0:0]\dc_bias_reg[3]_3 ;
  wire \encoded[2]_i_1__0_n_0 ;
  wire \encoded[4]_i_1_n_0 ;
  wire \encoded[6]_i_1_n_0 ;
  wire \encoded_reg[0]_0 ;
  wire \encoded_reg[1]_0 ;
  wire \encoded_reg[3]_0 ;
  wire \encoded_reg[5]_0 ;
  wire \encoded_reg[6]_0 ;
  wire \encoded_reg[6]_1 ;
  wire \encoded_reg[6]_2 ;
  wire \encoded_reg[7]_0 ;
  wire \encoded_reg[8]_0 ;
  wire \encoded_reg[9]_0 ;

  LUT6 #(
    .INIT(64'hF00F0EF00FF0F00E)) 
    \dc_bias[0]_i_1 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\dc_bias_reg[0]_1 ),
        .I4(Q[3]),
        .I5(\dc_bias_reg[0]_0 ),
        .O(\dc_bias[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFE21DD1D1)) 
    \dc_bias[1]_i_1 
       (.I0(\dc_bias[1]_i_2_n_0 ),
        .I1(\dc_bias_reg[1]_3 ),
        .I2(\dc_bias[1]_i_4_n_0 ),
        .I3(\dc_bias_reg[0]_1 ),
        .I4(Q[0]),
        .I5(\dc_bias_reg[1]_0 ),
        .O(\dc_bias[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \dc_bias[1]_i_2 
       (.I0(Q[1]),
        .I1(\encoded_reg[6]_0 ),
        .O(\dc_bias[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \dc_bias[1]_i_4 
       (.I0(Q[1]),
        .I1(\dc_bias_reg[0]_0 ),
        .O(\dc_bias[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h4155001400144155)) 
    \dc_bias[2]_i_1 
       (.I0(\dc_bias_reg[1]_0 ),
        .I1(\dc_bias_reg[0]_0 ),
        .I2(Q[3]),
        .I3(\dc_bias[2]_i_3_n_0 ),
        .I4(\dc_bias[2]_i_4_n_0 ),
        .I5(\dc_bias[2]_i_5_n_0 ),
        .O(\dc_bias[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT5 #(
    .INIT(32'h7FF88007)) 
    \dc_bias[2]_i_3 
       (.I0(Q[0]),
        .I1(\dc_bias_reg[0]_1 ),
        .I2(\encoded_reg[6]_0 ),
        .I3(Q[1]),
        .I4(Q[2]),
        .O(\dc_bias[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h5555BA55AAAA45AA)) 
    \dc_bias[2]_i_4 
       (.I0(Q[1]),
        .I1(\dc_bias_reg[0]_1 ),
        .I2(Q[0]),
        .I3(\dc_bias_reg[2]_0 ),
        .I4(\dc_bias_reg[3]_2 ),
        .I5(Q[2]),
        .O(\dc_bias[2]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT4 #(
    .INIT(16'hAA02)) 
    \dc_bias[2]_i_5 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(\dc_bias_reg[0]_0 ),
        .I3(\dc_bias_reg[0]_1 ),
        .O(\dc_bias[2]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h90FF90FF90FF9090)) 
    \dc_bias[3]_i_1 
       (.I0(\dc_bias[3]_i_2_n_0 ),
        .I1(\dc_bias[3]_i_3__0_n_0 ),
        .I2(\dc_bias_reg[3]_0 ),
        .I3(\dc_bias_reg[1]_0 ),
        .I4(\dc_bias_reg[1]_1 ),
        .I5(\dc_bias_reg[3]_1 ),
        .O(\dc_bias[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h11210020AA6AAA5A)) 
    \dc_bias[3]_i_2 
       (.I0(Q[2]),
        .I1(\dc_bias_reg[0]_1 ),
        .I2(\dc_bias_reg[2]_0 ),
        .I3(\dc_bias_reg[3]_2 ),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\dc_bias[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h566A566A565A566A)) 
    \dc_bias[3]_i_3__0 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(\dc_bias_reg[0]_0 ),
        .I3(Q[1]),
        .I4(Q[0]),
        .I5(\dc_bias_reg[0]_1 ),
        .O(\dc_bias[3]_i_3__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT5 #(
    .INIT(32'h99999998)) 
    \dc_bias[3]_i_4__0 
       (.I0(\dc_bias_reg[0]_0 ),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(Q[1]),
        .O(\dc_bias_reg[3]_0 ));
  LUT6 #(
    .INIT(64'h0000000100000000)) 
    \dc_bias[3]_i_5__0 
       (.I0(\encoded_reg[6]_0 ),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(Q[3]),
        .I5(\encoded_reg[6]_1 ),
        .O(\dc_bias_reg[1]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \dc_bias[3]_i_6__0 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[3]),
        .O(\dc_bias_reg[1]_1 ));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[0] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\dc_bias[0]_i_1_n_0 ),
        .Q(Q[0]),
        .R(\dc_bias_reg[3]_3 ));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[1] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\dc_bias[1]_i_1_n_0 ),
        .Q(Q[1]),
        .R(\dc_bias_reg[3]_3 ));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[2] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\dc_bias[2]_i_1_n_0 ),
        .Q(Q[2]),
        .R(\dc_bias_reg[3]_3 ));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[3] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\dc_bias[3]_i_1_n_0 ),
        .Q(Q[3]),
        .R(\dc_bias_reg[3]_3 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT4 #(
    .INIT(16'hFFEA)) 
    \encoded[2]_i_1__0 
       (.I0(\dc_bias_reg[1]_0 ),
        .I1(\encoded_reg[6]_1 ),
        .I2(Q[3]),
        .I3(\encoded_reg[6]_2 ),
        .O(\encoded[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT4 #(
    .INIT(16'hFFEA)) 
    \encoded[4]_i_1 
       (.I0(\dc_bias_reg[1]_0 ),
        .I1(\encoded_reg[6]_1 ),
        .I2(Q[3]),
        .I3(\encoded_reg[6]_2 ),
        .O(\encoded[4]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFFEA)) 
    \encoded[6]_i_1 
       (.I0(\dc_bias_reg[1]_0 ),
        .I1(\encoded_reg[6]_1 ),
        .I2(Q[3]),
        .I3(\encoded_reg[6]_2 ),
        .O(\encoded[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF0000000000FE00)) 
    \encoded[7]_i_3 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(\encoded_reg[6]_1 ),
        .I4(\encoded_reg[6]_0 ),
        .I5(Q[3]),
        .O(\dc_bias_reg[1]_2 ));
  FDRE \encoded_reg[0] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded_reg[0]_0 ),
        .Q(D[0]),
        .R(1'b0));
  FDRE \encoded_reg[1] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded_reg[1]_0 ),
        .Q(D[1]),
        .R(1'b0));
  FDRE \encoded_reg[2] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded[2]_i_1__0_n_0 ),
        .Q(D[2]),
        .R(1'b0));
  FDRE \encoded_reg[3] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded_reg[3]_0 ),
        .Q(D[3]),
        .R(1'b0));
  FDRE \encoded_reg[4] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded[4]_i_1_n_0 ),
        .Q(D[4]),
        .R(1'b0));
  FDRE \encoded_reg[5] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded_reg[5]_0 ),
        .Q(D[5]),
        .R(1'b0));
  FDRE \encoded_reg[6] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded[6]_i_1_n_0 ),
        .Q(D[6]),
        .R(1'b0));
  FDRE \encoded_reg[7] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded_reg[7]_0 ),
        .Q(D[7]),
        .R(1'b0));
  FDRE \encoded_reg[8] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded_reg[8]_0 ),
        .Q(D[8]),
        .R(1'b0));
  FDRE \encoded_reg[9] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded_reg[9]_0 ),
        .Q(D[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "TDMS_encoder" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_1
   (D,
    \dc_bias_reg[3]_0 ,
    Q,
    \dc_bias_reg[1]_0 ,
    \dc_bias_reg[0]_0 ,
    \encoded_reg[3]_0 ,
    clk_out1,
    \encoded_reg[7]_0 ,
    \encoded_reg[9]_0 ,
    \encoded_reg[8]_0 ,
    \dc_bias_reg[3]_1 ,
    \encoded_reg[2]_0 ,
    \dc_bias_reg[1]_1 ,
    \encoded_reg[7]_1 ,
    \dc_bias[3]_i_9 ,
    \dc_bias[3]_i_9_0 ,
    \dc_bias[3]_i_9_1 ,
    CO,
    \dc_bias_reg[1]_2 ,
    \dc_bias_reg[2]_0 ,
    \encoded_reg[2]_1 ,
    \dc_bias_reg[3]_2 );
  output [9:0]D;
  output \dc_bias_reg[3]_0 ;
  output [3:0]Q;
  output \dc_bias_reg[1]_0 ;
  output \dc_bias_reg[0]_0 ;
  input \encoded_reg[3]_0 ;
  input clk_out1;
  input [0:0]\encoded_reg[7]_0 ;
  input \encoded_reg[9]_0 ;
  input \encoded_reg[8]_0 ;
  input \dc_bias_reg[3]_1 ;
  input \encoded_reg[2]_0 ;
  input \dc_bias_reg[1]_1 ;
  input \encoded_reg[7]_1 ;
  input [0:0]\dc_bias[3]_i_9 ;
  input \dc_bias[3]_i_9_0 ;
  input \dc_bias[3]_i_9_1 ;
  input [0:0]CO;
  input \dc_bias_reg[1]_2 ;
  input \dc_bias_reg[2]_0 ;
  input \encoded_reg[2]_1 ;
  input [0:0]\dc_bias_reg[3]_2 ;

  wire [0:0]CO;
  wire [9:0]D;
  wire [3:0]Q;
  wire clk_out1;
  wire \dc_bias[0]_i_1__0_n_0 ;
  wire \dc_bias[1]_i_1__1_n_0 ;
  wire \dc_bias[2]_i_2__0_n_0 ;
  wire [0:0]\dc_bias[3]_i_9 ;
  wire \dc_bias[3]_i_9_0 ;
  wire \dc_bias[3]_i_9_1 ;
  wire \dc_bias_reg[0]_0 ;
  wire \dc_bias_reg[1]_0 ;
  wire \dc_bias_reg[1]_1 ;
  wire \dc_bias_reg[1]_2 ;
  wire \dc_bias_reg[2]_0 ;
  wire \dc_bias_reg[2]_i_1_n_0 ;
  wire \dc_bias_reg[3]_0 ;
  wire \dc_bias_reg[3]_1 ;
  wire [0:0]\dc_bias_reg[3]_2 ;
  wire \encoded[1]_i_1_n_0 ;
  wire \encoded[2]_i_1_n_0 ;
  wire \encoded[7]_i_1__0_n_0 ;
  wire \encoded_reg[2]_0 ;
  wire \encoded_reg[2]_1 ;
  wire \encoded_reg[3]_0 ;
  wire [0:0]\encoded_reg[7]_0 ;
  wire \encoded_reg[7]_1 ;
  wire \encoded_reg[8]_0 ;
  wire \encoded_reg[9]_0 ;

  LUT5 #(
    .INIT(32'h96FF6900)) 
    \dc_bias[0]_i_1__0 
       (.I0(\encoded_reg[2]_0 ),
        .I1(Q[3]),
        .I2(\encoded_reg[2]_1 ),
        .I3(\dc_bias_reg[0]_0 ),
        .I4(Q[0]),
        .O(\dc_bias[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT4 #(
    .INIT(16'hF066)) 
    \dc_bias[1]_i_1__1 
       (.I0(Q[1]),
        .I1(\dc_bias_reg[1]_1 ),
        .I2(\dc_bias_reg[1]_2 ),
        .I3(\dc_bias_reg[0]_0 ),
        .O(\dc_bias[1]_i_1__1_n_0 ));
  LUT4 #(
    .INIT(16'h6696)) 
    \dc_bias[2]_i_2__0 
       (.I0(Q[2]),
        .I1(\encoded_reg[7]_1 ),
        .I2(\dc_bias_reg[1]_1 ),
        .I3(Q[1]),
        .O(\dc_bias[2]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'h1111555511115551)) 
    \dc_bias[3]_i_17__0 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(\dc_bias[3]_i_9 ),
        .I3(\dc_bias[3]_i_9_0 ),
        .I4(\dc_bias[3]_i_9_1 ),
        .I5(CO),
        .O(\dc_bias_reg[1]_0 ));
  LUT5 #(
    .INIT(32'hAAAAAAA8)) 
    \dc_bias[3]_i_3 
       (.I0(\encoded_reg[7]_1 ),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[1]),
        .I4(Q[3]),
        .O(\dc_bias_reg[0]_0 ));
  LUT6 #(
    .INIT(64'h6655555566555655)) 
    \dc_bias[3]_i_6__1 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(\dc_bias_reg[3]_1 ),
        .I4(\encoded_reg[2]_0 ),
        .I5(Q[1]),
        .O(\dc_bias_reg[3]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[0] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\dc_bias[0]_i_1__0_n_0 ),
        .Q(Q[0]),
        .R(\encoded_reg[7]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[1] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\dc_bias[1]_i_1__1_n_0 ),
        .Q(Q[1]),
        .R(\encoded_reg[7]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[2] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\dc_bias_reg[2]_i_1_n_0 ),
        .Q(Q[2]),
        .R(\encoded_reg[7]_0 ));
  MUXF7 \dc_bias_reg[2]_i_1 
       (.I0(\dc_bias[2]_i_2__0_n_0 ),
        .I1(\dc_bias_reg[2]_0 ),
        .O(\dc_bias_reg[2]_i_1_n_0 ),
        .S(\dc_bias_reg[0]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[3] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\dc_bias_reg[3]_2 ),
        .Q(Q[3]),
        .R(\encoded_reg[7]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \encoded[1]_i_1 
       (.I0(\dc_bias_reg[1]_1 ),
        .I1(\dc_bias_reg[0]_0 ),
        .I2(Q[3]),
        .O(\encoded[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2882)) 
    \encoded[2]_i_1 
       (.I0(\dc_bias_reg[0]_0 ),
        .I1(Q[3]),
        .I2(\encoded_reg[2]_0 ),
        .I3(\encoded_reg[2]_1 ),
        .O(\encoded[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCCCCCCCEFFFFFFFF)) 
    \encoded[7]_i_1__0 
       (.I0(\dc_bias_reg[1]_1 ),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(Q[0]),
        .I5(\encoded_reg[7]_1 ),
        .O(\encoded[7]_i_1__0_n_0 ));
  FDRE \encoded_reg[0] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded[1]_i_1_n_0 ),
        .Q(D[0]),
        .R(\encoded_reg[7]_0 ));
  FDRE \encoded_reg[1] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded[1]_i_1_n_0 ),
        .Q(D[1]),
        .R(\encoded_reg[7]_0 ));
  FDSE \encoded_reg[2] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded[2]_i_1_n_0 ),
        .Q(D[2]),
        .S(\encoded_reg[7]_0 ));
  FDRE \encoded_reg[3] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded_reg[3]_0 ),
        .Q(D[3]),
        .R(1'b0));
  FDSE \encoded_reg[4] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded[7]_i_1__0_n_0 ),
        .Q(D[4]),
        .S(\encoded_reg[7]_0 ));
  FDRE \encoded_reg[5] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded[7]_i_1__0_n_0 ),
        .Q(D[5]),
        .R(\encoded_reg[7]_0 ));
  FDSE \encoded_reg[6] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded[1]_i_1_n_0 ),
        .Q(D[6]),
        .S(\encoded_reg[7]_0 ));
  FDRE \encoded_reg[7] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded[7]_i_1__0_n_0 ),
        .Q(D[7]),
        .R(\encoded_reg[7]_0 ));
  FDRE \encoded_reg[8] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded_reg[8]_0 ),
        .Q(D[8]),
        .R(1'b0));
  FDRE \encoded_reg[9] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded_reg[9]_0 ),
        .Q(D[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "TDMS_encoder" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_2
   (D,
    Q,
    clk_out1,
    \encoded_reg[5]_0 ,
    \encoded_reg[4]_0 ,
    \encoded_reg[0]_0 ,
    \encoded_reg[9]_0 ,
    \encoded_reg[8]_0 ,
    \dc_bias_reg[1]_0 ,
    \dc_bias_reg[2]_0 ,
    \dc_bias_reg[1]_1 );
  output [9:0]D;
  output [0:0]Q;
  input clk_out1;
  input [0:0]\encoded_reg[5]_0 ;
  input \encoded_reg[4]_0 ;
  input \encoded_reg[0]_0 ;
  input \encoded_reg[9]_0 ;
  input \encoded_reg[8]_0 ;
  input \dc_bias_reg[1]_0 ;
  input \dc_bias_reg[2]_0 ;
  input \dc_bias_reg[1]_1 ;

  wire [9:0]D;
  wire [0:0]Q;
  wire clk_out1;
  wire \dc_bias_reg[1]_0 ;
  wire \dc_bias_reg[1]_1 ;
  wire \dc_bias_reg[2]_0 ;
  wire \dc_bias_reg_n_0_[0] ;
  wire \dc_bias_reg_n_0_[1] ;
  wire \dc_bias_reg_n_0_[2] ;
  wire [6:2]encoded1_in;
  wire \encoded[7]_i_1_n_0 ;
  wire \encoded_reg[0]_0 ;
  wire \encoded_reg[4]_0 ;
  wire [0:0]\encoded_reg[5]_0 ;
  wire \encoded_reg[8]_0 ;
  wire \encoded_reg[9]_0 ;
  wire [3:0]p_0_in;

  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \dc_bias[0]_i_1__1 
       (.I0(\dc_bias_reg_n_0_[0] ),
        .I1(Q),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT5 #(
    .INIT(32'hB4CC4B33)) 
    \dc_bias[1]_i_1__0 
       (.I0(\dc_bias_reg[1]_0 ),
        .I1(\dc_bias_reg[1]_1 ),
        .I2(\dc_bias_reg_n_0_[0] ),
        .I3(Q),
        .I4(\dc_bias_reg_n_0_[1] ),
        .O(p_0_in[1]));
  LUT6 #(
    .INIT(64'hA587870F1E1E0FA5)) 
    \dc_bias[2]_i_1__0 
       (.I0(Q),
        .I1(\dc_bias_reg[1]_0 ),
        .I2(\dc_bias_reg_n_0_[2] ),
        .I3(\dc_bias_reg_n_0_[0] ),
        .I4(\dc_bias_reg_n_0_[1] ),
        .I5(\dc_bias_reg[2]_0 ),
        .O(p_0_in[2]));
  LUT6 #(
    .INIT(64'h8D0E0F0F8F0E87AF)) 
    \dc_bias[3]_i_2__0 
       (.I0(Q),
        .I1(\dc_bias_reg[1]_0 ),
        .I2(\dc_bias_reg_n_0_[2] ),
        .I3(\dc_bias_reg[2]_0 ),
        .I4(\dc_bias_reg_n_0_[1] ),
        .I5(\dc_bias_reg_n_0_[0] ),
        .O(p_0_in[3]));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[0] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\dc_bias_reg_n_0_[0] ),
        .R(\encoded_reg[5]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[1] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\dc_bias_reg_n_0_[1] ),
        .R(\encoded_reg[5]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[2] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\dc_bias_reg_n_0_[2] ),
        .R(\encoded_reg[5]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \dc_bias_reg[3] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(Q),
        .R(\encoded_reg[5]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \encoded[2]_i_1__1 
       (.I0(Q),
        .I1(\encoded_reg[0]_0 ),
        .O(encoded1_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \encoded[6]_i_1__0 
       (.I0(Q),
        .I1(\encoded_reg[0]_0 ),
        .O(encoded1_in[6]));
  LUT1 #(
    .INIT(2'h1)) 
    \encoded[7]_i_1 
       (.I0(Q),
        .O(\encoded[7]_i_1_n_0 ));
  FDRE \encoded_reg[0] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded_reg[0]_0 ),
        .Q(D[0]),
        .R(\encoded[7]_i_1_n_0 ));
  FDRE \encoded_reg[1] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded_reg[0]_0 ),
        .Q(D[1]),
        .R(\encoded[7]_i_1_n_0 ));
  FDRE \encoded_reg[2] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(encoded1_in[2]),
        .Q(D[2]),
        .R(1'b0));
  FDRE \encoded_reg[3] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded_reg[0]_0 ),
        .Q(D[3]),
        .R(\encoded[7]_i_1_n_0 ));
  FDSE \encoded_reg[4] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded_reg[4]_0 ),
        .Q(D[4]),
        .S(\encoded_reg[5]_0 ));
  FDRE \encoded_reg[5] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded_reg[4]_0 ),
        .Q(D[5]),
        .R(\encoded_reg[5]_0 ));
  FDRE \encoded_reg[6] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(encoded1_in[6]),
        .Q(D[6]),
        .R(1'b0));
  FDRE \encoded_reg[7] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded_reg[0]_0 ),
        .Q(D[7]),
        .R(\encoded[7]_i_1_n_0 ));
  FDRE \encoded_reg[8] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded_reg[8]_0 ),
        .Q(D[8]),
        .R(1'b0));
  FDRE \encoded_reg[9] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\encoded_reg[9]_0 ),
        .Q(D[9]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0
   (clk_in1,
    clk_out1,
    clk_out2,
    clk_out3,
    resetn);
  input clk_in1;
  output clk_out1;
  output clk_out2;
  output clk_out3;
  input resetn;


endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_colCounter
   (rollWire,
    \dc_bias_reg[3] ,
    \tempCnt_reg[8]_0 ,
    \tempCnt_reg[1]_0 ,
    \tempCnt_reg[9]_0 ,
    Q,
    S,
    \tempCnt_reg[9]_1 ,
    \tempCnt_reg[9]_2 ,
    \tempCnt_reg[2]_0 ,
    \tempCnt_reg[9]_3 ,
    \tempCnt_reg[9]_4 ,
    \tempCnt_reg[1]_1 ,
    \tempCnt_reg[9]_5 ,
    \dc_bias_reg[3]_0 ,
    \tempCnt_reg[8]_1 ,
    \tempCnt_reg[6]_0 ,
    \dc_bias_reg[3]_1 ,
    \dc_bias_reg[3]_2 ,
    \tempCnt_reg[8]_2 ,
    \tempCnt_reg[8]_3 ,
    \dc_bias_reg[3]_3 ,
    \tempCnt_reg[1]_2 ,
    \dc_bias_reg[3]_4 ,
    D,
    \tempCnt_reg[6]_1 ,
    \dc_bias_reg[2] ,
    \dc_bias_reg[3]_5 ,
    \dc_bias_reg[0] ,
    \tempCnt_reg[1]_3 ,
    \dc_bias_reg[3]_6 ,
    \dc_bias_reg[3]_7 ,
    \tempCnt_reg[1]_4 ,
    \tempCnt_reg[1]_5 ,
    encoded1_in,
    \tempCnt_reg[6]_2 ,
    E,
    \tempCnt_reg[9]_6 ,
    \tempCnt_reg[6]_3 ,
    \tempCnt_reg[0]_0 ,
    \tempCnt_reg[8]_4 ,
    \dc_bias_reg[3]_8 ,
    \tempCnt_reg[8]_5 ,
    \tempCnt_reg[6]_4 ,
    \tempCnt_reg[9]_7 ,
    clk_out1,
    \encoded_reg[0] ,
    CO,
    \encoded_reg[9] ,
    \encoded_reg[9]_0 ,
    s00_axi_aresetn,
    O,
    \encoded_reg[0]_0 ,
    \encoded_reg[1] ,
    \encoded_reg[4] ,
    \encoded_reg[9]_1 ,
    \encoded_reg[9]_2 ,
    \encoded_reg[8] ,
    \encoded_reg[9]_3 ,
    \dc_bias_reg[3]_9 ,
    \dc_bias_reg[3]_i_2_0 ,
    \dc_bias[3]_i_17__0 ,
    \dc_bias[3]_i_24_0 ,
    \dc_bias[3]_i_12_0 ,
    \dc_bias[3]_i_12_1 ,
    \dc_bias[3]_i_9__0 ,
    \dc_bias[3]_i_9__0_0 ,
    \dc_bias[3]_i_9__0_1 ,
    \dc_bias[3]_i_9__0_2 ,
    \encoded_reg[0]_1 ,
    \tempCnt_reg[9]_8 ,
    \dc_bias[3]_i_9__1_0 ,
    \dc_bias[3]_i_9__1_1 ,
    \dc_bias[3]_i_9__1_2 ,
    \dc_bias[3]_i_9__1_3 ,
    \dc_bias[3]_i_9__1_4 ,
    \r7_inferred__2/i___20_carry__0 ,
    \r7_inferred__2/i___20_carry__0_0 ,
    \dc_bias[3]_i_23 ,
    \dc_bias[3]_i_23_0 );
  output rollWire;
  output \dc_bias_reg[3] ;
  output \tempCnt_reg[8]_0 ;
  output \tempCnt_reg[1]_0 ;
  output \tempCnt_reg[9]_0 ;
  output [9:0]Q;
  output [3:0]S;
  output [3:0]\tempCnt_reg[9]_1 ;
  output [3:0]\tempCnt_reg[9]_2 ;
  output [1:0]\tempCnt_reg[2]_0 ;
  output [3:0]\tempCnt_reg[9]_3 ;
  output \tempCnt_reg[9]_4 ;
  output \tempCnt_reg[1]_1 ;
  output \tempCnt_reg[9]_5 ;
  output \dc_bias_reg[3]_0 ;
  output \tempCnt_reg[8]_1 ;
  output \tempCnt_reg[6]_0 ;
  output \dc_bias_reg[3]_1 ;
  output \dc_bias_reg[3]_2 ;
  output \tempCnt_reg[8]_2 ;
  output \tempCnt_reg[8]_3 ;
  output \dc_bias_reg[3]_3 ;
  output \tempCnt_reg[1]_2 ;
  output \dc_bias_reg[3]_4 ;
  output [0:0]D;
  output \tempCnt_reg[6]_1 ;
  output \dc_bias_reg[2] ;
  output \dc_bias_reg[3]_5 ;
  output \dc_bias_reg[0] ;
  output \tempCnt_reg[1]_3 ;
  output \dc_bias_reg[3]_6 ;
  output \dc_bias_reg[3]_7 ;
  output \tempCnt_reg[1]_4 ;
  output \tempCnt_reg[1]_5 ;
  output [0:0]encoded1_in;
  output \tempCnt_reg[6]_2 ;
  output [0:0]E;
  output [3:0]\tempCnt_reg[9]_6 ;
  output [0:0]\tempCnt_reg[6]_3 ;
  output \tempCnt_reg[0]_0 ;
  output \tempCnt_reg[8]_4 ;
  output \dc_bias_reg[3]_8 ;
  output \tempCnt_reg[8]_5 ;
  output [2:0]\tempCnt_reg[6]_4 ;
  output [2:0]\tempCnt_reg[9]_7 ;
  input clk_out1;
  input [3:0]\encoded_reg[0] ;
  input [0:0]CO;
  input [0:0]\encoded_reg[9] ;
  input [3:0]\encoded_reg[9]_0 ;
  input s00_axi_aresetn;
  input [0:0]O;
  input \encoded_reg[0]_0 ;
  input \encoded_reg[1] ;
  input [0:0]\encoded_reg[4] ;
  input \encoded_reg[9]_1 ;
  input \encoded_reg[9]_2 ;
  input \encoded_reg[8] ;
  input \encoded_reg[9]_3 ;
  input \dc_bias_reg[3]_9 ;
  input \dc_bias_reg[3]_i_2_0 ;
  input \dc_bias[3]_i_17__0 ;
  input \dc_bias[3]_i_24_0 ;
  input \dc_bias[3]_i_12_0 ;
  input \dc_bias[3]_i_12_1 ;
  input [0:0]\dc_bias[3]_i_9__0 ;
  input \dc_bias[3]_i_9__0_0 ;
  input \dc_bias[3]_i_9__0_1 ;
  input [0:0]\dc_bias[3]_i_9__0_2 ;
  input \encoded_reg[0]_1 ;
  input \tempCnt_reg[9]_8 ;
  input [0:0]\dc_bias[3]_i_9__1_0 ;
  input [0:0]\dc_bias[3]_i_9__1_1 ;
  input [0:0]\dc_bias[3]_i_9__1_2 ;
  input [0:0]\dc_bias[3]_i_9__1_3 ;
  input [1:0]\dc_bias[3]_i_9__1_4 ;
  input [2:0]\r7_inferred__2/i___20_carry__0 ;
  input [0:0]\r7_inferred__2/i___20_carry__0_0 ;
  input [2:0]\dc_bias[3]_i_23 ;
  input [0:0]\dc_bias[3]_i_23_0 ;

  wire [0:0]CO;
  wire [0:0]D;
  wire [0:0]E;
  wire [0:0]O;
  wire [9:0]Q;
  wire [3:0]S;
  wire clk_out1;
  wire \dc_bias[2]_i_4__0_n_0 ;
  wire \dc_bias[2]_i_5__0_n_0 ;
  wire \dc_bias[2]_i_6_n_0 ;
  wire \dc_bias[2]_i_7_n_0 ;
  wire \dc_bias[3]_i_10__0_n_0 ;
  wire \dc_bias[3]_i_11_n_0 ;
  wire \dc_bias[3]_i_12_0 ;
  wire \dc_bias[3]_i_12_1 ;
  wire \dc_bias[3]_i_12_n_0 ;
  wire \dc_bias[3]_i_14__0_n_0 ;
  wire \dc_bias[3]_i_14_n_0 ;
  wire \dc_bias[3]_i_15__0_n_0 ;
  wire \dc_bias[3]_i_15_n_0 ;
  wire \dc_bias[3]_i_16__0_n_0 ;
  wire \dc_bias[3]_i_16_n_0 ;
  wire \dc_bias[3]_i_17__0 ;
  wire \dc_bias[3]_i_18_n_0 ;
  wire [2:0]\dc_bias[3]_i_23 ;
  wire [0:0]\dc_bias[3]_i_23_0 ;
  wire \dc_bias[3]_i_24_0 ;
  wire \dc_bias[3]_i_24_n_0 ;
  wire \dc_bias[3]_i_25_n_0 ;
  wire \dc_bias[3]_i_26_n_0 ;
  wire \dc_bias[3]_i_29_n_0 ;
  wire \dc_bias[3]_i_44_n_0 ;
  wire \dc_bias[3]_i_45_n_0 ;
  wire \dc_bias[3]_i_46_n_0 ;
  wire \dc_bias[3]_i_47_n_0 ;
  wire \dc_bias[3]_i_48_n_0 ;
  wire \dc_bias[3]_i_49_n_0 ;
  wire \dc_bias[3]_i_4__1_n_0 ;
  wire \dc_bias[3]_i_50_n_0 ;
  wire \dc_bias[3]_i_51_n_0 ;
  wire \dc_bias[3]_i_52_n_0 ;
  wire \dc_bias[3]_i_5_n_0 ;
  wire \dc_bias[3]_i_60_n_0 ;
  wire \dc_bias[3]_i_61_n_0 ;
  wire \dc_bias[3]_i_62_n_0 ;
  wire \dc_bias[3]_i_63_n_0 ;
  wire \dc_bias[3]_i_64_n_0 ;
  wire \dc_bias[3]_i_65_n_0 ;
  wire \dc_bias[3]_i_66_n_0 ;
  wire \dc_bias[3]_i_67_n_0 ;
  wire \dc_bias[3]_i_68_n_0 ;
  wire \dc_bias[3]_i_6_n_0 ;
  wire \dc_bias[3]_i_70_n_0 ;
  wire \dc_bias[3]_i_71_n_0 ;
  wire \dc_bias[3]_i_74_n_0 ;
  wire \dc_bias[3]_i_75_n_0 ;
  wire \dc_bias[3]_i_76_n_0 ;
  wire \dc_bias[3]_i_79_n_0 ;
  wire \dc_bias[3]_i_7__0_n_0 ;
  wire \dc_bias[3]_i_80_n_0 ;
  wire \dc_bias[3]_i_81_n_0 ;
  wire \dc_bias[3]_i_82_n_0 ;
  wire \dc_bias[3]_i_8_n_0 ;
  wire [0:0]\dc_bias[3]_i_9__0 ;
  wire \dc_bias[3]_i_9__0_0 ;
  wire \dc_bias[3]_i_9__0_1 ;
  wire [0:0]\dc_bias[3]_i_9__0_2 ;
  wire [0:0]\dc_bias[3]_i_9__1_0 ;
  wire [0:0]\dc_bias[3]_i_9__1_1 ;
  wire [0:0]\dc_bias[3]_i_9__1_2 ;
  wire [0:0]\dc_bias[3]_i_9__1_3 ;
  wire [1:0]\dc_bias[3]_i_9__1_4 ;
  wire \dc_bias[3]_i_9_n_0 ;
  wire \dc_bias_reg[0] ;
  wire \dc_bias_reg[2] ;
  wire \dc_bias_reg[3] ;
  wire \dc_bias_reg[3]_0 ;
  wire \dc_bias_reg[3]_1 ;
  wire \dc_bias_reg[3]_2 ;
  wire \dc_bias_reg[3]_3 ;
  wire \dc_bias_reg[3]_4 ;
  wire \dc_bias_reg[3]_5 ;
  wire \dc_bias_reg[3]_6 ;
  wire \dc_bias_reg[3]_7 ;
  wire \dc_bias_reg[3]_8 ;
  wire \dc_bias_reg[3]_9 ;
  wire \dc_bias_reg[3]_i_2_0 ;
  wire \dc_bias_reg[3]_i_2_n_0 ;
  wire [0:0]encoded1_in;
  wire \encoded[7]_i_4_n_0 ;
  wire [3:0]\encoded_reg[0] ;
  wire \encoded_reg[0]_0 ;
  wire \encoded_reg[0]_1 ;
  wire \encoded_reg[1] ;
  wire [0:0]\encoded_reg[4] ;
  wire \encoded_reg[8] ;
  wire [0:0]\encoded_reg[9] ;
  wire [3:0]\encoded_reg[9]_0 ;
  wire \encoded_reg[9]_1 ;
  wire \encoded_reg[9]_2 ;
  wire \encoded_reg[9]_3 ;
  wire [9:0]plusOp;
  wire [2:0]\r7_inferred__2/i___20_carry__0 ;
  wire [0:0]\r7_inferred__2/i___20_carry__0_0 ;
  wire rollOUT_i_1_n_0;
  wire rollOUT_i_2_n_0;
  wire rollOUT_i_3_n_0;
  wire rollWire;
  wire s00_axi_aresetn;
  wire tempCnt02_out;
  wire \tempCnt[9]_i_1__0_n_0 ;
  wire \tempCnt[9]_i_4__0_n_0 ;
  wire \tempCnt[9]_i_5__0_n_0 ;
  wire \tempCnt[9]_i_6_n_0 ;
  wire \tempCnt[9]_i_7_n_0 ;
  wire \tempCnt_reg[0]_0 ;
  wire \tempCnt_reg[1]_0 ;
  wire \tempCnt_reg[1]_1 ;
  wire \tempCnt_reg[1]_2 ;
  wire \tempCnt_reg[1]_3 ;
  wire \tempCnt_reg[1]_4 ;
  wire \tempCnt_reg[1]_5 ;
  wire [1:0]\tempCnt_reg[2]_0 ;
  wire \tempCnt_reg[6]_0 ;
  wire \tempCnt_reg[6]_1 ;
  wire \tempCnt_reg[6]_2 ;
  wire [0:0]\tempCnt_reg[6]_3 ;
  wire [2:0]\tempCnt_reg[6]_4 ;
  wire \tempCnt_reg[8]_0 ;
  wire \tempCnt_reg[8]_1 ;
  wire \tempCnt_reg[8]_2 ;
  wire \tempCnt_reg[8]_3 ;
  wire \tempCnt_reg[8]_4 ;
  wire \tempCnt_reg[8]_5 ;
  wire \tempCnt_reg[9]_0 ;
  wire [3:0]\tempCnt_reg[9]_1 ;
  wire [3:0]\tempCnt_reg[9]_2 ;
  wire [3:0]\tempCnt_reg[9]_3 ;
  wire \tempCnt_reg[9]_4 ;
  wire \tempCnt_reg[9]_5 ;
  wire [3:0]\tempCnt_reg[9]_6 ;
  wire [2:0]\tempCnt_reg[9]_7 ;
  wire \tempCnt_reg[9]_8 ;

  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT5 #(
    .INIT(32'h0C0A0C0E)) 
    \dc_bias[0]_i_2 
       (.I0(CO),
        .I1(\tempCnt_reg[1]_0 ),
        .I2(\tempCnt_reg[8]_0 ),
        .I3(\dc_bias[3]_i_6_n_0 ),
        .I4(\encoded_reg[9] ),
        .O(\tempCnt_reg[1]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT5 #(
    .INIT(32'hFFF7FDF5)) 
    \dc_bias[1]_i_2__0 
       (.I0(\encoded_reg[9] ),
        .I1(\dc_bias[3]_i_6_n_0 ),
        .I2(\tempCnt_reg[8]_0 ),
        .I3(\tempCnt_reg[1]_0 ),
        .I4(CO),
        .O(\tempCnt_reg[9]_4 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \dc_bias[1]_i_2__1 
       (.I0(CO),
        .I1(\dc_bias[3]_i_6_n_0 ),
        .I2(\tempCnt_reg[8]_0 ),
        .O(\tempCnt_reg[9]_0 ));
  LUT6 #(
    .INIT(64'h04F8B34FFB074CB0)) 
    \dc_bias[1]_i_3 
       (.I0(\tempCnt_reg[1]_3 ),
        .I1(\dc_bias[3]_i_7__0_n_0 ),
        .I2(\encoded_reg[9]_0 [0]),
        .I3(\tempCnt_reg[9]_5 ),
        .I4(\tempCnt_reg[9]_0 ),
        .I5(\encoded_reg[9]_0 [1]),
        .O(\dc_bias_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT5 #(
    .INIT(32'hA5A5A5A6)) 
    \dc_bias[1]_i_3__0 
       (.I0(\encoded_reg[0] [3]),
        .I1(CO),
        .I2(\tempCnt_reg[8]_0 ),
        .I3(\tempCnt_reg[1]_0 ),
        .I4(\encoded_reg[9] ),
        .O(\dc_bias_reg[3] ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT4 #(
    .INIT(16'hF0F1)) 
    \dc_bias[2]_i_2 
       (.I0(\encoded_reg[9] ),
        .I1(\tempCnt_reg[1]_0 ),
        .I2(\tempCnt_reg[8]_0 ),
        .I3(CO),
        .O(\tempCnt_reg[1]_3 ));
  LUT4 #(
    .INIT(16'hEB28)) 
    \dc_bias[2]_i_3__0 
       (.I0(\dc_bias[2]_i_4__0_n_0 ),
        .I1(\encoded_reg[9]_0 [3]),
        .I2(\tempCnt_reg[1]_1 ),
        .I3(\dc_bias[2]_i_5__0_n_0 ),
        .O(\dc_bias_reg[3]_5 ));
  LUT6 #(
    .INIT(64'hD4422BBD2BBDD442)) 
    \dc_bias[2]_i_4__0 
       (.I0(\dc_bias[2]_i_6_n_0 ),
        .I1(\tempCnt_reg[9]_5 ),
        .I2(\dc_bias[2]_i_7_n_0 ),
        .I3(\encoded_reg[9]_0 [1]),
        .I4(\tempCnt_reg[1]_1 ),
        .I5(\encoded_reg[9]_0 [2]),
        .O(\dc_bias[2]_i_4__0_n_0 ));
  LUT6 #(
    .INIT(64'h0FF04BB4B44B0FF0)) 
    \dc_bias[2]_i_5__0 
       (.I0(\tempCnt_reg[9]_5 ),
        .I1(\encoded_reg[9]_0 [0]),
        .I2(\encoded_reg[9]_0 [2]),
        .I3(\tempCnt_reg[9]_4 ),
        .I4(\tempCnt_reg[9]_0 ),
        .I5(\encoded_reg[9]_0 [1]),
        .O(\dc_bias[2]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA0200AA00A8)) 
    \dc_bias[2]_i_6 
       (.I0(\tempCnt_reg[9]_0 ),
        .I1(\encoded_reg[9] ),
        .I2(\tempCnt_reg[1]_0 ),
        .I3(\tempCnt_reg[8]_0 ),
        .I4(CO),
        .I5(\encoded_reg[9]_0 [0]),
        .O(\dc_bias[2]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT5 #(
    .INIT(32'hCCCD0000)) 
    \dc_bias[2]_i_7 
       (.I0(CO),
        .I1(\tempCnt_reg[8]_0 ),
        .I2(\tempCnt_reg[1]_0 ),
        .I3(\encoded_reg[9] ),
        .I4(\encoded_reg[9]_0 [0]),
        .O(\dc_bias[2]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF202020F5)) 
    \dc_bias[3]_i_10__0 
       (.I0(CO),
        .I1(\dc_bias[3]_i_6_n_0 ),
        .I2(\encoded_reg[9]_0 [1]),
        .I3(\encoded_reg[9] ),
        .I4(\tempCnt_reg[1]_0 ),
        .I5(\tempCnt_reg[8]_0 ),
        .O(\dc_bias[3]_i_10__0_n_0 ));
  LUT6 #(
    .INIT(64'h0F0F0F0A0F070D00)) 
    \dc_bias[3]_i_11 
       (.I0(\encoded_reg[9] ),
        .I1(\dc_bias[3]_i_6_n_0 ),
        .I2(\tempCnt_reg[8]_0 ),
        .I3(\tempCnt_reg[1]_0 ),
        .I4(CO),
        .I5(\encoded_reg[9]_0 [0]),
        .O(\dc_bias[3]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h0C0A0C0E0F0F0E0E)) 
    \dc_bias[3]_i_12 
       (.I0(CO),
        .I1(\tempCnt_reg[1]_0 ),
        .I2(\tempCnt_reg[8]_0 ),
        .I3(\dc_bias[3]_i_6_n_0 ),
        .I4(\encoded_reg[9] ),
        .I5(\encoded_reg[9]_0 [0]),
        .O(\dc_bias[3]_i_12_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT4 #(
    .INIT(16'h00FE)) 
    \dc_bias[3]_i_13 
       (.I0(CO),
        .I1(\encoded_reg[9] ),
        .I2(\tempCnt_reg[1]_0 ),
        .I3(\tempCnt_reg[8]_0 ),
        .O(\tempCnt_reg[1]_4 ));
  LUT6 #(
    .INIT(64'h0001111111111111)) 
    \dc_bias[3]_i_14 
       (.I0(Q[8]),
        .I1(Q[7]),
        .I2(Q[4]),
        .I3(rollOUT_i_3_n_0),
        .I4(Q[6]),
        .I5(Q[5]),
        .O(\dc_bias[3]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hF2F2F2FFFFFFFFFF)) 
    \dc_bias[3]_i_14__0 
       (.I0(CO),
        .I1(\dc_bias[3]_i_6_n_0 ),
        .I2(\tempCnt_reg[8]_0 ),
        .I3(\tempCnt_reg[1]_0 ),
        .I4(\encoded_reg[9] ),
        .I5(\encoded_reg[9]_0 [0]),
        .O(\dc_bias[3]_i_14__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT5 #(
    .INIT(32'hFFFCFEFC)) 
    \dc_bias[3]_i_15 
       (.I0(Q[2]),
        .I1(Q[7]),
        .I2(Q[8]),
        .I3(Q[4]),
        .I4(Q[3]),
        .O(\dc_bias[3]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'hF5F000000A02FFF2)) 
    \dc_bias[3]_i_15__0 
       (.I0(CO),
        .I1(\dc_bias[3]_i_6_n_0 ),
        .I2(\tempCnt_reg[8]_0 ),
        .I3(\dc_bias[3]_i_18_n_0 ),
        .I4(\encoded_reg[9]_0 [0]),
        .I5(\encoded_reg[9]_0 [1]),
        .O(\dc_bias[3]_i_15__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \dc_bias[3]_i_16 
       (.I0(Q[6]),
        .I1(Q[5]),
        .O(\dc_bias[3]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'h0FF00FF0A5D02D0D)) 
    \dc_bias[3]_i_16__0 
       (.I0(CO),
        .I1(\dc_bias[3]_i_6_n_0 ),
        .I2(\encoded_reg[9]_0 [1]),
        .I3(\encoded_reg[9]_0 [0]),
        .I4(\dc_bias[3]_i_18_n_0 ),
        .I5(\tempCnt_reg[8]_0 ),
        .O(\dc_bias[3]_i_16__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000000055550041)) 
    \dc_bias[3]_i_18 
       (.I0(\encoded_reg[9] ),
        .I1(\dc_bias[3]_i_25_n_0 ),
        .I2(Q[1]),
        .I3(\dc_bias[3]_i_24_n_0 ),
        .I4(\dc_bias[3]_i_17__0 ),
        .I5(\dc_bias[3]_i_6_n_0 ),
        .O(\dc_bias[3]_i_18_n_0 ));
  LUT6 #(
    .INIT(64'hB888B8BBB8BBB888)) 
    \dc_bias[3]_i_1__0 
       (.I0(\dc_bias_reg[3]_i_2_n_0 ),
        .I1(\encoded_reg[9]_3 ),
        .I2(\dc_bias[3]_i_4__1_n_0 ),
        .I3(\tempCnt_reg[9]_5 ),
        .I4(\dc_bias[3]_i_5_n_0 ),
        .I5(\dc_bias_reg[3]_9 ),
        .O(D));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \dc_bias[3]_i_1__1 
       (.I0(\tempCnt_reg[8]_1 ),
        .O(encoded1_in));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'hFFFF6996)) 
    \dc_bias[3]_i_24 
       (.I0(Q[0]),
        .I1(\dc_bias[3]_i_44_n_0 ),
        .I2(\dc_bias[3]_i_45_n_0 ),
        .I3(\dc_bias[3]_i_46_n_0 ),
        .I4(\dc_bias[3]_i_47_n_0 ),
        .O(\dc_bias[3]_i_24_n_0 ));
  LUT6 #(
    .INIT(64'hF0F0FFF0F0B0F0F0)) 
    \dc_bias[3]_i_25 
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(\dc_bias[3]_i_48_n_0 ),
        .I3(\dc_bias[3]_i_49_n_0 ),
        .I4(\dc_bias[3]_i_44_n_0 ),
        .I5(\dc_bias[3]_i_50_n_0 ),
        .O(\dc_bias[3]_i_25_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEFFFFF)) 
    \dc_bias[3]_i_26 
       (.I0(\tempCnt[9]_i_5__0_n_0 ),
        .I1(\dc_bias[3]_i_51_n_0 ),
        .I2(\dc_bias[3]_i_52_n_0 ),
        .I3(Q[8]),
        .I4(Q[2]),
        .I5(Q[9]),
        .O(\dc_bias[3]_i_26_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFEFFF0FFF0)) 
    \dc_bias[3]_i_29 
       (.I0(\dc_bias[3]_i_9__1_0 ),
        .I1(\dc_bias[3]_i_9__1_1 ),
        .I2(\dc_bias[3]_i_9__1_2 ),
        .I3(\dc_bias[3]_i_9__1_3 ),
        .I4(\dc_bias[3]_i_9__1_4 [1]),
        .I5(\dc_bias[3]_i_9__1_4 [0]),
        .O(\dc_bias[3]_i_29_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'hFFFF222F)) 
    \dc_bias[3]_i_3__1 
       (.I0(CO),
        .I1(\dc_bias[3]_i_6_n_0 ),
        .I2(\encoded_reg[9] ),
        .I3(\tempCnt_reg[1]_0 ),
        .I4(\tempCnt_reg[8]_0 ),
        .O(\tempCnt_reg[9]_5 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'h0C000C04)) 
    \dc_bias[3]_i_4 
       (.I0(CO),
        .I1(\tempCnt_reg[1]_0 ),
        .I2(\tempCnt_reg[8]_0 ),
        .I3(\dc_bias[3]_i_6_n_0 ),
        .I4(\encoded_reg[9] ),
        .O(\tempCnt_reg[1]_2 ));
  LUT5 #(
    .INIT(32'hFFFEFFFF)) 
    \dc_bias[3]_i_43 
       (.I0(\dc_bias[3]_i_23 [0]),
        .I1(Q[0]),
        .I2(\dc_bias[3]_i_23 [1]),
        .I3(\dc_bias[3]_i_23_0 ),
        .I4(\dc_bias[3]_i_23 [2]),
        .O(\tempCnt_reg[0]_0 ));
  LUT6 #(
    .INIT(64'h6969669966666999)) 
    \dc_bias[3]_i_44 
       (.I0(Q[4]),
        .I1(\dc_bias[3]_i_60_n_0 ),
        .I2(Q[2]),
        .I3(\dc_bias[3]_i_61_n_0 ),
        .I4(Q[3]),
        .I5(\dc_bias[3]_i_62_n_0 ),
        .O(\dc_bias[3]_i_44_n_0 ));
  LUT6 #(
    .INIT(64'h5151555551517555)) 
    \dc_bias[3]_i_45 
       (.I0(\dc_bias[3]_i_63_n_0 ),
        .I1(\dc_bias[3]_i_64_n_0 ),
        .I2(\dc_bias[3]_i_65_n_0 ),
        .I3(Q[3]),
        .I4(\dc_bias[3]_i_61_n_0 ),
        .I5(Q[2]),
        .O(\dc_bias[3]_i_45_n_0 ));
  LUT6 #(
    .INIT(64'hFF09FFFF6F006F6F)) 
    \dc_bias[3]_i_46 
       (.I0(\dc_bias[3]_i_62_n_0 ),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(\dc_bias[3]_i_66_n_0 ),
        .I4(\dc_bias[3]_i_67_n_0 ),
        .I5(Q[1]),
        .O(\dc_bias[3]_i_46_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFDBDDBEBB)) 
    \dc_bias[3]_i_47 
       (.I0(\dc_bias[3]_i_68_n_0 ),
        .I1(Q[1]),
        .I2(\dc_bias[3]_i_66_n_0 ),
        .I3(\dc_bias[3]_i_67_n_0 ),
        .I4(Q[2]),
        .I5(\dc_bias[3]_i_24_0 ),
        .O(\dc_bias[3]_i_47_n_0 ));
  LUT6 #(
    .INIT(64'h9969969966966966)) 
    \dc_bias[3]_i_48 
       (.I0(\dc_bias[3]_i_70_n_0 ),
        .I1(\dc_bias[3]_i_71_n_0 ),
        .I2(Q[3]),
        .I3(Q[4]),
        .I4(\dc_bias[3]_i_60_n_0 ),
        .I5(\dc_bias[3]_i_62_n_0 ),
        .O(\dc_bias[3]_i_48_n_0 ));
  LUT6 #(
    .INIT(64'hF0FFF0F0F0F0D0F0)) 
    \dc_bias[3]_i_49 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(\dc_bias[3]_i_63_n_0 ),
        .I3(\dc_bias[3]_i_65_n_0 ),
        .I4(\dc_bias[3]_i_64_n_0 ),
        .I5(\dc_bias[3]_i_61_n_0 ),
        .O(\dc_bias[3]_i_49_n_0 ));
  LUT6 #(
    .INIT(64'h2F00D0FFD0FF2F00)) 
    \dc_bias[3]_i_4__1 
       (.I0(\tempCnt_reg[9]_0 ),
        .I1(\encoded_reg[9]_0 [1]),
        .I2(\encoded_reg[9]_0 [2]),
        .I3(\tempCnt_reg[9]_4 ),
        .I4(\tempCnt_reg[1]_1 ),
        .I5(\encoded_reg[9]_0 [3]),
        .O(\dc_bias[3]_i_4__1_n_0 ));
  LUT6 #(
    .INIT(64'hBABB777577BA7577)) 
    \dc_bias[3]_i_5 
       (.I0(\encoded_reg[9]_0 [2]),
        .I1(\dc_bias[3]_i_10__0_n_0 ),
        .I2(\dc_bias[3]_i_11_n_0 ),
        .I3(\encoded_reg[9]_0 [1]),
        .I4(\dc_bias[3]_i_12_n_0 ),
        .I5(\tempCnt_reg[1]_1 ),
        .O(\dc_bias[3]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h6066999960669099)) 
    \dc_bias[3]_i_50 
       (.I0(Q[3]),
        .I1(\dc_bias[3]_i_62_n_0 ),
        .I2(\dc_bias[3]_i_66_n_0 ),
        .I3(\dc_bias[3]_i_67_n_0 ),
        .I4(Q[2]),
        .I5(Q[1]),
        .O(\dc_bias[3]_i_50_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \dc_bias[3]_i_51 
       (.I0(Q[1]),
        .I1(Q[0]),
        .O(\dc_bias[3]_i_51_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \dc_bias[3]_i_52 
       (.I0(Q[4]),
        .I1(Q[3]),
        .O(\dc_bias[3]_i_52_n_0 ));
  LUT6 #(
    .INIT(64'hFDFDCDFDFDFDFDFD)) 
    \dc_bias[3]_i_56 
       (.I0(\tempCnt[9]_i_7_n_0 ),
        .I1(\dc_bias[3]_i_74_n_0 ),
        .I2(Q[6]),
        .I3(\dc_bias[3]_i_75_n_0 ),
        .I4(Q[3]),
        .I5(\dc_bias[3]_i_76_n_0 ),
        .O(\tempCnt_reg[6]_2 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF5053)) 
    \dc_bias[3]_i_6 
       (.I0(\dc_bias[3]_i_14_n_0 ),
        .I1(\dc_bias[3]_i_15_n_0 ),
        .I2(Q[9]),
        .I3(\dc_bias[3]_i_16_n_0 ),
        .I4(\dc_bias[3]_i_12_0 ),
        .I5(\dc_bias[3]_i_12_1 ),
        .O(\dc_bias[3]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h3F3FC0803FFF8000)) 
    \dc_bias[3]_i_60 
       (.I0(Q[4]),
        .I1(Q[7]),
        .I2(Q[6]),
        .I3(Q[9]),
        .I4(Q[8]),
        .I5(Q[5]),
        .O(\dc_bias[3]_i_60_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAA8AAAAEAAAAA)) 
    \dc_bias[3]_i_61 
       (.I0(\dc_bias[3]_i_79_n_0 ),
        .I1(\dc_bias[3]_i_71_n_0 ),
        .I2(Q[3]),
        .I3(Q[4]),
        .I4(\dc_bias[3]_i_60_n_0 ),
        .I5(\dc_bias[3]_i_80_n_0 ),
        .O(\dc_bias[3]_i_61_n_0 ));
  LUT6 #(
    .INIT(64'h00001000FFF7FFFF)) 
    \dc_bias[3]_i_62 
       (.I0(Q[3]),
        .I1(\dc_bias[3]_i_71_n_0 ),
        .I2(Q[4]),
        .I3(\dc_bias[3]_i_80_n_0 ),
        .I4(\dc_bias[3]_i_60_n_0 ),
        .I5(\dc_bias[3]_i_79_n_0 ),
        .O(\dc_bias[3]_i_62_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT5 #(
    .INIT(32'h08F710EF)) 
    \dc_bias[3]_i_63 
       (.I0(Q[3]),
        .I1(\dc_bias[3]_i_71_n_0 ),
        .I2(Q[4]),
        .I3(\dc_bias[3]_i_80_n_0 ),
        .I4(\dc_bias[3]_i_60_n_0 ),
        .O(\dc_bias[3]_i_63_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT4 #(
    .INIT(16'hA69A)) 
    \dc_bias[3]_i_64 
       (.I0(\dc_bias[3]_i_71_n_0 ),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(\dc_bias[3]_i_60_n_0 ),
        .O(\dc_bias[3]_i_64_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'h6969EDFF)) 
    \dc_bias[3]_i_65 
       (.I0(\dc_bias[3]_i_60_n_0 ),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[2]),
        .I4(\dc_bias[3]_i_62_n_0 ),
        .O(\dc_bias[3]_i_65_n_0 ));
  LUT6 #(
    .INIT(64'h00000000FAFAFEFF)) 
    \dc_bias[3]_i_66 
       (.I0(\dc_bias[3]_i_81_n_0 ),
        .I1(Q[3]),
        .I2(\dc_bias[3]_i_82_n_0 ),
        .I3(Q[2]),
        .I4(\dc_bias[3]_i_62_n_0 ),
        .I5(\dc_bias[3]_i_63_n_0 ),
        .O(\dc_bias[3]_i_66_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFAFEFFFF)) 
    \dc_bias[3]_i_67 
       (.I0(\dc_bias[3]_i_61_n_0 ),
        .I1(\dc_bias[3]_i_62_n_0 ),
        .I2(Q[2]),
        .I3(\dc_bias[3]_i_82_n_0 ),
        .I4(Q[3]),
        .I5(\dc_bias[3]_i_64_n_0 ),
        .O(\dc_bias[3]_i_67_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \dc_bias[3]_i_68 
       (.I0(Q[2]),
        .I1(\dc_bias[3]_i_62_n_0 ),
        .I2(Q[3]),
        .O(\dc_bias[3]_i_68_n_0 ));
  LUT6 #(
    .INIT(64'h0000000007373737)) 
    \dc_bias[3]_i_7 
       (.I0(\tempCnt_reg[9]_4 ),
        .I1(\encoded_reg[0] [2]),
        .I2(\encoded_reg[0] [1]),
        .I3(\tempCnt_reg[1]_1 ),
        .I4(\encoded_reg[0] [0]),
        .I5(\dc_bias_reg[3] ),
        .O(\dc_bias_reg[2] ));
  LUT6 #(
    .INIT(64'hDC5055FD50DCFD55)) 
    \dc_bias[3]_i_70 
       (.I0(\dc_bias[3]_i_61_n_0 ),
        .I1(\dc_bias[3]_i_62_n_0 ),
        .I2(Q[2]),
        .I3(Q[4]),
        .I4(Q[3]),
        .I5(\dc_bias[3]_i_60_n_0 ),
        .O(\dc_bias[3]_i_70_n_0 ));
  LUT6 #(
    .INIT(64'h7F8080FF807F7F00)) 
    \dc_bias[3]_i_71 
       (.I0(Q[8]),
        .I1(Q[6]),
        .I2(Q[7]),
        .I3(Q[5]),
        .I4(Q[9]),
        .I5(Q[4]),
        .O(\dc_bias[3]_i_71_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'hFFFD)) 
    \dc_bias[3]_i_74 
       (.I0(Q[8]),
        .I1(\dc_bias[3]_i_9__1_4 [0]),
        .I2(Q[9]),
        .I3(Q[7]),
        .O(\dc_bias[3]_i_74_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \dc_bias[3]_i_75 
       (.I0(Q[4]),
        .I1(Q[5]),
        .O(\dc_bias[3]_i_75_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \dc_bias[3]_i_76 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(Q[1]),
        .O(\dc_bias[3]_i_76_n_0 ));
  LUT6 #(
    .INIT(64'h01FF1FFFFA00A000)) 
    \dc_bias[3]_i_79 
       (.I0(Q[4]),
        .I1(Q[8]),
        .I2(Q[9]),
        .I3(Q[6]),
        .I4(Q[5]),
        .I5(Q[7]),
        .O(\dc_bias[3]_i_79_n_0 ));
  LUT6 #(
    .INIT(64'hAA55AAA5AA59AAAA)) 
    \dc_bias[3]_i_7__0 
       (.I0(\encoded_reg[9]_0 [3]),
        .I1(\encoded_reg[9] ),
        .I2(\dc_bias[3]_i_6_n_0 ),
        .I3(\tempCnt_reg[8]_0 ),
        .I4(\tempCnt_reg[1]_0 ),
        .I5(CO),
        .O(\dc_bias[3]_i_7__0_n_0 ));
  LUT6 #(
    .INIT(64'hA665A6A6A69A9AA6)) 
    \dc_bias[3]_i_8 
       (.I0(\dc_bias[3]_i_7__0_n_0 ),
        .I1(\tempCnt_reg[9]_4 ),
        .I2(\encoded_reg[9]_0 [2]),
        .I3(\encoded_reg[9]_0 [1]),
        .I4(\tempCnt_reg[9]_0 ),
        .I5(\dc_bias[3]_i_14__0_n_0 ),
        .O(\dc_bias[3]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h70F0E5A5E5A50F0F)) 
    \dc_bias[3]_i_80 
       (.I0(Q[4]),
        .I1(Q[8]),
        .I2(Q[6]),
        .I3(Q[7]),
        .I4(Q[9]),
        .I5(Q[5]),
        .O(\dc_bias[3]_i_80_n_0 ));
  LUT6 #(
    .INIT(64'h33F33F33FF3FFBFF)) 
    \dc_bias[3]_i_81 
       (.I0(\dc_bias[3]_i_80_n_0 ),
        .I1(\dc_bias[3]_i_79_n_0 ),
        .I2(\dc_bias[3]_i_60_n_0 ),
        .I3(Q[4]),
        .I4(Q[3]),
        .I5(\dc_bias[3]_i_71_n_0 ),
        .O(\dc_bias[3]_i_81_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \dc_bias[3]_i_82 
       (.I0(\dc_bias[3]_i_60_n_0 ),
        .I1(Q[3]),
        .I2(Q[4]),
        .O(\dc_bias[3]_i_82_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hAB)) 
    \dc_bias[3]_i_8__0 
       (.I0(\tempCnt_reg[8]_0 ),
        .I1(\tempCnt_reg[1]_0 ),
        .I2(\encoded_reg[9] ),
        .O(\tempCnt_reg[1]_5 ));
  LUT5 #(
    .INIT(32'hBABBBBBA)) 
    \dc_bias[3]_i_8__1 
       (.I0(\dc_bias[3]_i_6_n_0 ),
        .I1(\dc_bias[3]_i_17__0 ),
        .I2(\dc_bias[3]_i_24_n_0 ),
        .I3(Q[1]),
        .I4(\dc_bias[3]_i_25_n_0 ),
        .O(\tempCnt_reg[1]_0 ));
  LUT6 #(
    .INIT(64'hEF0EF1EF10F10E10)) 
    \dc_bias[3]_i_9 
       (.I0(\dc_bias[3]_i_15__0_n_0 ),
        .I1(\dc_bias[3]_i_16__0_n_0 ),
        .I2(\dc_bias_reg[3]_i_2_0 ),
        .I3(\encoded_reg[9]_0 [2]),
        .I4(\tempCnt_reg[1]_1 ),
        .I5(\encoded_reg[9]_0 [3]),
        .O(\dc_bias[3]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h54FF54FF54FF5454)) 
    \dc_bias[3]_i_9__1 
       (.I0(\dc_bias[3]_i_26_n_0 ),
        .I1(\dc_bias[3]_i_9__0 ),
        .I2(\dc_bias[3]_i_9__0_0 ),
        .I3(\dc_bias[3]_i_9__0_1 ),
        .I4(\dc_bias[3]_i_9__0_2 ),
        .I5(\dc_bias[3]_i_29_n_0 ),
        .O(\tempCnt_reg[8]_0 ));
  MUXF7 \dc_bias_reg[3]_i_2 
       (.I0(\dc_bias[3]_i_8_n_0 ),
        .I1(\dc_bias[3]_i_9_n_0 ),
        .O(\dc_bias_reg[3]_i_2_n_0 ),
        .S(\dc_bias[3]_i_7__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT4 #(
    .INIT(16'hFFB1)) 
    \encoded[0]_i_1 
       (.I0(\tempCnt_reg[8]_1 ),
        .I1(\tempCnt_reg[6]_0 ),
        .I2(\encoded_reg[0] [3]),
        .I3(\encoded_reg[0]_0 ),
        .O(\dc_bias_reg[3]_2 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hF1)) 
    \encoded[1]_i_1__0 
       (.I0(\tempCnt_reg[8]_1 ),
        .I1(\tempCnt_reg[6]_0 ),
        .I2(\encoded_reg[1] ),
        .O(\tempCnt_reg[8]_3 ));
  LUT6 #(
    .INIT(64'h08808008AAAAAAAA)) 
    \encoded[3]_i_1 
       (.I0(\tempCnt_reg[8]_1 ),
        .I1(\encoded_reg[9]_3 ),
        .I2(\encoded_reg[9]_0 [3]),
        .I3(\tempCnt_reg[1]_1 ),
        .I4(\tempCnt_reg[9]_5 ),
        .I5(\tempCnt_reg[9]_4 ),
        .O(\dc_bias_reg[3]_6 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT4 #(
    .INIT(16'hFFB1)) 
    \encoded[3]_i_1__0 
       (.I0(\tempCnt_reg[8]_1 ),
        .I1(\tempCnt_reg[6]_0 ),
        .I2(\encoded_reg[0] [3]),
        .I3(\encoded_reg[0]_0 ),
        .O(\dc_bias_reg[3]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT4 #(
    .INIT(16'hFFB1)) 
    \encoded[5]_i_1 
       (.I0(\tempCnt_reg[8]_1 ),
        .I1(\tempCnt_reg[6]_0 ),
        .I2(\encoded_reg[0] [3]),
        .I3(\encoded_reg[0]_0 ),
        .O(\dc_bias_reg[3]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \encoded[5]_i_1__0 
       (.I0(\encoded_reg[4] ),
        .I1(\tempCnt_reg[9]_5 ),
        .I2(\tempCnt_reg[1]_2 ),
        .O(\dc_bias_reg[3]_3 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hF1)) 
    \encoded[7]_i_1__1 
       (.I0(\tempCnt_reg[8]_1 ),
        .I1(\tempCnt_reg[6]_0 ),
        .I2(\encoded_reg[1] ),
        .O(\tempCnt_reg[8]_2 ));
  LUT6 #(
    .INIT(64'h00007E0000000000)) 
    \encoded[7]_i_2 
       (.I0(\encoded[7]_i_4_n_0 ),
        .I1(Q[6]),
        .I2(Q[5]),
        .I3(Q[7]),
        .I4(Q[8]),
        .I5(Q[9]),
        .O(\tempCnt_reg[6]_0 ));
  LUT6 #(
    .INIT(64'h000000000111FFFF)) 
    \encoded[7]_i_2__0 
       (.I0(Q[8]),
        .I1(Q[7]),
        .I2(Q[6]),
        .I3(\tempCnt[9]_i_7_n_0 ),
        .I4(Q[9]),
        .I5(\encoded_reg[0]_1 ),
        .O(\tempCnt_reg[8]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'hEAAAAAAA)) 
    \encoded[7]_i_4 
       (.I0(Q[4]),
        .I1(Q[3]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[2]),
        .O(\encoded[7]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'hEE2A)) 
    \encoded[8]_i_1 
       (.I0(\tempCnt_reg[6]_0 ),
        .I1(\tempCnt_reg[8]_1 ),
        .I2(\encoded_reg[8] ),
        .I3(\tempCnt_reg[1]_1 ),
        .O(\tempCnt_reg[6]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \encoded[8]_i_1__0 
       (.I0(\tempCnt_reg[8]_1 ),
        .I1(\tempCnt_reg[9]_5 ),
        .O(\tempCnt_reg[8]_4 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \encoded[8]_i_1__1 
       (.I0(\tempCnt_reg[8]_1 ),
        .I1(\tempCnt_reg[9]_5 ),
        .O(\tempCnt_reg[8]_5 ));
  LUT6 #(
    .INIT(64'h00C3D5C3D5C3D5C3)) 
    \encoded[9]_i_1 
       (.I0(\encoded_reg[9]_1 ),
        .I1(\encoded_reg[9]_2 ),
        .I2(\tempCnt_reg[6]_0 ),
        .I3(\tempCnt_reg[8]_1 ),
        .I4(\encoded_reg[8] ),
        .I5(\tempCnt_reg[1]_1 ),
        .O(\dc_bias_reg[3]_4 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'h9F9FFF0F)) 
    \encoded[9]_i_1__0 
       (.I0(\tempCnt_reg[1]_1 ),
        .I1(\encoded_reg[9]_0 [3]),
        .I2(\tempCnt_reg[8]_1 ),
        .I3(\tempCnt_reg[9]_5 ),
        .I4(\encoded_reg[9]_3 ),
        .O(\dc_bias_reg[3]_7 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'h7D)) 
    \encoded[9]_i_1__1 
       (.I0(\tempCnt_reg[8]_1 ),
        .I1(\encoded_reg[4] ),
        .I2(\tempCnt_reg[9]_5 ),
        .O(\dc_bias_reg[3]_8 ));
  LUT6 #(
    .INIT(64'hFF0100FE00FEFF01)) 
    i___20_carry__0_i_1
       (.I0(\r7_inferred__2/i___20_carry__0 [2]),
        .I1(\r7_inferred__2/i___20_carry__0 [0]),
        .I2(\r7_inferred__2/i___20_carry__0 [1]),
        .I3(O),
        .I4(\r7_inferred__2/i___20_carry__0_0 ),
        .I5(Q[6]),
        .O(\tempCnt_reg[6]_3 ));
  LUT2 #(
    .INIT(4'h9)) 
    i___20_carry_i_3
       (.I0(Q[2]),
        .I1(O),
        .O(\tempCnt_reg[2]_0 [1]));
  LUT1 #(
    .INIT(2'h1)) 
    i___20_carry_i_4
       (.I0(Q[1]),
        .O(\tempCnt_reg[2]_0 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__0_i_1__0
       (.I0(Q[9]),
        .I1(Q[5]),
        .O(\tempCnt_reg[9]_7 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__0_i_2__0
       (.I0(Q[8]),
        .I1(Q[4]),
        .O(\tempCnt_reg[9]_7 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__0_i_3__0
       (.I0(Q[7]),
        .I1(Q[3]),
        .O(\tempCnt_reg[9]_7 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_1
       (.I0(Q[9]),
        .O(\tempCnt_reg[9]_6 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry_i_1__7
       (.I0(Q[6]),
        .I1(Q[2]),
        .O(\tempCnt_reg[6]_4 [2]));
  LUT3 #(
    .INIT(8'h20)) 
    i__carry_i_2
       (.I0(Q[8]),
        .I1(Q[7]),
        .I2(Q[6]),
        .O(\tempCnt_reg[9]_6 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry_i_2__6
       (.I0(Q[5]),
        .I1(Q[1]),
        .O(\tempCnt_reg[6]_4 [1]));
  LUT3 #(
    .INIT(8'h01)) 
    i__carry_i_3
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[3]),
        .O(\tempCnt_reg[9]_6 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry_i_3__6
       (.I0(Q[4]),
        .I1(Q[0]),
        .O(\tempCnt_reg[6]_4 [0]));
  LUT3 #(
    .INIT(8'h01)) 
    i__carry_i_4__0
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .O(\tempCnt_reg[9]_6 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    r10_carry_i_1
       (.I0(Q[9]),
        .O(S[3]));
  LUT3 #(
    .INIT(8'h10)) 
    r10_carry_i_2
       (.I0(Q[6]),
        .I1(Q[7]),
        .I2(Q[8]),
        .O(S[2]));
  LUT3 #(
    .INIT(8'h80)) 
    r10_carry_i_3
       (.I0(Q[3]),
        .I1(Q[4]),
        .I2(Q[5]),
        .O(S[1]));
  LUT3 #(
    .INIT(8'h80)) 
    r10_carry_i_4
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .O(S[0]));
  LUT1 #(
    .INIT(2'h1)) 
    r7_carry_i_1
       (.I0(Q[9]),
        .O(\tempCnt_reg[9]_2 [3]));
  LUT3 #(
    .INIT(8'h20)) 
    r7_carry_i_2
       (.I0(Q[8]),
        .I1(Q[7]),
        .I2(Q[6]),
        .O(\tempCnt_reg[9]_2 [2]));
  LUT3 #(
    .INIT(8'h01)) 
    r7_carry_i_3
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[3]),
        .O(\tempCnt_reg[9]_2 [1]));
  LUT3 #(
    .INIT(8'h04)) 
    r7_carry_i_4
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .O(\tempCnt_reg[9]_2 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    r8_carry_i_1
       (.I0(Q[9]),
        .O(\tempCnt_reg[9]_3 [3]));
  LUT3 #(
    .INIT(8'h20)) 
    r8_carry_i_2
       (.I0(Q[8]),
        .I1(Q[7]),
        .I2(Q[6]),
        .O(\tempCnt_reg[9]_3 [2]));
  LUT3 #(
    .INIT(8'h01)) 
    r8_carry_i_3
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[3]),
        .O(\tempCnt_reg[9]_3 [1]));
  LUT3 #(
    .INIT(8'h04)) 
    r8_carry_i_4
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(Q[1]),
        .O(\tempCnt_reg[9]_3 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    r9_carry_i_1
       (.I0(Q[9]),
        .O(\tempCnt_reg[9]_1 [3]));
  LUT3 #(
    .INIT(8'h10)) 
    r9_carry_i_2
       (.I0(Q[6]),
        .I1(Q[7]),
        .I2(Q[8]),
        .O(\tempCnt_reg[9]_1 [2]));
  LUT3 #(
    .INIT(8'h80)) 
    r9_carry_i_3
       (.I0(Q[3]),
        .I1(Q[4]),
        .I2(Q[5]),
        .O(\tempCnt_reg[9]_1 [1]));
  LUT3 #(
    .INIT(8'h08)) 
    r9_carry_i_4
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .O(\tempCnt_reg[9]_1 [0]));
  LUT6 #(
    .INIT(64'h808F808000000000)) 
    rollOUT_i_1
       (.I0(rollOUT_i_2_n_0),
        .I1(rollOUT_i_3_n_0),
        .I2(tempCnt02_out),
        .I3(\tempCnt[9]_i_4__0_n_0 ),
        .I4(rollWire),
        .I5(s00_axi_aresetn),
        .O(rollOUT_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'h80)) 
    rollOUT_i_2
       (.I0(Q[8]),
        .I1(Q[9]),
        .I2(Q[4]),
        .O(rollOUT_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'h8880)) 
    rollOUT_i_3
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(Q[0]),
        .I3(Q[1]),
        .O(rollOUT_i_3_n_0));
  FDRE rollOUT_reg
       (.C(clk_out1),
        .CE(1'b1),
        .D(rollOUT_i_1_n_0),
        .Q(rollWire),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \tempCnt[0]_i_1 
       (.I0(Q[0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \tempCnt[1]_i_1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \tempCnt[2]_i_1__1 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(Q[1]),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \tempCnt[3]_i_1__1 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .O(plusOp[3]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \tempCnt[4]_i_1__1 
       (.I0(Q[4]),
        .I1(Q[3]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[2]),
        .O(plusOp[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \tempCnt[5]_i_1__0 
       (.I0(Q[5]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(Q[4]),
        .I5(Q[3]),
        .O(plusOp[5]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \tempCnt[6]_i_1__0 
       (.I0(Q[6]),
        .I1(\tempCnt[9]_i_7_n_0 ),
        .O(plusOp[6]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \tempCnt[7]_i_1__0 
       (.I0(Q[7]),
        .I1(Q[6]),
        .I2(\tempCnt[9]_i_7_n_0 ),
        .O(plusOp[7]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \tempCnt[8]_i_1__0 
       (.I0(Q[8]),
        .I1(Q[7]),
        .I2(Q[6]),
        .I3(\tempCnt[9]_i_7_n_0 ),
        .O(plusOp[8]));
  LUT2 #(
    .INIT(4'hB)) 
    \tempCnt[9]_i_1__0 
       (.I0(\tempCnt[9]_i_4__0_n_0 ),
        .I1(s00_axi_aresetn),
        .O(\tempCnt[9]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h1555FFFFFFFFFFFF)) 
    \tempCnt[9]_i_2__0 
       (.I0(\tempCnt[9]_i_5__0_n_0 ),
        .I1(\tempCnt[9]_i_6_n_0 ),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(Q[8]),
        .I5(Q[9]),
        .O(tempCnt02_out));
  LUT2 #(
    .INIT(4'h2)) 
    \tempCnt[9]_i_2__1 
       (.I0(rollWire),
        .I1(\tempCnt_reg[9]_8 ),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \tempCnt[9]_i_3__0 
       (.I0(Q[9]),
        .I1(Q[8]),
        .I2(Q[6]),
        .I3(Q[7]),
        .I4(\tempCnt[9]_i_7_n_0 ),
        .O(plusOp[9]));
  LUT6 #(
    .INIT(64'h0000000008000000)) 
    \tempCnt[9]_i_4__0 
       (.I0(rollOUT_i_2_n_0),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(Q[3]),
        .I5(\tempCnt[9]_i_5__0_n_0 ),
        .O(\tempCnt[9]_i_4__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \tempCnt[9]_i_5__0 
       (.I0(Q[7]),
        .I1(Q[5]),
        .I2(Q[6]),
        .O(\tempCnt[9]_i_5__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \tempCnt[9]_i_6 
       (.I0(Q[4]),
        .I1(Q[3]),
        .O(\tempCnt[9]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \tempCnt[9]_i_7 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[5]),
        .I4(Q[4]),
        .I5(Q[3]),
        .O(\tempCnt[9]_i_7_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[0] 
       (.C(clk_out1),
        .CE(tempCnt02_out),
        .D(plusOp[0]),
        .Q(Q[0]),
        .R(\tempCnt[9]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[1] 
       (.C(clk_out1),
        .CE(tempCnt02_out),
        .D(plusOp[1]),
        .Q(Q[1]),
        .R(\tempCnt[9]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[2] 
       (.C(clk_out1),
        .CE(tempCnt02_out),
        .D(plusOp[2]),
        .Q(Q[2]),
        .R(\tempCnt[9]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[3] 
       (.C(clk_out1),
        .CE(tempCnt02_out),
        .D(plusOp[3]),
        .Q(Q[3]),
        .R(\tempCnt[9]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[4] 
       (.C(clk_out1),
        .CE(tempCnt02_out),
        .D(plusOp[4]),
        .Q(Q[4]),
        .R(\tempCnt[9]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[5] 
       (.C(clk_out1),
        .CE(tempCnt02_out),
        .D(plusOp[5]),
        .Q(Q[5]),
        .R(\tempCnt[9]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[6] 
       (.C(clk_out1),
        .CE(tempCnt02_out),
        .D(plusOp[6]),
        .Q(Q[6]),
        .R(\tempCnt[9]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[7] 
       (.C(clk_out1),
        .CE(tempCnt02_out),
        .D(plusOp[7]),
        .Q(Q[7]),
        .R(\tempCnt[9]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[8] 
       (.C(clk_out1),
        .CE(tempCnt02_out),
        .D(plusOp[8]),
        .Q(Q[8]),
        .R(\tempCnt[9]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[9] 
       (.C(clk_out1),
        .CE(tempCnt02_out),
        .D(plusOp[9]),
        .Q(Q[9]),
        .R(\tempCnt[9]_i_1__0_n_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dvid
   (Q,
    \dc_bias_reg[3] ,
    \dc_bias_reg[3]_0 ,
    \dc_bias_reg[1] ,
    \dc_bias_reg[0] ,
    \dc_bias_reg[3]_1 ,
    \dc_bias_reg[1]_0 ,
    \dc_bias_reg[1]_1 ,
    \dc_bias_reg[3]_2 ,
    \dc_bias_reg[1]_2 ,
    CLK,
    clk_out3,
    \encoded_reg[5] ,
    clk_out1,
    \encoded_reg[3] ,
    \encoded_reg[7] ,
    \encoded_reg[5]_0 ,
    \encoded_reg[3]_0 ,
    \encoded_reg[1] ,
    \encoded_reg[0] ,
    \encoded_reg[4] ,
    \encoded_reg[0]_0 ,
    \encoded_reg[9] ,
    \encoded_reg[8] ,
    \encoded_reg[9]_0 ,
    \encoded_reg[8]_0 ,
    \encoded_reg[9]_1 ,
    \encoded_reg[8]_1 ,
    \dc_bias_reg[0]_0 ,
    \encoded_reg[2] ,
    \dc_bias_reg[3]_3 ,
    \dc_bias_reg[1]_3 ,
    \dc_bias_reg[1]_4 ,
    \dc_bias[3]_i_9 ,
    \dc_bias[3]_i_9_0 ,
    \dc_bias[3]_i_9_1 ,
    CO,
    \dc_bias_reg[1]_5 ,
    \dc_bias_reg[3]_4 ,
    \dc_bias_reg[2] ,
    \dc_bias_reg[3]_5 ,
    \dc_bias_reg[1]_6 ,
    \dc_bias_reg[2]_0 ,
    \dc_bias_reg[1]_7 ,
    \dc_bias_reg[2]_1 ,
    \encoded_reg[6] ,
    D);
  output [3:0]Q;
  output \dc_bias_reg[3] ;
  output [3:0]\dc_bias_reg[3]_0 ;
  output \dc_bias_reg[1] ;
  output \dc_bias_reg[0] ;
  output \dc_bias_reg[3]_1 ;
  output \dc_bias_reg[1]_0 ;
  output \dc_bias_reg[1]_1 ;
  output [0:0]\dc_bias_reg[3]_2 ;
  output \dc_bias_reg[1]_2 ;
  input CLK;
  input clk_out3;
  input [0:0]\encoded_reg[5] ;
  input clk_out1;
  input \encoded_reg[3] ;
  input \encoded_reg[7] ;
  input \encoded_reg[5]_0 ;
  input \encoded_reg[3]_0 ;
  input \encoded_reg[1] ;
  input \encoded_reg[0] ;
  input \encoded_reg[4] ;
  input \encoded_reg[0]_0 ;
  input \encoded_reg[9] ;
  input \encoded_reg[8] ;
  input \encoded_reg[9]_0 ;
  input \encoded_reg[8]_0 ;
  input \encoded_reg[9]_1 ;
  input \encoded_reg[8]_1 ;
  input \dc_bias_reg[0]_0 ;
  input \encoded_reg[2] ;
  input \dc_bias_reg[3]_3 ;
  input \dc_bias_reg[1]_3 ;
  input \dc_bias_reg[1]_4 ;
  input [0:0]\dc_bias[3]_i_9 ;
  input \dc_bias[3]_i_9_0 ;
  input \dc_bias[3]_i_9_1 ;
  input [0:0]CO;
  input \dc_bias_reg[1]_5 ;
  input \dc_bias_reg[3]_4 ;
  input \dc_bias_reg[2] ;
  input \dc_bias_reg[3]_5 ;
  input \dc_bias_reg[1]_6 ;
  input \dc_bias_reg[2]_0 ;
  input \dc_bias_reg[1]_7 ;
  input \dc_bias_reg[2]_1 ;
  input \encoded_reg[6] ;
  input [0:0]D;

  wire CLK;
  wire [0:0]CO;
  wire [0:0]D;
  wire D0;
  wire D1;
  wire [3:0]Q;
  wire TDMS_encoder_blue_n_0;
  wire TDMS_encoder_blue_n_1;
  wire TDMS_encoder_blue_n_2;
  wire TDMS_encoder_blue_n_3;
  wire TDMS_encoder_blue_n_4;
  wire TDMS_encoder_blue_n_5;
  wire TDMS_encoder_blue_n_6;
  wire TDMS_encoder_blue_n_7;
  wire TDMS_encoder_blue_n_8;
  wire TDMS_encoder_blue_n_9;
  wire TDMS_encoder_green_n_0;
  wire TDMS_encoder_green_n_1;
  wire TDMS_encoder_green_n_2;
  wire TDMS_encoder_green_n_3;
  wire TDMS_encoder_green_n_4;
  wire TDMS_encoder_green_n_5;
  wire TDMS_encoder_green_n_6;
  wire TDMS_encoder_green_n_7;
  wire TDMS_encoder_green_n_8;
  wire TDMS_encoder_green_n_9;
  wire TDMS_encoder_red_n_0;
  wire TDMS_encoder_red_n_1;
  wire TDMS_encoder_red_n_2;
  wire TDMS_encoder_red_n_3;
  wire TDMS_encoder_red_n_4;
  wire TDMS_encoder_red_n_5;
  wire TDMS_encoder_red_n_6;
  wire TDMS_encoder_red_n_7;
  wire TDMS_encoder_red_n_8;
  wire TDMS_encoder_red_n_9;
  wire blue_s;
  wire clk_out1;
  wire clk_out3;
  wire clock_s;
  wire [7:0]data1;
  wire [0:0]\dc_bias[3]_i_9 ;
  wire \dc_bias[3]_i_9_0 ;
  wire \dc_bias[3]_i_9_1 ;
  wire \dc_bias_reg[0] ;
  wire \dc_bias_reg[0]_0 ;
  wire \dc_bias_reg[1] ;
  wire \dc_bias_reg[1]_0 ;
  wire \dc_bias_reg[1]_1 ;
  wire \dc_bias_reg[1]_2 ;
  wire \dc_bias_reg[1]_3 ;
  wire \dc_bias_reg[1]_4 ;
  wire \dc_bias_reg[1]_5 ;
  wire \dc_bias_reg[1]_6 ;
  wire \dc_bias_reg[1]_7 ;
  wire \dc_bias_reg[2] ;
  wire \dc_bias_reg[2]_0 ;
  wire \dc_bias_reg[2]_1 ;
  wire \dc_bias_reg[3] ;
  wire [3:0]\dc_bias_reg[3]_0 ;
  wire \dc_bias_reg[3]_1 ;
  wire [0:0]\dc_bias_reg[3]_2 ;
  wire \dc_bias_reg[3]_3 ;
  wire \dc_bias_reg[3]_4 ;
  wire \dc_bias_reg[3]_5 ;
  wire \encoded_reg[0] ;
  wire \encoded_reg[0]_0 ;
  wire \encoded_reg[1] ;
  wire \encoded_reg[2] ;
  wire \encoded_reg[3] ;
  wire \encoded_reg[3]_0 ;
  wire \encoded_reg[4] ;
  wire [0:0]\encoded_reg[5] ;
  wire \encoded_reg[5]_0 ;
  wire \encoded_reg[6] ;
  wire \encoded_reg[7] ;
  wire \encoded_reg[8] ;
  wire \encoded_reg[8]_0 ;
  wire \encoded_reg[8]_1 ;
  wire \encoded_reg[9] ;
  wire \encoded_reg[9]_0 ;
  wire \encoded_reg[9]_1 ;
  wire green_s;
  wire [9:0]latched_blue;
  wire [9:0]latched_green;
  wire [9:0]latched_red;
  wire red_s;
  wire [9:0]shift_blue;
  wire \shift_blue[8]_i_2_n_0 ;
  wire \shift_blue[9]_i_2_n_0 ;
  wire \shift_blue_reg_n_0_[0] ;
  wire \shift_blue_reg_n_0_[1] ;
  wire \shift_blue_reg_n_0_[2] ;
  wire \shift_blue_reg_n_0_[3] ;
  wire \shift_blue_reg_n_0_[4] ;
  wire \shift_blue_reg_n_0_[5] ;
  wire \shift_blue_reg_n_0_[6] ;
  wire \shift_blue_reg_n_0_[7] ;
  wire \shift_blue_reg_n_0_[8] ;
  wire \shift_blue_reg_n_0_[9] ;
  wire [1:0]shift_clock;
  wire [9:2]shift_clock__0;
  wire [9:0]shift_green;
  wire \shift_green[8]_i_2_n_0 ;
  wire \shift_green[9]_i_2_n_0 ;
  wire \shift_green_reg_n_0_[0] ;
  wire \shift_green_reg_n_0_[1] ;
  wire \shift_green_reg_n_0_[2] ;
  wire \shift_green_reg_n_0_[3] ;
  wire \shift_green_reg_n_0_[4] ;
  wire \shift_green_reg_n_0_[5] ;
  wire \shift_green_reg_n_0_[6] ;
  wire \shift_green_reg_n_0_[7] ;
  wire \shift_green_reg_n_0_[8] ;
  wire \shift_green_reg_n_0_[9] ;
  wire [9:0]shift_red;
  wire \shift_red[7]_i_2_n_0 ;
  wire \shift_red[7]_i_3_n_0 ;
  wire \shift_red[8]_i_2_n_0 ;
  wire \shift_red[9]_i_2_n_0 ;
  wire \shift_red[9]_i_3_n_0 ;
  wire \shift_red[9]_i_4_n_0 ;
  wire NLW_ODDR2_blue_R_UNCONNECTED;
  wire NLW_ODDR2_blue_S_UNCONNECTED;
  wire NLW_ODDR2_clock_R_UNCONNECTED;
  wire NLW_ODDR2_clock_S_UNCONNECTED;
  wire NLW_ODDR2_green_R_UNCONNECTED;
  wire NLW_ODDR2_green_S_UNCONNECTED;
  wire NLW_ODDR2_red_R_UNCONNECTED;
  wire NLW_ODDR2_red_S_UNCONNECTED;

  (* OPT_MODIFIED = "MLO" *) 
  (* XILINX_LEGACY_PRIM = "ODDR2" *) 
  (* XILINX_TRANSFORM_PINMAP = "D0:D1 D1:D2 C0:C" *) 
  (* __SRVAL = "TRUE" *) 
  (* box_type = "PRIMITIVE" *) 
  ODDR #(
    .DDR_CLK_EDGE("SAME_EDGE"),
    .INIT(1'b0),
    .SRTYPE("ASYNC")) 
    ODDR2_blue
       (.C(CLK),
        .CE(1'b1),
        .D1(\shift_blue_reg_n_0_[0] ),
        .D2(\shift_blue_reg_n_0_[1] ),
        .Q(blue_s),
        .R(NLW_ODDR2_blue_R_UNCONNECTED),
        .S(NLW_ODDR2_blue_S_UNCONNECTED));
  (* OPT_MODIFIED = "MLO" *) 
  (* XILINX_LEGACY_PRIM = "ODDR2" *) 
  (* XILINX_TRANSFORM_PINMAP = "D0:D1 D1:D2 C0:C" *) 
  (* __SRVAL = "TRUE" *) 
  (* box_type = "PRIMITIVE" *) 
  ODDR #(
    .DDR_CLK_EDGE("SAME_EDGE"),
    .INIT(1'b0),
    .SRTYPE("ASYNC")) 
    ODDR2_clock
       (.C(CLK),
        .CE(1'b1),
        .D1(shift_clock[0]),
        .D2(shift_clock[1]),
        .Q(clock_s),
        .R(NLW_ODDR2_clock_R_UNCONNECTED),
        .S(NLW_ODDR2_clock_S_UNCONNECTED));
  (* OPT_MODIFIED = "MLO" *) 
  (* XILINX_LEGACY_PRIM = "ODDR2" *) 
  (* XILINX_TRANSFORM_PINMAP = "D0:D1 D1:D2 C0:C" *) 
  (* __SRVAL = "TRUE" *) 
  (* box_type = "PRIMITIVE" *) 
  ODDR #(
    .DDR_CLK_EDGE("SAME_EDGE"),
    .INIT(1'b0),
    .SRTYPE("ASYNC")) 
    ODDR2_green
       (.C(CLK),
        .CE(1'b1),
        .D1(\shift_green_reg_n_0_[0] ),
        .D2(\shift_green_reg_n_0_[1] ),
        .Q(green_s),
        .R(NLW_ODDR2_green_R_UNCONNECTED),
        .S(NLW_ODDR2_green_S_UNCONNECTED));
  (* OPT_MODIFIED = "MLO" *) 
  (* XILINX_LEGACY_PRIM = "ODDR2" *) 
  (* XILINX_TRANSFORM_PINMAP = "D0:D1 D1:D2 C0:C" *) 
  (* __SRVAL = "TRUE" *) 
  (* box_type = "PRIMITIVE" *) 
  ODDR #(
    .DDR_CLK_EDGE("SAME_EDGE"),
    .INIT(1'b0),
    .SRTYPE("ASYNC")) 
    ODDR2_red
       (.C(CLK),
        .CE(1'b1),
        .D1(D0),
        .D2(D1),
        .Q(red_s),
        .R(NLW_ODDR2_red_R_UNCONNECTED),
        .S(NLW_ODDR2_red_S_UNCONNECTED));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder TDMS_encoder_blue
       (.D({TDMS_encoder_blue_n_0,TDMS_encoder_blue_n_1,TDMS_encoder_blue_n_2,TDMS_encoder_blue_n_3,TDMS_encoder_blue_n_4,TDMS_encoder_blue_n_5,TDMS_encoder_blue_n_6,TDMS_encoder_blue_n_7,TDMS_encoder_blue_n_8,TDMS_encoder_blue_n_9}),
        .Q(Q),
        .clk_out1(clk_out1),
        .\dc_bias_reg[0]_0 (\dc_bias_reg[0]_0 ),
        .\dc_bias_reg[0]_1 (\encoded_reg[2] ),
        .\dc_bias_reg[1]_0 (\dc_bias_reg[1]_0 ),
        .\dc_bias_reg[1]_1 (\dc_bias_reg[1]_1 ),
        .\dc_bias_reg[1]_2 (\dc_bias_reg[1]_2 ),
        .\dc_bias_reg[1]_3 (\dc_bias_reg[1]_6 ),
        .\dc_bias_reg[2]_0 (\dc_bias_reg[2] ),
        .\dc_bias_reg[3]_0 (\dc_bias_reg[3]_1 ),
        .\dc_bias_reg[3]_1 (\dc_bias_reg[3]_4 ),
        .\dc_bias_reg[3]_2 (\dc_bias_reg[3]_5 ),
        .\dc_bias_reg[3]_3 (\encoded_reg[5] ),
        .\encoded_reg[0]_0 (\encoded_reg[0] ),
        .\encoded_reg[1]_0 (\encoded_reg[1] ),
        .\encoded_reg[3]_0 (\encoded_reg[3]_0 ),
        .\encoded_reg[5]_0 (\encoded_reg[5]_0 ),
        .\encoded_reg[6]_0 (\dc_bias_reg[1]_4 ),
        .\encoded_reg[6]_1 (\encoded_reg[0]_0 ),
        .\encoded_reg[6]_2 (\encoded_reg[6] ),
        .\encoded_reg[7]_0 (\encoded_reg[7] ),
        .\encoded_reg[8]_0 (\encoded_reg[8]_1 ),
        .\encoded_reg[9]_0 (\encoded_reg[9]_1 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_1 TDMS_encoder_green
       (.CO(CO),
        .D({TDMS_encoder_green_n_0,TDMS_encoder_green_n_1,TDMS_encoder_green_n_2,TDMS_encoder_green_n_3,TDMS_encoder_green_n_4,TDMS_encoder_green_n_5,TDMS_encoder_green_n_6,TDMS_encoder_green_n_7,TDMS_encoder_green_n_8,TDMS_encoder_green_n_9}),
        .Q(\dc_bias_reg[3]_0 ),
        .clk_out1(clk_out1),
        .\dc_bias[3]_i_9 (\dc_bias[3]_i_9 ),
        .\dc_bias[3]_i_9_0 (\dc_bias[3]_i_9_0 ),
        .\dc_bias[3]_i_9_1 (\dc_bias[3]_i_9_1 ),
        .\dc_bias_reg[0]_0 (\dc_bias_reg[0] ),
        .\dc_bias_reg[1]_0 (\dc_bias_reg[1] ),
        .\dc_bias_reg[1]_1 (\dc_bias_reg[1]_3 ),
        .\dc_bias_reg[1]_2 (\dc_bias_reg[1]_5 ),
        .\dc_bias_reg[2]_0 (\dc_bias_reg[2]_0 ),
        .\dc_bias_reg[3]_0 (\dc_bias_reg[3] ),
        .\dc_bias_reg[3]_1 (\dc_bias_reg[3]_3 ),
        .\dc_bias_reg[3]_2 (D),
        .\encoded_reg[2]_0 (\encoded_reg[2] ),
        .\encoded_reg[2]_1 (\dc_bias_reg[1]_7 ),
        .\encoded_reg[3]_0 (\encoded_reg[3] ),
        .\encoded_reg[7]_0 (\encoded_reg[5] ),
        .\encoded_reg[7]_1 (\dc_bias_reg[1]_4 ),
        .\encoded_reg[8]_0 (\encoded_reg[8]_0 ),
        .\encoded_reg[9]_0 (\encoded_reg[9]_0 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_TDMS_encoder_2 TDMS_encoder_red
       (.D({TDMS_encoder_red_n_0,TDMS_encoder_red_n_1,TDMS_encoder_red_n_2,TDMS_encoder_red_n_3,TDMS_encoder_red_n_4,TDMS_encoder_red_n_5,TDMS_encoder_red_n_6,TDMS_encoder_red_n_7,TDMS_encoder_red_n_8,TDMS_encoder_red_n_9}),
        .Q(\dc_bias_reg[3]_2 ),
        .clk_out1(clk_out1),
        .\dc_bias_reg[1]_0 (\dc_bias_reg[1]_7 ),
        .\dc_bias_reg[1]_1 (\dc_bias_reg[1]_4 ),
        .\dc_bias_reg[2]_0 (\dc_bias_reg[2]_1 ),
        .\encoded_reg[0]_0 (\encoded_reg[0]_0 ),
        .\encoded_reg[4]_0 (\encoded_reg[4] ),
        .\encoded_reg[5]_0 (\encoded_reg[5] ),
        .\encoded_reg[8]_0 (\encoded_reg[8] ),
        .\encoded_reg[9]_0 (\encoded_reg[9] ));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[0] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_9),
        .Q(latched_blue[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[1] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_8),
        .Q(latched_blue[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[2] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_7),
        .Q(latched_blue[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[3] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_6),
        .Q(latched_blue[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[4] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_5),
        .Q(latched_blue[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[5] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_4),
        .Q(latched_blue[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[6] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_3),
        .Q(latched_blue[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[7] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_2),
        .Q(latched_blue[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[8] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_1),
        .Q(latched_blue[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_blue_reg[9] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_blue_n_0),
        .Q(latched_blue[9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[0] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_9),
        .Q(latched_green[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[1] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_8),
        .Q(latched_green[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[2] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_7),
        .Q(latched_green[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[3] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_6),
        .Q(latched_green[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[4] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_5),
        .Q(latched_green[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[5] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_4),
        .Q(latched_green[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[6] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_3),
        .Q(latched_green[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[7] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_2),
        .Q(latched_green[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[8] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_1),
        .Q(latched_green[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_green_reg[9] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_green_n_0),
        .Q(latched_green[9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[0] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_9),
        .Q(latched_red[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[1] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_8),
        .Q(latched_red[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[2] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_7),
        .Q(latched_red[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[3] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_6),
        .Q(latched_red[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[4] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_5),
        .Q(latched_red[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[5] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_4),
        .Q(latched_red[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[6] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_3),
        .Q(latched_red[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[7] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_2),
        .Q(latched_red[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[8] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_1),
        .Q(latched_red[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \latched_red_reg[9] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(TDMS_encoder_red_n_0),
        .Q(latched_red[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_blue[0]_i_1 
       (.I0(\shift_blue_reg_n_0_[2] ),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_blue[0]),
        .O(shift_blue[0]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_blue[1]_i_1 
       (.I0(\shift_blue_reg_n_0_[3] ),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_blue[1]),
        .O(shift_blue[1]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_blue[2]_i_1 
       (.I0(\shift_blue_reg_n_0_[4] ),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_blue[2]),
        .O(shift_blue[2]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_blue[3]_i_1 
       (.I0(\shift_blue_reg_n_0_[5] ),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_blue[3]),
        .O(shift_blue[3]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_blue[4]_i_1 
       (.I0(\shift_blue_reg_n_0_[6] ),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_blue[4]),
        .O(shift_blue[4]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_blue[5]_i_1 
       (.I0(\shift_blue_reg_n_0_[7] ),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_blue[5]),
        .O(shift_blue[5]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_blue[6]_i_1 
       (.I0(\shift_blue_reg_n_0_[8] ),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_blue[6]),
        .O(shift_blue[6]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_blue[7]_i_1 
       (.I0(\shift_blue_reg_n_0_[9] ),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_blue[7]),
        .O(shift_blue[7]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \shift_blue[8]_i_1 
       (.I0(shift_clock__0[7]),
        .I1(shift_clock__0[8]),
        .I2(\shift_blue[8]_i_2_n_0 ),
        .I3(shift_clock__0[6]),
        .I4(shift_clock__0[9]),
        .O(shift_blue[8]));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \shift_blue[8]_i_2 
       (.I0(latched_blue[8]),
        .I1(shift_clock[0]),
        .I2(shift_clock__0[2]),
        .I3(shift_clock[1]),
        .I4(\shift_red[9]_i_3_n_0 ),
        .I5(\shift_red[9]_i_4_n_0 ),
        .O(\shift_blue[8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00000010)) 
    \shift_blue[9]_i_1 
       (.I0(shift_clock__0[7]),
        .I1(shift_clock__0[8]),
        .I2(\shift_blue[9]_i_2_n_0 ),
        .I3(shift_clock__0[6]),
        .I4(shift_clock__0[9]),
        .O(shift_blue[9]));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \shift_blue[9]_i_2 
       (.I0(latched_blue[9]),
        .I1(shift_clock[0]),
        .I2(shift_clock__0[2]),
        .I3(shift_clock[1]),
        .I4(\shift_red[9]_i_3_n_0 ),
        .I5(\shift_red[9]_i_4_n_0 ),
        .O(\shift_blue[9]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_blue[0]),
        .Q(\shift_blue_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_blue[1]),
        .Q(\shift_blue_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_blue[2]),
        .Q(\shift_blue_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_blue[3]),
        .Q(\shift_blue_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_blue[4]),
        .Q(\shift_blue_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_blue[5]),
        .Q(\shift_blue_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_blue[6]),
        .Q(\shift_blue_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_blue[7]),
        .Q(\shift_blue_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_blue[8]),
        .Q(\shift_blue_reg_n_0_[8] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_blue_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_blue[9]),
        .Q(\shift_blue_reg_n_0_[9] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \shift_clock_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_clock__0[2]),
        .Q(shift_clock[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \shift_clock_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_clock__0[3]),
        .Q(shift_clock[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \shift_clock_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_clock__0[4]),
        .Q(shift_clock__0[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \shift_clock_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_clock__0[5]),
        .Q(shift_clock__0[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \shift_clock_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_clock__0[6]),
        .Q(shift_clock__0[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_clock_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_clock__0[7]),
        .Q(shift_clock__0[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_clock_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_clock__0[8]),
        .Q(shift_clock__0[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_clock_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_clock__0[9]),
        .Q(shift_clock__0[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_clock_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_clock[0]),
        .Q(shift_clock__0[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_clock_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_clock[1]),
        .Q(shift_clock__0[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_green[0]_i_1 
       (.I0(\shift_green_reg_n_0_[2] ),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_green[0]),
        .O(shift_green[0]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_green[1]_i_1 
       (.I0(\shift_green_reg_n_0_[3] ),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_green[1]),
        .O(shift_green[1]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_green[2]_i_1 
       (.I0(\shift_green_reg_n_0_[4] ),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_green[2]),
        .O(shift_green[2]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_green[3]_i_1 
       (.I0(\shift_green_reg_n_0_[5] ),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_green[3]),
        .O(shift_green[3]));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_green[4]_i_1 
       (.I0(\shift_green_reg_n_0_[6] ),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_green[4]),
        .O(shift_green[4]));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_green[5]_i_1 
       (.I0(\shift_green_reg_n_0_[7] ),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_green[5]),
        .O(shift_green[5]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_green[6]_i_1 
       (.I0(\shift_green_reg_n_0_[8] ),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_green[6]),
        .O(shift_green[6]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_green[7]_i_1 
       (.I0(\shift_green_reg_n_0_[9] ),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_green[7]),
        .O(shift_green[7]));
  LUT5 #(
    .INIT(32'h00000010)) 
    \shift_green[8]_i_1 
       (.I0(shift_clock__0[7]),
        .I1(shift_clock__0[8]),
        .I2(\shift_green[8]_i_2_n_0 ),
        .I3(shift_clock__0[6]),
        .I4(shift_clock__0[9]),
        .O(shift_green[8]));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \shift_green[8]_i_2 
       (.I0(latched_green[8]),
        .I1(shift_clock[0]),
        .I2(shift_clock__0[2]),
        .I3(shift_clock[1]),
        .I4(\shift_red[9]_i_3_n_0 ),
        .I5(\shift_red[9]_i_4_n_0 ),
        .O(\shift_green[8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00000010)) 
    \shift_green[9]_i_1 
       (.I0(shift_clock__0[7]),
        .I1(shift_clock__0[8]),
        .I2(\shift_green[9]_i_2_n_0 ),
        .I3(shift_clock__0[6]),
        .I4(shift_clock__0[9]),
        .O(shift_green[9]));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \shift_green[9]_i_2 
       (.I0(latched_green[9]),
        .I1(shift_clock[0]),
        .I2(shift_clock__0[2]),
        .I3(shift_clock[1]),
        .I4(\shift_red[9]_i_3_n_0 ),
        .I5(\shift_red[9]_i_4_n_0 ),
        .O(\shift_green[9]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_green[0]),
        .Q(\shift_green_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_green[1]),
        .Q(\shift_green_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_green[2]),
        .Q(\shift_green_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_green[3]),
        .Q(\shift_green_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_green[4]),
        .Q(\shift_green_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_green[5]),
        .Q(\shift_green_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_green[6]),
        .Q(\shift_green_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_green[7]),
        .Q(\shift_green_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_green[8]),
        .Q(\shift_green_reg_n_0_[8] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_green_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_green[9]),
        .Q(\shift_green_reg_n_0_[9] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[0]_i_1 
       (.I0(data1[0]),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_red[0]),
        .O(shift_red[0]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[1]_i_1 
       (.I0(data1[1]),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_red[1]),
        .O(shift_red[1]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[2]_i_1 
       (.I0(data1[2]),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_red[2]),
        .O(shift_red[2]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[3]_i_1 
       (.I0(data1[3]),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_red[3]),
        .O(shift_red[3]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[4]_i_1 
       (.I0(data1[4]),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_red[4]),
        .O(shift_red[4]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[5]_i_1 
       (.I0(data1[5]),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_red[5]),
        .O(shift_red[5]));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[6]_i_1 
       (.I0(data1[6]),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_red[6]),
        .O(shift_red[6]));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \shift_red[7]_i_1 
       (.I0(data1[7]),
        .I1(\shift_red[7]_i_2_n_0 ),
        .I2(latched_red[7]),
        .O(shift_red[7]));
  LUT5 #(
    .INIT(32'hFFFFFFBF)) 
    \shift_red[7]_i_2 
       (.I0(\shift_red[7]_i_3_n_0 ),
        .I1(shift_clock[1]),
        .I2(shift_clock__0[2]),
        .I3(shift_clock__0[6]),
        .I4(shift_clock__0[9]),
        .O(\shift_red[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFEFFFFFFFFFFFFFF)) 
    \shift_red[7]_i_3 
       (.I0(shift_clock__0[7]),
        .I1(shift_clock__0[8]),
        .I2(shift_clock__0[5]),
        .I3(shift_clock__0[4]),
        .I4(shift_clock__0[3]),
        .I5(shift_clock[0]),
        .O(\shift_red[7]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h00000010)) 
    \shift_red[8]_i_1 
       (.I0(shift_clock__0[7]),
        .I1(shift_clock__0[8]),
        .I2(\shift_red[8]_i_2_n_0 ),
        .I3(shift_clock__0[6]),
        .I4(shift_clock__0[9]),
        .O(shift_red[8]));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \shift_red[8]_i_2 
       (.I0(latched_red[8]),
        .I1(shift_clock[0]),
        .I2(shift_clock__0[2]),
        .I3(shift_clock[1]),
        .I4(\shift_red[9]_i_3_n_0 ),
        .I5(\shift_red[9]_i_4_n_0 ),
        .O(\shift_red[8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00000010)) 
    \shift_red[9]_i_1 
       (.I0(shift_clock__0[7]),
        .I1(shift_clock__0[8]),
        .I2(\shift_red[9]_i_2_n_0 ),
        .I3(shift_clock__0[6]),
        .I4(shift_clock__0[9]),
        .O(shift_red[9]));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \shift_red[9]_i_2 
       (.I0(latched_red[9]),
        .I1(shift_clock[0]),
        .I2(shift_clock__0[2]),
        .I3(shift_clock[1]),
        .I4(\shift_red[9]_i_3_n_0 ),
        .I5(\shift_red[9]_i_4_n_0 ),
        .O(\shift_red[9]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    \shift_red[9]_i_3 
       (.I0(shift_clock__0[4]),
        .I1(shift_clock__0[3]),
        .I2(shift_clock__0[5]),
        .O(\shift_red[9]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \shift_red[9]_i_4 
       (.I0(shift_clock__0[5]),
        .I1(shift_clock__0[4]),
        .O(\shift_red[9]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_red[0]),
        .Q(D0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_red[1]),
        .Q(D1),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_red[2]),
        .Q(data1[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_red[3]),
        .Q(data1[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_red[4]),
        .Q(data1[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_red[5]),
        .Q(data1[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_red[6]),
        .Q(data1[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_red[7]),
        .Q(data1[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_red[8]),
        .Q(data1[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \shift_red_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(shift_red[9]),
        .Q(data1[7]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_i2s_ctl
   (SR,
    LRCLK_reg_0,
    s00_axi_aresetn_0,
    ready_sig_reg,
    s00_axi_aclk,
    s00_axi_aresetn,
    ac_lrclk_sig_prev_reg,
    ready_sig_reg_0,
    ac_lrclk_count);
  output [0:0]SR;
  output LRCLK_reg_0;
  output s00_axi_aresetn_0;
  output ready_sig_reg;
  input s00_axi_aclk;
  input s00_axi_aresetn;
  input ac_lrclk_sig_prev_reg;
  input ready_sig_reg_0;
  input [2:0]ac_lrclk_count;

  wire BCLK_Fall_int;
  wire BCLK_int;
  wire BCLK_int_i_1_n_0;
  wire Cnt_Bclk0;
  wire \Cnt_Bclk0_inferred__0/i__carry_n_3 ;
  wire \Cnt_Bclk[4]_i_1_n_0 ;
  wire [4:0]Cnt_Bclk_reg;
  wire [4:0]Cnt_Lrclk;
  wire \Cnt_Lrclk[0]_i_1_n_0 ;
  wire \Cnt_Lrclk[1]_i_1_n_0 ;
  wire \Cnt_Lrclk[2]_i_1_n_0 ;
  wire \Cnt_Lrclk[3]_i_1_n_0 ;
  wire \Cnt_Lrclk[4]_i_2_n_0 ;
  wire LRCLK_i_1_n_0;
  wire LRCLK_i_2_n_0;
  wire LRCLK_reg_0;
  wire [0:0]SR;
  wire [2:0]ac_lrclk_count;
  wire ac_lrclk_count0;
  wire ac_lrclk_sig_prev_reg;
  wire i__carry_i_1__0_n_0;
  wire i__carry_i_2__7_n_0;
  wire [4:0]p_0_in;
  wire ready_sig_reg;
  wire ready_sig_reg_0;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire s00_axi_aresetn_0;
  wire [3:2]\NLW_Cnt_Bclk0_inferred__0/i__carry_CO_UNCONNECTED ;
  wire [3:0]\NLW_Cnt_Bclk0_inferred__0/i__carry_O_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h6)) 
    BCLK_int_i_1
       (.I0(Cnt_Bclk0),
        .I1(BCLK_int),
        .O(BCLK_int_i_1_n_0));
  FDRE BCLK_int_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(BCLK_int_i_1_n_0),
        .Q(BCLK_int),
        .R(SR));
  CARRY4 \Cnt_Bclk0_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\NLW_Cnt_Bclk0_inferred__0/i__carry_CO_UNCONNECTED [3:2],Cnt_Bclk0,\Cnt_Bclk0_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_Cnt_Bclk0_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,i__carry_i_1__0_n_0,i__carry_i_2__7_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    \Cnt_Bclk[0]_i_1 
       (.I0(Cnt_Bclk_reg[0]),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \Cnt_Bclk[1]_i_1 
       (.I0(Cnt_Bclk_reg[0]),
        .I1(Cnt_Bclk_reg[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \Cnt_Bclk[2]_i_1 
       (.I0(Cnt_Bclk_reg[2]),
        .I1(Cnt_Bclk_reg[1]),
        .I2(Cnt_Bclk_reg[0]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \Cnt_Bclk[3]_i_1 
       (.I0(Cnt_Bclk_reg[3]),
        .I1(Cnt_Bclk_reg[2]),
        .I2(Cnt_Bclk_reg[0]),
        .I3(Cnt_Bclk_reg[1]),
        .O(p_0_in[3]));
  LUT2 #(
    .INIT(4'hB)) 
    \Cnt_Bclk[4]_i_1 
       (.I0(Cnt_Bclk0),
        .I1(s00_axi_aresetn),
        .O(\Cnt_Bclk[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \Cnt_Bclk[4]_i_2 
       (.I0(Cnt_Bclk_reg[4]),
        .I1(Cnt_Bclk_reg[1]),
        .I2(Cnt_Bclk_reg[0]),
        .I3(Cnt_Bclk_reg[2]),
        .I4(Cnt_Bclk_reg[3]),
        .O(p_0_in[4]));
  FDRE \Cnt_Bclk_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(Cnt_Bclk_reg[0]),
        .R(\Cnt_Bclk[4]_i_1_n_0 ));
  FDRE \Cnt_Bclk_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(Cnt_Bclk_reg[1]),
        .R(\Cnt_Bclk[4]_i_1_n_0 ));
  FDRE \Cnt_Bclk_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(Cnt_Bclk_reg[2]),
        .R(\Cnt_Bclk[4]_i_1_n_0 ));
  FDRE \Cnt_Bclk_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(Cnt_Bclk_reg[3]),
        .R(\Cnt_Bclk[4]_i_1_n_0 ));
  FDRE \Cnt_Bclk_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(Cnt_Bclk_reg[4]),
        .R(\Cnt_Bclk[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \Cnt_Lrclk[0]_i_1 
       (.I0(Cnt_Lrclk[0]),
        .O(\Cnt_Lrclk[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \Cnt_Lrclk[1]_i_1 
       (.I0(Cnt_Lrclk[1]),
        .I1(Cnt_Lrclk[0]),
        .O(\Cnt_Lrclk[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \Cnt_Lrclk[2]_i_1 
       (.I0(Cnt_Lrclk[2]),
        .I1(Cnt_Lrclk[0]),
        .I2(Cnt_Lrclk[1]),
        .O(\Cnt_Lrclk[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \Cnt_Lrclk[3]_i_1 
       (.I0(Cnt_Lrclk[3]),
        .I1(Cnt_Lrclk[1]),
        .I2(Cnt_Lrclk[0]),
        .I3(Cnt_Lrclk[2]),
        .O(\Cnt_Lrclk[3]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \Cnt_Lrclk[4]_i_1 
       (.I0(Cnt_Bclk0),
        .I1(BCLK_int),
        .O(BCLK_Fall_int));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \Cnt_Lrclk[4]_i_2 
       (.I0(Cnt_Lrclk[4]),
        .I1(Cnt_Lrclk[2]),
        .I2(Cnt_Lrclk[0]),
        .I3(Cnt_Lrclk[1]),
        .I4(Cnt_Lrclk[3]),
        .O(\Cnt_Lrclk[4]_i_2_n_0 ));
  FDRE \Cnt_Lrclk_reg[0] 
       (.C(s00_axi_aclk),
        .CE(BCLK_Fall_int),
        .D(\Cnt_Lrclk[0]_i_1_n_0 ),
        .Q(Cnt_Lrclk[0]),
        .R(SR));
  FDRE \Cnt_Lrclk_reg[1] 
       (.C(s00_axi_aclk),
        .CE(BCLK_Fall_int),
        .D(\Cnt_Lrclk[1]_i_1_n_0 ),
        .Q(Cnt_Lrclk[1]),
        .R(SR));
  FDRE \Cnt_Lrclk_reg[2] 
       (.C(s00_axi_aclk),
        .CE(BCLK_Fall_int),
        .D(\Cnt_Lrclk[2]_i_1_n_0 ),
        .Q(Cnt_Lrclk[2]),
        .R(SR));
  FDRE \Cnt_Lrclk_reg[3] 
       (.C(s00_axi_aclk),
        .CE(BCLK_Fall_int),
        .D(\Cnt_Lrclk[3]_i_1_n_0 ),
        .Q(Cnt_Lrclk[3]),
        .R(SR));
  FDRE \Cnt_Lrclk_reg[4] 
       (.C(s00_axi_aclk),
        .CE(BCLK_Fall_int),
        .D(\Cnt_Lrclk[4]_i_2_n_0 ),
        .Q(Cnt_Lrclk[4]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    LRCLK_i_1
       (.I0(Cnt_Lrclk[4]),
        .I1(LRCLK_i_2_n_0),
        .I2(Cnt_Bclk0),
        .I3(BCLK_int),
        .I4(LRCLK_reg_0),
        .O(LRCLK_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    LRCLK_i_2
       (.I0(Cnt_Lrclk[3]),
        .I1(Cnt_Lrclk[1]),
        .I2(Cnt_Lrclk[0]),
        .I3(Cnt_Lrclk[2]),
        .O(LRCLK_i_2_n_0));
  FDRE LRCLK_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(LRCLK_i_1_n_0),
        .Q(LRCLK_reg_0),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hD8)) 
    ac_lrclk_sig_prev_i_1
       (.I0(s00_axi_aresetn),
        .I1(LRCLK_reg_0),
        .I2(ac_lrclk_sig_prev_reg),
        .O(s00_axi_aresetn_0));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(SR));
  LUT2 #(
    .INIT(4'h1)) 
    i__carry_i_1__0
       (.I0(Cnt_Bclk_reg[3]),
        .I1(Cnt_Bclk_reg[4]),
        .O(i__carry_i_1__0_n_0));
  LUT3 #(
    .INIT(8'h04)) 
    i__carry_i_2__7
       (.I0(Cnt_Bclk_reg[2]),
        .I1(Cnt_Bclk_reg[0]),
        .I2(Cnt_Bclk_reg[1]),
        .O(i__carry_i_2__7_n_0));
  LUT6 #(
    .INIT(64'hE2A2A2A2A2A2A2A2)) 
    ready_sig_i_1
       (.I0(ready_sig_reg_0),
        .I1(s00_axi_aresetn),
        .I2(ac_lrclk_count0),
        .I3(ac_lrclk_count[1]),
        .I4(ac_lrclk_count[0]),
        .I5(ac_lrclk_count[2]),
        .O(ready_sig_reg));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h2)) 
    ready_sig_i_2
       (.I0(LRCLK_reg_0),
        .I1(ac_lrclk_sig_prev_reg),
        .O(ac_lrclk_count0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_datapath
   (SS,
    sw,
    \tempCnt_reg[9] ,
    \tempCnt_reg[8] ,
    D,
    CO,
    s00_axi_aclk,
    s00_axi_aresetn,
    cw,
    \axi_rdata_reg[0] ,
    Q,
    \axi_rdata_reg[0]_0 ,
    \axi_rdata_reg[0]_1 ,
    \tempCnt_reg[4] ,
    E);
  output [0:0]SS;
  output [1:0]sw;
  output \tempCnt_reg[9] ;
  output \tempCnt_reg[8] ;
  output [0:0]D;
  output [0:0]CO;
  input s00_axi_aclk;
  input s00_axi_aresetn;
  input [0:0]cw;
  input \axi_rdata_reg[0] ;
  input [4:0]Q;
  input \axi_rdata_reg[0]_0 ;
  input \axi_rdata_reg[0]_1 ;
  input [0:0]\tempCnt_reg[4] ;
  input [0:0]E;

  wire [0:0]CO;
  wire [0:0]D;
  wire [0:0]E;
  wire [17:17]L;
  wire [4:0]Q;
  wire [0:0]SS;
  wire \axi_rdata_reg[0] ;
  wire \axi_rdata_reg[0]_0 ;
  wire \axi_rdata_reg[0]_1 ;
  wire ch1;
  wire ch2;
  wire [9:0]columnWire;
  wire [9:0]count;
  wire [0:0]cw;
  wire lab2_Audio_Wrapper_n_2;
  wire lab2_video_n_20;
  wire leftBRAM_n_0;
  wire [9:0]rowWire;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire [1:0]sw;
  wire [0:0]\tempCnt_reg[4] ;
  wire \tempCnt_reg[8] ;
  wire \tempCnt_reg[9] ;
  wire [3:1]\NLW_FSM_sequential_state_reg[2]_i_3_CO_UNCONNECTED ;
  wire [3:0]\NLW_FSM_sequential_state_reg[2]_i_3_O_UNCONNECTED ;

  CARRY4 \FSM_sequential_state_reg[2]_i_3 
       (.CI(1'b0),
        .CO({\NLW_FSM_sequential_state_reg[2]_i_3_CO_UNCONNECTED [3:1],CO}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_FSM_sequential_state_reg[2]_i_3_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,L}));
  FDRE \currUnsignedLBus_reg[17] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(lab2_Audio_Wrapper_n_2),
        .Q(L),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Audio_Codec_Wrapper lab2_Audio_Wrapper
       (.D(D),
        .Q(Q),
        .S(L),
        .\axi_rdata_reg[0] (\axi_rdata_reg[0] ),
        .\axi_rdata_reg[0]_0 (\axi_rdata_reg[0]_0 ),
        .\axi_rdata_reg[0]_1 (\axi_rdata_reg[0]_1 ),
        .ready_sig_reg_0(sw[1]),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_aresetn_0(SS),
        .s00_axi_aresetn_1(lab2_Audio_Wrapper_n_2));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_BRAMCounter lab2_BRAMCntr
       (.E(E),
        .Q(count),
        .s00_axi_aclk(s00_axi_aclk),
        .sw(sw[1]),
        .\tempCnt_reg[4]_0 (\tempCnt_reg[4] ),
        .\tempCnt_reg[8]_0 (\tempCnt_reg[8] ),
        .\tempCnt_reg[9]_0 (\tempCnt_reg[9] ),
        .\tempCnt_reg[9]_1 (sw[0]));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_video lab2_video
       (.CO(ch1),
        .Q(rowWire),
        .\dc_bias_reg[3] (leftBRAM_n_0),
        .\encoded_reg[9] (ch2),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn),
        .\tempCnt_reg[8] (lab2_video_n_20),
        .\tempCnt_reg[9] (columnWire));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO leftBRAM
       (.CO(ch1),
        .Q(count),
        .cw(cw),
        .\dc_bias[2]_i_4 (lab2_video_n_20),
        .\dc_bias[3]_i_9__1 (leftBRAM_n_0),
        .\dc_bias_reg[3]_i_5_0 (rowWire),
        .s00_axi_aclk(s00_axi_aclk),
        .\sdp_bl.ramb18_dp_bl.ram18_bl_0 (SS),
        .\sdp_bl.ramb18_dp_bl.ram18_bl_1 (columnWire));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO_0 rightBRAM
       (.Q(count),
        .\dc_bias_reg[3]_i_7_0 (rowWire),
        .s00_axi_aclk(s00_axi_aclk),
        .\sdp_bl.ramb18_dp_bl.ram18_bl_0 (SS),
        .\sdp_bl.ramb18_dp_bl.ram18_bl_1 (columnWire),
        .\tempCnt_reg[9] (ch2));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_fsm
   (SS,
    cw,
    E,
    sw,
    s00_axi_aresetn,
    CO,
    \FSM_sequential_state_reg[2]_0 ,
    \FSM_sequential_state_reg[0]_0 ,
    \FSM_sequential_state_reg[0]_1 ,
    s00_axi_aclk);
  output [0:0]SS;
  output [0:0]cw;
  output [0:0]E;
  input [1:0]sw;
  input s00_axi_aresetn;
  input [0:0]CO;
  input \FSM_sequential_state_reg[2]_0 ;
  input \FSM_sequential_state_reg[0]_0 ;
  input [0:0]\FSM_sequential_state_reg[0]_1 ;
  input s00_axi_aclk;

  wire [0:0]CO;
  wire [0:0]E;
  wire \FSM_sequential_state[0]_i_1_n_0 ;
  wire \FSM_sequential_state[1]_i_1_n_0 ;
  wire \FSM_sequential_state[2]_i_1_n_0 ;
  wire \FSM_sequential_state[2]_i_2_n_0 ;
  wire \FSM_sequential_state_reg[0]_0 ;
  wire [0:0]\FSM_sequential_state_reg[0]_1 ;
  wire \FSM_sequential_state_reg[2]_0 ;
  wire \FSM_sequential_state_reg[2]_i_4_n_3 ;
  wire [0:0]SS;
  wire [0:0]cw;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire [2:0]state;
  wire [1:0]sw;
  wire [3:1]\NLW_FSM_sequential_state_reg[2]_i_4_CO_UNCONNECTED ;
  wire [3:0]\NLW_FSM_sequential_state_reg[2]_i_4_O_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00FF0700)) 
    \FSM_sequential_state[0]_i_1 
       (.I0(\FSM_sequential_state_reg[0]_0 ),
        .I1(state[1]),
        .I2(state[2]),
        .I3(\FSM_sequential_state[2]_i_2_n_0 ),
        .I4(state[0]),
        .O(\FSM_sequential_state[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0405FFFF0A5A0000)) 
    \FSM_sequential_state[1]_i_1 
       (.I0(state[0]),
        .I1(sw[1]),
        .I2(state[2]),
        .I3(sw[0]),
        .I4(\FSM_sequential_state[2]_i_2_n_0 ),
        .I5(state[1]),
        .O(\FSM_sequential_state[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h0F80)) 
    \FSM_sequential_state[2]_i_1 
       (.I0(state[0]),
        .I1(state[1]),
        .I2(\FSM_sequential_state[2]_i_2_n_0 ),
        .I3(state[2]),
        .O(\FSM_sequential_state[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000F0FF8F8FF0F)) 
    \FSM_sequential_state[2]_i_2 
       (.I0(CO),
        .I1(\FSM_sequential_state_reg[2]_i_4_n_3 ),
        .I2(state[1]),
        .I3(\FSM_sequential_state_reg[2]_0 ),
        .I4(state[0]),
        .I5(state[2]),
        .O(\FSM_sequential_state[2]_i_2_n_0 ));
  (* FSM_ENCODED_STATES = "waittrigger:001,inc_count:100,storebuffer:011,waitaudio:010,reset:000" *) 
  FDRE \FSM_sequential_state_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[0]_i_1_n_0 ),
        .Q(state[0]),
        .R(\FSM_sequential_state_reg[0]_1 ));
  (* FSM_ENCODED_STATES = "waittrigger:001,inc_count:100,storebuffer:011,waitaudio:010,reset:000" *) 
  FDRE \FSM_sequential_state_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1]_i_1_n_0 ),
        .Q(state[1]),
        .R(\FSM_sequential_state_reg[0]_1 ));
  (* FSM_ENCODED_STATES = "waittrigger:001,inc_count:100,storebuffer:011,waitaudio:010,reset:000" *) 
  FDRE \FSM_sequential_state_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\FSM_sequential_state[2]_i_1_n_0 ),
        .Q(state[2]),
        .R(\FSM_sequential_state_reg[0]_1 ));
  CARRY4 \FSM_sequential_state_reg[2]_i_4 
       (.CI(1'b0),
        .CO({\NLW_FSM_sequential_state_reg[2]_i_4_CO_UNCONNECTED [3:1],\FSM_sequential_state_reg[2]_i_4_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_FSM_sequential_state_reg[2]_i_4_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  LUT3 #(
    .INIT(8'h40)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl_i_1 
       (.I0(state[2]),
        .I1(state[0]),
        .I2(state[1]),
        .O(cw));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h0301FFFF)) 
    \tempCnt[9]_i_1 
       (.I0(state[2]),
        .I1(state[0]),
        .I2(state[1]),
        .I3(sw[0]),
        .I4(s00_axi_aresetn),
        .O(SS));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0100)) 
    \tempCnt[9]_i_2 
       (.I0(sw[0]),
        .I1(state[1]),
        .I2(state[0]),
        .I3(state[2]),
        .O(E));
endmodule

(* CHECK_LICENSE_TYPE = "my_oscope2_ip_0,my_oscope2_ip_v1_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "my_oscope2_ip_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 32, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 7, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input [6:0]s00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [6:0]s00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) input s00_axi_rready;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input s00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire s00_axi_aclk;
  wire [6:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [6:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_oscope2_ip_v1_0 U0
       (.S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[6:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[6:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_oscope2_ip_v1_0
   (S_AXI_WREADY,
    S_AXI_AWREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    s00_axi_aresetn,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_arvalid,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input s00_axi_aresetn;
  input s00_axi_aclk;
  input [4:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [4:0]s00_axi_araddr;
  input [3:0]s00_axi_wstrb;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input s00_axi_arvalid;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire aw_en_i_1_n_0;
  wire axi_bvalid_i_1_n_0;
  wire axi_rvalid_i_1_n_0;
  wire my_oscope2_ip_v1_0_S00_AXI_inst_n_4;
  wire s00_axi_aclk;
  wire [4:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [4:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  LUT6 #(
    .INIT(64'hF8F8F8F808F8F8F8)) 
    aw_en_i_1
       (.I0(s00_axi_bvalid),
        .I1(s00_axi_bready),
        .I2(my_oscope2_ip_v1_0_S00_AXI_inst_n_4),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_awvalid),
        .I5(S_AXI_AWREADY),
        .O(aw_en_i_1_n_0));
  LUT6 #(
    .INIT(64'h55555555C0000000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_bready),
        .I1(S_AXI_WREADY),
        .I2(s00_axi_awvalid),
        .I3(s00_axi_wvalid),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_oscope2_ip_v1_0_S00_AXI my_oscope2_ip_v1_0_S00_AXI_inst
       (.aw_en_reg_0(my_oscope2_ip_v1_0_S00_AXI_inst_n_4),
        .aw_en_reg_1(aw_en_i_1_n_0),
        .axi_arready_reg_0(S_AXI_ARREADY),
        .axi_awready_reg_0(S_AXI_AWREADY),
        .axi_bvalid_reg_0(axi_bvalid_i_1_n_0),
        .axi_rvalid_reg_0(axi_rvalid_i_1_n_0),
        .axi_wready_reg_0(S_AXI_WREADY),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_my_oscope2_ip_v1_0_S00_AXI
   (axi_wready_reg_0,
    axi_awready_reg_0,
    axi_arready_reg_0,
    s00_axi_bvalid,
    aw_en_reg_0,
    s00_axi_rvalid,
    s00_axi_rdata,
    s00_axi_aclk,
    axi_bvalid_reg_0,
    aw_en_reg_1,
    axi_rvalid_reg_0,
    s00_axi_aresetn,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_arvalid);
  output axi_wready_reg_0;
  output axi_awready_reg_0;
  output axi_arready_reg_0;
  output s00_axi_bvalid;
  output aw_en_reg_0;
  output s00_axi_rvalid;
  output [31:0]s00_axi_rdata;
  input s00_axi_aclk;
  input axi_bvalid_reg_0;
  input aw_en_reg_1;
  input axi_rvalid_reg_0;
  input s00_axi_aresetn;
  input [4:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [4:0]s00_axi_araddr;
  input [3:0]s00_axi_wstrb;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input s00_axi_arvalid;

  wire RST;
  wire aw_en_reg_0;
  wire aw_en_reg_1;
  wire axi_arready0;
  wire axi_arready_reg_0;
  wire axi_awready0;
  wire axi_awready_reg_0;
  wire axi_bvalid_reg_0;
  wire \axi_rdata[0]_i_5_n_0 ;
  wire \axi_rdata[0]_i_6_n_0 ;
  wire \axi_rdata[0]_i_7_n_0 ;
  wire \axi_rdata[0]_i_8_n_0 ;
  wire \axi_rdata[0]_i_9_n_0 ;
  wire \axi_rdata[10]_i_4_n_0 ;
  wire \axi_rdata[10]_i_5_n_0 ;
  wire \axi_rdata[10]_i_6_n_0 ;
  wire \axi_rdata[10]_i_7_n_0 ;
  wire \axi_rdata[10]_i_8_n_0 ;
  wire \axi_rdata[11]_i_4_n_0 ;
  wire \axi_rdata[11]_i_5_n_0 ;
  wire \axi_rdata[11]_i_6_n_0 ;
  wire \axi_rdata[11]_i_7_n_0 ;
  wire \axi_rdata[11]_i_8_n_0 ;
  wire \axi_rdata[12]_i_4_n_0 ;
  wire \axi_rdata[12]_i_5_n_0 ;
  wire \axi_rdata[12]_i_6_n_0 ;
  wire \axi_rdata[12]_i_7_n_0 ;
  wire \axi_rdata[12]_i_8_n_0 ;
  wire \axi_rdata[13]_i_4_n_0 ;
  wire \axi_rdata[13]_i_5_n_0 ;
  wire \axi_rdata[13]_i_6_n_0 ;
  wire \axi_rdata[13]_i_7_n_0 ;
  wire \axi_rdata[13]_i_8_n_0 ;
  wire \axi_rdata[14]_i_4_n_0 ;
  wire \axi_rdata[14]_i_5_n_0 ;
  wire \axi_rdata[14]_i_6_n_0 ;
  wire \axi_rdata[14]_i_7_n_0 ;
  wire \axi_rdata[14]_i_8_n_0 ;
  wire \axi_rdata[15]_i_4_n_0 ;
  wire \axi_rdata[15]_i_5_n_0 ;
  wire \axi_rdata[15]_i_6_n_0 ;
  wire \axi_rdata[15]_i_7_n_0 ;
  wire \axi_rdata[15]_i_8_n_0 ;
  wire \axi_rdata[16]_i_4_n_0 ;
  wire \axi_rdata[16]_i_5_n_0 ;
  wire \axi_rdata[16]_i_6_n_0 ;
  wire \axi_rdata[16]_i_7_n_0 ;
  wire \axi_rdata[16]_i_8_n_0 ;
  wire \axi_rdata[17]_i_4_n_0 ;
  wire \axi_rdata[17]_i_5_n_0 ;
  wire \axi_rdata[17]_i_6_n_0 ;
  wire \axi_rdata[17]_i_7_n_0 ;
  wire \axi_rdata[17]_i_8_n_0 ;
  wire \axi_rdata[18]_i_4_n_0 ;
  wire \axi_rdata[18]_i_5_n_0 ;
  wire \axi_rdata[18]_i_6_n_0 ;
  wire \axi_rdata[18]_i_7_n_0 ;
  wire \axi_rdata[18]_i_8_n_0 ;
  wire \axi_rdata[19]_i_4_n_0 ;
  wire \axi_rdata[19]_i_5_n_0 ;
  wire \axi_rdata[19]_i_6_n_0 ;
  wire \axi_rdata[19]_i_7_n_0 ;
  wire \axi_rdata[19]_i_8_n_0 ;
  wire \axi_rdata[1]_i_4_n_0 ;
  wire \axi_rdata[1]_i_5_n_0 ;
  wire \axi_rdata[1]_i_6_n_0 ;
  wire \axi_rdata[1]_i_7_n_0 ;
  wire \axi_rdata[1]_i_8_n_0 ;
  wire \axi_rdata[20]_i_4_n_0 ;
  wire \axi_rdata[20]_i_5_n_0 ;
  wire \axi_rdata[20]_i_6_n_0 ;
  wire \axi_rdata[20]_i_7_n_0 ;
  wire \axi_rdata[20]_i_8_n_0 ;
  wire \axi_rdata[21]_i_4_n_0 ;
  wire \axi_rdata[21]_i_5_n_0 ;
  wire \axi_rdata[21]_i_6_n_0 ;
  wire \axi_rdata[21]_i_7_n_0 ;
  wire \axi_rdata[21]_i_8_n_0 ;
  wire \axi_rdata[22]_i_4_n_0 ;
  wire \axi_rdata[22]_i_5_n_0 ;
  wire \axi_rdata[22]_i_6_n_0 ;
  wire \axi_rdata[22]_i_7_n_0 ;
  wire \axi_rdata[22]_i_8_n_0 ;
  wire \axi_rdata[23]_i_4_n_0 ;
  wire \axi_rdata[23]_i_5_n_0 ;
  wire \axi_rdata[23]_i_6_n_0 ;
  wire \axi_rdata[23]_i_7_n_0 ;
  wire \axi_rdata[23]_i_8_n_0 ;
  wire \axi_rdata[24]_i_4_n_0 ;
  wire \axi_rdata[24]_i_5_n_0 ;
  wire \axi_rdata[24]_i_6_n_0 ;
  wire \axi_rdata[24]_i_7_n_0 ;
  wire \axi_rdata[24]_i_8_n_0 ;
  wire \axi_rdata[25]_i_4_n_0 ;
  wire \axi_rdata[25]_i_5_n_0 ;
  wire \axi_rdata[25]_i_6_n_0 ;
  wire \axi_rdata[25]_i_7_n_0 ;
  wire \axi_rdata[25]_i_8_n_0 ;
  wire \axi_rdata[26]_i_4_n_0 ;
  wire \axi_rdata[26]_i_5_n_0 ;
  wire \axi_rdata[26]_i_6_n_0 ;
  wire \axi_rdata[26]_i_7_n_0 ;
  wire \axi_rdata[26]_i_8_n_0 ;
  wire \axi_rdata[27]_i_4_n_0 ;
  wire \axi_rdata[27]_i_5_n_0 ;
  wire \axi_rdata[27]_i_6_n_0 ;
  wire \axi_rdata[27]_i_7_n_0 ;
  wire \axi_rdata[27]_i_8_n_0 ;
  wire \axi_rdata[28]_i_4_n_0 ;
  wire \axi_rdata[28]_i_5_n_0 ;
  wire \axi_rdata[28]_i_6_n_0 ;
  wire \axi_rdata[28]_i_7_n_0 ;
  wire \axi_rdata[28]_i_8_n_0 ;
  wire \axi_rdata[29]_i_4_n_0 ;
  wire \axi_rdata[29]_i_5_n_0 ;
  wire \axi_rdata[29]_i_6_n_0 ;
  wire \axi_rdata[29]_i_7_n_0 ;
  wire \axi_rdata[29]_i_8_n_0 ;
  wire \axi_rdata[2]_i_3_n_0 ;
  wire \axi_rdata[2]_i_6_n_0 ;
  wire \axi_rdata[2]_i_7_n_0 ;
  wire \axi_rdata[2]_i_8_n_0 ;
  wire \axi_rdata[2]_i_9_n_0 ;
  wire \axi_rdata[30]_i_4_n_0 ;
  wire \axi_rdata[30]_i_5_n_0 ;
  wire \axi_rdata[30]_i_6_n_0 ;
  wire \axi_rdata[30]_i_7_n_0 ;
  wire \axi_rdata[30]_i_8_n_0 ;
  wire \axi_rdata[31]_i_5_n_0 ;
  wire \axi_rdata[31]_i_6_n_0 ;
  wire \axi_rdata[31]_i_7_n_0 ;
  wire \axi_rdata[31]_i_8_n_0 ;
  wire \axi_rdata[31]_i_9_n_0 ;
  wire \axi_rdata[3]_i_3_n_0 ;
  wire \axi_rdata[3]_i_6_n_0 ;
  wire \axi_rdata[3]_i_7_n_0 ;
  wire \axi_rdata[3]_i_8_n_0 ;
  wire \axi_rdata[3]_i_9_n_0 ;
  wire \axi_rdata[4]_i_3_n_0 ;
  wire \axi_rdata[4]_i_6_n_0 ;
  wire \axi_rdata[4]_i_7_n_0 ;
  wire \axi_rdata[4]_i_8_n_0 ;
  wire \axi_rdata[4]_i_9_n_0 ;
  wire \axi_rdata[5]_i_4_n_0 ;
  wire \axi_rdata[5]_i_5_n_0 ;
  wire \axi_rdata[5]_i_6_n_0 ;
  wire \axi_rdata[5]_i_7_n_0 ;
  wire \axi_rdata[5]_i_8_n_0 ;
  wire \axi_rdata[6]_i_4_n_0 ;
  wire \axi_rdata[6]_i_5_n_0 ;
  wire \axi_rdata[6]_i_6_n_0 ;
  wire \axi_rdata[6]_i_7_n_0 ;
  wire \axi_rdata[6]_i_8_n_0 ;
  wire \axi_rdata[6]_i_9_n_0 ;
  wire \axi_rdata[7]_i_10_n_0 ;
  wire \axi_rdata[7]_i_3_n_0 ;
  wire \axi_rdata[7]_i_4_n_0 ;
  wire \axi_rdata[7]_i_7_n_0 ;
  wire \axi_rdata[7]_i_8_n_0 ;
  wire \axi_rdata[7]_i_9_n_0 ;
  wire \axi_rdata[8]_i_4_n_0 ;
  wire \axi_rdata[8]_i_5_n_0 ;
  wire \axi_rdata[8]_i_6_n_0 ;
  wire \axi_rdata[8]_i_7_n_0 ;
  wire \axi_rdata[8]_i_8_n_0 ;
  wire \axi_rdata[8]_i_9_n_0 ;
  wire \axi_rdata[9]_i_4_n_0 ;
  wire \axi_rdata[9]_i_5_n_0 ;
  wire \axi_rdata[9]_i_6_n_0 ;
  wire \axi_rdata[9]_i_7_n_0 ;
  wire \axi_rdata[9]_i_8_n_0 ;
  wire \axi_rdata_reg[0]_i_2_n_0 ;
  wire \axi_rdata_reg[0]_i_3_n_0 ;
  wire \axi_rdata_reg[10]_i_2_n_0 ;
  wire \axi_rdata_reg[10]_i_3_n_0 ;
  wire \axi_rdata_reg[11]_i_2_n_0 ;
  wire \axi_rdata_reg[11]_i_3_n_0 ;
  wire \axi_rdata_reg[12]_i_2_n_0 ;
  wire \axi_rdata_reg[12]_i_3_n_0 ;
  wire \axi_rdata_reg[13]_i_2_n_0 ;
  wire \axi_rdata_reg[13]_i_3_n_0 ;
  wire \axi_rdata_reg[14]_i_2_n_0 ;
  wire \axi_rdata_reg[14]_i_3_n_0 ;
  wire \axi_rdata_reg[15]_i_2_n_0 ;
  wire \axi_rdata_reg[15]_i_3_n_0 ;
  wire \axi_rdata_reg[16]_i_2_n_0 ;
  wire \axi_rdata_reg[16]_i_3_n_0 ;
  wire \axi_rdata_reg[17]_i_2_n_0 ;
  wire \axi_rdata_reg[17]_i_3_n_0 ;
  wire \axi_rdata_reg[18]_i_2_n_0 ;
  wire \axi_rdata_reg[18]_i_3_n_0 ;
  wire \axi_rdata_reg[19]_i_2_n_0 ;
  wire \axi_rdata_reg[19]_i_3_n_0 ;
  wire \axi_rdata_reg[1]_i_2_n_0 ;
  wire \axi_rdata_reg[1]_i_3_n_0 ;
  wire \axi_rdata_reg[20]_i_2_n_0 ;
  wire \axi_rdata_reg[20]_i_3_n_0 ;
  wire \axi_rdata_reg[21]_i_2_n_0 ;
  wire \axi_rdata_reg[21]_i_3_n_0 ;
  wire \axi_rdata_reg[22]_i_2_n_0 ;
  wire \axi_rdata_reg[22]_i_3_n_0 ;
  wire \axi_rdata_reg[23]_i_2_n_0 ;
  wire \axi_rdata_reg[23]_i_3_n_0 ;
  wire \axi_rdata_reg[24]_i_2_n_0 ;
  wire \axi_rdata_reg[24]_i_3_n_0 ;
  wire \axi_rdata_reg[25]_i_2_n_0 ;
  wire \axi_rdata_reg[25]_i_3_n_0 ;
  wire \axi_rdata_reg[26]_i_2_n_0 ;
  wire \axi_rdata_reg[26]_i_3_n_0 ;
  wire \axi_rdata_reg[27]_i_2_n_0 ;
  wire \axi_rdata_reg[27]_i_3_n_0 ;
  wire \axi_rdata_reg[28]_i_2_n_0 ;
  wire \axi_rdata_reg[28]_i_3_n_0 ;
  wire \axi_rdata_reg[29]_i_2_n_0 ;
  wire \axi_rdata_reg[29]_i_3_n_0 ;
  wire \axi_rdata_reg[2]_i_2_n_0 ;
  wire \axi_rdata_reg[2]_i_4_n_0 ;
  wire \axi_rdata_reg[2]_i_5_n_0 ;
  wire \axi_rdata_reg[30]_i_2_n_0 ;
  wire \axi_rdata_reg[30]_i_3_n_0 ;
  wire \axi_rdata_reg[31]_i_3_n_0 ;
  wire \axi_rdata_reg[31]_i_4_n_0 ;
  wire \axi_rdata_reg[3]_i_2_n_0 ;
  wire \axi_rdata_reg[3]_i_4_n_0 ;
  wire \axi_rdata_reg[3]_i_5_n_0 ;
  wire \axi_rdata_reg[4]_i_2_n_0 ;
  wire \axi_rdata_reg[4]_i_4_n_0 ;
  wire \axi_rdata_reg[4]_i_5_n_0 ;
  wire \axi_rdata_reg[5]_i_2_n_0 ;
  wire \axi_rdata_reg[5]_i_3_n_0 ;
  wire \axi_rdata_reg[6]_i_2_n_0 ;
  wire \axi_rdata_reg[6]_i_3_n_0 ;
  wire \axi_rdata_reg[7]_i_2_n_0 ;
  wire \axi_rdata_reg[7]_i_5_n_0 ;
  wire \axi_rdata_reg[7]_i_6_n_0 ;
  wire \axi_rdata_reg[8]_i_2_n_0 ;
  wire \axi_rdata_reg[8]_i_3_n_0 ;
  wire \axi_rdata_reg[9]_i_2_n_0 ;
  wire \axi_rdata_reg[9]_i_3_n_0 ;
  wire axi_rvalid_reg_0;
  wire axi_wready0;
  wire axi_wready_reg_0;
  wire control_n_0;
  wire [2:2]cw;
  wire datapath_n_3;
  wire datapath_n_4;
  wire datapath_n_6;
  wire \lab2_BRAMCntr/tempCnt0 ;
  wire [4:0]p_0_in;
  wire [31:7]p_1_in;
  wire [31:0]reg_data_out;
  wire s00_axi_aclk;
  wire [4:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [4:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [4:0]sel0;
  wire [31:0]slv_reg12;
  wire \slv_reg12[31]_i_2_n_0 ;
  wire [31:0]slv_reg13;
  wire \slv_reg13[15]_i_1_n_0 ;
  wire \slv_reg13[23]_i_1_n_0 ;
  wire \slv_reg13[31]_i_1_n_0 ;
  wire \slv_reg13[7]_i_1_n_0 ;
  wire [31:0]slv_reg14;
  wire \slv_reg14[15]_i_1_n_0 ;
  wire \slv_reg14[23]_i_1_n_0 ;
  wire \slv_reg14[31]_i_1_n_0 ;
  wire \slv_reg14[7]_i_1_n_0 ;
  wire [31:0]slv_reg15;
  wire \slv_reg15[15]_i_1_n_0 ;
  wire \slv_reg15[23]_i_1_n_0 ;
  wire \slv_reg15[31]_i_1_n_0 ;
  wire \slv_reg15[7]_i_1_n_0 ;
  wire [31:0]slv_reg16;
  wire \slv_reg16[15]_i_1_n_0 ;
  wire \slv_reg16[23]_i_1_n_0 ;
  wire \slv_reg16[31]_i_1_n_0 ;
  wire \slv_reg16[31]_i_2_n_0 ;
  wire \slv_reg16[7]_i_1_n_0 ;
  wire [31:0]slv_reg17;
  wire \slv_reg17[15]_i_1_n_0 ;
  wire \slv_reg17[23]_i_1_n_0 ;
  wire \slv_reg17[31]_i_1_n_0 ;
  wire \slv_reg17[7]_i_1_n_0 ;
  wire [31:0]slv_reg18;
  wire \slv_reg18[15]_i_1_n_0 ;
  wire \slv_reg18[23]_i_1_n_0 ;
  wire \slv_reg18[31]_i_1_n_0 ;
  wire \slv_reg18[7]_i_1_n_0 ;
  wire [31:0]slv_reg19;
  wire \slv_reg19[15]_i_1_n_0 ;
  wire \slv_reg19[23]_i_1_n_0 ;
  wire \slv_reg19[31]_i_1_n_0 ;
  wire \slv_reg19[7]_i_1_n_0 ;
  wire [31:0]slv_reg20;
  wire \slv_reg20[15]_i_1_n_0 ;
  wire \slv_reg20[23]_i_1_n_0 ;
  wire \slv_reg20[31]_i_1_n_0 ;
  wire \slv_reg20[31]_i_2_n_0 ;
  wire \slv_reg20[7]_i_1_n_0 ;
  wire [31:0]slv_reg21;
  wire \slv_reg21[15]_i_1_n_0 ;
  wire \slv_reg21[23]_i_1_n_0 ;
  wire \slv_reg21[31]_i_1_n_0 ;
  wire \slv_reg21[7]_i_1_n_0 ;
  wire [31:0]slv_reg22;
  wire \slv_reg22[15]_i_1_n_0 ;
  wire \slv_reg22[23]_i_1_n_0 ;
  wire \slv_reg22[31]_i_1_n_0 ;
  wire \slv_reg22[7]_i_1_n_0 ;
  wire [31:0]slv_reg23;
  wire \slv_reg23[15]_i_1_n_0 ;
  wire \slv_reg23[23]_i_1_n_0 ;
  wire \slv_reg23[31]_i_1_n_0 ;
  wire \slv_reg23[7]_i_1_n_0 ;
  wire [31:0]slv_reg24;
  wire \slv_reg24[15]_i_1_n_0 ;
  wire \slv_reg24[23]_i_1_n_0 ;
  wire \slv_reg24[31]_i_1_n_0 ;
  wire \slv_reg24[7]_i_1_n_0 ;
  wire [31:0]slv_reg25;
  wire \slv_reg25[15]_i_1_n_0 ;
  wire \slv_reg25[23]_i_1_n_0 ;
  wire \slv_reg25[31]_i_1_n_0 ;
  wire \slv_reg25[7]_i_1_n_0 ;
  wire [31:0]slv_reg26;
  wire \slv_reg26[15]_i_1_n_0 ;
  wire \slv_reg26[23]_i_1_n_0 ;
  wire \slv_reg26[31]_i_1_n_0 ;
  wire \slv_reg26[7]_i_1_n_0 ;
  wire [31:0]slv_reg27;
  wire \slv_reg27[15]_i_1_n_0 ;
  wire \slv_reg27[23]_i_1_n_0 ;
  wire \slv_reg27[31]_i_1_n_0 ;
  wire \slv_reg27[7]_i_1_n_0 ;
  wire [31:0]slv_reg28;
  wire \slv_reg28[15]_i_1_n_0 ;
  wire \slv_reg28[23]_i_1_n_0 ;
  wire \slv_reg28[31]_i_1_n_0 ;
  wire \slv_reg28[7]_i_1_n_0 ;
  wire [31:0]slv_reg29;
  wire \slv_reg29[15]_i_1_n_0 ;
  wire \slv_reg29[23]_i_1_n_0 ;
  wire \slv_reg29[31]_i_1_n_0 ;
  wire \slv_reg29[7]_i_1_n_0 ;
  wire [31:0]slv_reg30;
  wire \slv_reg30[15]_i_1_n_0 ;
  wire \slv_reg30[23]_i_1_n_0 ;
  wire \slv_reg30[31]_i_1_n_0 ;
  wire \slv_reg30[7]_i_1_n_0 ;
  wire [31:0]slv_reg31;
  wire \slv_reg31[15]_i_1_n_0 ;
  wire \slv_reg31[23]_i_1_n_0 ;
  wire \slv_reg31[31]_i_1_n_0 ;
  wire \slv_reg31[7]_i_1_n_0 ;
  wire slv_reg_rden;
  wire [2:1]sw;

  FDSE aw_en_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(aw_en_reg_1),
        .Q(aw_en_reg_0),
        .S(RST));
  FDSE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[0]),
        .Q(sel0[0]),
        .S(RST));
  FDSE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[1]),
        .Q(sel0[1]),
        .S(RST));
  FDSE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[2]),
        .Q(sel0[2]),
        .S(RST));
  FDSE \axi_araddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[3]),
        .Q(sel0[3]),
        .S(RST));
  FDSE \axi_araddr_reg[6] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[4]),
        .Q(sel0[4]),
        .S(RST));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(axi_arready_reg_0),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(axi_arready_reg_0),
        .R(RST));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[0]),
        .Q(p_0_in[0]),
        .R(RST));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[1]),
        .Q(p_0_in[1]),
        .R(RST));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[2]),
        .Q(p_0_in[2]),
        .R(RST));
  FDRE \axi_awaddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[3]),
        .Q(p_0_in[3]),
        .R(RST));
  FDRE \axi_awaddr_reg[6] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[4]),
        .Q(p_0_in[4]),
        .R(RST));
  LUT4 #(
    .INIT(16'h0080)) 
    axi_awready_i_2
       (.I0(aw_en_reg_0),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(axi_awready_reg_0),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(axi_awready_reg_0),
        .R(RST));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_reg_0),
        .Q(s00_axi_bvalid),
        .R(RST));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_5 
       (.I0(slv_reg27[0]),
        .I1(slv_reg26[0]),
        .I2(sel0[1]),
        .I3(slv_reg25[0]),
        .I4(sel0[0]),
        .I5(slv_reg24[0]),
        .O(\axi_rdata[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_6 
       (.I0(slv_reg31[0]),
        .I1(slv_reg30[0]),
        .I2(sel0[1]),
        .I3(slv_reg29[0]),
        .I4(sel0[0]),
        .I5(slv_reg28[0]),
        .O(\axi_rdata[0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_7 
       (.I0(slv_reg19[0]),
        .I1(slv_reg18[0]),
        .I2(sel0[1]),
        .I3(slv_reg17[0]),
        .I4(sel0[0]),
        .I5(slv_reg16[0]),
        .O(\axi_rdata[0]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_8 
       (.I0(slv_reg23[0]),
        .I1(slv_reg22[0]),
        .I2(sel0[1]),
        .I3(slv_reg21[0]),
        .I4(sel0[0]),
        .I5(slv_reg20[0]),
        .O(\axi_rdata[0]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_9 
       (.I0(slv_reg15[0]),
        .I1(slv_reg14[0]),
        .I2(sel0[1]),
        .I3(slv_reg13[0]),
        .I4(sel0[0]),
        .I5(slv_reg12[0]),
        .O(\axi_rdata[0]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[10]_i_1 
       (.I0(\axi_rdata_reg[10]_i_2_n_0 ),
        .I1(\axi_rdata_reg[10]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[10]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[10]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_4 
       (.I0(slv_reg15[10]),
        .I1(slv_reg14[10]),
        .I2(sel0[1]),
        .I3(slv_reg13[10]),
        .I4(sel0[0]),
        .I5(slv_reg12[10]),
        .O(\axi_rdata[10]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_5 
       (.I0(slv_reg27[10]),
        .I1(slv_reg26[10]),
        .I2(sel0[1]),
        .I3(slv_reg25[10]),
        .I4(sel0[0]),
        .I5(slv_reg24[10]),
        .O(\axi_rdata[10]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_6 
       (.I0(slv_reg31[10]),
        .I1(slv_reg30[10]),
        .I2(sel0[1]),
        .I3(slv_reg29[10]),
        .I4(sel0[0]),
        .I5(slv_reg28[10]),
        .O(\axi_rdata[10]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_7 
       (.I0(slv_reg19[10]),
        .I1(slv_reg18[10]),
        .I2(sel0[1]),
        .I3(slv_reg17[10]),
        .I4(sel0[0]),
        .I5(slv_reg16[10]),
        .O(\axi_rdata[10]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_8 
       (.I0(slv_reg23[10]),
        .I1(slv_reg22[10]),
        .I2(sel0[1]),
        .I3(slv_reg21[10]),
        .I4(sel0[0]),
        .I5(slv_reg20[10]),
        .O(\axi_rdata[10]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[11]_i_1 
       (.I0(\axi_rdata_reg[11]_i_2_n_0 ),
        .I1(\axi_rdata_reg[11]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[11]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[11]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_4 
       (.I0(slv_reg15[11]),
        .I1(slv_reg14[11]),
        .I2(sel0[1]),
        .I3(slv_reg13[11]),
        .I4(sel0[0]),
        .I5(slv_reg12[11]),
        .O(\axi_rdata[11]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_5 
       (.I0(slv_reg27[11]),
        .I1(slv_reg26[11]),
        .I2(sel0[1]),
        .I3(slv_reg25[11]),
        .I4(sel0[0]),
        .I5(slv_reg24[11]),
        .O(\axi_rdata[11]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_6 
       (.I0(slv_reg31[11]),
        .I1(slv_reg30[11]),
        .I2(sel0[1]),
        .I3(slv_reg29[11]),
        .I4(sel0[0]),
        .I5(slv_reg28[11]),
        .O(\axi_rdata[11]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_7 
       (.I0(slv_reg19[11]),
        .I1(slv_reg18[11]),
        .I2(sel0[1]),
        .I3(slv_reg17[11]),
        .I4(sel0[0]),
        .I5(slv_reg16[11]),
        .O(\axi_rdata[11]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_8 
       (.I0(slv_reg23[11]),
        .I1(slv_reg22[11]),
        .I2(sel0[1]),
        .I3(slv_reg21[11]),
        .I4(sel0[0]),
        .I5(slv_reg20[11]),
        .O(\axi_rdata[11]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[12]_i_1 
       (.I0(\axi_rdata_reg[12]_i_2_n_0 ),
        .I1(\axi_rdata_reg[12]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[12]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[12]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_4 
       (.I0(slv_reg15[12]),
        .I1(slv_reg14[12]),
        .I2(sel0[1]),
        .I3(slv_reg13[12]),
        .I4(sel0[0]),
        .I5(slv_reg12[12]),
        .O(\axi_rdata[12]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_5 
       (.I0(slv_reg27[12]),
        .I1(slv_reg26[12]),
        .I2(sel0[1]),
        .I3(slv_reg25[12]),
        .I4(sel0[0]),
        .I5(slv_reg24[12]),
        .O(\axi_rdata[12]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_6 
       (.I0(slv_reg31[12]),
        .I1(slv_reg30[12]),
        .I2(sel0[1]),
        .I3(slv_reg29[12]),
        .I4(sel0[0]),
        .I5(slv_reg28[12]),
        .O(\axi_rdata[12]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_7 
       (.I0(slv_reg19[12]),
        .I1(slv_reg18[12]),
        .I2(sel0[1]),
        .I3(slv_reg17[12]),
        .I4(sel0[0]),
        .I5(slv_reg16[12]),
        .O(\axi_rdata[12]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_8 
       (.I0(slv_reg23[12]),
        .I1(slv_reg22[12]),
        .I2(sel0[1]),
        .I3(slv_reg21[12]),
        .I4(sel0[0]),
        .I5(slv_reg20[12]),
        .O(\axi_rdata[12]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[13]_i_1 
       (.I0(\axi_rdata_reg[13]_i_2_n_0 ),
        .I1(\axi_rdata_reg[13]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[13]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[13]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_4 
       (.I0(slv_reg15[13]),
        .I1(slv_reg14[13]),
        .I2(sel0[1]),
        .I3(slv_reg13[13]),
        .I4(sel0[0]),
        .I5(slv_reg12[13]),
        .O(\axi_rdata[13]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_5 
       (.I0(slv_reg27[13]),
        .I1(slv_reg26[13]),
        .I2(sel0[1]),
        .I3(slv_reg25[13]),
        .I4(sel0[0]),
        .I5(slv_reg24[13]),
        .O(\axi_rdata[13]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_6 
       (.I0(slv_reg31[13]),
        .I1(slv_reg30[13]),
        .I2(sel0[1]),
        .I3(slv_reg29[13]),
        .I4(sel0[0]),
        .I5(slv_reg28[13]),
        .O(\axi_rdata[13]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_7 
       (.I0(slv_reg19[13]),
        .I1(slv_reg18[13]),
        .I2(sel0[1]),
        .I3(slv_reg17[13]),
        .I4(sel0[0]),
        .I5(slv_reg16[13]),
        .O(\axi_rdata[13]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_8 
       (.I0(slv_reg23[13]),
        .I1(slv_reg22[13]),
        .I2(sel0[1]),
        .I3(slv_reg21[13]),
        .I4(sel0[0]),
        .I5(slv_reg20[13]),
        .O(\axi_rdata[13]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[14]_i_1 
       (.I0(\axi_rdata_reg[14]_i_2_n_0 ),
        .I1(\axi_rdata_reg[14]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[14]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[14]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_4 
       (.I0(slv_reg15[14]),
        .I1(slv_reg14[14]),
        .I2(sel0[1]),
        .I3(slv_reg13[14]),
        .I4(sel0[0]),
        .I5(slv_reg12[14]),
        .O(\axi_rdata[14]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_5 
       (.I0(slv_reg27[14]),
        .I1(slv_reg26[14]),
        .I2(sel0[1]),
        .I3(slv_reg25[14]),
        .I4(sel0[0]),
        .I5(slv_reg24[14]),
        .O(\axi_rdata[14]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_6 
       (.I0(slv_reg31[14]),
        .I1(slv_reg30[14]),
        .I2(sel0[1]),
        .I3(slv_reg29[14]),
        .I4(sel0[0]),
        .I5(slv_reg28[14]),
        .O(\axi_rdata[14]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_7 
       (.I0(slv_reg19[14]),
        .I1(slv_reg18[14]),
        .I2(sel0[1]),
        .I3(slv_reg17[14]),
        .I4(sel0[0]),
        .I5(slv_reg16[14]),
        .O(\axi_rdata[14]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_8 
       (.I0(slv_reg23[14]),
        .I1(slv_reg22[14]),
        .I2(sel0[1]),
        .I3(slv_reg21[14]),
        .I4(sel0[0]),
        .I5(slv_reg20[14]),
        .O(\axi_rdata[14]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[15]_i_1 
       (.I0(\axi_rdata_reg[15]_i_2_n_0 ),
        .I1(\axi_rdata_reg[15]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[15]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[15]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_4 
       (.I0(slv_reg15[15]),
        .I1(slv_reg14[15]),
        .I2(sel0[1]),
        .I3(slv_reg13[15]),
        .I4(sel0[0]),
        .I5(slv_reg12[15]),
        .O(\axi_rdata[15]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_5 
       (.I0(slv_reg27[15]),
        .I1(slv_reg26[15]),
        .I2(sel0[1]),
        .I3(slv_reg25[15]),
        .I4(sel0[0]),
        .I5(slv_reg24[15]),
        .O(\axi_rdata[15]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_6 
       (.I0(slv_reg31[15]),
        .I1(slv_reg30[15]),
        .I2(sel0[1]),
        .I3(slv_reg29[15]),
        .I4(sel0[0]),
        .I5(slv_reg28[15]),
        .O(\axi_rdata[15]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_7 
       (.I0(slv_reg19[15]),
        .I1(slv_reg18[15]),
        .I2(sel0[1]),
        .I3(slv_reg17[15]),
        .I4(sel0[0]),
        .I5(slv_reg16[15]),
        .O(\axi_rdata[15]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_8 
       (.I0(slv_reg23[15]),
        .I1(slv_reg22[15]),
        .I2(sel0[1]),
        .I3(slv_reg21[15]),
        .I4(sel0[0]),
        .I5(slv_reg20[15]),
        .O(\axi_rdata[15]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[16]_i_1 
       (.I0(\axi_rdata_reg[16]_i_2_n_0 ),
        .I1(\axi_rdata_reg[16]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[16]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[16]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_4 
       (.I0(slv_reg15[16]),
        .I1(slv_reg14[16]),
        .I2(sel0[1]),
        .I3(slv_reg13[16]),
        .I4(sel0[0]),
        .I5(slv_reg12[16]),
        .O(\axi_rdata[16]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_5 
       (.I0(slv_reg27[16]),
        .I1(slv_reg26[16]),
        .I2(sel0[1]),
        .I3(slv_reg25[16]),
        .I4(sel0[0]),
        .I5(slv_reg24[16]),
        .O(\axi_rdata[16]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_6 
       (.I0(slv_reg31[16]),
        .I1(slv_reg30[16]),
        .I2(sel0[1]),
        .I3(slv_reg29[16]),
        .I4(sel0[0]),
        .I5(slv_reg28[16]),
        .O(\axi_rdata[16]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_7 
       (.I0(slv_reg19[16]),
        .I1(slv_reg18[16]),
        .I2(sel0[1]),
        .I3(slv_reg17[16]),
        .I4(sel0[0]),
        .I5(slv_reg16[16]),
        .O(\axi_rdata[16]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_8 
       (.I0(slv_reg23[16]),
        .I1(slv_reg22[16]),
        .I2(sel0[1]),
        .I3(slv_reg21[16]),
        .I4(sel0[0]),
        .I5(slv_reg20[16]),
        .O(\axi_rdata[16]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[17]_i_1 
       (.I0(\axi_rdata_reg[17]_i_2_n_0 ),
        .I1(\axi_rdata_reg[17]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[17]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[17]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_4 
       (.I0(slv_reg15[17]),
        .I1(slv_reg14[17]),
        .I2(sel0[1]),
        .I3(slv_reg13[17]),
        .I4(sel0[0]),
        .I5(slv_reg12[17]),
        .O(\axi_rdata[17]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_5 
       (.I0(slv_reg27[17]),
        .I1(slv_reg26[17]),
        .I2(sel0[1]),
        .I3(slv_reg25[17]),
        .I4(sel0[0]),
        .I5(slv_reg24[17]),
        .O(\axi_rdata[17]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_6 
       (.I0(slv_reg31[17]),
        .I1(slv_reg30[17]),
        .I2(sel0[1]),
        .I3(slv_reg29[17]),
        .I4(sel0[0]),
        .I5(slv_reg28[17]),
        .O(\axi_rdata[17]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_7 
       (.I0(slv_reg19[17]),
        .I1(slv_reg18[17]),
        .I2(sel0[1]),
        .I3(slv_reg17[17]),
        .I4(sel0[0]),
        .I5(slv_reg16[17]),
        .O(\axi_rdata[17]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_8 
       (.I0(slv_reg23[17]),
        .I1(slv_reg22[17]),
        .I2(sel0[1]),
        .I3(slv_reg21[17]),
        .I4(sel0[0]),
        .I5(slv_reg20[17]),
        .O(\axi_rdata[17]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[18]_i_1 
       (.I0(\axi_rdata_reg[18]_i_2_n_0 ),
        .I1(\axi_rdata_reg[18]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[18]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[18]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_4 
       (.I0(slv_reg15[18]),
        .I1(slv_reg14[18]),
        .I2(sel0[1]),
        .I3(slv_reg13[18]),
        .I4(sel0[0]),
        .I5(slv_reg12[18]),
        .O(\axi_rdata[18]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_5 
       (.I0(slv_reg27[18]),
        .I1(slv_reg26[18]),
        .I2(sel0[1]),
        .I3(slv_reg25[18]),
        .I4(sel0[0]),
        .I5(slv_reg24[18]),
        .O(\axi_rdata[18]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_6 
       (.I0(slv_reg31[18]),
        .I1(slv_reg30[18]),
        .I2(sel0[1]),
        .I3(slv_reg29[18]),
        .I4(sel0[0]),
        .I5(slv_reg28[18]),
        .O(\axi_rdata[18]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_7 
       (.I0(slv_reg19[18]),
        .I1(slv_reg18[18]),
        .I2(sel0[1]),
        .I3(slv_reg17[18]),
        .I4(sel0[0]),
        .I5(slv_reg16[18]),
        .O(\axi_rdata[18]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_8 
       (.I0(slv_reg23[18]),
        .I1(slv_reg22[18]),
        .I2(sel0[1]),
        .I3(slv_reg21[18]),
        .I4(sel0[0]),
        .I5(slv_reg20[18]),
        .O(\axi_rdata[18]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[19]_i_1 
       (.I0(\axi_rdata_reg[19]_i_2_n_0 ),
        .I1(\axi_rdata_reg[19]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[19]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[19]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_4 
       (.I0(slv_reg15[19]),
        .I1(slv_reg14[19]),
        .I2(sel0[1]),
        .I3(slv_reg13[19]),
        .I4(sel0[0]),
        .I5(slv_reg12[19]),
        .O(\axi_rdata[19]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_5 
       (.I0(slv_reg27[19]),
        .I1(slv_reg26[19]),
        .I2(sel0[1]),
        .I3(slv_reg25[19]),
        .I4(sel0[0]),
        .I5(slv_reg24[19]),
        .O(\axi_rdata[19]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_6 
       (.I0(slv_reg31[19]),
        .I1(slv_reg30[19]),
        .I2(sel0[1]),
        .I3(slv_reg29[19]),
        .I4(sel0[0]),
        .I5(slv_reg28[19]),
        .O(\axi_rdata[19]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_7 
       (.I0(slv_reg19[19]),
        .I1(slv_reg18[19]),
        .I2(sel0[1]),
        .I3(slv_reg17[19]),
        .I4(sel0[0]),
        .I5(slv_reg16[19]),
        .O(\axi_rdata[19]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_8 
       (.I0(slv_reg23[19]),
        .I1(slv_reg22[19]),
        .I2(sel0[1]),
        .I3(slv_reg21[19]),
        .I4(sel0[0]),
        .I5(slv_reg20[19]),
        .O(\axi_rdata[19]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[1]_i_1 
       (.I0(\axi_rdata_reg[1]_i_2_n_0 ),
        .I1(\axi_rdata_reg[1]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[1]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[1]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_4 
       (.I0(slv_reg15[1]),
        .I1(slv_reg14[1]),
        .I2(sel0[1]),
        .I3(slv_reg13[1]),
        .I4(sel0[0]),
        .I5(slv_reg12[1]),
        .O(\axi_rdata[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_5 
       (.I0(slv_reg27[1]),
        .I1(slv_reg26[1]),
        .I2(sel0[1]),
        .I3(slv_reg25[1]),
        .I4(sel0[0]),
        .I5(slv_reg24[1]),
        .O(\axi_rdata[1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_6 
       (.I0(slv_reg31[1]),
        .I1(slv_reg30[1]),
        .I2(sel0[1]),
        .I3(slv_reg29[1]),
        .I4(sel0[0]),
        .I5(slv_reg28[1]),
        .O(\axi_rdata[1]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_7 
       (.I0(slv_reg19[1]),
        .I1(slv_reg18[1]),
        .I2(sel0[1]),
        .I3(slv_reg17[1]),
        .I4(sel0[0]),
        .I5(slv_reg16[1]),
        .O(\axi_rdata[1]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_8 
       (.I0(slv_reg23[1]),
        .I1(slv_reg22[1]),
        .I2(sel0[1]),
        .I3(slv_reg21[1]),
        .I4(sel0[0]),
        .I5(slv_reg20[1]),
        .O(\axi_rdata[1]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[20]_i_1 
       (.I0(\axi_rdata_reg[20]_i_2_n_0 ),
        .I1(\axi_rdata_reg[20]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[20]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[20]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_4 
       (.I0(slv_reg15[20]),
        .I1(slv_reg14[20]),
        .I2(sel0[1]),
        .I3(slv_reg13[20]),
        .I4(sel0[0]),
        .I5(slv_reg12[20]),
        .O(\axi_rdata[20]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_5 
       (.I0(slv_reg27[20]),
        .I1(slv_reg26[20]),
        .I2(sel0[1]),
        .I3(slv_reg25[20]),
        .I4(sel0[0]),
        .I5(slv_reg24[20]),
        .O(\axi_rdata[20]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_6 
       (.I0(slv_reg31[20]),
        .I1(slv_reg30[20]),
        .I2(sel0[1]),
        .I3(slv_reg29[20]),
        .I4(sel0[0]),
        .I5(slv_reg28[20]),
        .O(\axi_rdata[20]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_7 
       (.I0(slv_reg19[20]),
        .I1(slv_reg18[20]),
        .I2(sel0[1]),
        .I3(slv_reg17[20]),
        .I4(sel0[0]),
        .I5(slv_reg16[20]),
        .O(\axi_rdata[20]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_8 
       (.I0(slv_reg23[20]),
        .I1(slv_reg22[20]),
        .I2(sel0[1]),
        .I3(slv_reg21[20]),
        .I4(sel0[0]),
        .I5(slv_reg20[20]),
        .O(\axi_rdata[20]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[21]_i_1 
       (.I0(\axi_rdata_reg[21]_i_2_n_0 ),
        .I1(\axi_rdata_reg[21]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[21]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[21]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_4 
       (.I0(slv_reg15[21]),
        .I1(slv_reg14[21]),
        .I2(sel0[1]),
        .I3(slv_reg13[21]),
        .I4(sel0[0]),
        .I5(slv_reg12[21]),
        .O(\axi_rdata[21]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_5 
       (.I0(slv_reg27[21]),
        .I1(slv_reg26[21]),
        .I2(sel0[1]),
        .I3(slv_reg25[21]),
        .I4(sel0[0]),
        .I5(slv_reg24[21]),
        .O(\axi_rdata[21]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_6 
       (.I0(slv_reg31[21]),
        .I1(slv_reg30[21]),
        .I2(sel0[1]),
        .I3(slv_reg29[21]),
        .I4(sel0[0]),
        .I5(slv_reg28[21]),
        .O(\axi_rdata[21]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_7 
       (.I0(slv_reg19[21]),
        .I1(slv_reg18[21]),
        .I2(sel0[1]),
        .I3(slv_reg17[21]),
        .I4(sel0[0]),
        .I5(slv_reg16[21]),
        .O(\axi_rdata[21]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_8 
       (.I0(slv_reg23[21]),
        .I1(slv_reg22[21]),
        .I2(sel0[1]),
        .I3(slv_reg21[21]),
        .I4(sel0[0]),
        .I5(slv_reg20[21]),
        .O(\axi_rdata[21]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[22]_i_1 
       (.I0(\axi_rdata_reg[22]_i_2_n_0 ),
        .I1(\axi_rdata_reg[22]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[22]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[22]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_4 
       (.I0(slv_reg15[22]),
        .I1(slv_reg14[22]),
        .I2(sel0[1]),
        .I3(slv_reg13[22]),
        .I4(sel0[0]),
        .I5(slv_reg12[22]),
        .O(\axi_rdata[22]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_5 
       (.I0(slv_reg27[22]),
        .I1(slv_reg26[22]),
        .I2(sel0[1]),
        .I3(slv_reg25[22]),
        .I4(sel0[0]),
        .I5(slv_reg24[22]),
        .O(\axi_rdata[22]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_6 
       (.I0(slv_reg31[22]),
        .I1(slv_reg30[22]),
        .I2(sel0[1]),
        .I3(slv_reg29[22]),
        .I4(sel0[0]),
        .I5(slv_reg28[22]),
        .O(\axi_rdata[22]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_7 
       (.I0(slv_reg19[22]),
        .I1(slv_reg18[22]),
        .I2(sel0[1]),
        .I3(slv_reg17[22]),
        .I4(sel0[0]),
        .I5(slv_reg16[22]),
        .O(\axi_rdata[22]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_8 
       (.I0(slv_reg23[22]),
        .I1(slv_reg22[22]),
        .I2(sel0[1]),
        .I3(slv_reg21[22]),
        .I4(sel0[0]),
        .I5(slv_reg20[22]),
        .O(\axi_rdata[22]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[23]_i_1 
       (.I0(\axi_rdata_reg[23]_i_2_n_0 ),
        .I1(\axi_rdata_reg[23]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[23]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[23]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_4 
       (.I0(slv_reg15[23]),
        .I1(slv_reg14[23]),
        .I2(sel0[1]),
        .I3(slv_reg13[23]),
        .I4(sel0[0]),
        .I5(slv_reg12[23]),
        .O(\axi_rdata[23]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_5 
       (.I0(slv_reg27[23]),
        .I1(slv_reg26[23]),
        .I2(sel0[1]),
        .I3(slv_reg25[23]),
        .I4(sel0[0]),
        .I5(slv_reg24[23]),
        .O(\axi_rdata[23]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_6 
       (.I0(slv_reg31[23]),
        .I1(slv_reg30[23]),
        .I2(sel0[1]),
        .I3(slv_reg29[23]),
        .I4(sel0[0]),
        .I5(slv_reg28[23]),
        .O(\axi_rdata[23]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_7 
       (.I0(slv_reg19[23]),
        .I1(slv_reg18[23]),
        .I2(sel0[1]),
        .I3(slv_reg17[23]),
        .I4(sel0[0]),
        .I5(slv_reg16[23]),
        .O(\axi_rdata[23]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_8 
       (.I0(slv_reg23[23]),
        .I1(slv_reg22[23]),
        .I2(sel0[1]),
        .I3(slv_reg21[23]),
        .I4(sel0[0]),
        .I5(slv_reg20[23]),
        .O(\axi_rdata[23]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[24]_i_1 
       (.I0(\axi_rdata_reg[24]_i_2_n_0 ),
        .I1(\axi_rdata_reg[24]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[24]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[24]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_4 
       (.I0(slv_reg15[24]),
        .I1(slv_reg14[24]),
        .I2(sel0[1]),
        .I3(slv_reg13[24]),
        .I4(sel0[0]),
        .I5(slv_reg12[24]),
        .O(\axi_rdata[24]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_5 
       (.I0(slv_reg27[24]),
        .I1(slv_reg26[24]),
        .I2(sel0[1]),
        .I3(slv_reg25[24]),
        .I4(sel0[0]),
        .I5(slv_reg24[24]),
        .O(\axi_rdata[24]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_6 
       (.I0(slv_reg31[24]),
        .I1(slv_reg30[24]),
        .I2(sel0[1]),
        .I3(slv_reg29[24]),
        .I4(sel0[0]),
        .I5(slv_reg28[24]),
        .O(\axi_rdata[24]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_7 
       (.I0(slv_reg19[24]),
        .I1(slv_reg18[24]),
        .I2(sel0[1]),
        .I3(slv_reg17[24]),
        .I4(sel0[0]),
        .I5(slv_reg16[24]),
        .O(\axi_rdata[24]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_8 
       (.I0(slv_reg23[24]),
        .I1(slv_reg22[24]),
        .I2(sel0[1]),
        .I3(slv_reg21[24]),
        .I4(sel0[0]),
        .I5(slv_reg20[24]),
        .O(\axi_rdata[24]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[25]_i_1 
       (.I0(\axi_rdata_reg[25]_i_2_n_0 ),
        .I1(\axi_rdata_reg[25]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[25]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[25]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_4 
       (.I0(slv_reg15[25]),
        .I1(slv_reg14[25]),
        .I2(sel0[1]),
        .I3(slv_reg13[25]),
        .I4(sel0[0]),
        .I5(slv_reg12[25]),
        .O(\axi_rdata[25]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_5 
       (.I0(slv_reg27[25]),
        .I1(slv_reg26[25]),
        .I2(sel0[1]),
        .I3(slv_reg25[25]),
        .I4(sel0[0]),
        .I5(slv_reg24[25]),
        .O(\axi_rdata[25]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_6 
       (.I0(slv_reg31[25]),
        .I1(slv_reg30[25]),
        .I2(sel0[1]),
        .I3(slv_reg29[25]),
        .I4(sel0[0]),
        .I5(slv_reg28[25]),
        .O(\axi_rdata[25]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_7 
       (.I0(slv_reg19[25]),
        .I1(slv_reg18[25]),
        .I2(sel0[1]),
        .I3(slv_reg17[25]),
        .I4(sel0[0]),
        .I5(slv_reg16[25]),
        .O(\axi_rdata[25]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_8 
       (.I0(slv_reg23[25]),
        .I1(slv_reg22[25]),
        .I2(sel0[1]),
        .I3(slv_reg21[25]),
        .I4(sel0[0]),
        .I5(slv_reg20[25]),
        .O(\axi_rdata[25]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[26]_i_1 
       (.I0(\axi_rdata_reg[26]_i_2_n_0 ),
        .I1(\axi_rdata_reg[26]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[26]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[26]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_4 
       (.I0(slv_reg15[26]),
        .I1(slv_reg14[26]),
        .I2(sel0[1]),
        .I3(slv_reg13[26]),
        .I4(sel0[0]),
        .I5(slv_reg12[26]),
        .O(\axi_rdata[26]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_5 
       (.I0(slv_reg27[26]),
        .I1(slv_reg26[26]),
        .I2(sel0[1]),
        .I3(slv_reg25[26]),
        .I4(sel0[0]),
        .I5(slv_reg24[26]),
        .O(\axi_rdata[26]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_6 
       (.I0(slv_reg31[26]),
        .I1(slv_reg30[26]),
        .I2(sel0[1]),
        .I3(slv_reg29[26]),
        .I4(sel0[0]),
        .I5(slv_reg28[26]),
        .O(\axi_rdata[26]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_7 
       (.I0(slv_reg19[26]),
        .I1(slv_reg18[26]),
        .I2(sel0[1]),
        .I3(slv_reg17[26]),
        .I4(sel0[0]),
        .I5(slv_reg16[26]),
        .O(\axi_rdata[26]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_8 
       (.I0(slv_reg23[26]),
        .I1(slv_reg22[26]),
        .I2(sel0[1]),
        .I3(slv_reg21[26]),
        .I4(sel0[0]),
        .I5(slv_reg20[26]),
        .O(\axi_rdata[26]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[27]_i_1 
       (.I0(\axi_rdata_reg[27]_i_2_n_0 ),
        .I1(\axi_rdata_reg[27]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[27]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[27]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_4 
       (.I0(slv_reg15[27]),
        .I1(slv_reg14[27]),
        .I2(sel0[1]),
        .I3(slv_reg13[27]),
        .I4(sel0[0]),
        .I5(slv_reg12[27]),
        .O(\axi_rdata[27]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_5 
       (.I0(slv_reg27[27]),
        .I1(slv_reg26[27]),
        .I2(sel0[1]),
        .I3(slv_reg25[27]),
        .I4(sel0[0]),
        .I5(slv_reg24[27]),
        .O(\axi_rdata[27]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_6 
       (.I0(slv_reg31[27]),
        .I1(slv_reg30[27]),
        .I2(sel0[1]),
        .I3(slv_reg29[27]),
        .I4(sel0[0]),
        .I5(slv_reg28[27]),
        .O(\axi_rdata[27]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_7 
       (.I0(slv_reg19[27]),
        .I1(slv_reg18[27]),
        .I2(sel0[1]),
        .I3(slv_reg17[27]),
        .I4(sel0[0]),
        .I5(slv_reg16[27]),
        .O(\axi_rdata[27]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_8 
       (.I0(slv_reg23[27]),
        .I1(slv_reg22[27]),
        .I2(sel0[1]),
        .I3(slv_reg21[27]),
        .I4(sel0[0]),
        .I5(slv_reg20[27]),
        .O(\axi_rdata[27]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[28]_i_1 
       (.I0(\axi_rdata_reg[28]_i_2_n_0 ),
        .I1(\axi_rdata_reg[28]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[28]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[28]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_4 
       (.I0(slv_reg15[28]),
        .I1(slv_reg14[28]),
        .I2(sel0[1]),
        .I3(slv_reg13[28]),
        .I4(sel0[0]),
        .I5(slv_reg12[28]),
        .O(\axi_rdata[28]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_5 
       (.I0(slv_reg27[28]),
        .I1(slv_reg26[28]),
        .I2(sel0[1]),
        .I3(slv_reg25[28]),
        .I4(sel0[0]),
        .I5(slv_reg24[28]),
        .O(\axi_rdata[28]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_6 
       (.I0(slv_reg31[28]),
        .I1(slv_reg30[28]),
        .I2(sel0[1]),
        .I3(slv_reg29[28]),
        .I4(sel0[0]),
        .I5(slv_reg28[28]),
        .O(\axi_rdata[28]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_7 
       (.I0(slv_reg19[28]),
        .I1(slv_reg18[28]),
        .I2(sel0[1]),
        .I3(slv_reg17[28]),
        .I4(sel0[0]),
        .I5(slv_reg16[28]),
        .O(\axi_rdata[28]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_8 
       (.I0(slv_reg23[28]),
        .I1(slv_reg22[28]),
        .I2(sel0[1]),
        .I3(slv_reg21[28]),
        .I4(sel0[0]),
        .I5(slv_reg20[28]),
        .O(\axi_rdata[28]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[29]_i_1 
       (.I0(\axi_rdata_reg[29]_i_2_n_0 ),
        .I1(\axi_rdata_reg[29]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[29]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[29]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_4 
       (.I0(slv_reg15[29]),
        .I1(slv_reg14[29]),
        .I2(sel0[1]),
        .I3(slv_reg13[29]),
        .I4(sel0[0]),
        .I5(slv_reg12[29]),
        .O(\axi_rdata[29]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_5 
       (.I0(slv_reg27[29]),
        .I1(slv_reg26[29]),
        .I2(sel0[1]),
        .I3(slv_reg25[29]),
        .I4(sel0[0]),
        .I5(slv_reg24[29]),
        .O(\axi_rdata[29]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_6 
       (.I0(slv_reg31[29]),
        .I1(slv_reg30[29]),
        .I2(sel0[1]),
        .I3(slv_reg29[29]),
        .I4(sel0[0]),
        .I5(slv_reg28[29]),
        .O(\axi_rdata[29]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_7 
       (.I0(slv_reg19[29]),
        .I1(slv_reg18[29]),
        .I2(sel0[1]),
        .I3(slv_reg17[29]),
        .I4(sel0[0]),
        .I5(slv_reg16[29]),
        .O(\axi_rdata[29]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_8 
       (.I0(slv_reg23[29]),
        .I1(slv_reg22[29]),
        .I2(sel0[1]),
        .I3(slv_reg21[29]),
        .I4(sel0[0]),
        .I5(slv_reg20[29]),
        .O(\axi_rdata[29]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[2]_i_1 
       (.I0(\axi_rdata_reg[2]_i_2_n_0 ),
        .I1(sel0[4]),
        .I2(sel0[3]),
        .I3(\axi_rdata[7]_i_3_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[2]_i_3_n_0 ),
        .O(reg_data_out[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_3 
       (.I0(slv_reg15[2]),
        .I1(slv_reg14[2]),
        .I2(sel0[1]),
        .I3(slv_reg13[2]),
        .I4(sel0[0]),
        .I5(slv_reg12[2]),
        .O(\axi_rdata[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_6 
       (.I0(slv_reg19[2]),
        .I1(slv_reg18[2]),
        .I2(sel0[1]),
        .I3(slv_reg17[2]),
        .I4(sel0[0]),
        .I5(slv_reg16[2]),
        .O(\axi_rdata[2]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_7 
       (.I0(slv_reg23[2]),
        .I1(slv_reg22[2]),
        .I2(sel0[1]),
        .I3(slv_reg21[2]),
        .I4(sel0[0]),
        .I5(slv_reg20[2]),
        .O(\axi_rdata[2]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_8 
       (.I0(slv_reg27[2]),
        .I1(slv_reg26[2]),
        .I2(sel0[1]),
        .I3(slv_reg25[2]),
        .I4(sel0[0]),
        .I5(slv_reg24[2]),
        .O(\axi_rdata[2]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_9 
       (.I0(slv_reg31[2]),
        .I1(slv_reg30[2]),
        .I2(sel0[1]),
        .I3(slv_reg29[2]),
        .I4(sel0[0]),
        .I5(slv_reg28[2]),
        .O(\axi_rdata[2]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[30]_i_1 
       (.I0(\axi_rdata_reg[30]_i_2_n_0 ),
        .I1(\axi_rdata_reg[30]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[30]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[30]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_4 
       (.I0(slv_reg15[30]),
        .I1(slv_reg14[30]),
        .I2(sel0[1]),
        .I3(slv_reg13[30]),
        .I4(sel0[0]),
        .I5(slv_reg12[30]),
        .O(\axi_rdata[30]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_5 
       (.I0(slv_reg27[30]),
        .I1(slv_reg26[30]),
        .I2(sel0[1]),
        .I3(slv_reg25[30]),
        .I4(sel0[0]),
        .I5(slv_reg24[30]),
        .O(\axi_rdata[30]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_6 
       (.I0(slv_reg31[30]),
        .I1(slv_reg30[30]),
        .I2(sel0[1]),
        .I3(slv_reg29[30]),
        .I4(sel0[0]),
        .I5(slv_reg28[30]),
        .O(\axi_rdata[30]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_7 
       (.I0(slv_reg19[30]),
        .I1(slv_reg18[30]),
        .I2(sel0[1]),
        .I3(slv_reg17[30]),
        .I4(sel0[0]),
        .I5(slv_reg16[30]),
        .O(\axi_rdata[30]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_8 
       (.I0(slv_reg23[30]),
        .I1(slv_reg22[30]),
        .I2(sel0[1]),
        .I3(slv_reg21[30]),
        .I4(sel0[0]),
        .I5(slv_reg20[30]),
        .O(\axi_rdata[30]_i_8_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(axi_arready_reg_0),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .O(slv_reg_rden));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[31]_i_2 
       (.I0(\axi_rdata_reg[31]_i_3_n_0 ),
        .I1(\axi_rdata_reg[31]_i_4_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[31]_i_5_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[31]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_5 
       (.I0(slv_reg15[31]),
        .I1(slv_reg14[31]),
        .I2(sel0[1]),
        .I3(slv_reg13[31]),
        .I4(sel0[0]),
        .I5(slv_reg12[31]),
        .O(\axi_rdata[31]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_6 
       (.I0(slv_reg27[31]),
        .I1(slv_reg26[31]),
        .I2(sel0[1]),
        .I3(slv_reg25[31]),
        .I4(sel0[0]),
        .I5(slv_reg24[31]),
        .O(\axi_rdata[31]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_7 
       (.I0(slv_reg31[31]),
        .I1(slv_reg30[31]),
        .I2(sel0[1]),
        .I3(slv_reg29[31]),
        .I4(sel0[0]),
        .I5(slv_reg28[31]),
        .O(\axi_rdata[31]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_8 
       (.I0(slv_reg19[31]),
        .I1(slv_reg18[31]),
        .I2(sel0[1]),
        .I3(slv_reg17[31]),
        .I4(sel0[0]),
        .I5(slv_reg16[31]),
        .O(\axi_rdata[31]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_9 
       (.I0(slv_reg23[31]),
        .I1(slv_reg22[31]),
        .I2(sel0[1]),
        .I3(slv_reg21[31]),
        .I4(sel0[0]),
        .I5(slv_reg20[31]),
        .O(\axi_rdata[31]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[3]_i_1 
       (.I0(\axi_rdata_reg[3]_i_2_n_0 ),
        .I1(sel0[4]),
        .I2(sel0[3]),
        .I3(\axi_rdata[7]_i_3_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[3]_i_3_n_0 ),
        .O(reg_data_out[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_3 
       (.I0(slv_reg15[3]),
        .I1(slv_reg14[3]),
        .I2(sel0[1]),
        .I3(slv_reg13[3]),
        .I4(sel0[0]),
        .I5(slv_reg12[3]),
        .O(\axi_rdata[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_6 
       (.I0(slv_reg19[3]),
        .I1(slv_reg18[3]),
        .I2(sel0[1]),
        .I3(slv_reg17[3]),
        .I4(sel0[0]),
        .I5(slv_reg16[3]),
        .O(\axi_rdata[3]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_7 
       (.I0(slv_reg23[3]),
        .I1(slv_reg22[3]),
        .I2(sel0[1]),
        .I3(slv_reg21[3]),
        .I4(sel0[0]),
        .I5(slv_reg20[3]),
        .O(\axi_rdata[3]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_8 
       (.I0(slv_reg27[3]),
        .I1(slv_reg26[3]),
        .I2(sel0[1]),
        .I3(slv_reg25[3]),
        .I4(sel0[0]),
        .I5(slv_reg24[3]),
        .O(\axi_rdata[3]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_9 
       (.I0(slv_reg31[3]),
        .I1(slv_reg30[3]),
        .I2(sel0[1]),
        .I3(slv_reg29[3]),
        .I4(sel0[0]),
        .I5(slv_reg28[3]),
        .O(\axi_rdata[3]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[4]_i_1 
       (.I0(\axi_rdata_reg[4]_i_2_n_0 ),
        .I1(sel0[4]),
        .I2(sel0[3]),
        .I3(\axi_rdata[7]_i_3_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[4]_i_3_n_0 ),
        .O(reg_data_out[4]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_3 
       (.I0(slv_reg15[4]),
        .I1(slv_reg14[4]),
        .I2(sel0[1]),
        .I3(slv_reg13[4]),
        .I4(sel0[0]),
        .I5(slv_reg12[4]),
        .O(\axi_rdata[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_6 
       (.I0(slv_reg19[4]),
        .I1(slv_reg18[4]),
        .I2(sel0[1]),
        .I3(slv_reg17[4]),
        .I4(sel0[0]),
        .I5(slv_reg16[4]),
        .O(\axi_rdata[4]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_7 
       (.I0(slv_reg23[4]),
        .I1(slv_reg22[4]),
        .I2(sel0[1]),
        .I3(slv_reg21[4]),
        .I4(sel0[0]),
        .I5(slv_reg20[4]),
        .O(\axi_rdata[4]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_8 
       (.I0(slv_reg27[4]),
        .I1(slv_reg26[4]),
        .I2(sel0[1]),
        .I3(slv_reg25[4]),
        .I4(sel0[0]),
        .I5(slv_reg24[4]),
        .O(\axi_rdata[4]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_9 
       (.I0(slv_reg31[4]),
        .I1(slv_reg30[4]),
        .I2(sel0[1]),
        .I3(slv_reg29[4]),
        .I4(sel0[0]),
        .I5(slv_reg28[4]),
        .O(\axi_rdata[4]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[5]_i_1 
       (.I0(\axi_rdata_reg[5]_i_2_n_0 ),
        .I1(\axi_rdata_reg[5]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[5]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[5]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_4 
       (.I0(slv_reg15[5]),
        .I1(slv_reg14[5]),
        .I2(sel0[1]),
        .I3(slv_reg13[5]),
        .I4(sel0[0]),
        .I5(slv_reg12[5]),
        .O(\axi_rdata[5]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_5 
       (.I0(slv_reg27[5]),
        .I1(slv_reg26[5]),
        .I2(sel0[1]),
        .I3(slv_reg25[5]),
        .I4(sel0[0]),
        .I5(slv_reg24[5]),
        .O(\axi_rdata[5]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_6 
       (.I0(slv_reg31[5]),
        .I1(slv_reg30[5]),
        .I2(sel0[1]),
        .I3(slv_reg29[5]),
        .I4(sel0[0]),
        .I5(slv_reg28[5]),
        .O(\axi_rdata[5]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_7 
       (.I0(slv_reg19[5]),
        .I1(slv_reg18[5]),
        .I2(sel0[1]),
        .I3(slv_reg17[5]),
        .I4(sel0[0]),
        .I5(slv_reg16[5]),
        .O(\axi_rdata[5]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_8 
       (.I0(slv_reg23[5]),
        .I1(slv_reg22[5]),
        .I2(sel0[1]),
        .I3(slv_reg21[5]),
        .I4(sel0[0]),
        .I5(slv_reg20[5]),
        .O(\axi_rdata[5]_i_8_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_rdata[6]_i_1 
       (.I0(\axi_rdata_reg[6]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata_reg[6]_i_3_n_0 ),
        .I3(sel0[4]),
        .I4(\axi_rdata[6]_i_4_n_0 ),
        .O(reg_data_out[6]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT5 #(
    .INIT(32'hAA280028)) 
    \axi_rdata[6]_i_4 
       (.I0(sel0[3]),
        .I1(sel0[0]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_rdata[6]_i_9_n_0 ),
        .O(\axi_rdata[6]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_5 
       (.I0(slv_reg27[6]),
        .I1(slv_reg26[6]),
        .I2(sel0[1]),
        .I3(slv_reg25[6]),
        .I4(sel0[0]),
        .I5(slv_reg24[6]),
        .O(\axi_rdata[6]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_6 
       (.I0(slv_reg31[6]),
        .I1(slv_reg30[6]),
        .I2(sel0[1]),
        .I3(slv_reg29[6]),
        .I4(sel0[0]),
        .I5(slv_reg28[6]),
        .O(\axi_rdata[6]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_7 
       (.I0(slv_reg19[6]),
        .I1(slv_reg18[6]),
        .I2(sel0[1]),
        .I3(slv_reg17[6]),
        .I4(sel0[0]),
        .I5(slv_reg16[6]),
        .O(\axi_rdata[6]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_8 
       (.I0(slv_reg23[6]),
        .I1(slv_reg22[6]),
        .I2(sel0[1]),
        .I3(slv_reg21[6]),
        .I4(sel0[0]),
        .I5(slv_reg20[6]),
        .O(\axi_rdata[6]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_9 
       (.I0(slv_reg15[6]),
        .I1(slv_reg14[6]),
        .I2(sel0[1]),
        .I3(slv_reg13[6]),
        .I4(sel0[0]),
        .I5(slv_reg12[6]),
        .O(\axi_rdata[6]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8888888B888)) 
    \axi_rdata[7]_i_1 
       (.I0(\axi_rdata_reg[7]_i_2_n_0 ),
        .I1(sel0[4]),
        .I2(sel0[3]),
        .I3(\axi_rdata[7]_i_3_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[7]_i_4_n_0 ),
        .O(reg_data_out[7]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_10 
       (.I0(slv_reg31[7]),
        .I1(slv_reg30[7]),
        .I2(sel0[1]),
        .I3(slv_reg29[7]),
        .I4(sel0[0]),
        .I5(slv_reg28[7]),
        .O(\axi_rdata[7]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[7]_i_3 
       (.I0(sel0[0]),
        .I1(sel0[1]),
        .O(\axi_rdata[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_4 
       (.I0(slv_reg15[7]),
        .I1(slv_reg14[7]),
        .I2(sel0[1]),
        .I3(slv_reg13[7]),
        .I4(sel0[0]),
        .I5(slv_reg12[7]),
        .O(\axi_rdata[7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_7 
       (.I0(slv_reg19[7]),
        .I1(slv_reg18[7]),
        .I2(sel0[1]),
        .I3(slv_reg17[7]),
        .I4(sel0[0]),
        .I5(slv_reg16[7]),
        .O(\axi_rdata[7]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_8 
       (.I0(slv_reg23[7]),
        .I1(slv_reg22[7]),
        .I2(sel0[1]),
        .I3(slv_reg21[7]),
        .I4(sel0[0]),
        .I5(slv_reg20[7]),
        .O(\axi_rdata[7]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_9 
       (.I0(slv_reg27[7]),
        .I1(slv_reg26[7]),
        .I2(sel0[1]),
        .I3(slv_reg25[7]),
        .I4(sel0[0]),
        .I5(slv_reg24[7]),
        .O(\axi_rdata[7]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_rdata[8]_i_1 
       (.I0(\axi_rdata_reg[8]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata_reg[8]_i_3_n_0 ),
        .I3(sel0[4]),
        .I4(\axi_rdata[8]_i_4_n_0 ),
        .O(reg_data_out[8]));
  LUT5 #(
    .INIT(32'hAA200020)) 
    \axi_rdata[8]_i_4 
       (.I0(sel0[3]),
        .I1(sel0[0]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_rdata[8]_i_9_n_0 ),
        .O(\axi_rdata[8]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_5 
       (.I0(slv_reg27[8]),
        .I1(slv_reg26[8]),
        .I2(sel0[1]),
        .I3(slv_reg25[8]),
        .I4(sel0[0]),
        .I5(slv_reg24[8]),
        .O(\axi_rdata[8]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_6 
       (.I0(slv_reg31[8]),
        .I1(slv_reg30[8]),
        .I2(sel0[1]),
        .I3(slv_reg29[8]),
        .I4(sel0[0]),
        .I5(slv_reg28[8]),
        .O(\axi_rdata[8]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_7 
       (.I0(slv_reg19[8]),
        .I1(slv_reg18[8]),
        .I2(sel0[1]),
        .I3(slv_reg17[8]),
        .I4(sel0[0]),
        .I5(slv_reg16[8]),
        .O(\axi_rdata[8]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_8 
       (.I0(slv_reg23[8]),
        .I1(slv_reg22[8]),
        .I2(sel0[1]),
        .I3(slv_reg21[8]),
        .I4(sel0[0]),
        .I5(slv_reg20[8]),
        .O(\axi_rdata[8]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_9 
       (.I0(slv_reg15[8]),
        .I1(slv_reg14[8]),
        .I2(sel0[1]),
        .I3(slv_reg13[8]),
        .I4(sel0[0]),
        .I5(slv_reg12[8]),
        .O(\axi_rdata[8]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFC0A0C0A0C0A0C0)) 
    \axi_rdata[9]_i_1 
       (.I0(\axi_rdata_reg[9]_i_2_n_0 ),
        .I1(\axi_rdata_reg[9]_i_3_n_0 ),
        .I2(sel0[4]),
        .I3(sel0[3]),
        .I4(\axi_rdata[9]_i_4_n_0 ),
        .I5(sel0[2]),
        .O(reg_data_out[9]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_4 
       (.I0(slv_reg15[9]),
        .I1(slv_reg14[9]),
        .I2(sel0[1]),
        .I3(slv_reg13[9]),
        .I4(sel0[0]),
        .I5(slv_reg12[9]),
        .O(\axi_rdata[9]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_5 
       (.I0(slv_reg27[9]),
        .I1(slv_reg26[9]),
        .I2(sel0[1]),
        .I3(slv_reg25[9]),
        .I4(sel0[0]),
        .I5(slv_reg24[9]),
        .O(\axi_rdata[9]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_6 
       (.I0(slv_reg31[9]),
        .I1(slv_reg30[9]),
        .I2(sel0[1]),
        .I3(slv_reg29[9]),
        .I4(sel0[0]),
        .I5(slv_reg28[9]),
        .O(\axi_rdata[9]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_7 
       (.I0(slv_reg19[9]),
        .I1(slv_reg18[9]),
        .I2(sel0[1]),
        .I3(slv_reg17[9]),
        .I4(sel0[0]),
        .I5(slv_reg16[9]),
        .O(\axi_rdata[9]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_8 
       (.I0(slv_reg23[9]),
        .I1(slv_reg22[9]),
        .I2(sel0[1]),
        .I3(slv_reg21[9]),
        .I4(sel0[0]),
        .I5(slv_reg20[9]),
        .O(\axi_rdata[9]_i_8_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(RST));
  MUXF7 \axi_rdata_reg[0]_i_2 
       (.I0(\axi_rdata[0]_i_5_n_0 ),
        .I1(\axi_rdata[0]_i_6_n_0 ),
        .O(\axi_rdata_reg[0]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[0]_i_3 
       (.I0(\axi_rdata[0]_i_7_n_0 ),
        .I1(\axi_rdata[0]_i_8_n_0 ),
        .O(\axi_rdata_reg[0]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(RST));
  MUXF7 \axi_rdata_reg[10]_i_2 
       (.I0(\axi_rdata[10]_i_5_n_0 ),
        .I1(\axi_rdata[10]_i_6_n_0 ),
        .O(\axi_rdata_reg[10]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[10]_i_3 
       (.I0(\axi_rdata[10]_i_7_n_0 ),
        .I1(\axi_rdata[10]_i_8_n_0 ),
        .O(\axi_rdata_reg[10]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(RST));
  MUXF7 \axi_rdata_reg[11]_i_2 
       (.I0(\axi_rdata[11]_i_5_n_0 ),
        .I1(\axi_rdata[11]_i_6_n_0 ),
        .O(\axi_rdata_reg[11]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[11]_i_3 
       (.I0(\axi_rdata[11]_i_7_n_0 ),
        .I1(\axi_rdata[11]_i_8_n_0 ),
        .O(\axi_rdata_reg[11]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(RST));
  MUXF7 \axi_rdata_reg[12]_i_2 
       (.I0(\axi_rdata[12]_i_5_n_0 ),
        .I1(\axi_rdata[12]_i_6_n_0 ),
        .O(\axi_rdata_reg[12]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[12]_i_3 
       (.I0(\axi_rdata[12]_i_7_n_0 ),
        .I1(\axi_rdata[12]_i_8_n_0 ),
        .O(\axi_rdata_reg[12]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(RST));
  MUXF7 \axi_rdata_reg[13]_i_2 
       (.I0(\axi_rdata[13]_i_5_n_0 ),
        .I1(\axi_rdata[13]_i_6_n_0 ),
        .O(\axi_rdata_reg[13]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[13]_i_3 
       (.I0(\axi_rdata[13]_i_7_n_0 ),
        .I1(\axi_rdata[13]_i_8_n_0 ),
        .O(\axi_rdata_reg[13]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(RST));
  MUXF7 \axi_rdata_reg[14]_i_2 
       (.I0(\axi_rdata[14]_i_5_n_0 ),
        .I1(\axi_rdata[14]_i_6_n_0 ),
        .O(\axi_rdata_reg[14]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[14]_i_3 
       (.I0(\axi_rdata[14]_i_7_n_0 ),
        .I1(\axi_rdata[14]_i_8_n_0 ),
        .O(\axi_rdata_reg[14]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(RST));
  MUXF7 \axi_rdata_reg[15]_i_2 
       (.I0(\axi_rdata[15]_i_5_n_0 ),
        .I1(\axi_rdata[15]_i_6_n_0 ),
        .O(\axi_rdata_reg[15]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[15]_i_3 
       (.I0(\axi_rdata[15]_i_7_n_0 ),
        .I1(\axi_rdata[15]_i_8_n_0 ),
        .O(\axi_rdata_reg[15]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(RST));
  MUXF7 \axi_rdata_reg[16]_i_2 
       (.I0(\axi_rdata[16]_i_5_n_0 ),
        .I1(\axi_rdata[16]_i_6_n_0 ),
        .O(\axi_rdata_reg[16]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[16]_i_3 
       (.I0(\axi_rdata[16]_i_7_n_0 ),
        .I1(\axi_rdata[16]_i_8_n_0 ),
        .O(\axi_rdata_reg[16]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(RST));
  MUXF7 \axi_rdata_reg[17]_i_2 
       (.I0(\axi_rdata[17]_i_5_n_0 ),
        .I1(\axi_rdata[17]_i_6_n_0 ),
        .O(\axi_rdata_reg[17]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[17]_i_3 
       (.I0(\axi_rdata[17]_i_7_n_0 ),
        .I1(\axi_rdata[17]_i_8_n_0 ),
        .O(\axi_rdata_reg[17]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(RST));
  MUXF7 \axi_rdata_reg[18]_i_2 
       (.I0(\axi_rdata[18]_i_5_n_0 ),
        .I1(\axi_rdata[18]_i_6_n_0 ),
        .O(\axi_rdata_reg[18]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[18]_i_3 
       (.I0(\axi_rdata[18]_i_7_n_0 ),
        .I1(\axi_rdata[18]_i_8_n_0 ),
        .O(\axi_rdata_reg[18]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(RST));
  MUXF7 \axi_rdata_reg[19]_i_2 
       (.I0(\axi_rdata[19]_i_5_n_0 ),
        .I1(\axi_rdata[19]_i_6_n_0 ),
        .O(\axi_rdata_reg[19]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[19]_i_3 
       (.I0(\axi_rdata[19]_i_7_n_0 ),
        .I1(\axi_rdata[19]_i_8_n_0 ),
        .O(\axi_rdata_reg[19]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(RST));
  MUXF7 \axi_rdata_reg[1]_i_2 
       (.I0(\axi_rdata[1]_i_5_n_0 ),
        .I1(\axi_rdata[1]_i_6_n_0 ),
        .O(\axi_rdata_reg[1]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[1]_i_3 
       (.I0(\axi_rdata[1]_i_7_n_0 ),
        .I1(\axi_rdata[1]_i_8_n_0 ),
        .O(\axi_rdata_reg[1]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(RST));
  MUXF7 \axi_rdata_reg[20]_i_2 
       (.I0(\axi_rdata[20]_i_5_n_0 ),
        .I1(\axi_rdata[20]_i_6_n_0 ),
        .O(\axi_rdata_reg[20]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[20]_i_3 
       (.I0(\axi_rdata[20]_i_7_n_0 ),
        .I1(\axi_rdata[20]_i_8_n_0 ),
        .O(\axi_rdata_reg[20]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(RST));
  MUXF7 \axi_rdata_reg[21]_i_2 
       (.I0(\axi_rdata[21]_i_5_n_0 ),
        .I1(\axi_rdata[21]_i_6_n_0 ),
        .O(\axi_rdata_reg[21]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[21]_i_3 
       (.I0(\axi_rdata[21]_i_7_n_0 ),
        .I1(\axi_rdata[21]_i_8_n_0 ),
        .O(\axi_rdata_reg[21]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(RST));
  MUXF7 \axi_rdata_reg[22]_i_2 
       (.I0(\axi_rdata[22]_i_5_n_0 ),
        .I1(\axi_rdata[22]_i_6_n_0 ),
        .O(\axi_rdata_reg[22]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[22]_i_3 
       (.I0(\axi_rdata[22]_i_7_n_0 ),
        .I1(\axi_rdata[22]_i_8_n_0 ),
        .O(\axi_rdata_reg[22]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(RST));
  MUXF7 \axi_rdata_reg[23]_i_2 
       (.I0(\axi_rdata[23]_i_5_n_0 ),
        .I1(\axi_rdata[23]_i_6_n_0 ),
        .O(\axi_rdata_reg[23]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[23]_i_3 
       (.I0(\axi_rdata[23]_i_7_n_0 ),
        .I1(\axi_rdata[23]_i_8_n_0 ),
        .O(\axi_rdata_reg[23]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(RST));
  MUXF7 \axi_rdata_reg[24]_i_2 
       (.I0(\axi_rdata[24]_i_5_n_0 ),
        .I1(\axi_rdata[24]_i_6_n_0 ),
        .O(\axi_rdata_reg[24]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[24]_i_3 
       (.I0(\axi_rdata[24]_i_7_n_0 ),
        .I1(\axi_rdata[24]_i_8_n_0 ),
        .O(\axi_rdata_reg[24]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(RST));
  MUXF7 \axi_rdata_reg[25]_i_2 
       (.I0(\axi_rdata[25]_i_5_n_0 ),
        .I1(\axi_rdata[25]_i_6_n_0 ),
        .O(\axi_rdata_reg[25]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[25]_i_3 
       (.I0(\axi_rdata[25]_i_7_n_0 ),
        .I1(\axi_rdata[25]_i_8_n_0 ),
        .O(\axi_rdata_reg[25]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(RST));
  MUXF7 \axi_rdata_reg[26]_i_2 
       (.I0(\axi_rdata[26]_i_5_n_0 ),
        .I1(\axi_rdata[26]_i_6_n_0 ),
        .O(\axi_rdata_reg[26]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[26]_i_3 
       (.I0(\axi_rdata[26]_i_7_n_0 ),
        .I1(\axi_rdata[26]_i_8_n_0 ),
        .O(\axi_rdata_reg[26]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(RST));
  MUXF7 \axi_rdata_reg[27]_i_2 
       (.I0(\axi_rdata[27]_i_5_n_0 ),
        .I1(\axi_rdata[27]_i_6_n_0 ),
        .O(\axi_rdata_reg[27]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[27]_i_3 
       (.I0(\axi_rdata[27]_i_7_n_0 ),
        .I1(\axi_rdata[27]_i_8_n_0 ),
        .O(\axi_rdata_reg[27]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(RST));
  MUXF7 \axi_rdata_reg[28]_i_2 
       (.I0(\axi_rdata[28]_i_5_n_0 ),
        .I1(\axi_rdata[28]_i_6_n_0 ),
        .O(\axi_rdata_reg[28]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[28]_i_3 
       (.I0(\axi_rdata[28]_i_7_n_0 ),
        .I1(\axi_rdata[28]_i_8_n_0 ),
        .O(\axi_rdata_reg[28]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(RST));
  MUXF7 \axi_rdata_reg[29]_i_2 
       (.I0(\axi_rdata[29]_i_5_n_0 ),
        .I1(\axi_rdata[29]_i_6_n_0 ),
        .O(\axi_rdata_reg[29]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[29]_i_3 
       (.I0(\axi_rdata[29]_i_7_n_0 ),
        .I1(\axi_rdata[29]_i_8_n_0 ),
        .O(\axi_rdata_reg[29]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(RST));
  MUXF8 \axi_rdata_reg[2]_i_2 
       (.I0(\axi_rdata_reg[2]_i_4_n_0 ),
        .I1(\axi_rdata_reg[2]_i_5_n_0 ),
        .O(\axi_rdata_reg[2]_i_2_n_0 ),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[2]_i_4 
       (.I0(\axi_rdata[2]_i_6_n_0 ),
        .I1(\axi_rdata[2]_i_7_n_0 ),
        .O(\axi_rdata_reg[2]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[2]_i_5 
       (.I0(\axi_rdata[2]_i_8_n_0 ),
        .I1(\axi_rdata[2]_i_9_n_0 ),
        .O(\axi_rdata_reg[2]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(RST));
  MUXF7 \axi_rdata_reg[30]_i_2 
       (.I0(\axi_rdata[30]_i_5_n_0 ),
        .I1(\axi_rdata[30]_i_6_n_0 ),
        .O(\axi_rdata_reg[30]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[30]_i_3 
       (.I0(\axi_rdata[30]_i_7_n_0 ),
        .I1(\axi_rdata[30]_i_8_n_0 ),
        .O(\axi_rdata_reg[30]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(RST));
  MUXF7 \axi_rdata_reg[31]_i_3 
       (.I0(\axi_rdata[31]_i_6_n_0 ),
        .I1(\axi_rdata[31]_i_7_n_0 ),
        .O(\axi_rdata_reg[31]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[31]_i_4 
       (.I0(\axi_rdata[31]_i_8_n_0 ),
        .I1(\axi_rdata[31]_i_9_n_0 ),
        .O(\axi_rdata_reg[31]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(RST));
  MUXF8 \axi_rdata_reg[3]_i_2 
       (.I0(\axi_rdata_reg[3]_i_4_n_0 ),
        .I1(\axi_rdata_reg[3]_i_5_n_0 ),
        .O(\axi_rdata_reg[3]_i_2_n_0 ),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[3]_i_4 
       (.I0(\axi_rdata[3]_i_6_n_0 ),
        .I1(\axi_rdata[3]_i_7_n_0 ),
        .O(\axi_rdata_reg[3]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[3]_i_5 
       (.I0(\axi_rdata[3]_i_8_n_0 ),
        .I1(\axi_rdata[3]_i_9_n_0 ),
        .O(\axi_rdata_reg[3]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(RST));
  MUXF8 \axi_rdata_reg[4]_i_2 
       (.I0(\axi_rdata_reg[4]_i_4_n_0 ),
        .I1(\axi_rdata_reg[4]_i_5_n_0 ),
        .O(\axi_rdata_reg[4]_i_2_n_0 ),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[4]_i_4 
       (.I0(\axi_rdata[4]_i_6_n_0 ),
        .I1(\axi_rdata[4]_i_7_n_0 ),
        .O(\axi_rdata_reg[4]_i_4_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[4]_i_5 
       (.I0(\axi_rdata[4]_i_8_n_0 ),
        .I1(\axi_rdata[4]_i_9_n_0 ),
        .O(\axi_rdata_reg[4]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(RST));
  MUXF7 \axi_rdata_reg[5]_i_2 
       (.I0(\axi_rdata[5]_i_5_n_0 ),
        .I1(\axi_rdata[5]_i_6_n_0 ),
        .O(\axi_rdata_reg[5]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[5]_i_3 
       (.I0(\axi_rdata[5]_i_7_n_0 ),
        .I1(\axi_rdata[5]_i_8_n_0 ),
        .O(\axi_rdata_reg[5]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(RST));
  MUXF7 \axi_rdata_reg[6]_i_2 
       (.I0(\axi_rdata[6]_i_5_n_0 ),
        .I1(\axi_rdata[6]_i_6_n_0 ),
        .O(\axi_rdata_reg[6]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[6]_i_3 
       (.I0(\axi_rdata[6]_i_7_n_0 ),
        .I1(\axi_rdata[6]_i_8_n_0 ),
        .O(\axi_rdata_reg[6]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(RST));
  MUXF8 \axi_rdata_reg[7]_i_2 
       (.I0(\axi_rdata_reg[7]_i_5_n_0 ),
        .I1(\axi_rdata_reg[7]_i_6_n_0 ),
        .O(\axi_rdata_reg[7]_i_2_n_0 ),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[7]_i_5 
       (.I0(\axi_rdata[7]_i_7_n_0 ),
        .I1(\axi_rdata[7]_i_8_n_0 ),
        .O(\axi_rdata_reg[7]_i_5_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[7]_i_6 
       (.I0(\axi_rdata[7]_i_9_n_0 ),
        .I1(\axi_rdata[7]_i_10_n_0 ),
        .O(\axi_rdata_reg[7]_i_6_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(RST));
  MUXF7 \axi_rdata_reg[8]_i_2 
       (.I0(\axi_rdata[8]_i_5_n_0 ),
        .I1(\axi_rdata[8]_i_6_n_0 ),
        .O(\axi_rdata_reg[8]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[8]_i_3 
       (.I0(\axi_rdata[8]_i_7_n_0 ),
        .I1(\axi_rdata[8]_i_8_n_0 ),
        .O(\axi_rdata_reg[8]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(RST));
  MUXF7 \axi_rdata_reg[9]_i_2 
       (.I0(\axi_rdata[9]_i_5_n_0 ),
        .I1(\axi_rdata[9]_i_6_n_0 ),
        .O(\axi_rdata_reg[9]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[9]_i_3 
       (.I0(\axi_rdata[9]_i_7_n_0 ),
        .I1(\axi_rdata[9]_i_8_n_0 ),
        .O(\axi_rdata_reg[9]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_reg_0),
        .Q(s00_axi_rvalid),
        .R(RST));
  LUT4 #(
    .INIT(16'h0080)) 
    axi_wready_i_1
       (.I0(aw_en_reg_0),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(axi_wready_reg_0),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(axi_wready_reg_0),
        .R(RST));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_fsm control
       (.CO(datapath_n_6),
        .E(\lab2_BRAMCntr/tempCnt0 ),
        .\FSM_sequential_state_reg[0]_0 (datapath_n_4),
        .\FSM_sequential_state_reg[0]_1 (RST),
        .\FSM_sequential_state_reg[2]_0 (datapath_n_3),
        .SS(control_n_0),
        .cw(cw),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn),
        .sw(sw));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lab2_datapath datapath
       (.CO(datapath_n_6),
        .D(reg_data_out[0]),
        .E(\lab2_BRAMCntr/tempCnt0 ),
        .Q(sel0),
        .SS(RST),
        .\axi_rdata_reg[0] (\axi_rdata_reg[0]_i_2_n_0 ),
        .\axi_rdata_reg[0]_0 (\axi_rdata_reg[0]_i_3_n_0 ),
        .\axi_rdata_reg[0]_1 (\axi_rdata[0]_i_9_n_0 ),
        .cw(cw),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn),
        .sw(sw),
        .\tempCnt_reg[4] (control_n_0),
        .\tempCnt_reg[8] (datapath_n_4),
        .\tempCnt_reg[9] (datapath_n_3));
  LUT5 #(
    .INIT(32'h00004000)) 
    \slv_reg12[15]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[1]),
        .I2(\slv_reg12[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(p_1_in[15]));
  LUT5 #(
    .INIT(32'h00004000)) 
    \slv_reg12[23]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[2]),
        .I2(\slv_reg12[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(p_1_in[23]));
  LUT5 #(
    .INIT(32'h00004000)) 
    \slv_reg12[31]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[3]),
        .I2(\slv_reg12[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(p_1_in[31]));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg12[31]_i_2 
       (.I0(p_0_in[2]),
        .I1(axi_awready_reg_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(axi_wready_reg_0),
        .I5(p_0_in[4]),
        .O(\slv_reg12[31]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00004000)) 
    \slv_reg12[7]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[0]),
        .I2(\slv_reg12[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(p_1_in[7]));
  FDRE \slv_reg12_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg12[0]),
        .R(RST));
  FDRE \slv_reg12_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg12[10]),
        .R(RST));
  FDRE \slv_reg12_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg12[11]),
        .R(RST));
  FDRE \slv_reg12_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg12[12]),
        .R(RST));
  FDRE \slv_reg12_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg12[13]),
        .R(RST));
  FDRE \slv_reg12_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg12[14]),
        .R(RST));
  FDRE \slv_reg12_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg12[15]),
        .R(RST));
  FDRE \slv_reg12_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg12[16]),
        .R(RST));
  FDRE \slv_reg12_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg12[17]),
        .R(RST));
  FDRE \slv_reg12_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg12[18]),
        .R(RST));
  FDRE \slv_reg12_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg12[19]),
        .R(RST));
  FDRE \slv_reg12_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg12[1]),
        .R(RST));
  FDRE \slv_reg12_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg12[20]),
        .R(RST));
  FDRE \slv_reg12_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg12[21]),
        .R(RST));
  FDRE \slv_reg12_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg12[22]),
        .R(RST));
  FDRE \slv_reg12_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg12[23]),
        .R(RST));
  FDRE \slv_reg12_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg12[24]),
        .R(RST));
  FDRE \slv_reg12_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg12[25]),
        .R(RST));
  FDRE \slv_reg12_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg12[26]),
        .R(RST));
  FDRE \slv_reg12_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg12[27]),
        .R(RST));
  FDRE \slv_reg12_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg12[28]),
        .R(RST));
  FDRE \slv_reg12_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg12[29]),
        .R(RST));
  FDRE \slv_reg12_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg12[2]),
        .R(RST));
  FDRE \slv_reg12_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg12[30]),
        .R(RST));
  FDRE \slv_reg12_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg12[31]),
        .R(RST));
  FDRE \slv_reg12_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg12[3]),
        .R(RST));
  FDRE \slv_reg12_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg12[4]),
        .R(RST));
  FDRE \slv_reg12_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg12[5]),
        .R(RST));
  FDRE \slv_reg12_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg12[6]),
        .R(RST));
  FDRE \slv_reg12_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg12[7]),
        .R(RST));
  FDRE \slv_reg12_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg12[8]),
        .R(RST));
  FDRE \slv_reg12_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg12[9]),
        .R(RST));
  LUT5 #(
    .INIT(32'h00008000)) 
    \slv_reg13[15]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[1]),
        .I2(\slv_reg12[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg13[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00008000)) 
    \slv_reg13[23]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[2]),
        .I2(\slv_reg12[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg13[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00008000)) 
    \slv_reg13[31]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[3]),
        .I2(\slv_reg12[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg13[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00008000)) 
    \slv_reg13[7]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[0]),
        .I2(\slv_reg12[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg13[7]_i_1_n_0 ));
  FDRE \slv_reg13_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg13[0]),
        .R(RST));
  FDRE \slv_reg13_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg13[10]),
        .R(RST));
  FDRE \slv_reg13_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg13[11]),
        .R(RST));
  FDRE \slv_reg13_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg13[12]),
        .R(RST));
  FDRE \slv_reg13_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg13[13]),
        .R(RST));
  FDRE \slv_reg13_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg13[14]),
        .R(RST));
  FDRE \slv_reg13_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg13[15]),
        .R(RST));
  FDRE \slv_reg13_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg13[16]),
        .R(RST));
  FDRE \slv_reg13_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg13[17]),
        .R(RST));
  FDRE \slv_reg13_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg13[18]),
        .R(RST));
  FDRE \slv_reg13_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg13[19]),
        .R(RST));
  FDRE \slv_reg13_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg13[1]),
        .R(RST));
  FDRE \slv_reg13_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg13[20]),
        .R(RST));
  FDRE \slv_reg13_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg13[21]),
        .R(RST));
  FDRE \slv_reg13_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg13[22]),
        .R(RST));
  FDRE \slv_reg13_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg13[23]),
        .R(RST));
  FDRE \slv_reg13_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg13[24]),
        .R(RST));
  FDRE \slv_reg13_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg13[25]),
        .R(RST));
  FDRE \slv_reg13_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg13[26]),
        .R(RST));
  FDRE \slv_reg13_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg13[27]),
        .R(RST));
  FDRE \slv_reg13_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg13[28]),
        .R(RST));
  FDRE \slv_reg13_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg13[29]),
        .R(RST));
  FDRE \slv_reg13_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg13[2]),
        .R(RST));
  FDRE \slv_reg13_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg13[30]),
        .R(RST));
  FDRE \slv_reg13_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg13[31]),
        .R(RST));
  FDRE \slv_reg13_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg13[3]),
        .R(RST));
  FDRE \slv_reg13_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg13[4]),
        .R(RST));
  FDRE \slv_reg13_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg13[5]),
        .R(RST));
  FDRE \slv_reg13_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg13[6]),
        .R(RST));
  FDRE \slv_reg13_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg13[7]),
        .R(RST));
  FDRE \slv_reg13_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg13[8]),
        .R(RST));
  FDRE \slv_reg13_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg13[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg13[9]),
        .R(RST));
  LUT5 #(
    .INIT(32'h40000000)) 
    \slv_reg14[15]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[1]),
        .I2(\slv_reg12[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg14[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h40000000)) 
    \slv_reg14[23]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[2]),
        .I2(\slv_reg12[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg14[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h40000000)) 
    \slv_reg14[31]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[3]),
        .I2(\slv_reg12[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg14[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h40000000)) 
    \slv_reg14[7]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[0]),
        .I2(\slv_reg12[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg14[7]_i_1_n_0 ));
  FDRE \slv_reg14_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg14[0]),
        .R(RST));
  FDRE \slv_reg14_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg14[10]),
        .R(RST));
  FDRE \slv_reg14_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg14[11]),
        .R(RST));
  FDRE \slv_reg14_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg14[12]),
        .R(RST));
  FDRE \slv_reg14_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg14[13]),
        .R(RST));
  FDRE \slv_reg14_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg14[14]),
        .R(RST));
  FDRE \slv_reg14_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg14[15]),
        .R(RST));
  FDRE \slv_reg14_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg14[16]),
        .R(RST));
  FDRE \slv_reg14_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg14[17]),
        .R(RST));
  FDRE \slv_reg14_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg14[18]),
        .R(RST));
  FDRE \slv_reg14_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg14[19]),
        .R(RST));
  FDRE \slv_reg14_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg14[1]),
        .R(RST));
  FDRE \slv_reg14_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg14[20]),
        .R(RST));
  FDRE \slv_reg14_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg14[21]),
        .R(RST));
  FDRE \slv_reg14_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg14[22]),
        .R(RST));
  FDRE \slv_reg14_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg14[23]),
        .R(RST));
  FDRE \slv_reg14_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg14[24]),
        .R(RST));
  FDRE \slv_reg14_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg14[25]),
        .R(RST));
  FDRE \slv_reg14_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg14[26]),
        .R(RST));
  FDRE \slv_reg14_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg14[27]),
        .R(RST));
  FDRE \slv_reg14_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg14[28]),
        .R(RST));
  FDRE \slv_reg14_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg14[29]),
        .R(RST));
  FDRE \slv_reg14_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg14[2]),
        .R(RST));
  FDRE \slv_reg14_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg14[30]),
        .R(RST));
  FDRE \slv_reg14_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg14[31]),
        .R(RST));
  FDRE \slv_reg14_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg14[3]),
        .R(RST));
  FDRE \slv_reg14_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg14[4]),
        .R(RST));
  FDRE \slv_reg14_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg14[5]),
        .R(RST));
  FDRE \slv_reg14_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg14[6]),
        .R(RST));
  FDRE \slv_reg14_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg14[7]),
        .R(RST));
  FDRE \slv_reg14_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg14[8]),
        .R(RST));
  FDRE \slv_reg14_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg14[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg14[9]),
        .R(RST));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg15[15]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[1]),
        .I2(\slv_reg12[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg15[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg15[23]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[2]),
        .I2(\slv_reg12[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg15[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg15[31]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[3]),
        .I2(\slv_reg12[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg15[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg15[7]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[0]),
        .I2(\slv_reg12[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg15[7]_i_1_n_0 ));
  FDRE \slv_reg15_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg15[0]),
        .R(RST));
  FDRE \slv_reg15_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg15[10]),
        .R(RST));
  FDRE \slv_reg15_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg15[11]),
        .R(RST));
  FDRE \slv_reg15_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg15[12]),
        .R(RST));
  FDRE \slv_reg15_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg15[13]),
        .R(RST));
  FDRE \slv_reg15_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg15[14]),
        .R(RST));
  FDRE \slv_reg15_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg15[15]),
        .R(RST));
  FDRE \slv_reg15_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg15[16]),
        .R(RST));
  FDRE \slv_reg15_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg15[17]),
        .R(RST));
  FDRE \slv_reg15_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg15[18]),
        .R(RST));
  FDRE \slv_reg15_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg15[19]),
        .R(RST));
  FDRE \slv_reg15_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg15[1]),
        .R(RST));
  FDRE \slv_reg15_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg15[20]),
        .R(RST));
  FDRE \slv_reg15_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg15[21]),
        .R(RST));
  FDRE \slv_reg15_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg15[22]),
        .R(RST));
  FDRE \slv_reg15_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg15[23]),
        .R(RST));
  FDRE \slv_reg15_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg15[24]),
        .R(RST));
  FDRE \slv_reg15_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg15[25]),
        .R(RST));
  FDRE \slv_reg15_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg15[26]),
        .R(RST));
  FDRE \slv_reg15_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg15[27]),
        .R(RST));
  FDRE \slv_reg15_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg15[28]),
        .R(RST));
  FDRE \slv_reg15_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg15[29]),
        .R(RST));
  FDRE \slv_reg15_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg15[2]),
        .R(RST));
  FDRE \slv_reg15_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg15[30]),
        .R(RST));
  FDRE \slv_reg15_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg15[31]),
        .R(RST));
  FDRE \slv_reg15_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg15[3]),
        .R(RST));
  FDRE \slv_reg15_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg15[4]),
        .R(RST));
  FDRE \slv_reg15_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg15[5]),
        .R(RST));
  FDRE \slv_reg15_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg15[6]),
        .R(RST));
  FDRE \slv_reg15_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg15[7]),
        .R(RST));
  FDRE \slv_reg15_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg15[8]),
        .R(RST));
  FDRE \slv_reg15_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg15[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg15[9]),
        .R(RST));
  LUT5 #(
    .INIT(32'h00000040)) 
    \slv_reg16[15]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[1]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg16[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00000040)) 
    \slv_reg16[23]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[2]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg16[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00000040)) 
    \slv_reg16[31]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[3]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg16[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \slv_reg16[31]_i_2 
       (.I0(p_0_in[2]),
        .I1(axi_awready_reg_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(axi_wready_reg_0),
        .I5(p_0_in[4]),
        .O(\slv_reg16[31]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00000040)) 
    \slv_reg16[7]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[0]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg16[7]_i_1_n_0 ));
  FDRE \slv_reg16_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg16[0]),
        .R(RST));
  FDRE \slv_reg16_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg16[10]),
        .R(RST));
  FDRE \slv_reg16_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg16[11]),
        .R(RST));
  FDRE \slv_reg16_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg16[12]),
        .R(RST));
  FDRE \slv_reg16_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg16[13]),
        .R(RST));
  FDRE \slv_reg16_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg16[14]),
        .R(RST));
  FDRE \slv_reg16_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg16[15]),
        .R(RST));
  FDRE \slv_reg16_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg16[16]),
        .R(RST));
  FDRE \slv_reg16_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg16[17]),
        .R(RST));
  FDRE \slv_reg16_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg16[18]),
        .R(RST));
  FDRE \slv_reg16_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg16[19]),
        .R(RST));
  FDRE \slv_reg16_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg16[1]),
        .R(RST));
  FDRE \slv_reg16_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg16[20]),
        .R(RST));
  FDRE \slv_reg16_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg16[21]),
        .R(RST));
  FDRE \slv_reg16_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg16[22]),
        .R(RST));
  FDRE \slv_reg16_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg16[23]),
        .R(RST));
  FDRE \slv_reg16_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg16[24]),
        .R(RST));
  FDRE \slv_reg16_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg16[25]),
        .R(RST));
  FDRE \slv_reg16_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg16[26]),
        .R(RST));
  FDRE \slv_reg16_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg16[27]),
        .R(RST));
  FDRE \slv_reg16_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg16[28]),
        .R(RST));
  FDRE \slv_reg16_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg16[29]),
        .R(RST));
  FDRE \slv_reg16_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg16[2]),
        .R(RST));
  FDRE \slv_reg16_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg16[30]),
        .R(RST));
  FDRE \slv_reg16_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg16[31]),
        .R(RST));
  FDRE \slv_reg16_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg16[3]),
        .R(RST));
  FDRE \slv_reg16_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg16[4]),
        .R(RST));
  FDRE \slv_reg16_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg16[5]),
        .R(RST));
  FDRE \slv_reg16_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg16[6]),
        .R(RST));
  FDRE \slv_reg16_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg16[7]),
        .R(RST));
  FDRE \slv_reg16_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg16[8]),
        .R(RST));
  FDRE \slv_reg16_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg16[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg16[9]),
        .R(RST));
  LUT5 #(
    .INIT(32'h00000080)) 
    \slv_reg17[15]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[1]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg17[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00000080)) 
    \slv_reg17[23]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[2]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg17[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00000080)) 
    \slv_reg17[31]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[3]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg17[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00000080)) 
    \slv_reg17[7]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[0]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg17[7]_i_1_n_0 ));
  FDRE \slv_reg17_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg17[0]),
        .R(RST));
  FDRE \slv_reg17_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg17[10]),
        .R(RST));
  FDRE \slv_reg17_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg17[11]),
        .R(RST));
  FDRE \slv_reg17_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg17[12]),
        .R(RST));
  FDRE \slv_reg17_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg17[13]),
        .R(RST));
  FDRE \slv_reg17_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg17[14]),
        .R(RST));
  FDRE \slv_reg17_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg17[15]),
        .R(RST));
  FDRE \slv_reg17_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg17[16]),
        .R(RST));
  FDRE \slv_reg17_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg17[17]),
        .R(RST));
  FDRE \slv_reg17_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg17[18]),
        .R(RST));
  FDRE \slv_reg17_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg17[19]),
        .R(RST));
  FDRE \slv_reg17_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg17[1]),
        .R(RST));
  FDRE \slv_reg17_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg17[20]),
        .R(RST));
  FDRE \slv_reg17_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg17[21]),
        .R(RST));
  FDRE \slv_reg17_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg17[22]),
        .R(RST));
  FDRE \slv_reg17_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg17[23]),
        .R(RST));
  FDRE \slv_reg17_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg17[24]),
        .R(RST));
  FDRE \slv_reg17_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg17[25]),
        .R(RST));
  FDRE \slv_reg17_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg17[26]),
        .R(RST));
  FDRE \slv_reg17_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg17[27]),
        .R(RST));
  FDRE \slv_reg17_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg17[28]),
        .R(RST));
  FDRE \slv_reg17_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg17[29]),
        .R(RST));
  FDRE \slv_reg17_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg17[2]),
        .R(RST));
  FDRE \slv_reg17_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg17[30]),
        .R(RST));
  FDRE \slv_reg17_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg17[31]),
        .R(RST));
  FDRE \slv_reg17_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg17[3]),
        .R(RST));
  FDRE \slv_reg17_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg17[4]),
        .R(RST));
  FDRE \slv_reg17_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg17[5]),
        .R(RST));
  FDRE \slv_reg17_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg17[6]),
        .R(RST));
  FDRE \slv_reg17_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg17[7]),
        .R(RST));
  FDRE \slv_reg17_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg17[8]),
        .R(RST));
  FDRE \slv_reg17_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg17[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg17[9]),
        .R(RST));
  LUT5 #(
    .INIT(32'h00400000)) 
    \slv_reg18[15]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[1]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg18[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00400000)) 
    \slv_reg18[23]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[2]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg18[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00400000)) 
    \slv_reg18[31]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[3]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg18[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00400000)) 
    \slv_reg18[7]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[0]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg18[7]_i_1_n_0 ));
  FDRE \slv_reg18_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg18[0]),
        .R(RST));
  FDRE \slv_reg18_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg18[10]),
        .R(RST));
  FDRE \slv_reg18_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg18[11]),
        .R(RST));
  FDRE \slv_reg18_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg18[12]),
        .R(RST));
  FDRE \slv_reg18_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg18[13]),
        .R(RST));
  FDRE \slv_reg18_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg18[14]),
        .R(RST));
  FDRE \slv_reg18_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg18[15]),
        .R(RST));
  FDRE \slv_reg18_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg18[16]),
        .R(RST));
  FDRE \slv_reg18_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg18[17]),
        .R(RST));
  FDRE \slv_reg18_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg18[18]),
        .R(RST));
  FDRE \slv_reg18_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg18[19]),
        .R(RST));
  FDRE \slv_reg18_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg18[1]),
        .R(RST));
  FDRE \slv_reg18_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg18[20]),
        .R(RST));
  FDRE \slv_reg18_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg18[21]),
        .R(RST));
  FDRE \slv_reg18_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg18[22]),
        .R(RST));
  FDRE \slv_reg18_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg18[23]),
        .R(RST));
  FDRE \slv_reg18_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg18[24]),
        .R(RST));
  FDRE \slv_reg18_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg18[25]),
        .R(RST));
  FDRE \slv_reg18_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg18[26]),
        .R(RST));
  FDRE \slv_reg18_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg18[27]),
        .R(RST));
  FDRE \slv_reg18_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg18[28]),
        .R(RST));
  FDRE \slv_reg18_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg18[29]),
        .R(RST));
  FDRE \slv_reg18_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg18[2]),
        .R(RST));
  FDRE \slv_reg18_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg18[30]),
        .R(RST));
  FDRE \slv_reg18_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg18[31]),
        .R(RST));
  FDRE \slv_reg18_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg18[3]),
        .R(RST));
  FDRE \slv_reg18_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg18[4]),
        .R(RST));
  FDRE \slv_reg18_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg18[5]),
        .R(RST));
  FDRE \slv_reg18_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg18[6]),
        .R(RST));
  FDRE \slv_reg18_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg18[7]),
        .R(RST));
  FDRE \slv_reg18_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg18[8]),
        .R(RST));
  FDRE \slv_reg18_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg18[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg18[9]),
        .R(RST));
  LUT5 #(
    .INIT(32'h00800000)) 
    \slv_reg19[15]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[1]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg19[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00800000)) 
    \slv_reg19[23]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[2]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg19[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00800000)) 
    \slv_reg19[31]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[3]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg19[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00800000)) 
    \slv_reg19[7]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[0]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg19[7]_i_1_n_0 ));
  FDRE \slv_reg19_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg19[0]),
        .R(RST));
  FDRE \slv_reg19_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg19[10]),
        .R(RST));
  FDRE \slv_reg19_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg19[11]),
        .R(RST));
  FDRE \slv_reg19_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg19[12]),
        .R(RST));
  FDRE \slv_reg19_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg19[13]),
        .R(RST));
  FDRE \slv_reg19_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg19[14]),
        .R(RST));
  FDRE \slv_reg19_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg19[15]),
        .R(RST));
  FDRE \slv_reg19_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg19[16]),
        .R(RST));
  FDRE \slv_reg19_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg19[17]),
        .R(RST));
  FDRE \slv_reg19_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg19[18]),
        .R(RST));
  FDRE \slv_reg19_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg19[19]),
        .R(RST));
  FDRE \slv_reg19_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg19[1]),
        .R(RST));
  FDRE \slv_reg19_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg19[20]),
        .R(RST));
  FDRE \slv_reg19_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg19[21]),
        .R(RST));
  FDRE \slv_reg19_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg19[22]),
        .R(RST));
  FDRE \slv_reg19_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg19[23]),
        .R(RST));
  FDRE \slv_reg19_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg19[24]),
        .R(RST));
  FDRE \slv_reg19_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg19[25]),
        .R(RST));
  FDRE \slv_reg19_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg19[26]),
        .R(RST));
  FDRE \slv_reg19_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg19[27]),
        .R(RST));
  FDRE \slv_reg19_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg19[28]),
        .R(RST));
  FDRE \slv_reg19_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg19[29]),
        .R(RST));
  FDRE \slv_reg19_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg19[2]),
        .R(RST));
  FDRE \slv_reg19_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg19[30]),
        .R(RST));
  FDRE \slv_reg19_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg19[31]),
        .R(RST));
  FDRE \slv_reg19_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg19[3]),
        .R(RST));
  FDRE \slv_reg19_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg19[4]),
        .R(RST));
  FDRE \slv_reg19_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg19[5]),
        .R(RST));
  FDRE \slv_reg19_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg19[6]),
        .R(RST));
  FDRE \slv_reg19_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg19[7]),
        .R(RST));
  FDRE \slv_reg19_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg19[8]),
        .R(RST));
  FDRE \slv_reg19_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg19[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg19[9]),
        .R(RST));
  LUT5 #(
    .INIT(32'h00000040)) 
    \slv_reg20[15]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[1]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg20[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00000040)) 
    \slv_reg20[23]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[2]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg20[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00000040)) 
    \slv_reg20[31]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[3]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg20[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_reg20[31]_i_2 
       (.I0(p_0_in[2]),
        .I1(axi_awready_reg_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(axi_wready_reg_0),
        .I5(p_0_in[4]),
        .O(\slv_reg20[31]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00000040)) 
    \slv_reg20[7]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[0]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg20[7]_i_1_n_0 ));
  FDRE \slv_reg20_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg20[0]),
        .R(RST));
  FDRE \slv_reg20_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg20[10]),
        .R(RST));
  FDRE \slv_reg20_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg20[11]),
        .R(RST));
  FDRE \slv_reg20_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg20[12]),
        .R(RST));
  FDRE \slv_reg20_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg20[13]),
        .R(RST));
  FDRE \slv_reg20_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg20[14]),
        .R(RST));
  FDRE \slv_reg20_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg20[15]),
        .R(RST));
  FDRE \slv_reg20_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg20[16]),
        .R(RST));
  FDRE \slv_reg20_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg20[17]),
        .R(RST));
  FDRE \slv_reg20_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg20[18]),
        .R(RST));
  FDRE \slv_reg20_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg20[19]),
        .R(RST));
  FDRE \slv_reg20_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg20[1]),
        .R(RST));
  FDRE \slv_reg20_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg20[20]),
        .R(RST));
  FDRE \slv_reg20_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg20[21]),
        .R(RST));
  FDRE \slv_reg20_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg20[22]),
        .R(RST));
  FDRE \slv_reg20_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg20[23]),
        .R(RST));
  FDRE \slv_reg20_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg20[24]),
        .R(RST));
  FDRE \slv_reg20_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg20[25]),
        .R(RST));
  FDRE \slv_reg20_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg20[26]),
        .R(RST));
  FDRE \slv_reg20_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg20[27]),
        .R(RST));
  FDRE \slv_reg20_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg20[28]),
        .R(RST));
  FDRE \slv_reg20_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg20[29]),
        .R(RST));
  FDRE \slv_reg20_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg20[2]),
        .R(RST));
  FDRE \slv_reg20_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg20[30]),
        .R(RST));
  FDRE \slv_reg20_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg20[31]),
        .R(RST));
  FDRE \slv_reg20_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg20[3]),
        .R(RST));
  FDRE \slv_reg20_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg20[4]),
        .R(RST));
  FDRE \slv_reg20_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg20[5]),
        .R(RST));
  FDRE \slv_reg20_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg20[6]),
        .R(RST));
  FDRE \slv_reg20_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg20[7]),
        .R(RST));
  FDRE \slv_reg20_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg20[8]),
        .R(RST));
  FDRE \slv_reg20_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg20[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg20[9]),
        .R(RST));
  LUT5 #(
    .INIT(32'h00000080)) 
    \slv_reg21[15]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[1]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg21[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00000080)) 
    \slv_reg21[23]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[2]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg21[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00000080)) 
    \slv_reg21[31]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[3]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg21[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00000080)) 
    \slv_reg21[7]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[0]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg21[7]_i_1_n_0 ));
  FDRE \slv_reg21_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg21[0]),
        .R(RST));
  FDRE \slv_reg21_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg21[10]),
        .R(RST));
  FDRE \slv_reg21_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg21[11]),
        .R(RST));
  FDRE \slv_reg21_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg21[12]),
        .R(RST));
  FDRE \slv_reg21_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg21[13]),
        .R(RST));
  FDRE \slv_reg21_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg21[14]),
        .R(RST));
  FDRE \slv_reg21_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg21[15]),
        .R(RST));
  FDRE \slv_reg21_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg21[16]),
        .R(RST));
  FDRE \slv_reg21_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg21[17]),
        .R(RST));
  FDRE \slv_reg21_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg21[18]),
        .R(RST));
  FDRE \slv_reg21_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg21[19]),
        .R(RST));
  FDRE \slv_reg21_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg21[1]),
        .R(RST));
  FDRE \slv_reg21_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg21[20]),
        .R(RST));
  FDRE \slv_reg21_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg21[21]),
        .R(RST));
  FDRE \slv_reg21_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg21[22]),
        .R(RST));
  FDRE \slv_reg21_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg21[23]),
        .R(RST));
  FDRE \slv_reg21_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg21[24]),
        .R(RST));
  FDRE \slv_reg21_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg21[25]),
        .R(RST));
  FDRE \slv_reg21_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg21[26]),
        .R(RST));
  FDRE \slv_reg21_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg21[27]),
        .R(RST));
  FDRE \slv_reg21_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg21[28]),
        .R(RST));
  FDRE \slv_reg21_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg21[29]),
        .R(RST));
  FDRE \slv_reg21_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg21[2]),
        .R(RST));
  FDRE \slv_reg21_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg21[30]),
        .R(RST));
  FDRE \slv_reg21_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg21[31]),
        .R(RST));
  FDRE \slv_reg21_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg21[3]),
        .R(RST));
  FDRE \slv_reg21_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg21[4]),
        .R(RST));
  FDRE \slv_reg21_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg21[5]),
        .R(RST));
  FDRE \slv_reg21_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg21[6]),
        .R(RST));
  FDRE \slv_reg21_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg21[7]),
        .R(RST));
  FDRE \slv_reg21_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg21[8]),
        .R(RST));
  FDRE \slv_reg21_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg21[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg21[9]),
        .R(RST));
  LUT5 #(
    .INIT(32'h00400000)) 
    \slv_reg22[15]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[1]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg22[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00400000)) 
    \slv_reg22[23]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[2]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg22[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00400000)) 
    \slv_reg22[31]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[3]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg22[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00400000)) 
    \slv_reg22[7]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[0]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg22[7]_i_1_n_0 ));
  FDRE \slv_reg22_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg22[0]),
        .R(RST));
  FDRE \slv_reg22_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg22[10]),
        .R(RST));
  FDRE \slv_reg22_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg22[11]),
        .R(RST));
  FDRE \slv_reg22_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg22[12]),
        .R(RST));
  FDRE \slv_reg22_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg22[13]),
        .R(RST));
  FDRE \slv_reg22_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg22[14]),
        .R(RST));
  FDRE \slv_reg22_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg22[15]),
        .R(RST));
  FDRE \slv_reg22_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg22[16]),
        .R(RST));
  FDRE \slv_reg22_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg22[17]),
        .R(RST));
  FDRE \slv_reg22_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg22[18]),
        .R(RST));
  FDRE \slv_reg22_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg22[19]),
        .R(RST));
  FDRE \slv_reg22_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg22[1]),
        .R(RST));
  FDRE \slv_reg22_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg22[20]),
        .R(RST));
  FDRE \slv_reg22_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg22[21]),
        .R(RST));
  FDRE \slv_reg22_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg22[22]),
        .R(RST));
  FDRE \slv_reg22_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg22[23]),
        .R(RST));
  FDRE \slv_reg22_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg22[24]),
        .R(RST));
  FDRE \slv_reg22_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg22[25]),
        .R(RST));
  FDRE \slv_reg22_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg22[26]),
        .R(RST));
  FDRE \slv_reg22_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg22[27]),
        .R(RST));
  FDRE \slv_reg22_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg22[28]),
        .R(RST));
  FDRE \slv_reg22_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg22[29]),
        .R(RST));
  FDRE \slv_reg22_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg22[2]),
        .R(RST));
  FDRE \slv_reg22_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg22[30]),
        .R(RST));
  FDRE \slv_reg22_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg22[31]),
        .R(RST));
  FDRE \slv_reg22_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg22[3]),
        .R(RST));
  FDRE \slv_reg22_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg22[4]),
        .R(RST));
  FDRE \slv_reg22_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg22[5]),
        .R(RST));
  FDRE \slv_reg22_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg22[6]),
        .R(RST));
  FDRE \slv_reg22_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg22[7]),
        .R(RST));
  FDRE \slv_reg22_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg22[8]),
        .R(RST));
  FDRE \slv_reg22_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg22[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg22[9]),
        .R(RST));
  LUT5 #(
    .INIT(32'h00800000)) 
    \slv_reg23[15]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[1]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg23[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00800000)) 
    \slv_reg23[23]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[2]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg23[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00800000)) 
    \slv_reg23[31]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[3]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg23[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00800000)) 
    \slv_reg23[7]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[0]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg23[7]_i_1_n_0 ));
  FDRE \slv_reg23_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg23[0]),
        .R(RST));
  FDRE \slv_reg23_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg23[10]),
        .R(RST));
  FDRE \slv_reg23_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg23[11]),
        .R(RST));
  FDRE \slv_reg23_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg23[12]),
        .R(RST));
  FDRE \slv_reg23_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg23[13]),
        .R(RST));
  FDRE \slv_reg23_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg23[14]),
        .R(RST));
  FDRE \slv_reg23_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg23[15]),
        .R(RST));
  FDRE \slv_reg23_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg23[16]),
        .R(RST));
  FDRE \slv_reg23_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg23[17]),
        .R(RST));
  FDRE \slv_reg23_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg23[18]),
        .R(RST));
  FDRE \slv_reg23_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg23[19]),
        .R(RST));
  FDRE \slv_reg23_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg23[1]),
        .R(RST));
  FDRE \slv_reg23_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg23[20]),
        .R(RST));
  FDRE \slv_reg23_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg23[21]),
        .R(RST));
  FDRE \slv_reg23_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg23[22]),
        .R(RST));
  FDRE \slv_reg23_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg23[23]),
        .R(RST));
  FDRE \slv_reg23_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg23[24]),
        .R(RST));
  FDRE \slv_reg23_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg23[25]),
        .R(RST));
  FDRE \slv_reg23_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg23[26]),
        .R(RST));
  FDRE \slv_reg23_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg23[27]),
        .R(RST));
  FDRE \slv_reg23_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg23[28]),
        .R(RST));
  FDRE \slv_reg23_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg23[29]),
        .R(RST));
  FDRE \slv_reg23_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg23[2]),
        .R(RST));
  FDRE \slv_reg23_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg23[30]),
        .R(RST));
  FDRE \slv_reg23_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg23[31]),
        .R(RST));
  FDRE \slv_reg23_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg23[3]),
        .R(RST));
  FDRE \slv_reg23_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg23[4]),
        .R(RST));
  FDRE \slv_reg23_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg23[5]),
        .R(RST));
  FDRE \slv_reg23_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg23[6]),
        .R(RST));
  FDRE \slv_reg23_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg23[7]),
        .R(RST));
  FDRE \slv_reg23_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg23[8]),
        .R(RST));
  FDRE \slv_reg23_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg23[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg23[9]),
        .R(RST));
  LUT5 #(
    .INIT(32'h00004000)) 
    \slv_reg24[15]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[1]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg24[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00004000)) 
    \slv_reg24[23]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[2]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg24[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00004000)) 
    \slv_reg24[31]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[3]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg24[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00004000)) 
    \slv_reg24[7]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[0]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg24[7]_i_1_n_0 ));
  FDRE \slv_reg24_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg24[0]),
        .R(RST));
  FDRE \slv_reg24_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg24[10]),
        .R(RST));
  FDRE \slv_reg24_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg24[11]),
        .R(RST));
  FDRE \slv_reg24_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg24[12]),
        .R(RST));
  FDRE \slv_reg24_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg24[13]),
        .R(RST));
  FDRE \slv_reg24_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg24[14]),
        .R(RST));
  FDRE \slv_reg24_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg24[15]),
        .R(RST));
  FDRE \slv_reg24_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg24[16]),
        .R(RST));
  FDRE \slv_reg24_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg24[17]),
        .R(RST));
  FDRE \slv_reg24_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg24[18]),
        .R(RST));
  FDRE \slv_reg24_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg24[19]),
        .R(RST));
  FDRE \slv_reg24_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg24[1]),
        .R(RST));
  FDRE \slv_reg24_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg24[20]),
        .R(RST));
  FDRE \slv_reg24_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg24[21]),
        .R(RST));
  FDRE \slv_reg24_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg24[22]),
        .R(RST));
  FDRE \slv_reg24_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg24[23]),
        .R(RST));
  FDRE \slv_reg24_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg24[24]),
        .R(RST));
  FDRE \slv_reg24_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg24[25]),
        .R(RST));
  FDRE \slv_reg24_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg24[26]),
        .R(RST));
  FDRE \slv_reg24_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg24[27]),
        .R(RST));
  FDRE \slv_reg24_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg24[28]),
        .R(RST));
  FDRE \slv_reg24_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg24[29]),
        .R(RST));
  FDRE \slv_reg24_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg24[2]),
        .R(RST));
  FDRE \slv_reg24_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg24[30]),
        .R(RST));
  FDRE \slv_reg24_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg24[31]),
        .R(RST));
  FDRE \slv_reg24_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg24[3]),
        .R(RST));
  FDRE \slv_reg24_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg24[4]),
        .R(RST));
  FDRE \slv_reg24_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg24[5]),
        .R(RST));
  FDRE \slv_reg24_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg24[6]),
        .R(RST));
  FDRE \slv_reg24_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg24[7]),
        .R(RST));
  FDRE \slv_reg24_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg24[8]),
        .R(RST));
  FDRE \slv_reg24_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg24[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg24[9]),
        .R(RST));
  LUT5 #(
    .INIT(32'h00008000)) 
    \slv_reg25[15]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[1]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg25[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00008000)) 
    \slv_reg25[23]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[2]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg25[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00008000)) 
    \slv_reg25[31]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[3]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg25[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00008000)) 
    \slv_reg25[7]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[0]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg25[7]_i_1_n_0 ));
  FDRE \slv_reg25_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg25[0]),
        .R(RST));
  FDRE \slv_reg25_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg25[10]),
        .R(RST));
  FDRE \slv_reg25_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg25[11]),
        .R(RST));
  FDRE \slv_reg25_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg25[12]),
        .R(RST));
  FDRE \slv_reg25_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg25[13]),
        .R(RST));
  FDRE \slv_reg25_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg25[14]),
        .R(RST));
  FDRE \slv_reg25_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg25[15]),
        .R(RST));
  FDRE \slv_reg25_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg25[16]),
        .R(RST));
  FDRE \slv_reg25_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg25[17]),
        .R(RST));
  FDRE \slv_reg25_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg25[18]),
        .R(RST));
  FDRE \slv_reg25_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg25[19]),
        .R(RST));
  FDRE \slv_reg25_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg25[1]),
        .R(RST));
  FDRE \slv_reg25_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg25[20]),
        .R(RST));
  FDRE \slv_reg25_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg25[21]),
        .R(RST));
  FDRE \slv_reg25_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg25[22]),
        .R(RST));
  FDRE \slv_reg25_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg25[23]),
        .R(RST));
  FDRE \slv_reg25_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg25[24]),
        .R(RST));
  FDRE \slv_reg25_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg25[25]),
        .R(RST));
  FDRE \slv_reg25_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg25[26]),
        .R(RST));
  FDRE \slv_reg25_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg25[27]),
        .R(RST));
  FDRE \slv_reg25_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg25[28]),
        .R(RST));
  FDRE \slv_reg25_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg25[29]),
        .R(RST));
  FDRE \slv_reg25_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg25[2]),
        .R(RST));
  FDRE \slv_reg25_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg25[30]),
        .R(RST));
  FDRE \slv_reg25_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg25[31]),
        .R(RST));
  FDRE \slv_reg25_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg25[3]),
        .R(RST));
  FDRE \slv_reg25_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg25[4]),
        .R(RST));
  FDRE \slv_reg25_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg25[5]),
        .R(RST));
  FDRE \slv_reg25_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg25[6]),
        .R(RST));
  FDRE \slv_reg25_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg25[7]),
        .R(RST));
  FDRE \slv_reg25_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg25[8]),
        .R(RST));
  FDRE \slv_reg25_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg25[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg25[9]),
        .R(RST));
  LUT5 #(
    .INIT(32'h40000000)) 
    \slv_reg26[15]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[1]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg26[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h40000000)) 
    \slv_reg26[23]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[2]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg26[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h40000000)) 
    \slv_reg26[31]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[3]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg26[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h40000000)) 
    \slv_reg26[7]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[0]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg26[7]_i_1_n_0 ));
  FDRE \slv_reg26_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg26[0]),
        .R(RST));
  FDRE \slv_reg26_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg26[10]),
        .R(RST));
  FDRE \slv_reg26_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg26[11]),
        .R(RST));
  FDRE \slv_reg26_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg26[12]),
        .R(RST));
  FDRE \slv_reg26_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg26[13]),
        .R(RST));
  FDRE \slv_reg26_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg26[14]),
        .R(RST));
  FDRE \slv_reg26_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg26[15]),
        .R(RST));
  FDRE \slv_reg26_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg26[16]),
        .R(RST));
  FDRE \slv_reg26_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg26[17]),
        .R(RST));
  FDRE \slv_reg26_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg26[18]),
        .R(RST));
  FDRE \slv_reg26_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg26[19]),
        .R(RST));
  FDRE \slv_reg26_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg26[1]),
        .R(RST));
  FDRE \slv_reg26_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg26[20]),
        .R(RST));
  FDRE \slv_reg26_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg26[21]),
        .R(RST));
  FDRE \slv_reg26_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg26[22]),
        .R(RST));
  FDRE \slv_reg26_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg26[23]),
        .R(RST));
  FDRE \slv_reg26_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg26[24]),
        .R(RST));
  FDRE \slv_reg26_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg26[25]),
        .R(RST));
  FDRE \slv_reg26_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg26[26]),
        .R(RST));
  FDRE \slv_reg26_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg26[27]),
        .R(RST));
  FDRE \slv_reg26_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg26[28]),
        .R(RST));
  FDRE \slv_reg26_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg26[29]),
        .R(RST));
  FDRE \slv_reg26_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg26[2]),
        .R(RST));
  FDRE \slv_reg26_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg26[30]),
        .R(RST));
  FDRE \slv_reg26_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg26[31]),
        .R(RST));
  FDRE \slv_reg26_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg26[3]),
        .R(RST));
  FDRE \slv_reg26_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg26[4]),
        .R(RST));
  FDRE \slv_reg26_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg26[5]),
        .R(RST));
  FDRE \slv_reg26_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg26[6]),
        .R(RST));
  FDRE \slv_reg26_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg26[7]),
        .R(RST));
  FDRE \slv_reg26_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg26[8]),
        .R(RST));
  FDRE \slv_reg26_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg26[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg26[9]),
        .R(RST));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg27[15]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[1]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg27[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg27[23]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[2]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg27[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg27[31]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[3]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg27[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg27[7]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[0]),
        .I2(\slv_reg16[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg27[7]_i_1_n_0 ));
  FDRE \slv_reg27_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg27[0]),
        .R(RST));
  FDRE \slv_reg27_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg27[10]),
        .R(RST));
  FDRE \slv_reg27_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg27[11]),
        .R(RST));
  FDRE \slv_reg27_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg27[12]),
        .R(RST));
  FDRE \slv_reg27_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg27[13]),
        .R(RST));
  FDRE \slv_reg27_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg27[14]),
        .R(RST));
  FDRE \slv_reg27_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg27[15]),
        .R(RST));
  FDRE \slv_reg27_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg27[16]),
        .R(RST));
  FDRE \slv_reg27_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg27[17]),
        .R(RST));
  FDRE \slv_reg27_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg27[18]),
        .R(RST));
  FDRE \slv_reg27_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg27[19]),
        .R(RST));
  FDRE \slv_reg27_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg27[1]),
        .R(RST));
  FDRE \slv_reg27_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg27[20]),
        .R(RST));
  FDRE \slv_reg27_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg27[21]),
        .R(RST));
  FDRE \slv_reg27_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg27[22]),
        .R(RST));
  FDRE \slv_reg27_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg27[23]),
        .R(RST));
  FDRE \slv_reg27_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg27[24]),
        .R(RST));
  FDRE \slv_reg27_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg27[25]),
        .R(RST));
  FDRE \slv_reg27_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg27[26]),
        .R(RST));
  FDRE \slv_reg27_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg27[27]),
        .R(RST));
  FDRE \slv_reg27_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg27[28]),
        .R(RST));
  FDRE \slv_reg27_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg27[29]),
        .R(RST));
  FDRE \slv_reg27_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg27[2]),
        .R(RST));
  FDRE \slv_reg27_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg27[30]),
        .R(RST));
  FDRE \slv_reg27_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg27[31]),
        .R(RST));
  FDRE \slv_reg27_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg27[3]),
        .R(RST));
  FDRE \slv_reg27_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg27[4]),
        .R(RST));
  FDRE \slv_reg27_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg27[5]),
        .R(RST));
  FDRE \slv_reg27_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg27[6]),
        .R(RST));
  FDRE \slv_reg27_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg27[7]),
        .R(RST));
  FDRE \slv_reg27_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg27[8]),
        .R(RST));
  FDRE \slv_reg27_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg27[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg27[9]),
        .R(RST));
  LUT5 #(
    .INIT(32'h00004000)) 
    \slv_reg28[15]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[1]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg28[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00004000)) 
    \slv_reg28[23]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[2]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg28[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00004000)) 
    \slv_reg28[31]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[3]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg28[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00004000)) 
    \slv_reg28[7]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[0]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg28[7]_i_1_n_0 ));
  FDRE \slv_reg28_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg28[0]),
        .R(RST));
  FDRE \slv_reg28_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg28[10]),
        .R(RST));
  FDRE \slv_reg28_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg28[11]),
        .R(RST));
  FDRE \slv_reg28_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg28[12]),
        .R(RST));
  FDRE \slv_reg28_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg28[13]),
        .R(RST));
  FDRE \slv_reg28_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg28[14]),
        .R(RST));
  FDRE \slv_reg28_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg28[15]),
        .R(RST));
  FDRE \slv_reg28_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg28[16]),
        .R(RST));
  FDRE \slv_reg28_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg28[17]),
        .R(RST));
  FDRE \slv_reg28_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg28[18]),
        .R(RST));
  FDRE \slv_reg28_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg28[19]),
        .R(RST));
  FDRE \slv_reg28_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg28[1]),
        .R(RST));
  FDRE \slv_reg28_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg28[20]),
        .R(RST));
  FDRE \slv_reg28_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg28[21]),
        .R(RST));
  FDRE \slv_reg28_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg28[22]),
        .R(RST));
  FDRE \slv_reg28_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg28[23]),
        .R(RST));
  FDRE \slv_reg28_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg28[24]),
        .R(RST));
  FDRE \slv_reg28_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg28[25]),
        .R(RST));
  FDRE \slv_reg28_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg28[26]),
        .R(RST));
  FDRE \slv_reg28_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg28[27]),
        .R(RST));
  FDRE \slv_reg28_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg28[28]),
        .R(RST));
  FDRE \slv_reg28_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg28[29]),
        .R(RST));
  FDRE \slv_reg28_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg28[2]),
        .R(RST));
  FDRE \slv_reg28_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg28[30]),
        .R(RST));
  FDRE \slv_reg28_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg28[31]),
        .R(RST));
  FDRE \slv_reg28_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg28[3]),
        .R(RST));
  FDRE \slv_reg28_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg28[4]),
        .R(RST));
  FDRE \slv_reg28_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg28[5]),
        .R(RST));
  FDRE \slv_reg28_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg28[6]),
        .R(RST));
  FDRE \slv_reg28_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg28[7]),
        .R(RST));
  FDRE \slv_reg28_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg28[8]),
        .R(RST));
  FDRE \slv_reg28_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg28[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg28[9]),
        .R(RST));
  LUT5 #(
    .INIT(32'h00008000)) 
    \slv_reg29[15]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[1]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg29[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00008000)) 
    \slv_reg29[23]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[2]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg29[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00008000)) 
    \slv_reg29[31]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[3]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg29[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00008000)) 
    \slv_reg29[7]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[0]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg29[7]_i_1_n_0 ));
  FDRE \slv_reg29_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg29[0]),
        .R(RST));
  FDRE \slv_reg29_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg29[10]),
        .R(RST));
  FDRE \slv_reg29_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg29[11]),
        .R(RST));
  FDRE \slv_reg29_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg29[12]),
        .R(RST));
  FDRE \slv_reg29_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg29[13]),
        .R(RST));
  FDRE \slv_reg29_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg29[14]),
        .R(RST));
  FDRE \slv_reg29_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg29[15]),
        .R(RST));
  FDRE \slv_reg29_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg29[16]),
        .R(RST));
  FDRE \slv_reg29_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg29[17]),
        .R(RST));
  FDRE \slv_reg29_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg29[18]),
        .R(RST));
  FDRE \slv_reg29_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg29[19]),
        .R(RST));
  FDRE \slv_reg29_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg29[1]),
        .R(RST));
  FDRE \slv_reg29_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg29[20]),
        .R(RST));
  FDRE \slv_reg29_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg29[21]),
        .R(RST));
  FDRE \slv_reg29_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg29[22]),
        .R(RST));
  FDRE \slv_reg29_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg29[23]),
        .R(RST));
  FDRE \slv_reg29_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg29[24]),
        .R(RST));
  FDRE \slv_reg29_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg29[25]),
        .R(RST));
  FDRE \slv_reg29_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg29[26]),
        .R(RST));
  FDRE \slv_reg29_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg29[27]),
        .R(RST));
  FDRE \slv_reg29_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg29[28]),
        .R(RST));
  FDRE \slv_reg29_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg29[29]),
        .R(RST));
  FDRE \slv_reg29_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg29[2]),
        .R(RST));
  FDRE \slv_reg29_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg29[30]),
        .R(RST));
  FDRE \slv_reg29_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg29[31]),
        .R(RST));
  FDRE \slv_reg29_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg29[3]),
        .R(RST));
  FDRE \slv_reg29_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg29[4]),
        .R(RST));
  FDRE \slv_reg29_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg29[5]),
        .R(RST));
  FDRE \slv_reg29_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg29[6]),
        .R(RST));
  FDRE \slv_reg29_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg29[7]),
        .R(RST));
  FDRE \slv_reg29_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg29[8]),
        .R(RST));
  FDRE \slv_reg29_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg29[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg29[9]),
        .R(RST));
  LUT5 #(
    .INIT(32'h40000000)) 
    \slv_reg30[15]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[1]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg30[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h40000000)) 
    \slv_reg30[23]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[2]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg30[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h40000000)) 
    \slv_reg30[31]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[3]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg30[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h40000000)) 
    \slv_reg30[7]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[0]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg30[7]_i_1_n_0 ));
  FDRE \slv_reg30_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg30[0]),
        .R(RST));
  FDRE \slv_reg30_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg30[10]),
        .R(RST));
  FDRE \slv_reg30_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg30[11]),
        .R(RST));
  FDRE \slv_reg30_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg30[12]),
        .R(RST));
  FDRE \slv_reg30_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg30[13]),
        .R(RST));
  FDRE \slv_reg30_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg30[14]),
        .R(RST));
  FDRE \slv_reg30_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg30[15]),
        .R(RST));
  FDRE \slv_reg30_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg30[16]),
        .R(RST));
  FDRE \slv_reg30_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg30[17]),
        .R(RST));
  FDRE \slv_reg30_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg30[18]),
        .R(RST));
  FDRE \slv_reg30_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg30[19]),
        .R(RST));
  FDRE \slv_reg30_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg30[1]),
        .R(RST));
  FDRE \slv_reg30_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg30[20]),
        .R(RST));
  FDRE \slv_reg30_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg30[21]),
        .R(RST));
  FDRE \slv_reg30_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg30[22]),
        .R(RST));
  FDRE \slv_reg30_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg30[23]),
        .R(RST));
  FDRE \slv_reg30_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg30[24]),
        .R(RST));
  FDRE \slv_reg30_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg30[25]),
        .R(RST));
  FDRE \slv_reg30_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg30[26]),
        .R(RST));
  FDRE \slv_reg30_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg30[27]),
        .R(RST));
  FDRE \slv_reg30_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg30[28]),
        .R(RST));
  FDRE \slv_reg30_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg30[29]),
        .R(RST));
  FDRE \slv_reg30_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg30[2]),
        .R(RST));
  FDRE \slv_reg30_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg30[30]),
        .R(RST));
  FDRE \slv_reg30_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg30[31]),
        .R(RST));
  FDRE \slv_reg30_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg30[3]),
        .R(RST));
  FDRE \slv_reg30_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg30[4]),
        .R(RST));
  FDRE \slv_reg30_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg30[5]),
        .R(RST));
  FDRE \slv_reg30_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg30[6]),
        .R(RST));
  FDRE \slv_reg30_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg30[7]),
        .R(RST));
  FDRE \slv_reg30_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg30[8]),
        .R(RST));
  FDRE \slv_reg30_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg30[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg30[9]),
        .R(RST));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg31[15]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[1]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg31[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg31[23]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[2]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg31[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg31[31]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[3]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg31[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg31[7]_i_1 
       (.I0(p_0_in[0]),
        .I1(s00_axi_wstrb[0]),
        .I2(\slv_reg20[31]_i_2_n_0 ),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg31[7]_i_1_n_0 ));
  FDRE \slv_reg31_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg31[0]),
        .R(RST));
  FDRE \slv_reg31_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg31[10]),
        .R(RST));
  FDRE \slv_reg31_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg31[11]),
        .R(RST));
  FDRE \slv_reg31_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg31[12]),
        .R(RST));
  FDRE \slv_reg31_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg31[13]),
        .R(RST));
  FDRE \slv_reg31_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg31[14]),
        .R(RST));
  FDRE \slv_reg31_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg31[15]),
        .R(RST));
  FDRE \slv_reg31_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg31[16]),
        .R(RST));
  FDRE \slv_reg31_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg31[17]),
        .R(RST));
  FDRE \slv_reg31_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg31[18]),
        .R(RST));
  FDRE \slv_reg31_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg31[19]),
        .R(RST));
  FDRE \slv_reg31_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg31[1]),
        .R(RST));
  FDRE \slv_reg31_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg31[20]),
        .R(RST));
  FDRE \slv_reg31_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg31[21]),
        .R(RST));
  FDRE \slv_reg31_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg31[22]),
        .R(RST));
  FDRE \slv_reg31_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg31[23]),
        .R(RST));
  FDRE \slv_reg31_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg31[24]),
        .R(RST));
  FDRE \slv_reg31_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg31[25]),
        .R(RST));
  FDRE \slv_reg31_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg31[26]),
        .R(RST));
  FDRE \slv_reg31_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg31[27]),
        .R(RST));
  FDRE \slv_reg31_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg31[28]),
        .R(RST));
  FDRE \slv_reg31_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg31[29]),
        .R(RST));
  FDRE \slv_reg31_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg31[2]),
        .R(RST));
  FDRE \slv_reg31_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg31[30]),
        .R(RST));
  FDRE \slv_reg31_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg31[31]),
        .R(RST));
  FDRE \slv_reg31_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg31[3]),
        .R(RST));
  FDRE \slv_reg31_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg31[4]),
        .R(RST));
  FDRE \slv_reg31_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg31[5]),
        .R(RST));
  FDRE \slv_reg31_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg31[6]),
        .R(RST));
  FDRE \slv_reg31_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg31[7]),
        .R(RST));
  FDRE \slv_reg31_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg31[8]),
        .R(RST));
  FDRE \slv_reg31_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg31[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg31[9]),
        .R(RST));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rowCounter
   (Q,
    S,
    \tempCnt_reg[3]_0 ,
    \tempCnt_reg[3]_1 ,
    \tempCnt_reg[3]_2 ,
    \tempCnt_reg[9]_0 ,
    \tempCnt_reg[2]_0 ,
    \tempCnt_reg[9]_1 ,
    \tempCnt_reg[9]_2 ,
    \tempCnt_reg[0]_0 ,
    \tempCnt_reg[9]_3 ,
    \tempCnt_reg[4]_0 ,
    \tempCnt_reg[0]_1 ,
    \tempCnt_reg[9]_4 ,
    \tempCnt_reg[9]_5 ,
    \tempCnt_reg[9]_6 ,
    \tempCnt_reg[9]_7 ,
    DI,
    \tempCnt_reg[9]_8 ,
    \tempCnt_reg[4]_1 ,
    \tempCnt_reg[8]_0 ,
    \tempCnt_reg[9]_9 ,
    s00_axi_aresetn,
    O,
    \r7_inferred__1/i___23_carry ,
    \r7_inferred__1/i___29_carry ,
    \dc_bias[3]_i_8__1 ,
    \dc_bias[3]_i_8__1_0 ,
    \dc_bias[3]_i_8__1_1 ,
    \dc_bias[3]_i_23_0 ,
    rollWire,
    CO,
    \dc_bias[3]_i_9__1 ,
    \dc_bias[3]_i_9__1_0 ,
    \dc_bias[3]_i_9__1_1 ,
    \dc_bias[3]_i_9__1_2 ,
    \r7_inferred__1/i___23_carry_0 ,
    \dc_bias[3]_i_23_1 ,
    \dc_bias[3]_i_42_0 ,
    E,
    clk_out1);
  output [9:0]Q;
  output [2:0]S;
  output [3:0]\tempCnt_reg[3]_0 ;
  output \tempCnt_reg[3]_1 ;
  output \tempCnt_reg[3]_2 ;
  output \tempCnt_reg[9]_0 ;
  output \tempCnt_reg[2]_0 ;
  output \tempCnt_reg[9]_1 ;
  output [3:0]\tempCnt_reg[9]_2 ;
  output \tempCnt_reg[0]_0 ;
  output \tempCnt_reg[9]_3 ;
  output \tempCnt_reg[4]_0 ;
  output \tempCnt_reg[0]_1 ;
  output [3:0]\tempCnt_reg[9]_4 ;
  output [3:0]\tempCnt_reg[9]_5 ;
  output [3:0]\tempCnt_reg[9]_6 ;
  output [3:0]\tempCnt_reg[9]_7 ;
  output [0:0]DI;
  output [1:0]\tempCnt_reg[9]_8 ;
  output [2:0]\tempCnt_reg[4]_1 ;
  output [3:0]\tempCnt_reg[8]_0 ;
  output [0:0]\tempCnt_reg[9]_9 ;
  input s00_axi_aresetn;
  input [2:0]O;
  input [2:0]\r7_inferred__1/i___23_carry ;
  input [0:0]\r7_inferred__1/i___29_carry ;
  input [0:0]\dc_bias[3]_i_8__1 ;
  input [0:0]\dc_bias[3]_i_8__1_0 ;
  input \dc_bias[3]_i_8__1_1 ;
  input \dc_bias[3]_i_23_0 ;
  input rollWire;
  input [0:0]CO;
  input [0:0]\dc_bias[3]_i_9__1 ;
  input [0:0]\dc_bias[3]_i_9__1_0 ;
  input [0:0]\dc_bias[3]_i_9__1_1 ;
  input [1:0]\dc_bias[3]_i_9__1_2 ;
  input [0:0]\r7_inferred__1/i___23_carry_0 ;
  input [2:0]\dc_bias[3]_i_23_1 ;
  input [3:0]\dc_bias[3]_i_42_0 ;
  input [0:0]E;
  input clk_out1;

  wire [0:0]CO;
  wire [0:0]DI;
  wire [0:0]E;
  wire [2:0]O;
  wire [9:0]Q;
  wire [2:0]S;
  wire clk_out1;
  wire \dc_bias[3]_i_23_0 ;
  wire [2:0]\dc_bias[3]_i_23_1 ;
  wire \dc_bias[3]_i_34_n_0 ;
  wire \dc_bias[3]_i_35_n_0 ;
  wire \dc_bias[3]_i_40_n_0 ;
  wire \dc_bias[3]_i_41_n_0 ;
  wire [3:0]\dc_bias[3]_i_42_0 ;
  wire \dc_bias[3]_i_42_n_0 ;
  wire \dc_bias[3]_i_53_n_0 ;
  wire \dc_bias[3]_i_54_n_0 ;
  wire \dc_bias[3]_i_55_n_0 ;
  wire \dc_bias[3]_i_57_n_0 ;
  wire \dc_bias[3]_i_58_n_0 ;
  wire \dc_bias[3]_i_59_n_0 ;
  wire \dc_bias[3]_i_72_n_0 ;
  wire \dc_bias[3]_i_73_n_0 ;
  wire \dc_bias[3]_i_77_n_0 ;
  wire \dc_bias[3]_i_78_n_0 ;
  wire \dc_bias[3]_i_83_n_0 ;
  wire \dc_bias[3]_i_84_n_0 ;
  wire [0:0]\dc_bias[3]_i_8__1 ;
  wire [0:0]\dc_bias[3]_i_8__1_0 ;
  wire \dc_bias[3]_i_8__1_1 ;
  wire [0:0]\dc_bias[3]_i_9__1 ;
  wire [0:0]\dc_bias[3]_i_9__1_0 ;
  wire [0:0]\dc_bias[3]_i_9__1_1 ;
  wire [1:0]\dc_bias[3]_i_9__1_2 ;
  wire \encoded[9]_i_3_n_0 ;
  wire [9:0]plusOp__0;
  wire [2:0]\r7_inferred__1/i___23_carry ;
  wire [0:0]\r7_inferred__1/i___23_carry_0 ;
  wire [0:0]\r7_inferred__1/i___29_carry ;
  wire rollWire;
  wire s00_axi_aresetn;
  wire \tempCnt[6]_i_2_n_0 ;
  wire \tempCnt[9]_i_1__1_n_0 ;
  wire \tempCnt[9]_i_4__1_n_0 ;
  wire \tempCnt[9]_i_5__1_n_0 ;
  wire \tempCnt[9]_i_6__0_n_0 ;
  wire \tempCnt[9]_i_8_n_0 ;
  wire \tempCnt_reg[0]_0 ;
  wire \tempCnt_reg[0]_1 ;
  wire \tempCnt_reg[2]_0 ;
  wire [3:0]\tempCnt_reg[3]_0 ;
  wire \tempCnt_reg[3]_1 ;
  wire \tempCnt_reg[3]_2 ;
  wire \tempCnt_reg[4]_0 ;
  wire [2:0]\tempCnt_reg[4]_1 ;
  wire [3:0]\tempCnt_reg[8]_0 ;
  wire \tempCnt_reg[9]_0 ;
  wire \tempCnt_reg[9]_1 ;
  wire [3:0]\tempCnt_reg[9]_2 ;
  wire \tempCnt_reg[9]_3 ;
  wire [3:0]\tempCnt_reg[9]_4 ;
  wire [3:0]\tempCnt_reg[9]_5 ;
  wire [3:0]\tempCnt_reg[9]_6 ;
  wire [3:0]\tempCnt_reg[9]_7 ;
  wire [1:0]\tempCnt_reg[9]_8 ;
  wire [0:0]\tempCnt_reg[9]_9 ;

  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT5 #(
    .INIT(32'hBBBFAAAA)) 
    \dc_bias[3]_i_17 
       (.I0(Q[9]),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(\tempCnt[9]_i_4__1_n_0 ),
        .O(\tempCnt_reg[9]_3 ));
  LUT6 #(
    .INIT(64'h00FF00FE00FF0000)) 
    \dc_bias[3]_i_18__0 
       (.I0(Q[4]),
        .I1(Q[3]),
        .I2(\dc_bias[3]_i_34_n_0 ),
        .I3(\dc_bias[3]_i_35_n_0 ),
        .I4(Q[6]),
        .I5(Q[5]),
        .O(\tempCnt_reg[4]_0 ));
  LUT6 #(
    .INIT(64'hF4F4F4F4F4FFFFF4)) 
    \dc_bias[3]_i_23 
       (.I0(\dc_bias[3]_i_40_n_0 ),
        .I1(\dc_bias[3]_i_41_n_0 ),
        .I2(\dc_bias[3]_i_42_n_0 ),
        .I3(\dc_bias[3]_i_8__1 ),
        .I4(\dc_bias[3]_i_8__1_0 ),
        .I5(\dc_bias[3]_i_8__1_1 ),
        .O(\tempCnt_reg[3]_1 ));
  LUT6 #(
    .INIT(64'h0000EEEEFFFEEEEE)) 
    \dc_bias[3]_i_27 
       (.I0(CO),
        .I1(\dc_bias[3]_i_9__1 ),
        .I2(\dc_bias[3]_i_9__1_0 ),
        .I3(\dc_bias[3]_i_9__1_1 ),
        .I4(\dc_bias[3]_i_9__1_2 [0]),
        .I5(\dc_bias[3]_i_9__1_2 [1]),
        .O(\tempCnt_reg[0]_1 ));
  LUT6 #(
    .INIT(64'hFFFF1FFFFFFFFFFF)) 
    \dc_bias[3]_i_28 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(\dc_bias[3]_i_53_n_0 ),
        .I4(Q[9]),
        .I5(\tempCnt[9]_i_4__1_n_0 ),
        .O(\tempCnt_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'hA8)) 
    \dc_bias[3]_i_34 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .O(\dc_bias[3]_i_34_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \dc_bias[3]_i_35 
       (.I0(Q[8]),
        .I1(Q[7]),
        .O(\dc_bias[3]_i_35_n_0 ));
  LUT6 #(
    .INIT(64'hFFF1F5F5F5F5FFF5)) 
    \dc_bias[3]_i_40 
       (.I0(\dc_bias[3]_i_54_n_0 ),
        .I1(\dc_bias[3]_i_55_n_0 ),
        .I2(\dc_bias[3]_i_23_0 ),
        .I3(\dc_bias[3]_i_57_n_0 ),
        .I4(Q[3]),
        .I5(Q[1]),
        .O(\dc_bias[3]_i_40_n_0 ));
  LUT6 #(
    .INIT(64'h57105702FFFFFFFF)) 
    \dc_bias[3]_i_41 
       (.I0(\dc_bias[3]_i_55_n_0 ),
        .I1(\dc_bias[3]_i_58_n_0 ),
        .I2(Q[3]),
        .I3(\dc_bias[3]_i_57_n_0 ),
        .I4(Q[1]),
        .I5(Q[2]),
        .O(\dc_bias[3]_i_41_n_0 ));
  LUT4 #(
    .INIT(16'h1004)) 
    \dc_bias[3]_i_42 
       (.I0(\dc_bias[3]_i_59_n_0 ),
        .I1(\dc_bias[3]_i_23_1 [0]),
        .I2(\dc_bias[3]_i_23_1 [2]),
        .I3(\dc_bias[3]_i_42_0 [1]),
        .O(\dc_bias[3]_i_42_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \dc_bias[3]_i_53 
       (.I0(Q[4]),
        .I1(Q[3]),
        .O(\dc_bias[3]_i_53_n_0 ));
  LUT6 #(
    .INIT(64'hABAAAAFAFAAFAFAB)) 
    \dc_bias[3]_i_54 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[3]),
        .I3(Q[4]),
        .I4(\dc_bias[3]_i_72_n_0 ),
        .I5(\dc_bias[3]_i_73_n_0 ),
        .O(\dc_bias[3]_i_54_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \dc_bias[3]_i_55 
       (.I0(\dc_bias[3]_i_72_n_0 ),
        .I1(Q[4]),
        .I2(Q[3]),
        .O(\dc_bias[3]_i_55_n_0 ));
  LUT6 #(
    .INIT(64'h71990088EEFF6671)) 
    \dc_bias[3]_i_57 
       (.I0(Q[5]),
        .I1(\dc_bias[3]_i_77_n_0 ),
        .I2(Q[3]),
        .I3(Q[4]),
        .I4(\dc_bias[3]_i_72_n_0 ),
        .I5(\dc_bias[3]_i_78_n_0 ),
        .O(\dc_bias[3]_i_57_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT5 #(
    .INIT(32'h6C9336C9)) 
    \dc_bias[3]_i_58 
       (.I0(Q[3]),
        .I1(\dc_bias[3]_i_77_n_0 ),
        .I2(Q[4]),
        .I3(Q[5]),
        .I4(\dc_bias[3]_i_72_n_0 ),
        .O(\dc_bias[3]_i_58_n_0 ));
  LUT4 #(
    .INIT(16'hFFEF)) 
    \dc_bias[3]_i_59 
       (.I0(\dc_bias[3]_i_42_0 [0]),
        .I1(\dc_bias[3]_i_23_1 [1]),
        .I2(\dc_bias[3]_i_42_0 [2]),
        .I3(\dc_bias[3]_i_42_0 [3]),
        .O(\dc_bias[3]_i_59_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT4 #(
    .INIT(16'hEBFF)) 
    \dc_bias[3]_i_69 
       (.I0(\dc_bias[3]_i_83_n_0 ),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .O(\tempCnt_reg[2]_0 ));
  LUT6 #(
    .INIT(64'h75599AAE51188AA6)) 
    \dc_bias[3]_i_72 
       (.I0(Q[8]),
        .I1(Q[7]),
        .I2(Q[9]),
        .I3(Q[5]),
        .I4(Q[6]),
        .I5(Q[4]),
        .O(\dc_bias[3]_i_72_n_0 ));
  LUT6 #(
    .INIT(64'h6B669499D69D2962)) 
    \dc_bias[3]_i_73 
       (.I0(Q[9]),
        .I1(Q[7]),
        .I2(Q[6]),
        .I3(Q[8]),
        .I4(Q[4]),
        .I5(Q[5]),
        .O(\dc_bias[3]_i_73_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT5 #(
    .INIT(32'h0CFBB20C)) 
    \dc_bias[3]_i_77 
       (.I0(Q[5]),
        .I1(Q[8]),
        .I2(Q[6]),
        .I3(Q[7]),
        .I4(Q[9]),
        .O(\dc_bias[3]_i_77_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT5 #(
    .INIT(32'h659A18E7)) 
    \dc_bias[3]_i_78 
       (.I0(Q[8]),
        .I1(Q[7]),
        .I2(Q[9]),
        .I3(Q[5]),
        .I4(Q[6]),
        .O(\dc_bias[3]_i_78_n_0 ));
  LUT6 #(
    .INIT(64'hFBFFFFFFFFFFFFFF)) 
    \dc_bias[3]_i_83 
       (.I0(Q[5]),
        .I1(Q[6]),
        .I2(Q[8]),
        .I3(\dc_bias[3]_i_84_n_0 ),
        .I4(Q[3]),
        .I5(Q[4]),
        .O(\dc_bias[3]_i_83_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \dc_bias[3]_i_84 
       (.I0(Q[7]),
        .I1(Q[9]),
        .O(\dc_bias[3]_i_84_n_0 ));
  LUT6 #(
    .INIT(64'hEAAAEAAAEAAAAAAA)) 
    \encoded[7]_i_3__0 
       (.I0(Q[9]),
        .I1(Q[7]),
        .I2(Q[8]),
        .I3(Q[6]),
        .I4(Q[5]),
        .I5(\tempCnt[9]_i_8_n_0 ),
        .O(\tempCnt_reg[9]_1 ));
  LUT6 #(
    .INIT(64'h0000000000000440)) 
    \encoded[9]_i_2 
       (.I0(\encoded[9]_i_3_n_0 ),
        .I1(Q[3]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[2]),
        .I5(Q[4]),
        .O(\tempCnt_reg[3]_2 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT5 #(
    .INIT(32'hFF7FFFFF)) 
    \encoded[9]_i_3 
       (.I0(Q[5]),
        .I1(Q[6]),
        .I2(Q[7]),
        .I3(Q[9]),
        .I4(Q[8]),
        .O(\encoded[9]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    i___23_carry_i_1
       (.I0(\r7_inferred__1/i___23_carry [2]),
        .I1(\r7_inferred__1/i___29_carry ),
        .O(DI));
  LUT4 #(
    .INIT(16'hD22D)) 
    i___23_carry_i_2
       (.I0(\r7_inferred__1/i___23_carry_0 ),
        .I1(\r7_inferred__1/i___23_carry [0]),
        .I2(\r7_inferred__1/i___23_carry [1]),
        .I3(\r7_inferred__1/i___29_carry ),
        .O(\tempCnt_reg[9]_8 [1]));
  LUT4 #(
    .INIT(16'h2DD2)) 
    i___23_carry_i_3
       (.I0(\r7_inferred__1/i___29_carry ),
        .I1(\r7_inferred__1/i___23_carry [2]),
        .I2(\r7_inferred__1/i___23_carry_0 ),
        .I3(\r7_inferred__1/i___23_carry [0]),
        .O(\tempCnt_reg[9]_8 [0]));
  LUT2 #(
    .INIT(4'h9)) 
    i___29_carry__0_i_1
       (.I0(Q[6]),
        .I1(O[2]),
        .O(S[2]));
  LUT2 #(
    .INIT(4'h9)) 
    i___29_carry__0_i_2
       (.I0(Q[5]),
        .I1(O[1]),
        .O(S[1]));
  LUT2 #(
    .INIT(4'h9)) 
    i___29_carry__0_i_3
       (.I0(Q[4]),
        .I1(O[0]),
        .O(S[0]));
  LUT2 #(
    .INIT(4'h9)) 
    i___29_carry_i_1
       (.I0(Q[3]),
        .I1(\r7_inferred__1/i___23_carry [1]),
        .O(\tempCnt_reg[3]_0 [3]));
  LUT2 #(
    .INIT(4'h9)) 
    i___29_carry_i_2
       (.I0(Q[2]),
        .I1(\r7_inferred__1/i___23_carry [0]),
        .O(\tempCnt_reg[3]_0 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    i___29_carry_i_3
       (.I0(Q[1]),
        .I1(\r7_inferred__1/i___29_carry ),
        .O(\tempCnt_reg[3]_0 [1]));
  LUT1 #(
    .INIT(2'h1)) 
    i___29_carry_i_4
       (.I0(Q[0]),
        .O(\tempCnt_reg[3]_0 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__0_i_1
       (.I0(Q[8]),
        .I1(Q[6]),
        .O(\tempCnt_reg[8]_0 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__0_i_2
       (.I0(Q[7]),
        .I1(Q[5]),
        .O(\tempCnt_reg[8]_0 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__0_i_3
       (.I0(Q[6]),
        .I1(Q[4]),
        .O(\tempCnt_reg[8]_0 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__0_i_4
       (.I0(Q[5]),
        .I1(Q[3]),
        .O(\tempCnt_reg[8]_0 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry__1_i_1
       (.I0(Q[9]),
        .I1(Q[7]),
        .O(\tempCnt_reg[9]_9 ));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_1__1
       (.I0(Q[9]),
        .O(\tempCnt_reg[9]_5 [3]));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_1__2
       (.I0(Q[9]),
        .O(\tempCnt_reg[9]_4 [3]));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_1__3
       (.I0(Q[9]),
        .O(\tempCnt_reg[9]_6 [3]));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_1__4
       (.I0(Q[9]),
        .O(\tempCnt_reg[9]_2 [3]));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_1__5
       (.I0(Q[9]),
        .O(\tempCnt_reg[9]_7 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry_i_1__6
       (.I0(Q[4]),
        .I1(Q[2]),
        .O(\tempCnt_reg[4]_1 [2]));
  LUT3 #(
    .INIT(8'h08)) 
    i__carry_i_2__0
       (.I0(Q[6]),
        .I1(Q[7]),
        .I2(Q[8]),
        .O(\tempCnt_reg[9]_5 [2]));
  LUT3 #(
    .INIT(8'h08)) 
    i__carry_i_2__1
       (.I0(Q[6]),
        .I1(Q[7]),
        .I2(Q[8]),
        .O(\tempCnt_reg[9]_6 [2]));
  LUT3 #(
    .INIT(8'h08)) 
    i__carry_i_2__2
       (.I0(Q[6]),
        .I1(Q[7]),
        .I2(Q[8]),
        .O(\tempCnt_reg[9]_4 [2]));
  LUT3 #(
    .INIT(8'h08)) 
    i__carry_i_2__3
       (.I0(Q[6]),
        .I1(Q[7]),
        .I2(Q[8]),
        .O(\tempCnt_reg[9]_7 [2]));
  LUT3 #(
    .INIT(8'h08)) 
    i__carry_i_2__4
       (.I0(Q[6]),
        .I1(Q[7]),
        .I2(Q[8]),
        .O(\tempCnt_reg[9]_2 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry_i_2__5
       (.I0(Q[3]),
        .I1(Q[1]),
        .O(\tempCnt_reg[4]_1 [1]));
  LUT3 #(
    .INIT(8'h08)) 
    i__carry_i_3__0
       (.I0(Q[3]),
        .I1(Q[4]),
        .I2(Q[5]),
        .O(\tempCnt_reg[9]_5 [1]));
  LUT3 #(
    .INIT(8'h08)) 
    i__carry_i_3__1
       (.I0(Q[3]),
        .I1(Q[4]),
        .I2(Q[5]),
        .O(\tempCnt_reg[9]_6 [1]));
  LUT3 #(
    .INIT(8'h08)) 
    i__carry_i_3__2
       (.I0(Q[3]),
        .I1(Q[4]),
        .I2(Q[5]),
        .O(\tempCnt_reg[9]_4 [1]));
  LUT3 #(
    .INIT(8'h40)) 
    i__carry_i_3__3
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[3]),
        .O(\tempCnt_reg[9]_7 [1]));
  LUT3 #(
    .INIT(8'h40)) 
    i__carry_i_3__4
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[3]),
        .O(\tempCnt_reg[9]_2 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    i__carry_i_3__5
       (.I0(Q[2]),
        .I1(Q[0]),
        .O(\tempCnt_reg[4]_1 [0]));
  LUT3 #(
    .INIT(8'h08)) 
    i__carry_i_4
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .O(\tempCnt_reg[9]_2 [0]));
  LUT3 #(
    .INIT(8'h40)) 
    i__carry_i_4__1
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .O(\tempCnt_reg[9]_4 [0]));
  LUT3 #(
    .INIT(8'h04)) 
    i__carry_i_4__2
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .O(\tempCnt_reg[9]_7 [0]));
  LUT3 #(
    .INIT(8'h40)) 
    i__carry_i_4__3
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .O(\tempCnt_reg[9]_6 [0]));
  LUT3 #(
    .INIT(8'h10)) 
    i__carry_i_4__4
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .O(\tempCnt_reg[9]_5 [0]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \tempCnt[0]_i_1__0 
       (.I0(Q[0]),
        .O(plusOp__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \tempCnt[1]_i_1__0 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(plusOp__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \tempCnt[2]_i_1__0 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .O(plusOp__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \tempCnt[3]_i_1__0 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(Q[1]),
        .O(plusOp__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \tempCnt[4]_i_1__0 
       (.I0(Q[4]),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(Q[2]),
        .O(plusOp__0[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \tempCnt[5]_i_1__1 
       (.I0(Q[5]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[1]),
        .I4(Q[0]),
        .I5(Q[2]),
        .O(plusOp__0[5]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \tempCnt[6]_i_1__1 
       (.I0(Q[6]),
        .I1(Q[5]),
        .I2(Q[2]),
        .I3(\tempCnt[6]_i_2_n_0 ),
        .I4(Q[4]),
        .I5(Q[3]),
        .O(plusOp__0[6]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \tempCnt[6]_i_2 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(\tempCnt[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \tempCnt[7]_i_1__1 
       (.I0(Q[7]),
        .I1(Q[6]),
        .I2(Q[5]),
        .I3(\tempCnt[9]_i_8_n_0 ),
        .O(plusOp__0[7]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \tempCnt[8]_i_1__1 
       (.I0(Q[8]),
        .I1(Q[7]),
        .I2(\tempCnt[9]_i_8_n_0 ),
        .I3(Q[5]),
        .I4(Q[6]),
        .O(plusOp__0[8]));
  LUT5 #(
    .INIT(32'h8000FFFF)) 
    \tempCnt[9]_i_1__1 
       (.I0(\tempCnt[9]_i_4__1_n_0 ),
        .I1(\tempCnt[9]_i_5__1_n_0 ),
        .I2(Q[9]),
        .I3(\tempCnt[9]_i_6__0_n_0 ),
        .I4(s00_axi_aresetn),
        .O(\tempCnt[9]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \tempCnt[9]_i_3__1 
       (.I0(Q[9]),
        .I1(Q[8]),
        .I2(Q[7]),
        .I3(\tempCnt[9]_i_8_n_0 ),
        .I4(Q[5]),
        .I5(Q[6]),
        .O(plusOp__0[9]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \tempCnt[9]_i_4__1 
       (.I0(Q[5]),
        .I1(Q[6]),
        .I2(Q[7]),
        .I3(Q[8]),
        .O(\tempCnt[9]_i_4__1_n_0 ));
  LUT6 #(
    .INIT(64'h0000080000000000)) 
    \tempCnt[9]_i_5__1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(Q[4]),
        .I5(rollWire),
        .O(\tempCnt[9]_i_5__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT5 #(
    .INIT(32'hFEEEAAAA)) 
    \tempCnt[9]_i_6__0 
       (.I0(Q[4]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(Q[3]),
        .O(\tempCnt[9]_i_6__0_n_0 ));
  LUT6 #(
    .INIT(64'hAAA88888AAAAAAAA)) 
    \tempCnt[9]_i_7__0 
       (.I0(Q[9]),
        .I1(Q[4]),
        .I2(Q[2]),
        .I3(\tempCnt[6]_i_2_n_0 ),
        .I4(Q[3]),
        .I5(\tempCnt[9]_i_4__1_n_0 ),
        .O(\tempCnt_reg[9]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \tempCnt[9]_i_8 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[4]),
        .I4(Q[3]),
        .O(\tempCnt[9]_i_8_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[0] 
       (.C(clk_out1),
        .CE(E),
        .D(plusOp__0[0]),
        .Q(Q[0]),
        .R(\tempCnt[9]_i_1__1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[1] 
       (.C(clk_out1),
        .CE(E),
        .D(plusOp__0[1]),
        .Q(Q[1]),
        .R(\tempCnt[9]_i_1__1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[2] 
       (.C(clk_out1),
        .CE(E),
        .D(plusOp__0[2]),
        .Q(Q[2]),
        .R(\tempCnt[9]_i_1__1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[3] 
       (.C(clk_out1),
        .CE(E),
        .D(plusOp__0[3]),
        .Q(Q[3]),
        .R(\tempCnt[9]_i_1__1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[4] 
       (.C(clk_out1),
        .CE(E),
        .D(plusOp__0[4]),
        .Q(Q[4]),
        .R(\tempCnt[9]_i_1__1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[5] 
       (.C(clk_out1),
        .CE(E),
        .D(plusOp__0[5]),
        .Q(Q[5]),
        .R(\tempCnt[9]_i_1__1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[6] 
       (.C(clk_out1),
        .CE(E),
        .D(plusOp__0[6]),
        .Q(Q[6]),
        .R(\tempCnt[9]_i_1__1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[7] 
       (.C(clk_out1),
        .CE(E),
        .D(plusOp__0[7]),
        .Q(Q[7]),
        .R(\tempCnt[9]_i_1__1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[8] 
       (.C(clk_out1),
        .CE(E),
        .D(plusOp__0[8]),
        .Q(Q[8]),
        .R(\tempCnt[9]_i_1__1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tempCnt_reg[9] 
       (.C(clk_out1),
        .CE(E),
        .D(plusOp__0[9]),
        .Q(Q[9]),
        .R(\tempCnt[9]_i_1__1_n_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_scopeFace
   (\tempCnt_reg[9] ,
    \tempCnt_reg[9]_0 ,
    \tempCnt_reg[9]_1 ,
    \tempCnt_reg[9]_2 ,
    \tempCnt_reg[9]_3 ,
    CO,
    \tempCnt_reg[9]_4 ,
    \tempCnt_reg[9]_5 ,
    \tempCnt_reg[9]_6 ,
    \tempCnt_reg[9]_7 ,
    \tempCnt_reg[8] ,
    \tempCnt_reg[9]_8 ,
    \tempCnt_reg[9]_9 ,
    O,
    \tempCnt_reg[3] ,
    \tempCnt_reg[5] ,
    \tempCnt_reg[9]_10 ,
    \tempCnt_reg[9]_11 ,
    \tempCnt_reg[9]_12 ,
    \tempCnt_reg[4] ,
    \tempCnt_reg[5]_0 ,
    S,
    \dc_bias[3]_i_29 ,
    \dc_bias[3]_i_29_0 ,
    \dc_bias[3]_i_29_1 ,
    \dc_bias[3]_i_9__1 ,
    \dc_bias[3]_i_27 ,
    \dc_bias[3]_i_27_0 ,
    \dc_bias[3]_i_27_1 ,
    \dc_bias[3]_i_27_2 ,
    \dc_bias[3]_i_9__1_0 ,
    Q,
    \r7_inferred__1/i__carry__0_0 ,
    i___29_carry_i_3,
    i___29_carry_i_2,
    DI,
    i___29_carry__0_i_3,
    \dc_bias[3]_i_59 ,
    \dc_bias[3]_i_42 ,
    i___20_carry_i_3,
    \r7_inferred__2/i__carry__0_0 ,
    i___20_carry_i_3_0,
    \dc_bias[3]_i_43 ,
    \dc_bias[3]_i_43_0 );
  output [0:0]\tempCnt_reg[9] ;
  output [0:0]\tempCnt_reg[9]_0 ;
  output [0:0]\tempCnt_reg[9]_1 ;
  output [0:0]\tempCnt_reg[9]_2 ;
  output [0:0]\tempCnt_reg[9]_3 ;
  output [0:0]CO;
  output [0:0]\tempCnt_reg[9]_4 ;
  output [0:0]\tempCnt_reg[9]_5 ;
  output [0:0]\tempCnt_reg[9]_6 ;
  output [0:0]\tempCnt_reg[9]_7 ;
  output [0:0]\tempCnt_reg[8] ;
  output [0:0]\tempCnt_reg[9]_8 ;
  output [2:0]\tempCnt_reg[9]_9 ;
  output [2:0]O;
  output [3:0]\tempCnt_reg[3] ;
  output [2:0]\tempCnt_reg[5] ;
  output [0:0]\tempCnt_reg[9]_10 ;
  output [0:0]\tempCnt_reg[9]_11 ;
  output [2:0]\tempCnt_reg[9]_12 ;
  output [3:0]\tempCnt_reg[4] ;
  output [1:0]\tempCnt_reg[5]_0 ;
  input [3:0]S;
  input [3:0]\dc_bias[3]_i_29 ;
  input [3:0]\dc_bias[3]_i_29_0 ;
  input [3:0]\dc_bias[3]_i_29_1 ;
  input [3:0]\dc_bias[3]_i_9__1 ;
  input [3:0]\dc_bias[3]_i_27 ;
  input [3:0]\dc_bias[3]_i_27_0 ;
  input [3:0]\dc_bias[3]_i_27_1 ;
  input [3:0]\dc_bias[3]_i_27_2 ;
  input [3:0]\dc_bias[3]_i_9__1_0 ;
  input [9:0]Q;
  input [2:0]\r7_inferred__1/i__carry__0_0 ;
  input [3:0]i___29_carry_i_3;
  input [0:0]i___29_carry_i_2;
  input [0:0]DI;
  input [1:0]i___29_carry__0_i_3;
  input [3:0]\dc_bias[3]_i_59 ;
  input [2:0]\dc_bias[3]_i_42 ;
  input [7:0]i___20_carry_i_3;
  input [2:0]\r7_inferred__2/i__carry__0_0 ;
  input [2:0]i___20_carry_i_3_0;
  input [1:0]\dc_bias[3]_i_43 ;
  input [0:0]\dc_bias[3]_i_43_0 ;

  wire [0:0]CO;
  wire [0:0]DI;
  wire [2:0]O;
  wire [9:0]Q;
  wire [3:0]S;
  wire [3:0]\dc_bias[3]_i_27 ;
  wire [3:0]\dc_bias[3]_i_27_0 ;
  wire [3:0]\dc_bias[3]_i_27_1 ;
  wire [3:0]\dc_bias[3]_i_27_2 ;
  wire [3:0]\dc_bias[3]_i_29 ;
  wire [3:0]\dc_bias[3]_i_29_0 ;
  wire [3:0]\dc_bias[3]_i_29_1 ;
  wire [2:0]\dc_bias[3]_i_42 ;
  wire [1:0]\dc_bias[3]_i_43 ;
  wire [0:0]\dc_bias[3]_i_43_0 ;
  wire [3:0]\dc_bias[3]_i_59 ;
  wire [3:0]\dc_bias[3]_i_9__1 ;
  wire [3:0]\dc_bias[3]_i_9__1_0 ;
  wire i___20_carry__0_i_2_n_0;
  wire i___20_carry_i_1_n_0;
  wire i___20_carry_i_2_n_0;
  wire [7:0]i___20_carry_i_3;
  wire [2:0]i___20_carry_i_3_0;
  wire i___23_carry_i_4_n_0;
  wire [1:0]i___29_carry__0_i_3;
  wire [0:0]i___29_carry_i_2;
  wire [3:0]i___29_carry_i_3;
  wire r10_carry_n_1;
  wire r10_carry_n_2;
  wire r10_carry_n_3;
  wire \r10_inferred__0/i__carry_n_1 ;
  wire \r10_inferred__0/i__carry_n_2 ;
  wire \r10_inferred__0/i__carry_n_3 ;
  wire \r10_inferred__1/i__carry_n_1 ;
  wire \r10_inferred__1/i__carry_n_2 ;
  wire \r10_inferred__1/i__carry_n_3 ;
  wire \r10_inferred__2/i__carry_n_1 ;
  wire \r10_inferred__2/i__carry_n_2 ;
  wire \r10_inferred__2/i__carry_n_3 ;
  wire r7_carry_n_1;
  wire r7_carry_n_2;
  wire r7_carry_n_3;
  wire \r7_inferred__0/i__carry_n_1 ;
  wire \r7_inferred__0/i__carry_n_2 ;
  wire \r7_inferred__0/i__carry_n_3 ;
  wire \r7_inferred__1/i___23_carry_n_2 ;
  wire \r7_inferred__1/i___23_carry_n_3 ;
  wire \r7_inferred__1/i___29_carry__0_n_2 ;
  wire \r7_inferred__1/i___29_carry__0_n_3 ;
  wire \r7_inferred__1/i___29_carry_n_0 ;
  wire \r7_inferred__1/i___29_carry_n_1 ;
  wire \r7_inferred__1/i___29_carry_n_2 ;
  wire \r7_inferred__1/i___29_carry_n_3 ;
  wire [2:0]\r7_inferred__1/i__carry__0_0 ;
  wire \r7_inferred__1/i__carry__0_n_0 ;
  wire \r7_inferred__1/i__carry__0_n_1 ;
  wire \r7_inferred__1/i__carry__0_n_2 ;
  wire \r7_inferred__1/i__carry__0_n_3 ;
  wire \r7_inferred__1/i__carry__1_n_2 ;
  wire \r7_inferred__1/i__carry__1_n_3 ;
  wire \r7_inferred__1/i__carry_n_0 ;
  wire \r7_inferred__1/i__carry_n_1 ;
  wire \r7_inferred__1/i__carry_n_2 ;
  wire \r7_inferred__1/i__carry_n_3 ;
  wire \r7_inferred__2/i___20_carry__0_n_3 ;
  wire \r7_inferred__2/i___20_carry_n_0 ;
  wire \r7_inferred__2/i___20_carry_n_1 ;
  wire \r7_inferred__2/i___20_carry_n_2 ;
  wire \r7_inferred__2/i___20_carry_n_3 ;
  wire [2:0]\r7_inferred__2/i__carry__0_0 ;
  wire \r7_inferred__2/i__carry__0_n_0 ;
  wire \r7_inferred__2/i__carry__0_n_1 ;
  wire \r7_inferred__2/i__carry__0_n_2 ;
  wire \r7_inferred__2/i__carry__0_n_3 ;
  wire \r7_inferred__2/i__carry__1_n_2 ;
  wire \r7_inferred__2/i__carry__1_n_3 ;
  wire \r7_inferred__2/i__carry_n_0 ;
  wire \r7_inferred__2/i__carry_n_1 ;
  wire \r7_inferred__2/i__carry_n_2 ;
  wire \r7_inferred__2/i__carry_n_3 ;
  wire r8_carry_n_1;
  wire r8_carry_n_2;
  wire r8_carry_n_3;
  wire \r8_inferred__0/i__carry_n_1 ;
  wire \r8_inferred__0/i__carry_n_2 ;
  wire \r8_inferred__0/i__carry_n_3 ;
  wire r9_carry_n_1;
  wire r9_carry_n_2;
  wire r9_carry_n_3;
  wire \r9_inferred__0/i__carry_n_1 ;
  wire \r9_inferred__0/i__carry_n_2 ;
  wire \r9_inferred__0/i__carry_n_3 ;
  wire [3:0]\tempCnt_reg[3] ;
  wire [3:0]\tempCnt_reg[4] ;
  wire [2:0]\tempCnt_reg[5] ;
  wire [1:0]\tempCnt_reg[5]_0 ;
  wire [0:0]\tempCnt_reg[8] ;
  wire [0:0]\tempCnt_reg[9] ;
  wire [0:0]\tempCnt_reg[9]_0 ;
  wire [0:0]\tempCnt_reg[9]_1 ;
  wire [0:0]\tempCnt_reg[9]_10 ;
  wire [0:0]\tempCnt_reg[9]_11 ;
  wire [2:0]\tempCnt_reg[9]_12 ;
  wire [0:0]\tempCnt_reg[9]_2 ;
  wire [0:0]\tempCnt_reg[9]_3 ;
  wire [0:0]\tempCnt_reg[9]_4 ;
  wire [0:0]\tempCnt_reg[9]_5 ;
  wire [0:0]\tempCnt_reg[9]_6 ;
  wire [0:0]\tempCnt_reg[9]_7 ;
  wire [0:0]\tempCnt_reg[9]_8 ;
  wire [2:0]\tempCnt_reg[9]_9 ;
  wire [3:0]NLW_r10_carry_O_UNCONNECTED;
  wire [3:0]\NLW_r10_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:0]\NLW_r10_inferred__1/i__carry_O_UNCONNECTED ;
  wire [3:0]\NLW_r10_inferred__2/i__carry_O_UNCONNECTED ;
  wire [3:0]NLW_r7_carry_O_UNCONNECTED;
  wire [3:0]\NLW_r7_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:2]\NLW_r7_inferred__1/i___23_carry_CO_UNCONNECTED ;
  wire [3:3]\NLW_r7_inferred__1/i___23_carry_O_UNCONNECTED ;
  wire [3:2]\NLW_r7_inferred__1/i___29_carry__0_CO_UNCONNECTED ;
  wire [3:3]\NLW_r7_inferred__1/i___29_carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_r7_inferred__1/i__carry_O_UNCONNECTED ;
  wire [2:0]\NLW_r7_inferred__1/i__carry__0_O_UNCONNECTED ;
  wire [2:2]\NLW_r7_inferred__1/i__carry__1_CO_UNCONNECTED ;
  wire [3:3]\NLW_r7_inferred__1/i__carry__1_O_UNCONNECTED ;
  wire [3:1]\NLW_r7_inferred__2/i___20_carry__0_CO_UNCONNECTED ;
  wire [3:2]\NLW_r7_inferred__2/i___20_carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_r7_inferred__2/i__carry_O_UNCONNECTED ;
  wire [2:0]\NLW_r7_inferred__2/i__carry__0_O_UNCONNECTED ;
  wire [2:2]\NLW_r7_inferred__2/i__carry__1_CO_UNCONNECTED ;
  wire [3:3]\NLW_r7_inferred__2/i__carry__1_O_UNCONNECTED ;
  wire [3:0]NLW_r8_carry_O_UNCONNECTED;
  wire [3:0]\NLW_r8_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:0]NLW_r9_carry_O_UNCONNECTED;
  wire [3:0]\NLW_r9_inferred__0/i__carry_O_UNCONNECTED ;

  LUT5 #(
    .INIT(32'h5556AAA9)) 
    i___20_carry__0_i_2
       (.I0(i___20_carry_i_3[3]),
        .I1(\tempCnt_reg[9]_12 [0]),
        .I2(\tempCnt_reg[9]_10 ),
        .I3(\tempCnt_reg[9]_12 [1]),
        .I4(\tempCnt_reg[9]_12 [2]),
        .O(i___20_carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h56A9)) 
    i___20_carry_i_1
       (.I0(i___20_carry_i_3[2]),
        .I1(\tempCnt_reg[9]_10 ),
        .I2(\tempCnt_reg[9]_12 [0]),
        .I3(\tempCnt_reg[9]_12 [1]),
        .O(i___20_carry_i_1_n_0));
  LUT3 #(
    .INIT(8'h69)) 
    i___20_carry_i_2
       (.I0(i___20_carry_i_3[1]),
        .I1(\tempCnt_reg[9]_12 [0]),
        .I2(\tempCnt_reg[9]_10 ),
        .O(i___20_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___23_carry_i_4
       (.I0(\tempCnt_reg[9]_9 [2]),
        .I1(\tempCnt_reg[8] ),
        .O(i___23_carry_i_4_n_0));
  CARRY4 r10_carry
       (.CI(1'b0),
        .CO({\tempCnt_reg[9] ,r10_carry_n_1,r10_carry_n_2,r10_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_r10_carry_O_UNCONNECTED[3:0]),
        .S(S));
  CARRY4 \r10_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\tempCnt_reg[9]_3 ,\r10_inferred__0/i__carry_n_1 ,\r10_inferred__0/i__carry_n_2 ,\r10_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r10_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S(\dc_bias[3]_i_9__1 ));
  CARRY4 \r10_inferred__1/i__carry 
       (.CI(1'b0),
        .CO({CO,\r10_inferred__1/i__carry_n_1 ,\r10_inferred__1/i__carry_n_2 ,\r10_inferred__1/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r10_inferred__1/i__carry_O_UNCONNECTED [3:0]),
        .S(\dc_bias[3]_i_27 ));
  CARRY4 \r10_inferred__2/i__carry 
       (.CI(1'b0),
        .CO({\tempCnt_reg[9]_7 ,\r10_inferred__2/i__carry_n_1 ,\r10_inferred__2/i__carry_n_2 ,\r10_inferred__2/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r10_inferred__2/i__carry_O_UNCONNECTED [3:0]),
        .S(\dc_bias[3]_i_9__1_0 ));
  CARRY4 r7_carry
       (.CI(1'b0),
        .CO({\tempCnt_reg[9]_2 ,r7_carry_n_1,r7_carry_n_2,r7_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_r7_carry_O_UNCONNECTED[3:0]),
        .S(\dc_bias[3]_i_29_1 ));
  CARRY4 \r7_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\tempCnt_reg[9]_6 ,\r7_inferred__0/i__carry_n_1 ,\r7_inferred__0/i__carry_n_2 ,\r7_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r7_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S(\dc_bias[3]_i_27_2 ));
  CARRY4 \r7_inferred__1/i___23_carry 
       (.CI(1'b0),
        .CO({\NLW_r7_inferred__1/i___23_carry_CO_UNCONNECTED [3:2],\r7_inferred__1/i___23_carry_n_2 ,\r7_inferred__1/i___23_carry_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,DI,1'b0}),
        .O({\NLW_r7_inferred__1/i___23_carry_O_UNCONNECTED [3],O}),
        .S({1'b0,i___29_carry__0_i_3,i___23_carry_i_4_n_0}));
  CARRY4 \r7_inferred__1/i___29_carry 
       (.CI(1'b0),
        .CO({\r7_inferred__1/i___29_carry_n_0 ,\r7_inferred__1/i___29_carry_n_1 ,\r7_inferred__1/i___29_carry_n_2 ,\r7_inferred__1/i___29_carry_n_3 }),
        .CYINIT(1'b1),
        .DI(Q[3:0]),
        .O(\tempCnt_reg[3] ),
        .S(\dc_bias[3]_i_59 ));
  CARRY4 \r7_inferred__1/i___29_carry__0 
       (.CI(\r7_inferred__1/i___29_carry_n_0 ),
        .CO({\NLW_r7_inferred__1/i___29_carry__0_CO_UNCONNECTED [3:2],\r7_inferred__1/i___29_carry__0_n_2 ,\r7_inferred__1/i___29_carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,Q[5:4]}),
        .O({\NLW_r7_inferred__1/i___29_carry__0_O_UNCONNECTED [3],\tempCnt_reg[5] }),
        .S({1'b0,\dc_bias[3]_i_42 }));
  CARRY4 \r7_inferred__1/i__carry 
       (.CI(1'b0),
        .CO({\r7_inferred__1/i__carry_n_0 ,\r7_inferred__1/i__carry_n_1 ,\r7_inferred__1/i__carry_n_2 ,\r7_inferred__1/i__carry_n_3 }),
        .CYINIT(1'b0),
        .DI({Q[4:2],1'b0}),
        .O(\NLW_r7_inferred__1/i__carry_O_UNCONNECTED [3:0]),
        .S({\r7_inferred__1/i__carry__0_0 ,Q[1]}));
  CARRY4 \r7_inferred__1/i__carry__0 
       (.CI(\r7_inferred__1/i__carry_n_0 ),
        .CO({\r7_inferred__1/i__carry__0_n_0 ,\r7_inferred__1/i__carry__0_n_1 ,\r7_inferred__1/i__carry__0_n_2 ,\r7_inferred__1/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI(Q[8:5]),
        .O({\tempCnt_reg[8] ,\NLW_r7_inferred__1/i__carry__0_O_UNCONNECTED [2:0]}),
        .S(i___29_carry_i_3));
  CARRY4 \r7_inferred__1/i__carry__1 
       (.CI(\r7_inferred__1/i__carry__0_n_0 ),
        .CO({\tempCnt_reg[9]_8 ,\NLW_r7_inferred__1/i__carry__1_CO_UNCONNECTED [2],\r7_inferred__1/i__carry__1_n_2 ,\r7_inferred__1/i__carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,Q[9]}),
        .O({\NLW_r7_inferred__1/i__carry__1_O_UNCONNECTED [3],\tempCnt_reg[9]_9 }),
        .S({1'b1,Q[9:8],i___29_carry_i_2}));
  CARRY4 \r7_inferred__2/i___20_carry 
       (.CI(1'b0),
        .CO({\r7_inferred__2/i___20_carry_n_0 ,\r7_inferred__2/i___20_carry_n_1 ,\r7_inferred__2/i___20_carry_n_2 ,\r7_inferred__2/i___20_carry_n_3 }),
        .CYINIT(1'b1),
        .DI({i___20_carry_i_3[2:0],1'b1}),
        .O(\tempCnt_reg[4] ),
        .S({i___20_carry_i_1_n_0,i___20_carry_i_2_n_0,\dc_bias[3]_i_43 }));
  CARRY4 \r7_inferred__2/i___20_carry__0 
       (.CI(\r7_inferred__2/i___20_carry_n_0 ),
        .CO({\NLW_r7_inferred__2/i___20_carry__0_CO_UNCONNECTED [3:1],\r7_inferred__2/i___20_carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,i___20_carry_i_3[3]}),
        .O({\NLW_r7_inferred__2/i___20_carry__0_O_UNCONNECTED [3:2],\tempCnt_reg[5]_0 }),
        .S({1'b0,1'b0,\dc_bias[3]_i_43_0 ,i___20_carry__0_i_2_n_0}));
  CARRY4 \r7_inferred__2/i__carry 
       (.CI(1'b0),
        .CO({\r7_inferred__2/i__carry_n_0 ,\r7_inferred__2/i__carry_n_1 ,\r7_inferred__2/i__carry_n_2 ,\r7_inferred__2/i__carry_n_3 }),
        .CYINIT(1'b0),
        .DI({i___20_carry_i_3[4:2],1'b0}),
        .O(\NLW_r7_inferred__2/i__carry_O_UNCONNECTED [3:0]),
        .S({\r7_inferred__2/i__carry__0_0 ,i___20_carry_i_3[1]}));
  CARRY4 \r7_inferred__2/i__carry__0 
       (.CI(\r7_inferred__2/i__carry_n_0 ),
        .CO({\r7_inferred__2/i__carry__0_n_0 ,\r7_inferred__2/i__carry__0_n_1 ,\r7_inferred__2/i__carry__0_n_2 ,\r7_inferred__2/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,i___20_carry_i_3[7:5]}),
        .O({\tempCnt_reg[9]_10 ,\NLW_r7_inferred__2/i__carry__0_O_UNCONNECTED [2:0]}),
        .S({i___20_carry_i_3[4],i___20_carry_i_3_0}));
  CARRY4 \r7_inferred__2/i__carry__1 
       (.CI(\r7_inferred__2/i__carry__0_n_0 ),
        .CO({\tempCnt_reg[9]_11 ,\NLW_r7_inferred__2/i__carry__1_CO_UNCONNECTED [2],\r7_inferred__2/i__carry__1_n_2 ,\r7_inferred__2/i__carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_r7_inferred__2/i__carry__1_O_UNCONNECTED [3],\tempCnt_reg[9]_12 }),
        .S({1'b1,i___20_carry_i_3[7:5]}));
  CARRY4 r8_carry
       (.CI(1'b0),
        .CO({\tempCnt_reg[9]_1 ,r8_carry_n_1,r8_carry_n_2,r8_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_r8_carry_O_UNCONNECTED[3:0]),
        .S(\dc_bias[3]_i_29_0 ));
  CARRY4 \r8_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\tempCnt_reg[9]_5 ,\r8_inferred__0/i__carry_n_1 ,\r8_inferred__0/i__carry_n_2 ,\r8_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r8_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S(\dc_bias[3]_i_27_1 ));
  CARRY4 r9_carry
       (.CI(1'b0),
        .CO({\tempCnt_reg[9]_0 ,r9_carry_n_1,r9_carry_n_2,r9_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_r9_carry_O_UNCONNECTED[3:0]),
        .S(\dc_bias[3]_i_29 ));
  CARRY4 \r9_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\tempCnt_reg[9]_4 ,\r9_inferred__0/i__carry_n_1 ,\r9_inferred__0/i__carry_n_2 ,\r9_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r9_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S(\dc_bias[3]_i_27_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO
   (\dc_bias[3]_i_9__1 ,
    CO,
    s00_axi_aclk,
    cw,
    \sdp_bl.ramb18_dp_bl.ram18_bl_0 ,
    \sdp_bl.ramb18_dp_bl.ram18_bl_1 ,
    Q,
    \dc_bias_reg[3]_i_5_0 ,
    \dc_bias[2]_i_4 );
  output \dc_bias[3]_i_9__1 ;
  output [0:0]CO;
  input s00_axi_aclk;
  input [0:0]cw;
  input \sdp_bl.ramb18_dp_bl.ram18_bl_0 ;
  input [9:0]\sdp_bl.ramb18_dp_bl.ram18_bl_1 ;
  input [9:0]Q;
  input [9:0]\dc_bias_reg[3]_i_5_0 ;
  input \dc_bias[2]_i_4 ;

  wire [0:0]CO;
  wire [9:0]Q;
  wire [0:0]cw;
  wire \dc_bias[2]_i_4 ;
  wire \dc_bias[3]_i_10_n_0 ;
  wire \dc_bias[3]_i_11__0_n_0 ;
  wire \dc_bias[3]_i_12__0_n_0 ;
  wire \dc_bias[3]_i_13__0_n_0 ;
  wire \dc_bias[3]_i_30_n_0 ;
  wire \dc_bias[3]_i_31_n_0 ;
  wire \dc_bias[3]_i_32_n_0 ;
  wire \dc_bias[3]_i_33_n_0 ;
  wire \dc_bias[3]_i_9__1 ;
  wire [9:0]\dc_bias_reg[3]_i_5_0 ;
  wire \dc_bias_reg[3]_i_5_n_1 ;
  wire \dc_bias_reg[3]_i_5_n_2 ;
  wire \dc_bias_reg[3]_i_5_n_3 ;
  wire [17:8]readL;
  wire s00_axi_aclk;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_0 ;
  wire [9:0]\sdp_bl.ramb18_dp_bl.ram18_bl_1 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_10 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_11 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_12 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_13 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_14 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_15 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_8 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_9 ;
  wire [3:0]\NLW_dc_bias_reg[3]_i_5_O_UNCONNECTED ;
  wire [15:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED ;
  wire [1:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED ;

  LUT4 #(
    .INIT(16'h659A)) 
    \dc_bias[3]_i_10 
       (.I0(\dc_bias_reg[3]_i_5_0 [9]),
        .I1(\dc_bias[3]_i_30_n_0 ),
        .I2(readL[16]),
        .I3(readL[17]),
        .O(\dc_bias[3]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0000000006609006)) 
    \dc_bias[3]_i_11__0 
       (.I0(readL[16]),
        .I1(\dc_bias_reg[3]_i_5_0 [8]),
        .I2(\dc_bias_reg[3]_i_5_0 [7]),
        .I3(\dc_bias[3]_i_31_n_0 ),
        .I4(readL[15]),
        .I5(\dc_bias[3]_i_32_n_0 ),
        .O(\dc_bias[3]_i_11__0_n_0 ));
  LUT6 #(
    .INIT(64'h000000005556AAA9)) 
    \dc_bias[3]_i_12__0 
       (.I0(readL[13]),
        .I1(readL[12]),
        .I2(readL[11]),
        .I3(readL[10]),
        .I4(\dc_bias_reg[3]_i_5_0 [5]),
        .I5(\dc_bias[3]_i_33_n_0 ),
        .O(\dc_bias[3]_i_12__0_n_0 ));
  LUT6 #(
    .INIT(64'h6006000000006006)) 
    \dc_bias[3]_i_13__0 
       (.I0(readL[10]),
        .I1(\dc_bias_reg[3]_i_5_0 [2]),
        .I2(\dc_bias_reg[3]_i_5_0 [1]),
        .I3(readL[9]),
        .I4(\dc_bias_reg[3]_i_5_0 [0]),
        .I5(readL[8]),
        .O(\dc_bias[3]_i_13__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000005557)) 
    \dc_bias[3]_i_30 
       (.I0(readL[13]),
        .I1(readL[12]),
        .I2(readL[11]),
        .I3(readL[10]),
        .I4(readL[14]),
        .I5(readL[15]),
        .O(\dc_bias[3]_i_30_n_0 ));
  LUT5 #(
    .INIT(32'h00015555)) 
    \dc_bias[3]_i_31 
       (.I0(readL[14]),
        .I1(readL[10]),
        .I2(readL[11]),
        .I3(readL[12]),
        .I4(readL[13]),
        .O(\dc_bias[3]_i_31_n_0 ));
  LUT6 #(
    .INIT(64'h6666666A99999995)) 
    \dc_bias[3]_i_32 
       (.I0(\dc_bias_reg[3]_i_5_0 [6]),
        .I1(readL[13]),
        .I2(readL[12]),
        .I3(readL[11]),
        .I4(readL[10]),
        .I5(readL[14]),
        .O(\dc_bias[3]_i_32_n_0 ));
  LUT5 #(
    .INIT(32'h7BBDDEE7)) 
    \dc_bias[3]_i_33 
       (.I0(\dc_bias_reg[3]_i_5_0 [3]),
        .I1(readL[12]),
        .I2(readL[10]),
        .I3(readL[11]),
        .I4(\dc_bias_reg[3]_i_5_0 [4]),
        .O(\dc_bias[3]_i_33_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \dc_bias[3]_i_9__0 
       (.I0(CO),
        .I1(\dc_bias[2]_i_4 ),
        .O(\dc_bias[3]_i_9__1 ));
  CARRY4 \dc_bias_reg[3]_i_5 
       (.CI(1'b0),
        .CO({CO,\dc_bias_reg[3]_i_5_n_1 ,\dc_bias_reg[3]_i_5_n_2 ,\dc_bias_reg[3]_i_5_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_dc_bias_reg[3]_i_5_O_UNCONNECTED [3:0]),
        .S({\dc_bias[3]_i_10_n_0 ,\dc_bias[3]_i_11__0_n_0 ,\dc_bias[3]_i_12__0_n_0 ,\dc_bias[3]_i_13__0_n_0 }));
  (* box_type = "PRIMITIVE" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("NONE"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(18)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl 
       (.ADDRARDADDR({\sdp_bl.ramb18_dp_bl.ram18_bl_1 ,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({Q,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b0}),
        .DOADO({readL[16:9],\sdp_bl.ramb18_dp_bl.ram18_bl_n_8 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_9 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_10 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_11 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_12 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_13 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_14 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_15 }),
        .DOBDO(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED [15:0]),
        .DOPADOP({readL[17],readL[8]}),
        .DOPBDOP(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED [1:0]),
        .ENARDEN(1'b1),
        .ENBWREN(cw),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(\sdp_bl.ramb18_dp_bl.ram18_bl_0 ),
        .RSTRAMB(\sdp_bl.ramb18_dp_bl.ram18_bl_0 ),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({1'b1,1'b1,1'b1,1'b1}));
endmodule

(* ORIG_REF_NAME = "unimacro_BRAM_SDP_MACRO" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_unimacro_BRAM_SDP_MACRO_0
   (\tempCnt_reg[9] ,
    s00_axi_aclk,
    \sdp_bl.ramb18_dp_bl.ram18_bl_0 ,
    \sdp_bl.ramb18_dp_bl.ram18_bl_1 ,
    Q,
    \dc_bias_reg[3]_i_7_0 );
  output [0:0]\tempCnt_reg[9] ;
  input s00_axi_aclk;
  input \sdp_bl.ramb18_dp_bl.ram18_bl_0 ;
  input [9:0]\sdp_bl.ramb18_dp_bl.ram18_bl_1 ;
  input [9:0]Q;
  input [9:0]\dc_bias_reg[3]_i_7_0 ;

  wire [9:0]Q;
  wire \dc_bias[3]_i_19_n_0 ;
  wire \dc_bias[3]_i_20_n_0 ;
  wire \dc_bias[3]_i_21_n_0 ;
  wire \dc_bias[3]_i_22_n_0 ;
  wire \dc_bias[3]_i_36_n_0 ;
  wire \dc_bias[3]_i_37_n_0 ;
  wire \dc_bias[3]_i_38_n_0 ;
  wire \dc_bias[3]_i_39_n_0 ;
  wire [9:0]\dc_bias_reg[3]_i_7_0 ;
  wire \dc_bias_reg[3]_i_7_n_1 ;
  wire \dc_bias_reg[3]_i_7_n_2 ;
  wire \dc_bias_reg[3]_i_7_n_3 ;
  wire [17:8]readR;
  wire s00_axi_aclk;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_0 ;
  wire [9:0]\sdp_bl.ramb18_dp_bl.ram18_bl_1 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_10 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_11 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_12 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_13 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_14 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_15 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_8 ;
  wire \sdp_bl.ramb18_dp_bl.ram18_bl_n_9 ;
  wire [0:0]\tempCnt_reg[9] ;
  wire [3:0]\NLW_dc_bias_reg[3]_i_7_O_UNCONNECTED ;
  wire [15:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED ;
  wire [1:0]\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED ;

  LUT4 #(
    .INIT(16'h659A)) 
    \dc_bias[3]_i_19 
       (.I0(\dc_bias_reg[3]_i_7_0 [9]),
        .I1(\dc_bias[3]_i_36_n_0 ),
        .I2(readR[16]),
        .I3(readR[17]),
        .O(\dc_bias[3]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'h0000000006609006)) 
    \dc_bias[3]_i_20 
       (.I0(readR[16]),
        .I1(\dc_bias_reg[3]_i_7_0 [8]),
        .I2(\dc_bias_reg[3]_i_7_0 [7]),
        .I3(\dc_bias[3]_i_37_n_0 ),
        .I4(readR[15]),
        .I5(\dc_bias[3]_i_38_n_0 ),
        .O(\dc_bias[3]_i_20_n_0 ));
  LUT6 #(
    .INIT(64'h000000005556AAA9)) 
    \dc_bias[3]_i_21 
       (.I0(readR[13]),
        .I1(readR[12]),
        .I2(readR[11]),
        .I3(readR[10]),
        .I4(\dc_bias_reg[3]_i_7_0 [5]),
        .I5(\dc_bias[3]_i_39_n_0 ),
        .O(\dc_bias[3]_i_21_n_0 ));
  LUT6 #(
    .INIT(64'h6006000000006006)) 
    \dc_bias[3]_i_22 
       (.I0(readR[10]),
        .I1(\dc_bias_reg[3]_i_7_0 [2]),
        .I2(\dc_bias_reg[3]_i_7_0 [0]),
        .I3(readR[8]),
        .I4(\dc_bias_reg[3]_i_7_0 [1]),
        .I5(readR[9]),
        .O(\dc_bias[3]_i_22_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000005557)) 
    \dc_bias[3]_i_36 
       (.I0(readR[13]),
        .I1(readR[12]),
        .I2(readR[11]),
        .I3(readR[10]),
        .I4(readR[14]),
        .I5(readR[15]),
        .O(\dc_bias[3]_i_36_n_0 ));
  LUT5 #(
    .INIT(32'h00015555)) 
    \dc_bias[3]_i_37 
       (.I0(readR[14]),
        .I1(readR[10]),
        .I2(readR[11]),
        .I3(readR[12]),
        .I4(readR[13]),
        .O(\dc_bias[3]_i_37_n_0 ));
  LUT6 #(
    .INIT(64'h6666666A99999995)) 
    \dc_bias[3]_i_38 
       (.I0(\dc_bias_reg[3]_i_7_0 [6]),
        .I1(readR[13]),
        .I2(readR[12]),
        .I3(readR[11]),
        .I4(readR[10]),
        .I5(readR[14]),
        .O(\dc_bias[3]_i_38_n_0 ));
  LUT5 #(
    .INIT(32'h7BBDDEE7)) 
    \dc_bias[3]_i_39 
       (.I0(\dc_bias_reg[3]_i_7_0 [3]),
        .I1(readR[12]),
        .I2(readR[10]),
        .I3(readR[11]),
        .I4(\dc_bias_reg[3]_i_7_0 [4]),
        .O(\dc_bias[3]_i_39_n_0 ));
  CARRY4 \dc_bias_reg[3]_i_7 
       (.CI(1'b0),
        .CO({\tempCnt_reg[9] ,\dc_bias_reg[3]_i_7_n_1 ,\dc_bias_reg[3]_i_7_n_2 ,\dc_bias_reg[3]_i_7_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_dc_bias_reg[3]_i_7_O_UNCONNECTED [3:0]),
        .S({\dc_bias[3]_i_19_n_0 ,\dc_bias[3]_i_20_n_0 ,\dc_bias[3]_i_21_n_0 ,\dc_bias[3]_i_22_n_0 }));
  (* box_type = "PRIMITIVE" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("NONE"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(18)) 
    \sdp_bl.ramb18_dp_bl.ram18_bl 
       (.ADDRARDADDR({\sdp_bl.ramb18_dp_bl.ram18_bl_1 ,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({Q,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b0}),
        .DOADO({readR[16:9],\sdp_bl.ramb18_dp_bl.ram18_bl_n_8 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_9 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_10 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_11 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_12 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_13 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_14 ,\sdp_bl.ramb18_dp_bl.ram18_bl_n_15 }),
        .DOBDO(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOBDO_UNCONNECTED [15:0]),
        .DOPADOP({readR[17],readR[8]}),
        .DOPBDOP(\NLW_sdp_bl.ramb18_dp_bl.ram18_bl_DOPBDOP_UNCONNECTED [1:0]),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(\sdp_bl.ramb18_dp_bl.ram18_bl_0 ),
        .RSTRAMB(\sdp_bl.ramb18_dp_bl.ram18_bl_0 ),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({1'b1,1'b1,1'b1,1'b1}));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga
   (Q,
    \tempCnt_reg[9] ,
    \dc_bias_reg[3] ,
    \tempCnt_reg[8] ,
    \tempCnt_reg[1] ,
    \tempCnt_reg[9]_0 ,
    \tempCnt_reg[9]_1 ,
    \tempCnt_reg[1]_0 ,
    \tempCnt_reg[9]_2 ,
    \dc_bias_reg[3]_0 ,
    \tempCnt_reg[8]_0 ,
    \tempCnt_reg[6] ,
    \dc_bias_reg[3]_1 ,
    \dc_bias_reg[3]_2 ,
    \tempCnt_reg[8]_1 ,
    \tempCnt_reg[8]_2 ,
    \dc_bias_reg[3]_3 ,
    \tempCnt_reg[1]_1 ,
    \dc_bias_reg[3]_4 ,
    D,
    \tempCnt_reg[6]_0 ,
    \dc_bias_reg[2] ,
    \dc_bias_reg[3]_5 ,
    \dc_bias_reg[0] ,
    \tempCnt_reg[1]_2 ,
    \dc_bias_reg[3]_6 ,
    \dc_bias_reg[3]_7 ,
    \tempCnt_reg[1]_3 ,
    \tempCnt_reg[1]_4 ,
    encoded1_in,
    \tempCnt_reg[8]_3 ,
    \dc_bias_reg[3]_8 ,
    \tempCnt_reg[8]_4 ,
    clk_out1,
    \encoded_reg[0] ,
    CO,
    \encoded_reg[9] ,
    \encoded_reg[9]_0 ,
    s00_axi_aresetn,
    \encoded_reg[0]_0 ,
    \encoded_reg[1] ,
    \encoded_reg[4] ,
    \encoded_reg[9]_1 ,
    \encoded_reg[8] ,
    \encoded_reg[9]_2 ,
    \dc_bias_reg[3]_9 ,
    \dc_bias_reg[3]_i_2 );
  output [9:0]Q;
  output [9:0]\tempCnt_reg[9] ;
  output \dc_bias_reg[3] ;
  output \tempCnt_reg[8] ;
  output \tempCnt_reg[1] ;
  output \tempCnt_reg[9]_0 ;
  output \tempCnt_reg[9]_1 ;
  output \tempCnt_reg[1]_0 ;
  output \tempCnt_reg[9]_2 ;
  output \dc_bias_reg[3]_0 ;
  output \tempCnt_reg[8]_0 ;
  output \tempCnt_reg[6] ;
  output \dc_bias_reg[3]_1 ;
  output \dc_bias_reg[3]_2 ;
  output \tempCnt_reg[8]_1 ;
  output \tempCnt_reg[8]_2 ;
  output \dc_bias_reg[3]_3 ;
  output \tempCnt_reg[1]_1 ;
  output \dc_bias_reg[3]_4 ;
  output [0:0]D;
  output \tempCnt_reg[6]_0 ;
  output \dc_bias_reg[2] ;
  output \dc_bias_reg[3]_5 ;
  output \dc_bias_reg[0] ;
  output \tempCnt_reg[1]_2 ;
  output \dc_bias_reg[3]_6 ;
  output \dc_bias_reg[3]_7 ;
  output \tempCnt_reg[1]_3 ;
  output \tempCnt_reg[1]_4 ;
  output [0:0]encoded1_in;
  output \tempCnt_reg[8]_3 ;
  output \dc_bias_reg[3]_8 ;
  output \tempCnt_reg[8]_4 ;
  input clk_out1;
  input [3:0]\encoded_reg[0] ;
  input [0:0]CO;
  input [0:0]\encoded_reg[9] ;
  input [3:0]\encoded_reg[9]_0 ;
  input s00_axi_aresetn;
  input \encoded_reg[0]_0 ;
  input \encoded_reg[1] ;
  input [0:0]\encoded_reg[4] ;
  input \encoded_reg[9]_1 ;
  input \encoded_reg[8] ;
  input \encoded_reg[9]_2 ;
  input \dc_bias_reg[3]_9 ;
  input \dc_bias_reg[3]_i_2 ;

  wire [0:0]CO;
  wire [0:0]D;
  wire [9:0]Q;
  wire clk_out1;
  wire colBox_n_15;
  wire colBox_n_16;
  wire colBox_n_17;
  wire colBox_n_18;
  wire colBox_n_19;
  wire colBox_n_20;
  wire colBox_n_21;
  wire colBox_n_22;
  wire colBox_n_23;
  wire colBox_n_24;
  wire colBox_n_25;
  wire colBox_n_26;
  wire colBox_n_27;
  wire colBox_n_28;
  wire colBox_n_29;
  wire colBox_n_30;
  wire colBox_n_31;
  wire colBox_n_32;
  wire colBox_n_57;
  wire colBox_n_59;
  wire colBox_n_60;
  wire colBox_n_61;
  wire colBox_n_62;
  wire colBox_n_63;
  wire colBox_n_64;
  wire colBox_n_68;
  wire colBox_n_69;
  wire colBox_n_70;
  wire colBox_n_71;
  wire colBox_n_72;
  wire colBox_n_73;
  wire \dc_bias_reg[0] ;
  wire \dc_bias_reg[2] ;
  wire \dc_bias_reg[3] ;
  wire \dc_bias_reg[3]_0 ;
  wire \dc_bias_reg[3]_1 ;
  wire \dc_bias_reg[3]_2 ;
  wire \dc_bias_reg[3]_3 ;
  wire \dc_bias_reg[3]_4 ;
  wire \dc_bias_reg[3]_5 ;
  wire \dc_bias_reg[3]_6 ;
  wire \dc_bias_reg[3]_7 ;
  wire \dc_bias_reg[3]_8 ;
  wire \dc_bias_reg[3]_9 ;
  wire \dc_bias_reg[3]_i_2 ;
  wire [0:0]encoded1_in;
  wire [3:0]\encoded_reg[0] ;
  wire \encoded_reg[0]_0 ;
  wire \encoded_reg[1] ;
  wire [0:0]\encoded_reg[4] ;
  wire \encoded_reg[8] ;
  wire [0:0]\encoded_reg[9] ;
  wire [3:0]\encoded_reg[9]_0 ;
  wire \encoded_reg[9]_1 ;
  wire \encoded_reg[9]_2 ;
  wire r10;
  wire r1010_out;
  wire r1011_out;
  wire r102_out;
  wire r7;
  wire r74_out;
  wire r8;
  wire r86_out;
  wire r9;
  wire r98_out;
  wire rollWire;
  wire rowBox_n_10;
  wire rowBox_n_11;
  wire rowBox_n_12;
  wire rowBox_n_13;
  wire rowBox_n_14;
  wire rowBox_n_15;
  wire rowBox_n_16;
  wire rowBox_n_17;
  wire rowBox_n_18;
  wire rowBox_n_19;
  wire rowBox_n_20;
  wire rowBox_n_21;
  wire rowBox_n_22;
  wire rowBox_n_23;
  wire rowBox_n_24;
  wire rowBox_n_25;
  wire rowBox_n_26;
  wire rowBox_n_27;
  wire rowBox_n_28;
  wire rowBox_n_29;
  wire rowBox_n_30;
  wire rowBox_n_31;
  wire rowBox_n_32;
  wire rowBox_n_33;
  wire rowBox_n_34;
  wire rowBox_n_35;
  wire rowBox_n_36;
  wire rowBox_n_37;
  wire rowBox_n_38;
  wire rowBox_n_39;
  wire rowBox_n_40;
  wire rowBox_n_41;
  wire rowBox_n_42;
  wire rowBox_n_43;
  wire rowBox_n_44;
  wire rowBox_n_45;
  wire rowBox_n_46;
  wire rowBox_n_47;
  wire rowBox_n_48;
  wire rowBox_n_49;
  wire rowBox_n_50;
  wire rowBox_n_51;
  wire rowBox_n_52;
  wire rowBox_n_53;
  wire rowBox_n_54;
  wire rowBox_n_55;
  wire rowBox_n_56;
  wire s00_axi_aresetn;
  wire scopeF_n_10;
  wire scopeF_n_11;
  wire scopeF_n_12;
  wire scopeF_n_13;
  wire scopeF_n_14;
  wire scopeF_n_15;
  wire scopeF_n_16;
  wire scopeF_n_17;
  wire scopeF_n_18;
  wire scopeF_n_19;
  wire scopeF_n_20;
  wire scopeF_n_21;
  wire scopeF_n_22;
  wire scopeF_n_23;
  wire scopeF_n_24;
  wire scopeF_n_25;
  wire scopeF_n_26;
  wire scopeF_n_27;
  wire scopeF_n_28;
  wire scopeF_n_29;
  wire scopeF_n_30;
  wire scopeF_n_31;
  wire scopeF_n_32;
  wire scopeF_n_33;
  wire scopeF_n_34;
  wire scopeF_n_35;
  wire tempCnt02_out;
  wire \tempCnt_reg[1] ;
  wire \tempCnt_reg[1]_0 ;
  wire \tempCnt_reg[1]_1 ;
  wire \tempCnt_reg[1]_2 ;
  wire \tempCnt_reg[1]_3 ;
  wire \tempCnt_reg[1]_4 ;
  wire \tempCnt_reg[6] ;
  wire \tempCnt_reg[6]_0 ;
  wire \tempCnt_reg[8] ;
  wire \tempCnt_reg[8]_0 ;
  wire \tempCnt_reg[8]_1 ;
  wire \tempCnt_reg[8]_2 ;
  wire \tempCnt_reg[8]_3 ;
  wire \tempCnt_reg[8]_4 ;
  wire [9:0]\tempCnt_reg[9] ;
  wire \tempCnt_reg[9]_0 ;
  wire \tempCnt_reg[9]_1 ;
  wire \tempCnt_reg[9]_2 ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_colCounter colBox
       (.CO(CO),
        .D(D),
        .E(tempCnt02_out),
        .O(scopeF_n_25),
        .Q(\tempCnt_reg[9] ),
        .S({colBox_n_15,colBox_n_16,colBox_n_17,colBox_n_18}),
        .clk_out1(clk_out1),
        .\dc_bias[3]_i_12_0 (rowBox_n_27),
        .\dc_bias[3]_i_12_1 (rowBox_n_28),
        .\dc_bias[3]_i_17__0 (rowBox_n_17),
        .\dc_bias[3]_i_23 ({scopeF_n_30,scopeF_n_31,scopeF_n_33}),
        .\dc_bias[3]_i_23_0 (scopeF_n_35),
        .\dc_bias[3]_i_24_0 (rowBox_n_20),
        .\dc_bias[3]_i_9__0 (r1011_out),
        .\dc_bias[3]_i_9__0_0 (rowBox_n_29),
        .\dc_bias[3]_i_9__0_1 (rowBox_n_26),
        .\dc_bias[3]_i_9__0_2 (r102_out),
        .\dc_bias[3]_i_9__1_0 (r7),
        .\dc_bias[3]_i_9__1_1 (r9),
        .\dc_bias[3]_i_9__1_2 (r10),
        .\dc_bias[3]_i_9__1_3 (r8),
        .\dc_bias[3]_i_9__1_4 (Q[1:0]),
        .\dc_bias_reg[0] (\dc_bias_reg[0] ),
        .\dc_bias_reg[2] (\dc_bias_reg[2] ),
        .\dc_bias_reg[3] (\dc_bias_reg[3] ),
        .\dc_bias_reg[3]_0 (\dc_bias_reg[3]_0 ),
        .\dc_bias_reg[3]_1 (\dc_bias_reg[3]_1 ),
        .\dc_bias_reg[3]_2 (\dc_bias_reg[3]_2 ),
        .\dc_bias_reg[3]_3 (\dc_bias_reg[3]_3 ),
        .\dc_bias_reg[3]_4 (\dc_bias_reg[3]_4 ),
        .\dc_bias_reg[3]_5 (\dc_bias_reg[3]_5 ),
        .\dc_bias_reg[3]_6 (\dc_bias_reg[3]_6 ),
        .\dc_bias_reg[3]_7 (\dc_bias_reg[3]_7 ),
        .\dc_bias_reg[3]_8 (\dc_bias_reg[3]_8 ),
        .\dc_bias_reg[3]_9 (\dc_bias_reg[3]_9 ),
        .\dc_bias_reg[3]_i_2_0 (\dc_bias_reg[3]_i_2 ),
        .encoded1_in(encoded1_in),
        .\encoded_reg[0] (\encoded_reg[0] ),
        .\encoded_reg[0]_0 (\encoded_reg[0]_0 ),
        .\encoded_reg[0]_1 (rowBox_n_21),
        .\encoded_reg[1] (\encoded_reg[1] ),
        .\encoded_reg[4] (\encoded_reg[4] ),
        .\encoded_reg[8] (\encoded_reg[8] ),
        .\encoded_reg[9] (\encoded_reg[9] ),
        .\encoded_reg[9]_0 (\encoded_reg[9]_0 ),
        .\encoded_reg[9]_1 (\encoded_reg[9]_1 ),
        .\encoded_reg[9]_2 (rowBox_n_18),
        .\encoded_reg[9]_3 (\encoded_reg[9]_2 ),
        .\r7_inferred__2/i___20_carry__0 ({scopeF_n_27,scopeF_n_28,scopeF_n_29}),
        .\r7_inferred__2/i___20_carry__0_0 (scopeF_n_26),
        .rollWire(rollWire),
        .s00_axi_aresetn(s00_axi_aresetn),
        .\tempCnt_reg[0]_0 (colBox_n_64),
        .\tempCnt_reg[1]_0 (\tempCnt_reg[1] ),
        .\tempCnt_reg[1]_1 (\tempCnt_reg[1]_0 ),
        .\tempCnt_reg[1]_2 (\tempCnt_reg[1]_1 ),
        .\tempCnt_reg[1]_3 (\tempCnt_reg[1]_2 ),
        .\tempCnt_reg[1]_4 (\tempCnt_reg[1]_3 ),
        .\tempCnt_reg[1]_5 (\tempCnt_reg[1]_4 ),
        .\tempCnt_reg[2]_0 ({colBox_n_27,colBox_n_28}),
        .\tempCnt_reg[6]_0 (\tempCnt_reg[6] ),
        .\tempCnt_reg[6]_1 (\tempCnt_reg[6]_0 ),
        .\tempCnt_reg[6]_2 (colBox_n_57),
        .\tempCnt_reg[6]_3 (colBox_n_63),
        .\tempCnt_reg[6]_4 ({colBox_n_68,colBox_n_69,colBox_n_70}),
        .\tempCnt_reg[8]_0 (\tempCnt_reg[8] ),
        .\tempCnt_reg[8]_1 (\tempCnt_reg[8]_0 ),
        .\tempCnt_reg[8]_2 (\tempCnt_reg[8]_1 ),
        .\tempCnt_reg[8]_3 (\tempCnt_reg[8]_2 ),
        .\tempCnt_reg[8]_4 (\tempCnt_reg[8]_3 ),
        .\tempCnt_reg[8]_5 (\tempCnt_reg[8]_4 ),
        .\tempCnt_reg[9]_0 (\tempCnt_reg[9]_0 ),
        .\tempCnt_reg[9]_1 ({colBox_n_19,colBox_n_20,colBox_n_21,colBox_n_22}),
        .\tempCnt_reg[9]_2 ({colBox_n_23,colBox_n_24,colBox_n_25,colBox_n_26}),
        .\tempCnt_reg[9]_3 ({colBox_n_29,colBox_n_30,colBox_n_31,colBox_n_32}),
        .\tempCnt_reg[9]_4 (\tempCnt_reg[9]_1 ),
        .\tempCnt_reg[9]_5 (\tempCnt_reg[9]_2 ),
        .\tempCnt_reg[9]_6 ({colBox_n_59,colBox_n_60,colBox_n_61,colBox_n_62}),
        .\tempCnt_reg[9]_7 ({colBox_n_71,colBox_n_72,colBox_n_73}),
        .\tempCnt_reg[9]_8 (rowBox_n_19));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rowCounter rowBox
       (.CO(r1010_out),
        .DI(rowBox_n_46),
        .E(tempCnt02_out),
        .O({scopeF_n_15,scopeF_n_16,scopeF_n_17}),
        .Q(Q),
        .S({rowBox_n_10,rowBox_n_11,rowBox_n_12}),
        .clk_out1(clk_out1),
        .\dc_bias[3]_i_23_0 (colBox_n_57),
        .\dc_bias[3]_i_23_1 ({scopeF_n_22,scopeF_n_23,scopeF_n_24}),
        .\dc_bias[3]_i_42_0 ({scopeF_n_18,scopeF_n_19,scopeF_n_20,scopeF_n_21}),
        .\dc_bias[3]_i_8__1 (scopeF_n_34),
        .\dc_bias[3]_i_8__1_0 (scopeF_n_32),
        .\dc_bias[3]_i_8__1_1 (colBox_n_64),
        .\dc_bias[3]_i_9__1 (r86_out),
        .\dc_bias[3]_i_9__1_0 (r74_out),
        .\dc_bias[3]_i_9__1_1 (r98_out),
        .\dc_bias[3]_i_9__1_2 (\tempCnt_reg[9] [1:0]),
        .\r7_inferred__1/i___23_carry ({scopeF_n_12,scopeF_n_13,scopeF_n_14}),
        .\r7_inferred__1/i___23_carry_0 (scopeF_n_11),
        .\r7_inferred__1/i___29_carry (scopeF_n_10),
        .rollWire(rollWire),
        .s00_axi_aresetn(s00_axi_aresetn),
        .\tempCnt_reg[0]_0 (rowBox_n_26),
        .\tempCnt_reg[0]_1 (rowBox_n_29),
        .\tempCnt_reg[2]_0 (rowBox_n_20),
        .\tempCnt_reg[3]_0 ({rowBox_n_13,rowBox_n_14,rowBox_n_15,rowBox_n_16}),
        .\tempCnt_reg[3]_1 (rowBox_n_17),
        .\tempCnt_reg[3]_2 (rowBox_n_18),
        .\tempCnt_reg[4]_0 (rowBox_n_28),
        .\tempCnt_reg[4]_1 ({rowBox_n_49,rowBox_n_50,rowBox_n_51}),
        .\tempCnt_reg[8]_0 ({rowBox_n_52,rowBox_n_53,rowBox_n_54,rowBox_n_55}),
        .\tempCnt_reg[9]_0 (rowBox_n_19),
        .\tempCnt_reg[9]_1 (rowBox_n_21),
        .\tempCnt_reg[9]_2 ({rowBox_n_22,rowBox_n_23,rowBox_n_24,rowBox_n_25}),
        .\tempCnt_reg[9]_3 (rowBox_n_27),
        .\tempCnt_reg[9]_4 ({rowBox_n_30,rowBox_n_31,rowBox_n_32,rowBox_n_33}),
        .\tempCnt_reg[9]_5 ({rowBox_n_34,rowBox_n_35,rowBox_n_36,rowBox_n_37}),
        .\tempCnt_reg[9]_6 ({rowBox_n_38,rowBox_n_39,rowBox_n_40,rowBox_n_41}),
        .\tempCnt_reg[9]_7 ({rowBox_n_42,rowBox_n_43,rowBox_n_44,rowBox_n_45}),
        .\tempCnt_reg[9]_8 ({rowBox_n_47,rowBox_n_48}),
        .\tempCnt_reg[9]_9 (rowBox_n_56));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_scopeFace scopeF
       (.CO(r1010_out),
        .DI(rowBox_n_46),
        .O({scopeF_n_15,scopeF_n_16,scopeF_n_17}),
        .Q(Q),
        .S({colBox_n_15,colBox_n_16,colBox_n_17,colBox_n_18}),
        .\dc_bias[3]_i_27 ({rowBox_n_30,rowBox_n_31,rowBox_n_32,rowBox_n_33}),
        .\dc_bias[3]_i_27_0 ({rowBox_n_38,rowBox_n_39,rowBox_n_40,rowBox_n_41}),
        .\dc_bias[3]_i_27_1 ({rowBox_n_22,rowBox_n_23,rowBox_n_24,rowBox_n_25}),
        .\dc_bias[3]_i_27_2 ({rowBox_n_42,rowBox_n_43,rowBox_n_44,rowBox_n_45}),
        .\dc_bias[3]_i_29 ({colBox_n_19,colBox_n_20,colBox_n_21,colBox_n_22}),
        .\dc_bias[3]_i_29_0 ({colBox_n_29,colBox_n_30,colBox_n_31,colBox_n_32}),
        .\dc_bias[3]_i_29_1 ({colBox_n_23,colBox_n_24,colBox_n_25,colBox_n_26}),
        .\dc_bias[3]_i_42 ({rowBox_n_10,rowBox_n_11,rowBox_n_12}),
        .\dc_bias[3]_i_43 ({colBox_n_27,colBox_n_28}),
        .\dc_bias[3]_i_43_0 (colBox_n_63),
        .\dc_bias[3]_i_59 ({rowBox_n_13,rowBox_n_14,rowBox_n_15,rowBox_n_16}),
        .\dc_bias[3]_i_9__1 ({colBox_n_59,colBox_n_60,colBox_n_61,colBox_n_62}),
        .\dc_bias[3]_i_9__1_0 ({rowBox_n_34,rowBox_n_35,rowBox_n_36,rowBox_n_37}),
        .i___20_carry_i_3(\tempCnt_reg[9] [9:2]),
        .i___20_carry_i_3_0({colBox_n_71,colBox_n_72,colBox_n_73}),
        .i___29_carry__0_i_3({rowBox_n_47,rowBox_n_48}),
        .i___29_carry_i_2(rowBox_n_56),
        .i___29_carry_i_3({rowBox_n_52,rowBox_n_53,rowBox_n_54,rowBox_n_55}),
        .\r7_inferred__1/i__carry__0_0 ({rowBox_n_49,rowBox_n_50,rowBox_n_51}),
        .\r7_inferred__2/i__carry__0_0 ({colBox_n_68,colBox_n_69,colBox_n_70}),
        .\tempCnt_reg[3] ({scopeF_n_18,scopeF_n_19,scopeF_n_20,scopeF_n_21}),
        .\tempCnt_reg[4] ({scopeF_n_30,scopeF_n_31,scopeF_n_32,scopeF_n_33}),
        .\tempCnt_reg[5] ({scopeF_n_22,scopeF_n_23,scopeF_n_24}),
        .\tempCnt_reg[5]_0 ({scopeF_n_34,scopeF_n_35}),
        .\tempCnt_reg[8] (scopeF_n_10),
        .\tempCnt_reg[9] (r10),
        .\tempCnt_reg[9]_0 (r9),
        .\tempCnt_reg[9]_1 (r8),
        .\tempCnt_reg[9]_10 (scopeF_n_25),
        .\tempCnt_reg[9]_11 (scopeF_n_26),
        .\tempCnt_reg[9]_12 ({scopeF_n_27,scopeF_n_28,scopeF_n_29}),
        .\tempCnt_reg[9]_2 (r7),
        .\tempCnt_reg[9]_3 (r102_out),
        .\tempCnt_reg[9]_4 (r98_out),
        .\tempCnt_reg[9]_5 (r86_out),
        .\tempCnt_reg[9]_6 (r74_out),
        .\tempCnt_reg[9]_7 (r1011_out),
        .\tempCnt_reg[9]_8 (scopeF_n_11),
        .\tempCnt_reg[9]_9 ({scopeF_n_12,scopeF_n_13,scopeF_n_14}));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_video
   (Q,
    \tempCnt_reg[9] ,
    \tempCnt_reg[8] ,
    s00_axi_aclk,
    s00_axi_aresetn,
    CO,
    \encoded_reg[9] ,
    \dc_bias_reg[3] );
  output [9:0]Q;
  output [9:0]\tempCnt_reg[9] ;
  output \tempCnt_reg[8] ;
  input s00_axi_aclk;
  input s00_axi_aresetn;
  input [0:0]CO;
  input [0:0]\encoded_reg[9] ;
  input \dc_bias_reg[3] ;

  wire [0:0]CO;
  wire Inst_vga_n_20;
  wire Inst_vga_n_22;
  wire Inst_vga_n_23;
  wire Inst_vga_n_24;
  wire Inst_vga_n_25;
  wire Inst_vga_n_26;
  wire Inst_vga_n_27;
  wire Inst_vga_n_28;
  wire Inst_vga_n_29;
  wire Inst_vga_n_30;
  wire Inst_vga_n_31;
  wire Inst_vga_n_32;
  wire Inst_vga_n_33;
  wire Inst_vga_n_34;
  wire Inst_vga_n_35;
  wire Inst_vga_n_36;
  wire Inst_vga_n_37;
  wire Inst_vga_n_38;
  wire Inst_vga_n_39;
  wire Inst_vga_n_40;
  wire Inst_vga_n_41;
  wire Inst_vga_n_42;
  wire Inst_vga_n_43;
  wire Inst_vga_n_44;
  wire Inst_vga_n_45;
  wire Inst_vga_n_46;
  wire Inst_vga_n_48;
  wire Inst_vga_n_49;
  wire Inst_vga_n_50;
  wire [9:0]Q;
  wire \TDMS_encoder_blue/p_1_in ;
  wire \TDMS_encoder_green/p_1_in ;
  wire \TDMS_encoder_red/p_1_in ;
  wire \dc_bias_reg[3] ;
  wire [9:9]encoded1_in;
  wire [0:0]\encoded_reg[9] ;
  wire inst_dvid_n_1;
  wire inst_dvid_n_10;
  wire inst_dvid_n_11;
  wire inst_dvid_n_12;
  wire inst_dvid_n_13;
  wire inst_dvid_n_15;
  wire inst_dvid_n_2;
  wire inst_dvid_n_3;
  wire inst_dvid_n_4;
  wire inst_dvid_n_6;
  wire inst_dvid_n_7;
  wire inst_dvid_n_8;
  wire inst_dvid_n_9;
  wire pixel_clk;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire serialize_clk;
  wire serialize_clk_n;
  wire \tempCnt_reg[8] ;
  wire [9:0]\tempCnt_reg[9] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga Inst_vga
       (.CO(CO),
        .D(Inst_vga_n_37),
        .Q(Q),
        .clk_out1(pixel_clk),
        .\dc_bias_reg[0] (Inst_vga_n_41),
        .\dc_bias_reg[2] (Inst_vga_n_39),
        .\dc_bias_reg[3] (Inst_vga_n_20),
        .\dc_bias_reg[3]_0 (Inst_vga_n_27),
        .\dc_bias_reg[3]_1 (Inst_vga_n_30),
        .\dc_bias_reg[3]_2 (Inst_vga_n_31),
        .\dc_bias_reg[3]_3 (Inst_vga_n_34),
        .\dc_bias_reg[3]_4 (Inst_vga_n_36),
        .\dc_bias_reg[3]_5 (Inst_vga_n_40),
        .\dc_bias_reg[3]_6 (Inst_vga_n_43),
        .\dc_bias_reg[3]_7 (Inst_vga_n_44),
        .\dc_bias_reg[3]_8 (Inst_vga_n_49),
        .\dc_bias_reg[3]_9 (inst_dvid_n_4),
        .\dc_bias_reg[3]_i_2 (inst_dvid_n_9),
        .encoded1_in(encoded1_in),
        .\encoded_reg[0] ({\TDMS_encoder_blue/p_1_in ,inst_dvid_n_1,inst_dvid_n_2,inst_dvid_n_3}),
        .\encoded_reg[0]_0 (inst_dvid_n_12),
        .\encoded_reg[1] (inst_dvid_n_15),
        .\encoded_reg[4] (\TDMS_encoder_red/p_1_in ),
        .\encoded_reg[8] (inst_dvid_n_13),
        .\encoded_reg[9] (\encoded_reg[9] ),
        .\encoded_reg[9]_0 ({\TDMS_encoder_green/p_1_in ,inst_dvid_n_6,inst_dvid_n_7,inst_dvid_n_8}),
        .\encoded_reg[9]_1 (inst_dvid_n_11),
        .\encoded_reg[9]_2 (inst_dvid_n_10),
        .s00_axi_aresetn(s00_axi_aresetn),
        .\tempCnt_reg[1] (Inst_vga_n_22),
        .\tempCnt_reg[1]_0 (Inst_vga_n_25),
        .\tempCnt_reg[1]_1 (Inst_vga_n_35),
        .\tempCnt_reg[1]_2 (Inst_vga_n_42),
        .\tempCnt_reg[1]_3 (Inst_vga_n_45),
        .\tempCnt_reg[1]_4 (Inst_vga_n_46),
        .\tempCnt_reg[6] (Inst_vga_n_29),
        .\tempCnt_reg[6]_0 (Inst_vga_n_38),
        .\tempCnt_reg[8] (\tempCnt_reg[8] ),
        .\tempCnt_reg[8]_0 (Inst_vga_n_28),
        .\tempCnt_reg[8]_1 (Inst_vga_n_32),
        .\tempCnt_reg[8]_2 (Inst_vga_n_33),
        .\tempCnt_reg[8]_3 (Inst_vga_n_48),
        .\tempCnt_reg[8]_4 (Inst_vga_n_50),
        .\tempCnt_reg[9] (\tempCnt_reg[9] ),
        .\tempCnt_reg[9]_0 (Inst_vga_n_23),
        .\tempCnt_reg[9]_1 (Inst_vga_n_24),
        .\tempCnt_reg[9]_2 (Inst_vga_n_26));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dvid inst_dvid
       (.CLK(serialize_clk),
        .CO(CO),
        .D(Inst_vga_n_37),
        .Q({\TDMS_encoder_blue/p_1_in ,inst_dvid_n_1,inst_dvid_n_2,inst_dvid_n_3}),
        .clk_out1(pixel_clk),
        .clk_out3(serialize_clk_n),
        .\dc_bias[3]_i_9 (\encoded_reg[9] ),
        .\dc_bias[3]_i_9_0 (Inst_vga_n_22),
        .\dc_bias[3]_i_9_1 (\tempCnt_reg[8] ),
        .\dc_bias_reg[0] (inst_dvid_n_10),
        .\dc_bias_reg[0]_0 (Inst_vga_n_42),
        .\dc_bias_reg[1] (inst_dvid_n_9),
        .\dc_bias_reg[1]_0 (inst_dvid_n_12),
        .\dc_bias_reg[1]_1 (inst_dvid_n_13),
        .\dc_bias_reg[1]_2 (inst_dvid_n_15),
        .\dc_bias_reg[1]_3 (Inst_vga_n_23),
        .\dc_bias_reg[1]_4 (Inst_vga_n_24),
        .\dc_bias_reg[1]_5 (Inst_vga_n_41),
        .\dc_bias_reg[1]_6 (Inst_vga_n_20),
        .\dc_bias_reg[1]_7 (Inst_vga_n_26),
        .\dc_bias_reg[2] (Inst_vga_n_46),
        .\dc_bias_reg[2]_0 (Inst_vga_n_40),
        .\dc_bias_reg[2]_1 (Inst_vga_n_35),
        .\dc_bias_reg[3] (inst_dvid_n_4),
        .\dc_bias_reg[3]_0 ({\TDMS_encoder_green/p_1_in ,inst_dvid_n_6,inst_dvid_n_7,inst_dvid_n_8}),
        .\dc_bias_reg[3]_1 (inst_dvid_n_11),
        .\dc_bias_reg[3]_2 (\TDMS_encoder_red/p_1_in ),
        .\dc_bias_reg[3]_3 (Inst_vga_n_45),
        .\dc_bias_reg[3]_4 (Inst_vga_n_39),
        .\dc_bias_reg[3]_5 (\dc_bias_reg[3] ),
        .\encoded_reg[0] (Inst_vga_n_31),
        .\encoded_reg[0]_0 (Inst_vga_n_28),
        .\encoded_reg[1] (Inst_vga_n_33),
        .\encoded_reg[2] (Inst_vga_n_25),
        .\encoded_reg[3] (Inst_vga_n_43),
        .\encoded_reg[3]_0 (Inst_vga_n_30),
        .\encoded_reg[4] (Inst_vga_n_34),
        .\encoded_reg[5] (encoded1_in),
        .\encoded_reg[5]_0 (Inst_vga_n_27),
        .\encoded_reg[6] (Inst_vga_n_29),
        .\encoded_reg[7] (Inst_vga_n_32),
        .\encoded_reg[8] (Inst_vga_n_50),
        .\encoded_reg[8]_0 (Inst_vga_n_48),
        .\encoded_reg[8]_1 (Inst_vga_n_38),
        .\encoded_reg[9] (Inst_vga_n_49),
        .\encoded_reg[9]_0 (Inst_vga_n_44),
        .\encoded_reg[9]_1 (Inst_vga_n_36));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0 mmcm_adv_inst_display_clocks
       (.clk_in1(s00_axi_aclk),
        .clk_out1(pixel_clk),
        .clk_out2(serialize_clk),
        .clk_out3(serialize_clk_n),
        .resetn(s00_axi_aresetn));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
