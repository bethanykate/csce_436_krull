----------------------------------------------------------------------------------
-- Bethany Krull
-- CSCE 436
-- Homework 1
-- filename: Q6.vhd
-- Description: Implement a circuit that idexes the most significant 1 in the input.
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Q6 is
    Port ( I3 : in STD_LOGIC;
           I2 : in STD_LOGIC;
           I1 : in STD_LOGIC;
           I0 : in STD_LOGIC;
           O1 : out STD_LOGIC;
           O0 : out STD_LOGIC);
end Q6;

architecture implementation of Q6 is
begin
    O0 <= (not I2 and I1) or (I3);
    O1 <= I2 or I3;

end implementation;
