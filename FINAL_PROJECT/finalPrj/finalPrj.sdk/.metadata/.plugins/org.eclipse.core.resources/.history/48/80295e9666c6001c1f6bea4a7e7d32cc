/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
/*--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	03/09/22
-- File:	lab3.c
-- Event:	lab 3
-- Crs:		CSCE 436
--
-- Purp:	operate o-scope from MicroBlaze - use keyboard direction
--
-- Documentation:	MicroBlaze Tutorial - modified from lec20.c given in class
--					help from instructor and TAs
--
-- Academic Integrity Statement: I certify that, while others may have
-- assisted me in brain storming, debugging and validating this program,
-- the program itself is my own work. I understand that submitting code
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to
-- another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
/***************************** Include Files ********************************/

#include "xparameters.h"
#include "stdio.h"
#include "xstatus.h"

#include "platform.h"
#include "xil_printf.h"						// Contains xil_printf
#include <xuartlite_l.h>					// Contains XUartLite_RecvByte
#include <xil_io.h>							// Contains Xil_Out8 and its variations
#include <xil_exception.h>


/************************** Constant Definitions ****************************/

/*
 * The following constants define the slave registers used for our Counter PCORE
 */
#define wordleBase			0x44a00000
#define blueToothIn			wordleBase+0x38		//slv 14
#define letter1				wordleBase+0x3C		//slv 15
#define letter2				wordleBase+0x40		//slv 16
#define letter3				wordleBase+0x44		//slv 17
#define letter4				wordleBase+0x48		//slv 18
#define letter5				wordleBase+0x4C		//slv 19

#define printf xil_printf			/* A smaller footprint printf */

#define	uartRegAddr			0x40600000		// read <= RX, write => TX


/************************** Function Prototypes ****************************/

/************************** Variable Definitions **************************/



int main()
{
	unsigned char c;

	int bluetooth[29];
	char guessWord[5] = {'0','0','0','0','0'};
	char trueWord[5] = {'a','b','c','d','e'};
	int letterCount = 0;
	char guessLetter;

	init_platform();

	print("Welcome to Wordle - guess a 5-letter word 1 letter at a time!\n\r");

	Xil_Out8(letter1,0);
	Xil_Out8(letter2,0);
	Xil_Out8(letter3,0);
	Xil_Out8(letter4,0);
	Xil_Out8(letter5,0);

//	Xil_Out8(letter1,1);
//	Xil_Out8(letter2,0);
//	Xil_Out8(letter3,0);
//	Xil_Out8(letter4,1);
//	Xil_Out8(letter5,0);

	while(1) {

		if(Xil_In8(blueToothIn) == 0){
			for(int i = 0; i <=28; i++){
				bluetooth[i] = Xil_In8(blueToothIn);
				usleep(100);
			}
			for(int i = 0; i<= 28; i++){
				printf("%d", bluetooth[i]);
			}
			printf("\r\n");


			if(bluetooth[5] == 0){
				if(bluetooth[1] == 1 && bluetooth[2] == 0 && bluetooth[3] == 0 && bluetooth[4] == 0){
					guessLetter = 'a';
				}else if(bluetooth[1] == 0 && bluetooth[2] == 1 && bluetooth[3] == 0 && bluetooth[4] == 0){
					guessLetter = 'b';
				}else if(bluetooth[1] == 1 && bluetooth[2] == 1 && bluetooth[3] == 0 && bluetooth[4] == 0){
					guessLetter = 'c';
				}else if(bluetooth[1] == 0 && bluetooth[2] == 0 && bluetooth[3] == 1 && bluetooth[4] == 0){
					guessLetter = 'd';
				}else if(bluetooth[1] == 1 && bluetooth[2] == 0 && bluetooth[3] == 1 && bluetooth[4] == 0){
					guessLetter = 'e';
				}else if(bluetooth[1] == 0 && bluetooth[2] == 1 && bluetooth[3] == 1 && bluetooth[4] == 0){
					guessLetter = 'f';
				}else if(bluetooth[1] == 1 && bluetooth[2] == 1 && bluetooth[3] == 1 && bluetooth[4] == 0){
					guessLetter = 'g';
				}else if(bluetooth[1] == 0 && bluetooth[2] == 0 && bluetooth[3] == 0 && bluetooth[4] == 1){
					guessLetter = 'h';
				}else if(bluetooth[1] == 1 && bluetooth[2] == 0 && bluetooth[3] == 0 && bluetooth[4] == 1){
					guessLetter = 'i';
				}else if(bluetooth[1] == 0 && bluetooth[2] == 1 && bluetooth[3] == 0 && bluetooth[4] == 1){
					guessLetter = 'j';
				}else if(bluetooth[1] == 1 && bluetooth[2] == 1 && bluetooth[3] == 0 && bluetooth[4] == 1){
					guessLetter = 'k';
				}else if(bluetooth[1] == 0 && bluetooth[2] == 0 && bluetooth[3] == 1 && bluetooth[4] == 1){
					guessLetter = 'l';
				}else if(bluetooth[1] == 1 && bluetooth[2] == 0 && bluetooth[3] == 1 && bluetooth[4] == 1){
					guessLetter = 'm';
				}else if(bluetooth[1] == 0 && bluetooth[2] == 1 && bluetooth[3] == 1 && bluetooth[4] == 1){
					guessLetter = 'n';
				}else if(bluetooth[1] == 1 && bluetooth[2] == 1 && bluetooth[3] == 1 && bluetooth[4] == 1){
					guessLetter = 'o';
				}
			}else{
				if(bluetooth[1] == 0 && bluetooth[2] == 0 && bluetooth[3] == 0 && bluetooth[4] == 0){
					guessLetter = 'p';
				}else if(bluetooth[1] == 1 && bluetooth[2] == 0 && bluetooth[3] == 0 && bluetooth[4] == 0){
					guessLetter = 'q';
				}else if(bluetooth[1] == 0 && bluetooth[2] == 1 && bluetooth[3] == 0 && bluetooth[4] == 0){
					guessLetter = 'r';
				}else if(bluetooth[1] == 1 && bluetooth[2] == 1 && bluetooth[3] == 0 && bluetooth[4] == 0){
					guessLetter = 's';
				}else if(bluetooth[1] == 0 && bluetooth[2] == 0 && bluetooth[3] == 1 && bluetooth[4] == 0){
					guessLetter = 't';
				}else if(bluetooth[1] == 1 && bluetooth[2] == 0 && bluetooth[3] == 1 && bluetooth[4] == 0){
					guessLetter = 'u';
				}else if(bluetooth[1] == 0 && bluetooth[2] == 1 && bluetooth[3] == 1 && bluetooth[4] == 0){
					guessLetter = 'v';
				}else if(bluetooth[1] == 1 && bluetooth[2] == 1 && bluetooth[3] == 1 && bluetooth[4] == 0){
					guessLetter = 'w';
				}else if(bluetooth[1] == 0 && bluetooth[2] == 0 && bluetooth[3] == 0 && bluetooth[4] == 1){
					guessLetter = 'x';
				}else if(bluetooth[1] == 1 && bluetooth[2] == 0 && bluetooth[3] == 0 && bluetooth[4] == 1){
					guessLetter = 'y';
				}else if(bluetooth[1] == 0 && bluetooth[2] == 1 && bluetooth[3] == 0 && bluetooth[4] == 1){
					guessLetter = 'z';
				}
			}


			printf("%c\r\n",guessLetter);

			if(letterCount < 5){
				guessWord[letterCount] = guessLetter;
				printf("%d\r\n",letterCount);
				letterCount++;
				printf("%d\r\n",letterCount);
			}

			for(int j = 0; j<5; j++){
				printf("%c\r\n",guessWord[j]);
			}
			printf("\r\n");
		}

		if(letterCount == 5){
			letterCount++;
			if(trueWord[0] == guessWord[0]){
				Xil_Out8(letter1,1);
			} else {
				Xil_Out8(letter1,0);
			}
			if(trueWord[1] == guessWord[1]){
				Xil_Out8(letter2,1);
			} else {
				Xil_Out8(letter2,0);
			}
			if(trueWord[2] == guessWord[2]){
				Xil_Out8(letter3,1);
			} else {
				Xil_Out8(letter3,0);
			}
			if(trueWord[3] == guessWord[3]){
				Xil_Out8(letter4,1);
			} else {
				Xil_Out8(letter4,0);
			}
			if(trueWord[4] == guessWord[4]){
				Xil_Out8(letter5,1);
			} else {
				Xil_Out8(letter5,0);
			}
		}

    } // end while

    cleanup_platform();

    return 0;

} // end main



