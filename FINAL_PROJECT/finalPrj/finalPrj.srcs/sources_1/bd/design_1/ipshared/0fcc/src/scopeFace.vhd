--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	02/02/22
-- File:	scopeFace.vhd
-- HW:		Lab 1
-- Crs:		CSCE 436
--
-- Purp:	Output the correct color for each pixel according to the 
--          counters' coordinates
--
-- Documentation:	I worked on my own - used the give entity scopeFace 
--                  code
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;

entity scopeFace is
    Port (  row : in  unsigned(9 downto 0);
            column : in  unsigned(9 downto 0);
			letter1 : in std_logic;
			letter2 : in std_logic;
			letter3 : in std_logic;
			letter4 : in std_logic;
			letter5 : in std_logic;
            r : out  std_logic_vector(7 downto 0);
            g : out  std_logic_vector(7 downto 0);
            b : out  std_logic_vector(7 downto 0));
end scopeFace;

architecture Behavioral of scopeFace is
signal midLine : std_logic;
signal L1yes : std_logic;
signal L2yes : std_logic;
signal L3yes : std_logic;
signal L4yes : std_logic;
signal L5yes : std_logic;
signal L1no : std_logic;
signal L2no : std_logic;
signal L3no : std_logic;
signal L4no : std_logic;
signal L5no : std_logic;
begin

    -- the RGB value is set according to which screen item is true at a given coordinate
    -- priority goes from top -> down

    R <=    x"00" when (L1yes = '1' and letter1 = '1') else
            x"00" when (L2yes = '1' and letter2 = '1') else
            x"00" when (L3yes = '1' and letter3 = '1') else
            x"00" when (L4yes = '1' and letter4 = '1') else
            x"00" when (L5yes = '1' and letter5 = '1') else
            x"FF" when (L1no = '1' and letter1 = '0') else
            x"FF" when (L2no = '1' and letter2 = '0') else
            x"FF" when (L3no = '1' and letter3 = '0') else
            x"FF" when (L4no = '1'and letter4 = '0') else
            x"FF" when (L5no = '1' and letter5 = '0') else
            x"FF" when (midLine = '1') else x"00";
    G <=    x"FF" when (L1yes = '1' and letter1 = '1') else
            x"FF" when (L2yes = '1' and letter2 = '1') else
            x"FF" when (L3yes = '1' and letter3 = '1') else
            x"FF" when (L4yes = '1' and letter4 = '1') else
            x"FF" when (L5yes = '1' and letter5 = '1') else
            x"00" when (L1no = '1' and letter1 = '0') else
            x"00" when (L2no = '1' and letter2 = '0') else
            x"00" when (L3no = '1' and letter3 = '0') else
            x"00" when (L4no = '1' and letter4 = '0') else
            x"00" when (L5no = '1' and letter5 = '0') else
            x"FF" when (midLine = '1') else x"00";  
    B <=    x"00" when (L1yes = '1' and letter1 = '1') else
            x"00" when (L2yes = '1' and letter2 = '1') else
            x"00" when (L3yes = '1' and letter3 = '1') else
            x"00" when (L4yes = '1' and letter4 = '1') else
            x"00" when (L5yes = '1' and letter5 = '1') else
            x"00" when (L1no = '1' and letter1 = '0') else
            x"00" when (L2no = '1' and letter2 = '0') else
            x"00" when (L3no = '1' and letter3 = '0') else
            x"00" when (L4no = '1' and letter4 = '0') else
            x"00" when (L5no = '1' and letter5 = '0') else
            x"FF" when (midLine = '1') else x"00";       

    -- the plain grid lines
    midLine <= '1' when (row >= 20 and row <= 420 and column>= 20 and column <= 620) and
                         row = 220 and 
                        ((column > 50 and column <= 110) or
                         (column > 170 and column <= 230) or
                         (column > 290 and column <= 350) or
                         (column > 410 and column <= 470) or
                         (column > 530 and column <= 590))
                          else '0';
                        
    L1yes <= '1' when (column > 50 and column <= 110) and
                   (row = column + 110 or row = 270 - column)
                    else '0';
                    
    L2yes <= '1' when (column > 170 and column <= 230) and
                   (row = column - 10 or row = 390 - column)
                    else '0';

    L3yes <= '1' when (column > 290 and column <= 350) and
                    (row = column - 130 or row = 510 - column)
                    else '0';
                    
    L4yes <= '1' when (column > 410 and column <= 470) and
                    (row = column - 250 or row = 630 - column)
                    else '0';                    
                    
    L5yes <= '1' when (column > 530 and column <= 590) and
                    (row = column - 370 or row = 750 - column)
                    else '0';  
      
    ------------------------------------------------------------------------------                  
                    
    L1no <= '1' when (column > 50 and column <= 110) and
                   (row = column + 110 or row = 270 - column)
                    else '0';
                    
    L2no <= '1' when (column > 170 and column <= 230) and
                   (row = column - 10 or row = 390 - column)
                    else '0';

    L3no <= '1' when (column > 290 and column <= 350) and
                    (row = column - 130 or row = 510 - column)
                    else '0';
                    
    L4no <= '1' when (column > 410 and column <= 470) and
                    (row = column - 250 or row = 630 - column)
                    else '0';                    
                    
    L5no <= '1' when (column > 530 and column <= 590) and
                    (row = column - 370 or row = 750 - column)
                    else '0'; 
                            
end Behavioral;






