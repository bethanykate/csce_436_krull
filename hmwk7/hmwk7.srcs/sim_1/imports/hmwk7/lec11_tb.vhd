--------------------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	02/15/22
-- File:	lec11_tb.vhdl
-- Event: 	homework 7
--
-- Purp:	testbench for hmwk7.vhd
--
-- Documentation:	Modified from the given lec11_tb.vhd file to include hmwk7.vhd
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY lec11_tb IS
END lec11_tb;
 
ARCHITECTURE behavior OF lec11_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
component hmwk7
        port(   clk     : in std_logic;
                reset   : in std_logic;
                Lbtn    : in std_logic;
                Rbtn    : in std_logic;
                count   : out unsigned(2 downto 0));
end component;

   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal Lbtn : std_logic := '0';
   signal Rbtn : std_logic := '0';
   signal count : unsigned(2 downto 0);

   -- Clock period definitions
   constant clk_period : time := 500 ns;

BEGIN
	-- Instantiate the Unit Under Test (UUT)
    uut: hmwk7
        port map(   clk => clk,
                    reset => reset,
                    Lbtn => Lbtn,
                    Rbtn => Rbtn,
                    count => count);     

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
    reset <= '0', '1' after 1us;
    Lbtn <= '0', '1' after 3us, '0' after 5us, '1' after 7us, '0' after 9us, '1' after 19us, '0' after 21us;
    Rbtn <= '0', '1' after 11us, '0' after 13us, '1' after 15us, '0' after 17us, '1' after 23us, '0' after 25us;
END;