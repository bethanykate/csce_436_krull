--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	02/15/22
-- File:	hmwk7.vhd
-- HW:		homework 7
-- Crs:		CSCE 436
--
-- Purp:	provide state logic - use lec11 generaic counter
--
-- Documentation:	I talked with Bearden - used code given in class.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;

entity hmwk7 is
    port(   clk     : in std_logic;
            reset   : in std_logic;
            Lbtn    : in std_logic;
            Rbtn    : in std_logic;
            count   : out unsigned(2 downto 0));
end hmwk7;

architecture Behavioral of hmwk7 is
    COMPONENT lec11
		generic (N: integer := 4);
		Port(	clk: in  STD_LOGIC;
				reset : in  STD_LOGIC;
				crtl: in std_logic_vector(1 downto 0);
				D: in unsigned (N-1 downto 0);
				Q: out unsigned (N-1 downto 0));
    END COMPONENT;

type STATE_TYPE is (waitL, init, Lpressed, waitR, Rpressed, inc);
signal current_state : STATE_TYPE;
signal next_state: STATE_TYPE;
signal cw : std_logic_vector(1 downto 0);
signal D : unsigned(2 downto 0) := "111";

begin
    counter: lec11 
	Generic map(N => 3)
	PORT MAP (
        clk => clk,
        reset => reset,
	    crtl => cw,
        D => D,
        Q => count);  
    
process(clk)
    begin
        if(rising_edge(clk)) then
            if(reset = '0') then
                current_state <= init;
            else
                current_state <= next_state;
            end if;
            
            -- cw LOGIC TO CONTROL COUNTER
            case current_state is
                when init =>
                    cw <= "11";
                when waitL =>
                    cw <= "00";
                when Lpressed =>
                    cw <= "00";
                when waitR =>
                    cw <= "00";
                when Rpressed =>
                    cw <= "00";
                when inc =>
                    cw <= "01";
                when others =>
                    cw <= "00";
            end case;  
        end if;
end process;

    -- STATE LOGIC
    next_state <=   waitL when ((current_state = init) or (current_state = inc)) else
                    Lpressed when ((current_state = waitL) and Lbtn = '1') else
                    waitR when ((current_state = Lpressed) and Lbtn = '0') else
                    Rpressed when ((current_state = waitR) and Rbtn = '1') else
                    inc when ((current_state = Rpressed) and Rbtn = '0');

end Behavioral;
