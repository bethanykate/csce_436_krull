# LAB 4 Report - CSCE439 Advanced Embedded Systems

## By Bethany Krull

## Table of Contents
1. Objectives or Purpose
2. Preliminary Design
3. Software flow chart or algorithms
4. Hardware schematic
5. Debugging
6. Testing methodology or results
7. Answers to Lab Questions
8. Observations and Conclusions
9. Documentation
 
### Objectives or Purpose 
The objective of lab 4 was to design and build a function generator using the components and understanding we had gained through implementing the oscilliscope in labs 1 through 3. For the most part the function generator operated quite similarly to the oscilliscope in that it used the BRAM as a look-up-table and the Audio Codec Wrapper to manage the I/O of the audio signals. We were to make the initialization hex values to feed into the BRAM to make both a sin wave and another non-sin wave. This was done using a given excel spreadsheet from an earlier lecture. The other major piece of functionality for the lab was to use the board's buttons to adjust the frequency and amplitude of the displayed waveform. This was done by changing the address used on the BRAM to either skip over addresses and make the frequency higher or to slow the address increment down to make the waveform appear stretched. A-level functionality was to implement keyboard controls similar to lab 3 that can set the frequency and amplitude via command line. Additional functionality was to include a more sofisticated way to build and display the waveform involving linear interpolation to predict half of the values.

<br>
### Preliminary design
<br>
![Memory Dump](doc/prelimDesign.jpg)
*Fig1 - working preliminary block design*
<br>

Above is the preliminary design I used when I began coding lab 4. It differs from the schematic I submitted for gatecheck 1 (shown later) in the details of the logic flow, but for the most part they are similar. Basically, upon instantiating all the components both the waveforms (sin and not-sin) are initialized and written into the BRAM LUT. A process continually increments the address index value (used as the address for that cycles's BRAM value) by the phase_inc value. This phase_inc value is initialized to 0x259 but the left/right button on the board can increase/decrease the phase_inc according to the number of switches flipped to ON. The index value then goes to the BRAM read address port to dictate what point of the initialized wave will be output on BRAMout. This output is modified (+x"8000" + "00") to match the range of the audio codec values before being sent to the L_bus_in port of the Audio Codec Wrapper. The wrapper handles all the details to send the signal out through the LINE OUT audio port on the board. This is the base design to achieve required functionality which is also I considered in the preliminary stages of the lab.

Prelab:  <br>
There was no prelab for lab 4.
<br>

#### Code:
<br>
Code is available in bitbucket repository.

### Software flow chart or algorithms
<br>
![Memory Dump](doc/stateDiag.png)
*Fig2 - state diagram*
<br>

The software flow for the function generator is much simpler than that of the oscilliscope. There are 5 states, but only the incIndex state has a nonzero cw and only the transition from waitReady to incIndex requires a certain sw value. The system starts in the reset state and immediately returns there if reset_n = 0. The reset states flows straight to the waitReady state and waits for the Audio Codec Wrapper's ready signal to be 1. Once this occurs the system will move to the incIndex state which outputs a control word that tells the index_cntr value to increment by phase_in. Then the system moves to the audioLUT state where the BRAM is finding the value at the indicated address and sending that out to the datapath file. Then it moves to the audioOut state where the output of the BRAM is modified and plugged into the Audio Codec Wrapper and out of the board as a whole.

#### Pseudocode:
<br>
There is no pseudocode.

### Hardware schematic
<br>
![Memory Dump](doc/finalHardware.png)
*Fig3 - final block design*
<br>

Adding B-Level functionality doesn't modify the hardware design much. The up/down board buttons are enabled and set to increase/decrease the amp variable according to the status of the switches. The amp variable is multiplied with the BRAMout value before being adjusted to fit the audio range (+x"8000" + "00") and sent to the Audio Codec Wrapper. The final part of B-Level functionality is the ability to swap the sin waveform for the not-sin waveform using the center button. This was pretty straight forward after implementing the reset of the functionality for the sin wave. I made another BRAM LUT to hold the initialized not-sin wave - I chose to make a wave for the function sin(x)*cos^2(x). Then I made a mux to swap which BRAMout I plugged into the Audio Codec Wrapper. This mux was determined by the swap wave variable I added to my button logic process. Sharing the left channel between the sin and not-sin wave instead of making one of them go to the right channel was neccessary because of the limitations of the small audio jack board we were given. It can only send either the left or right channel at one time. So, I couldn't design it so enable/disable the left/right channels like how I did in lab 3.

A-Level functionality was done in a different Vivado project called lab4_microblaze and directly utilizes the lab4 (B-Level funtionality). Adding the microblaze requires lab4 to be made into an IP package with slave registers set up to send data from the SDK files to the board logic. This hardware design is directly based off lab3's design minus the element of interrupts. The c code in the SDK sends uart messages to the board that allow the frequency and amplitude to be set directly instead of mashing a button till the waveform is set correctly.

### Debugging
I had a lot of issues in creating my sin wave initialization values. I used the given excel spreadsheet, but I neglected to fill each line backwards. So, when I ran my code I got an output on the oscope but it looked like a sawtooth instead of a smooth sin wave. This makes sense since each "slice" of the wave is in order but the indiviual datapoints within the slice were backwards so the slices didn't come together to make smooth rounded wave - instead the wave was jagged. Flipping the order of the data points within my slices fixed my problem instantly.

Additionally, I don't have an audio jack on my phone so I have to use an adapter for usb-c. My waveform was initialized correctly, and Falkinburg even tested it on a standard osilliscope, but my waveform was blocky and generally misshapened. He suggested that I switch adapters to see if the wire was just bad and that fixed it immediately.

### Testing methodology or results
Gatecheck 1:<br>
<br>
![Memory Dump](doc/lab4check1.png)
*Fig4 - initial gatecheck block design*
<br>

Gatecheck 2:<br>
<br>
![Memory Dump](doc/gate2Sim.png)
*Fig5 - required functionality simulation*
<br>

Required Functionality:<br>
LINK TO REQUIRED FUNCTIONALITY: https://photos.app.goo.gl/S7a3WdmLNHp1xevb9

B-Level Functionality:<br>
I could not hook my function generator to the husker scope on my laptop so I had to use my phone. So I don't have any of my button presses in view, but I am just using the up/down and left/right and center buttons to adjust the waveform. I also change the switches a few times to increase/decrease the sensitivity of the phase increment and amplitude changes.
LINK TO B-LEVEL FUNCTIONALITY: https://photos.app.goo.gl/6JSmqy9gT7hf5BNJ9

### Answers to Lab Questions 
<br> 
There are no questions for this lab.
### Observations and Conclusions

This lab helped bring my understanding of the whole class-long lab project together. I understood how our oscilliscope worked during lab 2 and 3, but I feel like modifying the oscope stuff to make a function generator really helped me understand the full functionality of all the components in the lab system - especially the BRAM and Audio Codec Wrapper. I struggled initially with how the BRAM processed the init values and also how we were to use the Excel graph to produce the hex values of the sin wave. Learning how to properly build and supply the wave to the BRAM made me much more aware of how the BRAM works. Additionally, we were encouraged to design the solution to the function generator ourselves with significantly less guidance in the initial stages of the lab. This forced me to think creatively and reflect on the previous labs' hardware designs to come up with a preliminary design. I was rather close in my initial hardware design, but I definatly modified it after gatecheck 1 to reflect the hints Falkinburg gave us in class. I was fairly close, but in the future I need to think about the design as a whole before getting stuck in the details. In general I enjoyed this lab and felt like it was a good wrap-up to the string of oscope labs.

### Documentation
<br>
I utilized the help of the TAs and the professor as well as code given to us throughout the class and code from my previous assignments.
