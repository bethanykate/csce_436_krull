--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	04/04/22
-- File:	lab4_tb.vhd
-- HW:		Lab 4
-- Crs:		CSCE 436
--
-- Purp:	testbench for lab 4
--
-- Documentation:	I worked on my own
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;
library UNIMACRO;
use UNIMACRO.vcomponents.all;
use ieee.std_logic_unsigned.all;

entity lab4_tb is
end lab4_tb;

architecture Behavioral of lab4_tb is

    component lab4
        port(  clk : in  STD_LOGIC;
               reset_n : in  STD_LOGIC;
               ac_mclk : out STD_LOGIC;
               ac_adc_sdata : in STD_LOGIC;
               ac_dac_sdata : out STD_LOGIC;
               ac_bclk : out STD_LOGIC;
               ac_lrclk : out STD_LOGIC;
               scl : inout STD_LOGIC;
               sda : inout STD_LOGIC;
               --tmds : out  STD_LOGIC_VECTOR (3 downto 0);
               --tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
               ready : in std_logic;
               switch : in std_logic_vector(7 downto 0);
               btn: in	STD_LOGIC_VECTOR(4 downto 0));
    end component;

    signal clk : STD_LOGIC;
    signal reset_n : STD_LOGIC;
    signal ac_mclk : STD_LOGIC;
    signal ac_adc_sdata : STD_LOGIC;
    signal ac_dac_sdata : STD_LOGIC;
    signal ac_bclk : STD_LOGIC;
    signal ac_lrclk : STD_LOGIC;
    signal scl : STD_LOGIC;
    signal sda : STD_LOGIC;
    signal tmds : STD_LOGIC_VECTOR (3 downto 0);
    signal tmdsb : STD_LOGIC_VECTOR (3 downto 0);
    signal ready : std_logic;
    signal switch : std_logic_vector(7 downto 0);
    signal btn: STD_LOGIC_VECTOR(4 downto 0);
    
    constant clk_period : time := 10 ns;  -- Sets clock to ~ 100MHz
--   constant BIT_CLK_period : time := 80 ns;  -- Sets Bit Clock for AC'97 to the necessary 12.288 MHz
    constant BIT_CLK_period : time := 40 ns;  -- Sets Bit Clock for Audio Codec to the necessary 25 MHz

begin

    uut: lab4
        port map(  clk => clk,
                   reset_n => reset_n,
                   ac_mclk => ac_mclk,
                   ac_adc_sdata => ac_adc_sdata,
                   ac_dac_sdata => ac_dac_sdata,
                   ac_bclk => ac_bclk,
                   ac_lrclk => ac_lrclk,
                   scl => scl,
                   sda => sda,
                   --tmds => tmds,
                   --tmdsb => tmdsb,
                   ready => ready,
                   switch => switch,
                   btn => btn);
                   
            SDATA_process :process  -- Inputs alternating 1's and 0's on each Bit Clock
            begin
                 ac_adc_sdata <= '0';
                 wait for BIT_CLK_period;
                 ac_adc_sdata <= '1';
                 wait for BIT_CLK_period*2;
            end process;
        
           clk_process :process
           begin
                clk <= '0';
                wait for clk_period/2;
                clk <= '1';
                wait for clk_period/2;
           end process;
   
   btn <= "00000", "00010" after 20ns, "00000" after 40ns, "00010" after 60ns, "00000" after 80ns, "01000" after 130ns, "00000" after 160ns;
   reset_n <= '0', '1' after 10ns;
   switch <= "00000000", "00000001" after 30ns, "00000010" after 50ns, "00000011" after 70ns;
   ready <= '0', '1' after 10ns, '0' after 20ns, '1' after 30ns, '0' after 40ns, '1' after 50ns, '0' after 60ns, '1' after 70ns, '0' after 80ns, '1' after 90ns, '0' after 100ns, '1' after 110ns, '0' after 120ns, '1' after 130ns, '0' after 140ns, '1' after 150ns, '0' after 160ns, '1' after 170ns, '0' after 180ns;
   
end Behavioral;
