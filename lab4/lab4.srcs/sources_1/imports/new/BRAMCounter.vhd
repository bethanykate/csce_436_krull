--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	01/31/22
-- File:	BRAMCounter.vhd
-- HW:		lab 2
-- Crs:		CSCE 436
--
-- Purp:	Count 0 - 1024, cycles through the BRAM write addresses
--
-- Documentation:	modified from column/row counter (Lab1)
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;
use UNISIM.VComponents.all;
--use work.lab2Parts.all;	

entity BRAMCounter is
    port(   clk, reset  : in std_logic;
            ctrl        : in std_logic_vector(1 downto 0);
            count       : out unsigned(9 downto 0));
end BRAMCounter;

        -- CONTROL
        -- 00 hold
        -- 01 inc
        -- 10 X
        -- 11 0

architecture Behavioral of BRAMCounter is
signal tempCnt : unsigned(9 downto 0) := "0000000000";
begin
    process(clk)
    begin
        -- increments to 1024 then resets to 20
        if (rising_edge(clk)) then
            if (reset = '0') then
                tempCnt <= "0000010100";
            elsif ((ctrl = "01") and (tempCnt < 1023)) then
                tempCnt <= tempCnt + 1;
            elsif ((ctrl = "01") and (tempCnt = 1023)) then
                tempCnt <= "0000010100";
            elsif (ctrl = "00") then
                tempCnt <= tempCnt;
            elsif (ctrl = "11") then
                tempCnt <= "0000010100";
            end if;
        end if;
    end process;
    count <= tempCnt;
end Behavioral;
