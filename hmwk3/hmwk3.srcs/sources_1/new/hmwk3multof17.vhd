--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	01/24/22
-- File:	hmwk3multof17.vdh
-- HW:		homework 3
-- Crs:		CSCE 436
--
-- Purp:	turn on an LED on the NEXY board if the input (8-bit)
--          is a multiple of 17 - input is determined by the DIP
--          switches on the board
--
-- Documentation:	I worked on my own
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;

entity hmwk3multof17 is
    Port (  sw : in STD_LOGIC_VECTOR (7 downto 0);
            led : out STD_LOGIC);
end hmwk3multof17;

architecture Behavioral of hmwk3multof17 is
begin
    led <= '1' when sw(7 downto 4) = sw(3 downto 0) else '0';
end Behavioral;
