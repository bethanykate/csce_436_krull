--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	04/04/22
-- File:	lab4_cu.vhd
-- HW:		Lab 4
-- Crs:		CSCE 436
--
-- Purp:	state control for Lab 4
--
-- Documentation:	I used code from class
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;
library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity lab4_cu is
-- 3 bits of control -- not sure what I need yet
    port(   clk : in  STD_LOGIC;
            reset_n : in  STD_LOGIC;
            sw: in std_logic_vector(0 downto 0) := (others => '0');
            cw: out std_logic_vector (0 downto 0) := (others => '0'));
end lab4_cu;

architecture Behavioral of lab4_cu is
type state_type is (reset, waitReady, incIndex, audioLUT, audioOUT);
signal state : state_type;

--  sw
    -- 0: ready (audio codec)
    
begin
    process(clk)
    begin
        if(rising_edge(clk))then
            if(reset_n = '0') then
                state <= reset;
            else
                case state is
                    when reset =>
                        state <= waitReady;
                    when waitReady =>
                        if(sw(0) = '1') then        -- ready (audio codec)
                            state <= incIndex;
                        end if;
                    when incIndex =>
                        state <= audioLUT;
                    when audioLUT =>
                        state <= audioOUT;
                    when audioOUT =>
                        state <= waitReady;
                end case;
            end if;
        end if;
    end process;
    
    cw <=   "0" when state = reset else
            "0" when state = waitReady else
            "1" when state = incIndex else
            "0" when state = audioLUT else
            "0" when state = audioOUT else
            "0";

end Behavioral;














