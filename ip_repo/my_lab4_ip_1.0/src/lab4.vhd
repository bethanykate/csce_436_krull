--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	04/04/22
-- File:	lab4.vhd
-- HW:		Lab 4
-- Crs:		CSCE 436
--
-- Purp:	a function generator
--
-- Documentation:	I used code from class
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;
library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity lab4 is
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
		   ac_mclk : out STD_LOGIC;
		   ac_adc_sdata : in STD_LOGIC;
		   ac_dac_sdata : out STD_LOGIC;
		   ac_bclk : out STD_LOGIC;
		   ac_lrclk : out STD_LOGIC;
           scl : inout STD_LOGIC;
           sda : inout STD_LOGIC;
		   --tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           --tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
           --ready : in std_logic;
		   btn : in	STD_LOGIC_VECTOR(4 downto 0);
		   switch : in std_logic_vector(7 downto 0));
end lab4;

architecture Behavioral of lab4 is

    signal sw: std_logic_vector(0 downto 0);
	signal cw: std_logic_vector (0 downto 0);

    component lab4_cu is 
        port(   clk : in  STD_LOGIC;
                reset_n : in  STD_LOGIC;
                sw: in std_logic_vector(0 downto 0) := (others => '0');
                cw: out std_logic_vector (0 downto 0) := (others => '0'));
    end component;
    
    component lab4_dp is
        port(   clk : in  STD_LOGIC;
                reset_n : in  STD_LOGIC;
                ac_mclk : out STD_LOGIC;
                ac_adc_sdata : in STD_LOGIC;
                ac_dac_sdata : out STD_LOGIC;
                ac_bclk : out STD_LOGIC;
                ac_lrclk : out STD_LOGIC;
                scl : inout STD_LOGIC;
                sda : inout STD_LOGIC;
                --tmds : out  STD_LOGIC_VECTOR (3 downto 0);
                --tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
                --ready : in std_logic;
                btn : in	STD_LOGIC_VECTOR(4 downto 0);
                switch : in std_logic_vector(7 downto 0);
                sw : out std_logic_vector(0 downto 0);
                cw : in std_logic_vector(0 downto 0));
        end component;

begin

    datapath : lab4_dp 
        port map(   clk => clk,
		            reset_n => reset_n,
		            ac_mclk => ac_mclk,
		            ac_adc_sdata => ac_adc_sdata,
		            ac_dac_sdata => ac_dac_sdata,
		            ac_bclk => ac_bclk,
		            ac_lrclk => ac_lrclk,
                    scl => scl,
                    sda => sda,
		            --tmds => tmds,
		            --tmdsb => tmdsb,
		            --ready => ready,
		            btn => btn,
		            switch => switch,
		            sw => sw,
		            cw => cw);
		             
    control : lab4_cu
        port map(   clk => clk,
		            reset_n => reset_n,
		            sw => sw,
		            cw => cw);
		            
end Behavioral;












