--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	04/21/22
-- File:	vga.vhd
-- HW:		final project
-- Crs:		CSCE 436
--
-- Purp:	controls drawing the hdmi display for final project
--
-- Documentation:	modeled off of class code
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;

entity vga is
    Port(	clk: in  STD_LOGIC;
			reset_n : in  STD_LOGIC;
			letter1 : in std_logic;
			letter2 : in std_logic;
			letter3 : in std_logic;
			letter4 : in std_logic;
			letter5 : in std_logic;
			h_sync : out  STD_LOGIC;
			v_sync : out  STD_LOGIC; 
			blank : out  STD_LOGIC;
			r: out STD_LOGIC_VECTOR(7 downto 0);
			g: out STD_LOGIC_VECTOR(7 downto 0);
			b: out STD_LOGIC_VECTOR(7 downto 0);
			row: out unsigned(9 downto 0);
			column: out unsigned(9 downto 0));
end vga;

architecture Behavioral of vga is
component colCounter 
    port(   clk, reset  : in std_logic;
            ctrl        : in std_logic;
            count       : out unsigned(9 downto 0);
            rollOUT     : out std_logic);
end component;
component rowCounter
    port(   clk, reset  : in std_logic;
            ctrl        : in std_logic;
            count       : out unsigned(9 downto 0);
            rollOUT     : out std_logic);
end component;
component scopeFace
    Port (  row : in  unsigned(9 downto 0);
            column : in  unsigned(9 downto 0);
			letter1 : in std_logic;
			letter2 : in std_logic;
			letter3 : in std_logic;
			letter4 : in std_logic;
			letter5 : in std_logic;
            r : out  std_logic_vector(7 downto 0);
            g : out  std_logic_vector(7 downto 0);
            b : out  std_logic_vector(7 downto 0));
end component;

signal rollWire     : std_logic;
signal ctrlWire     : std_logic := '1';
signal h_syncWire   : std_logic;
signal v_syncWire   : std_logic;
signal h_blank      : std_logic;
signal v_blank      : std_logic;
signal columnWire   : unsigned(9 downto 0);
signal rowWire      : unsigned(9 downto 0);

begin
    colBox : colCounter
        port map(   clk => clk,
                    reset => reset_n,
                    ctrl => ctrlWire,
                    count => columnWire,
                    rollOUT => rollWire);
    rowBox : rowCounter
        port map(   clk => clk,
                    reset => reset_n,
                    ctrl => rollWire,
                    count => rowWire);
    scopeF: scopeFace
        port map(   row => rowWire,
                    column => columnWire,
                    letter1 => letter1,
                    letter2 => letter2,
                    letter3 => letter3,
                    letter4 => letter4,
                    letter5 => letter5,
                    r => r,
                    g => g,
                    b => b);

-- sets sync and blank signals according to prelab diagram
h_syncWire <= '0' when ((columnWire >= 655) AND (columnWire < 751)) else '1';
v_syncWire <= '0' when ((rowWire >= 489) AND (rowWire < 491)) else '1';
h_blank <= '0' when (columnWire < 639) else '1';
v_blank <= '0' when (rowWire < 479) else '1';

-- the system blank detemines where the edge of the screen is
blank <= h_blank or v_blank;

column <= columnWire;
row <= rowWire;
h_sync <= h_syncWire;
v_sync <= v_syncWire;
end Behavioral;
