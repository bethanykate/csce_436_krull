--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	04/21/22
-- File:	finalPrj.vhd
-- HW:		final project
-- Crs:		CSCE 436
--
-- Purp:	hold the datapath and control unit for the final project
--
-- Documentation:	modeled off of class code
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity finalPrj is
    port(  clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
           blueToothIn : in std_logic;
           blueToothOut : out std_logic);
end finalPrj;

architecture Behavioral of finalPrj is
   component finalPrj_dp is
        port(  clk : in  STD_LOGIC;
               reset_n : in  STD_LOGIC;
               tmds : out  STD_LOGIC_VECTOR (3 downto 0);
               tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
               sw: out std_logic_vector(2 downto 0);
               cw: in std_logic_vector (2 downto 0);
               blueToothIn : in std_logic;
               blueToothOut : out std_logic);
   end component;
    

begin



end Behavioral;
