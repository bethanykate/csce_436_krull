--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	04/21/22
-- File:	finalPrj_cu.vhd
-- HW:		final project
-- Crs:		CSCE 436
--
-- Purp:	control unit for final project
--
-- Documentation:	modeled off of class code
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;
use UNISIM.VComponents.all;

entity lab2_fsm is
    port(   clk : in  STD_LOGIC;
            reset_n : in  STD_LOGIC;
            sw: in std_logic_vector(2 downto 0) := (others => '0');
            cw: out std_logic_vector (2 downto 0) := (others => '0'));
end lab2_fsm;


architecture Behavioral of lab2_fsm is
type state_type is (reset, waitTrigger, inc_count, storeBuffer, waitAudio);
signal state : state_type;
begin
    process(clk)
    begin
        if(rising_edge(clk))then
            if(reset_n = '0')then
                state <= reset;
            else
--                case state is
--                    when reset =>                               -- reset -> waitTrigger
--                        state <= waitTrigger;                   
--                end case;
            end if;
        end if;
    end process;
    
--    cw <= 
            

end Behavioral;
