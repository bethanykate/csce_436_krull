--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	01/31/22
-- File:	rowCounter.vhd
-- HW:		lab 1
-- Crs:		CSCE 436
--
-- Purp:	Count 0 - 523; counts each horizontal row (increment row
--          only once all the columns are counted) - most significant
--
-- Documentation:	I modified my code from homework 4.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;

entity rowCounter is
    port(   clk, reset  : in std_logic;
            ctrl        : in std_logic;
            count       : out unsigned(9 downto 0);
            rollOUT     : out std_logic);
end rowCounter;

architecture Behavioral of rowCounter is
signal tempCnt : unsigned(9 downto 0) := "0000000000";
begin
    process(clk)
    begin
        -- increments to 523 then resets and sends out a rollover signal
        if (rising_edge(clk)) then
            if (reset = '0') then
                tempCnt <= "0000000000";
                rollOUT <= '0';
            elsif ((ctrl = '1') and (tempCnt < 523)) then
                tempCnt <= tempCnt + 1;
                if (tempCnt < 522) then
                    rollOUT <= '0';
                else rollOUT <= '1';
                end if;
            elsif ((ctrl = '1') and (tempCnt = 523)) then
                tempCnt <= "0000000000";
                rollOUT <= '0';
            end if;
        end if;
    end process;
    count <= tempCnt;
end Behavioral;