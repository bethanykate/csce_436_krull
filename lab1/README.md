# LAB 1 Report - CSCE439 Advanced Embedded Systems

## By Bethany Krull

## Table of Contents
1. Objectives or Purpose
2. Preliminary Design
3. Software flow chart or algorithms
4. Hardware schematic
5. Debugging
6. Testing methodology or results
7. Answers to Lab Questions
8. Observations and Conclusions
9. Documentation
 
### Objectives or Purpose 
This lab focuses on creating a VGA (Video Graphics Array) controller to build the foundation of an larger project to implement an oscilliscope. The VGA
controller hosts a component (scopeFace) that builds a white grid with a time and volt trigger that increment left/right and up/down appropriatly. These 
triggers are connected to and controlled by 4 buttons on the FPGA board. Additionally, the project will develop to read in signals and display them on the 
grid, so placeholder lines are added to this lab to represent channel-1 and channel-2 inputs.  
<br>
### Preliminary design
The process of developing the functionality in this lab can be chunked into 3 stages - planning and setting the counters, implementing the scopeFace to draw
a line, and finishing the full functionality. The system hinges on the 2 counters in the VGA that establish the flow of building the screen filling each
pixel from left -> right and top -> down. Once the counters are set up the scopeFace component needs to be added to the VGA using the code given to us. The
scopeFace logic is fairly straightforward - it assignes each pixel its RGB values according to the grid design and input signals that are fed into it.  
<br>
Prelab:  <br>
I made a detailed design plan of the pixels that would need to be coded to white to form 
the grid - including the tick marks along the center lines and the 9-pixel triangles to 
mark the trigger volt and trigger time values. This provided a map for my scopeFace logic
later on in the development of this lab.  
<br>  
![Memory Dump](gateCheck/lab1Grid.png)  
*Fig1 - pixel map*
<br>  

The pixels that are included in the screen layout are less than the total pixels possible
so blank signals controll how far the system goes horizontally as it fills each row and 
vertically as it fills the screen. Additionally, sync signals controll the coutners' 
rollover along rows and each iteration of the screen. Both sync and blank signals need to
be accuratley aligned with the row and column count - the waveform below details these exact
counts.  
<br>
![Memory Dump](gateCheck/timeDiag.png)  
*Fig2 - timing diagram for horizontal and verticle counts*
<br>

The screen fills in pixel by pixel along each row and then down until the screen is completely drawn, then the whole process repeats and the screen is drawn
over again. This constant update happens at a rate that keeps the grid accurate to the current button states. If a button is pressed a process in the lab1
component takes this change into account and increments/decrements the trigger_volt or trigger_time count. This trigger signal is sent into the VGA component
and ultimately the scopeFace component to set the position of the trigger arrows on the grid screen. This process only takes place upon a change in the button
state, meaning that when a button is held longer than a single clock cycle the trigger arrow does not continue to move.
<br>
![Memory Dump](gateCheck/initialBlockDiag.png)
*Fig3 - initial block diagram*
<br>

#### Code:
No code shown for this lab.
	
### Software flow chart or algorithms
No software flow chart or algorithm shown for this lab.

#### Pseudocode:
No pseudocode shown for this lab.

### Hardware schematic
Updated block diagram according to component variable/wire names:
![Memory Dump](gateCheck/finalBlockDiag.png)
*Fig4 - updated block diagram*
<br>

### Debugging
When I initially implemented button functionality to the system I thought I got the logic flow 
correct, but when I loaded the program to the board and pressed a button the trigger triangle
kept incrementing at every clock cycle. This resulted in a scatter of single white pixels along the 
x and y-axis because the trigger was moving so quickly the system didn't have enough time to draw the
full triangle before drawing it in the next position. I fixed this by going into the if-else logic
of the process in the lab1 component responsible for tracking if a button press is
unique and only being processed once per button state change. I realized that I 
needed to set the button_activity variable to zero if there was no button state 
change. Originally, I didn't ever clear the button_activity and was constantly telling
the process responsible for incrementing/decrementing the trigger volt/time that there
was a new button state to implement.

### Testing methodology or results
Gatecheck 1:
Simulation waveform testing the column and row counters rollover: 
<br>
![Memory Dump](gateCheck/gateCheck1.png)
*Fig5 - gatecheck 1*
<br>

Gatecheck 2:
Test drawing a line:
<br>
![Memory Dump](gateCheck/gateCheck2DrawLine.jpg)
<br>
*Fig6 - test line drawn on screen*
<br>

h_sync -> 0:
<br>
![Memory Dump](gateCheck/h_syncDown.png)
<br>
*Fig7 - h_sync signal going low*
<br>

h_sync -> 1:
<br>
![Memory Dump](gateCheck/h_syncUp.png)
<br>
*Fig8 - h_sync signal going high*
<br>

v_sync 1 -> 0 -> 1:
<br>
![Memory Dump](gateCheck/v_syncAll.png)
<br>
*Fig9 - v_sync signal compared to row count*
<br>

h_blank -> 0:
<br>
![Memory Dump](gateCheck/h_blankDown.png)
<br>
*Fig10 - h_blank signal going low*
<br>

h_blank -> 1:
<br>
![Memory Dump](gateCheck/h_blankUp.png)
<br>
*Fig11 - h_blank signal going high*
<br>

v_blank -> 0:
<br>
![Memory Dump](gateCheck/v_blankDown.png)
<br>
*Fig12 - v_blank signal going low*
<br>

v_blank -> 1:
<br>
![Memory Dump](gateCheck/v_blankUp.png)
<br>
*Fig13 - v_blank signal going high*
<br>
<br>
Proof of A-Level Functionality:  <br>
[functionality-video](https://photos.app.goo.gl/HEevXQ8XANHcgMLT8)


### Answers to Lab Questions
1. The O-scope screen takes 16.8ms to render.
<br>
2. The O-scope is refreshed at a frequency of 60Hz.
<br>
3. There are 307,200 visible pixels on the O-scope screen (480 * 640).

### Observations and Conclusions
The project was a good overall review of the principles of the previous embedded systems 
class. I haven't taken that class in a while and I appreciated the refresher on how 
VHDL components are built and connected to eachother in heirarchial components. I also 
was refreshed on how to build a testbench and utilize the simulator to check my board logic efficiently.
The goal of the project was to build a system that would draw a grid with dummy channel 1 and channel 2
input signals and movable trigger arrows for the volt and time axises. Eventually, this project will 
evolve to intake actual signals and draw them on the grid created in this first lab.

### Documentation
I utilized all the given code and edited it as needed. I didn't use much else to guide me besides rewatching
lecture videos and the associated powerpoints.
