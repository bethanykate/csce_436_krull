--------------------------------------------------------------------
-- Name:	Bethany Krull
-- Date:	01/25/22
-- File:	counterSim_tb.vhd
-- HW:		homework 4
-- Crs:		CSCE 436
--
-- Purp:	provide testing for counter components
--
-- Documentation:	I worked on my own.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library unisim;
use unisim.vcomponents.all;
use ieee.numeric_std.all;

entity colCounter_tb is
end colCounter_tb;

architecture Behavioral of colCounter_tb is
component colCounter
    port(   clk, reset  : in std_logic;
            ctrl        : in std_logic;
            count       : out unsigned(9 downto 0);
            rollOUT     : out std_logic);
end component;
signal clkSIM, resetSIM, ctrlSIM : std_logic;
signal rollOUTSIM : std_logic;
signal countSIM : unsigned(9 downto 0);
constant clk_period : time := 500 ns;

begin
    uut: colCounter port map(
        clk => clkSIM,
        reset => resetSIM,
        ctrl => ctrlSIM,
        count => countSIM,
        rollOUT => rollOUTSIM);
        
    clk_process : process
    begin
        clkSIM <= '0';
        wait for clk_period/2;
        clkSIM <= '1';
        wait for clk_period/2;
    end process;

resetSIM <= '0', '1' after 10us;
ctrlSIM <= '1', '0' after 24.5us, '1' after 16us;

end Behavioral;
